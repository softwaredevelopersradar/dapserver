﻿using DspDataModel;
using DspDataModel.DataProcessor;
using SettingsUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace externalDll
{
    public class exDll
    {
        private Yaml yaml = new Yaml();

        public static Config config = new Config();

        DataProcessor.DataProcessor dataProcessor;

        public exDll()
        {
            InitConst initConst = new InitConst();
            //yaml.YamlSave<InitConst>(initConst, "InitConst.yaml");
            initConst = yaml.YamlLoad<InitConst>("InitConst.yaml");

            Constants.ReInitConst(
                initConst.BandwidthKhz,
                initConst.FirstBandMinKhz,
                initConst.ReceiverMinWorkFrequencyKhz,
                initConst.ReceiverMaxWorkFrequencyKhz,
                initConst.ReceiverBandwidthKhz,
                initConst.ReceiverSampleCount
                                     );

            config = yaml.YamlLoad<Config>(Config.ConfigPath);
            dataProcessor = new DataProcessor.DataProcessor(config);
        }

        public DspDataModel.DataProcessor.ProcessResult GetSignals(List<float> Amplitudes, int bandNumber, float Threshold, int scanIndex = 0)
        {
            DspDataModel.Data.IAmplitudeScan amplitudeScan = new DspDataModel.Data.AmplitudeScan(Amplitudes.ToArray(), bandNumber, DateTime.Now, scanIndex);
            ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(Threshold);
            return dataProcessor.GetSignals(amplitudeScan, scanProcessConfig);
        }


        /// <summary>
        /// CalcTauFromCorrFunc
        /// </summary>
        /// <param name="CorrDoubleList"> Where 0<=[i]<=1 </param>
        /// <param name="BandWidthMHz"></param>
        /// <param name="CorrThreshold"></param>
        /// <param name="CorrDivide"></param>
        /// <returns>time 'tau' in nano-seconds</returns>
        public double CalcTauFromCorrFunc(List<double> CorrDoubleList, double BandWidthMHz = 62.5d, float CorrThreshold = 0.2f, float CorrDivide = 0.5f)
        {
            double tau = -1;

            if (CorrDoubleList.Count() > 0)
            {
                double Max = CorrDoubleList.Max();

                if (Max >= CorrThreshold)
                {
                    double localMax = (Max * CorrDivide);

                    int maxindex = CorrDoubleList.IndexOf(Max);
                    int lbindex = -1;
                    int hbindex = -1;
                    for (int i = 0; i < CorrDoubleList.Count(); i++)
                    {
                        if (i < maxindex && CorrDoubleList[i] >= localMax && lbindex == -1)
                        {
                            lbindex = i;
                        }
                        if (i > maxindex && CorrDoubleList[i] <= localMax && lbindex != -1 && hbindex == -1)
                        {
                            hbindex = i;
                            break;
                        }
                    }

                    List<double> subRangeX = new List<double>();
                    for (int j = lbindex; j < hbindex; j++)
                        subRangeX.Add(j);

                    List<double> subRangeY = CorrDoubleList.GetRange(lbindex, hbindex - lbindex);

                    var ABC = approximationParabellum_returnValue(subRangeX, subRangeY);

                    tau = ((-1d) * (ABC[1] / (2d * ABC[0]))) * (1d / (BandWidthMHz * 10e6)) * 10e9;
                }
            }
            return tau;
        }

        private List<double> approximationParabellum_returnValue(List<double> x, List<double> y)
        {
            List<double> x2 = new List<double>();
            List<double> x3 = new List<double>();
            List<double> x4 = new List<double>();
            List<double> xy = new List<double>();
            List<double> x2y = new List<double>();

            int n = x.Count();

            for (int i = 0; i < n; i++)
            {
                x2.Add(Math.Pow(x[i], 2));
                x3.Add(Math.Pow(x[i], 3));
                x4.Add(Math.Pow(x[i], 4));
                xy.Add(x[i] * y[i]);
                x2y.Add(Math.Pow(x[i], 2) * y[i]);
            }

            double sumX = x.Sum();
            double sumY = y.Sum();
            double sumX2 = x2.Sum();
            double sumX3 = x3.Sum();
            double sumX4 = x4.Sum();
            double sumXY = xy.Sum();
            double sumX2Y = x2y.Sum();

            double delta = determinant(sumX2, sumX, n, sumX3, sumX2, sumX, sumX4, sumX3, sumX2);

            double delta_a = determinant(sumY, sumX, n, sumXY, sumX2, sumX, sumX2Y, sumX3, sumX2);
            double delta_b = determinant(sumX2, sumY, n, sumX3, sumXY, sumX, sumX4, sumX2Y, sumX2);
            double delta_c = determinant(sumX2, sumX, sumY, sumX3, sumX2, sumXY, sumX4, sumX3, sumX2Y);

            double COEF_A = delta_a / delta;
            double COEF_B = delta_b / delta;
            double COEF_C = delta_c / delta;

            return new List<double>() { COEF_A, COEF_B, COEF_C };
        }

        private double determinant(double a1, double a2, double a3,
                                         double a4, double a5, double a6,
                                         double a7, double a8, double a9)
        {
            return a1 * (a5 * a9 - a8 * a6) - a2 * (a4 * a9 - a7 * a6) + a3 * (a4 * a8 - a7 * a5);
        }
    }
}
