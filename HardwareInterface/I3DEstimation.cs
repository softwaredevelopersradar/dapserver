﻿namespace HardwareInterface
{
    using System;
    using System.Collections.Generic;
    using CommonClassesSOE;
    using KirasaModelsDBLib;

    public interface I3DEstimation
    {
        /// <summary>
        /// Gets of sets class that contains all hardware settings.
        /// </summary>
        GlobalProperties Properties { get; set; }

        /// <summary>
        /// Request to get filtered coordinates.
        /// </summary>
        /// <param name="id">Frequency-bound identifier.</param>
        /// <param name="inputCorrelationSamples">Correlation samples.</param>
        /// <param name="inputTime">Time of arrival of correlation samples.</param>
        /// <returns>Filtered coordinates.</returns>
        IDTargetDesignation Get3DEstimation(int id,
            List<List<double>> inputCorrelationSamples, 
            double inputTime);

        /// <summary>
        /// Method to invoke when sending a log message.
        /// </summary>
        Action<string> Logger { get; set; }

        /// <summary>
        /// Request to get list of tracked UAV trajectories.
        /// </summary>
        /// <param name="id">Frequency-bound identifier on which the work is in progress.</param>
        /// <returns>List of tracked UAV trajectories.</returns>
        List<TrackUAV> GetTrackUAV(int id);

        /// <summary>
        /// Request to get list of tying UAV trajectories.
        /// </summary>
        /// <param name="id">Frequency-bound identifier on which the work is in progress.</param>
        /// <returns>List of tying UAV trajectories.</returns>
        List<TrackUAV> GetTracksSOE(int id);

        /// <summary>
        /// Removes id specified correlator from dictionary.
        /// </summary>
        /// <param name="id">Frequency-bound identifier on which the work is in progress.</param>
        void RemoveFromDictionaryById(int id);

        /// <summary>
        /// Removes all dictionary entries.
        /// </summary>
        void ClearDictionary();

    }
}