﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YamlTestCorrFunc
{
    public class TestCorrFunc
    {
        public double Tau1 { get; set; }
        public double Tau2 { get; set; }
        public double Tau3 { get; set; }

        public double A1 { get; set; }
        public double A2 { get; set; }
        public double A3 { get; set; }
        public double A4 { get; set; }

        public double[] Corr1 { get; set; }
        public double[] Corr2 { get; set; }
        public double[] Corr3 { get; set; }

        public TestCorrFunc()
        {
            Tau1 = 0;
            Tau2 = 0;
            Tau3 = 0;

            A1 = 0;
            A2 = 0;
            A3 = 0;
            A4 = 0;

            Corr1 = new double[121];
            Corr2 = new double[121];
            Corr3 = new double[121];
        }

        public TestCorrFunc(
            double tau1, double tau2, double tau3,
            double a1, double a2, double a3, double a4,
            double[] corr1, double[] corr2, double[] corr3)
        {
            Tau1 = tau1;
            Tau2 = tau2;
            Tau3 = tau3;

            A1 = a1;
            A2 = a2;
            A3 = a3;
            A4 = a4;

            Corr1 = corr1;
            Corr2 = corr2;
            Corr3 = corr3;
        }
    }
}
