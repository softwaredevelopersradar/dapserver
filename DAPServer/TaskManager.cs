﻿using DspDataModel.DataProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;

using UDP_Cuirasse;

using DspDataModel;
using WpfMapRastr;
using WorkPortNew;
using YamlTestCorrFunc;
using YamlReverseExpertise;
using Nito.AsyncEx;

namespace DAPServer
{
    public partial class MainWindow
    {
        private async Task<SetFreqEventArgs> WaitAny2Tasks(int FreqkHz, byte Gain)
        {
            int timeout = 3000;

            var task = UDPSetFreq(FreqkHz, Gain);
            var task2 = W3s(timeout);

            if (await Task.WhenAny(task, task2) == task)
            {
                //Console.WriteLine("task completed within timeout");
                return task.Result;
            }
            else
            {
                //Console.WriteLine("timeout logic");
                return null;
            }

            async Task W3s(int t)
            {
                //Console.WriteLine("W3s start");
                await Task.Delay(t);
            }

            async Task<SetFreqEventArgs> UDPSetFreq(int innerFreqkHz, byte innerGain)
            {
                //Console.WriteLine("UDPSetFreq start");
                SetFreqEventArgs answerSetFreq = await uDP.SetFreq(innerFreqkHz, innerGain);
                return answerSetFreq;
            }
        }

        private async Task<T> WaitTask3000<T>(Task<T> taskToCheck) where T : EventArgs
        {
            int timeout = 3000;

            var task = taskToCheck;
            var task2 = W3s(timeout);

            if (await Task.WhenAny(task, task2) == task)
            {
                //Console.WriteLine("task completed within timeout");
                return task.Result;
            }
            else
            {
                //Console.WriteLine("timeout logic");
                return null;
            }

            async Task W3s(int t)
            {
                //Console.WriteLine("W3s start");
                await Task.Delay(t);
            }
        }

        private async Task<T> WaitTask<T>(Task<T> taskToCheck, int timeout = 3000) where T : EventArgs
        {
            var task = taskToCheck;
            var task2 = W3s(timeout);

            if (await Task.WhenAny(task, task2) == task)
            {
                //Console.WriteLine("task completed within timeout");
                return task.Result;
            }
            else
            {
                //Console.WriteLine("timeout logic");
                return null;
            }

            async Task W3s(int t)
            {
                //Console.WriteLine("W3s start");
                await Task.Delay(t);
            }
        }

        private async Task<bool> TaskTimeChecker(Task taskToCheck)
        {
            int timeout = 3000;

            var task = taskToCheck;
            var task2 = W3s(timeout);

            if (await Task.WhenAny(task, task2) == task)
            {
                //Console.WriteLine("task completed within timeout");
                return true;
            }
            else
            {
                //Console.WriteLine("timeout logic");
                return false;
            }

            async Task W3s(int t)
            {
                //Console.WriteLine("W3s start");
                await Task.Delay(t);
            }
        }

        private async Task<bool> TaskTimeChecker(Task taskToCheck, int timeout = 3000)
        {
            var task = taskToCheck;
            var task2 = W3s(timeout);

            if (await Task.WhenAny(task, task2) == task)
            {
                //Console.WriteLine("task completed within timeout");
                return true;
            }
            else
            {
                //Console.WriteLine("timeout logic");
                return false;
            }

            async Task W3s(int t)
            {
                //Console.WriteLine("W3s start");
                await Task.Delay(t);
            }
        }

        private async Task<T> AttemptCountCmdUdp<T>(int cmdNumber, int value1, int value2, int count = 3) where T : EventArgs
        {
            async Task<GetStateEventArgs> UDPGetState()
            {
                //Console.WriteLine("UDPGetState start");
                GetStateEventArgs answerGetState = await uDP.GetState();
                return answerGetState;
            }

            async Task<SetFreqEventArgs> UDPSetFreq(int innerFreqkHz, byte innerGain)
            {
                //Console.WriteLine("UDPSetFreq start");
                SetFreqEventArgs answerSetFreq = await uDP.SetFreq(innerFreqkHz, innerGain);
                return answerSetFreq;
            }

            async Task<GetSpectrumEventArgs> UDPGetSpectrum(byte innerNumber)
            {
                //Console.WriteLine("UDPGetSpectrum start");
                GetSpectrumEventArgs answerGetSpectrum = await uDP.GetSpectrum(innerNumber);
                return answerGetSpectrum;
            }

            async Task<GetCorrelationFuncEventArgs> UDPGetCorrelationFunc()
            {
                //Console.WriteLine("UDPGetCorrelationFunc start");
                GetCorrelationFuncEventArgs answerGetCorrelationFunc = await uDP.GetCorrelationFunc();
                return answerGetCorrelationFunc;
            }

            async Task<SetParamEventArgs> UDPSetParam(int innerOffset, byte innerFilterValue)
            {
                //Console.WriteLine("UDPSetParam start");
                SetParamEventArgs answerSetParam = await uDP.SetParam(innerOffset, innerFilterValue, Alpha1, Alpha2);
                return answerSetParam;
            }

            async Task<StartStopEventArgs> UDPSetStartStop(byte innerStartStop)
            {
                StartStopEventArgs answerSetStartStop = await uDP.Start_Stop((START_STOP)innerStartStop);
                return answerSetStartStop;
            }

            EventArgs t = null;
            int counter = 0;
            do
            {
                switch (cmdNumber)
                {
                    case 1:
                        t = await WaitTask<GetStateEventArgs>(UDPGetState(), 200);
                        break;
                    case 2:
                        t = await WaitTask<SetFreqEventArgs>(UDPSetFreq(value1, (byte)value2), 200);
                        break;
                    case 3:
                        t = await WaitTask<GetSpectrumEventArgs>(UDPGetSpectrum((byte)value1), 200);
                        break;
                    case 4:
                        t = await WaitTask<GetCorrelationFuncEventArgs>(UDPGetCorrelationFunc(), 200);
                        break;
                    case 5:
                        t = await WaitTask<SetParamEventArgs>(UDPSetParam(value1, (byte)value2), 200);
                        break;
                    case 6:
                        t = await WaitTask<StartStopEventArgs>(UDPSetStartStop((byte)value1), 200);
                        break;
                    default:
                        return null;
                }
                if (t == null) counter++;
                if (counter == count) return null;
            }
            while (t == null);
            return (T)t;
        }

    }
}
