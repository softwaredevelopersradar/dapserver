﻿using DAPprotocols;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using YamlDotNet.Serialization;

namespace DAPServer
{
    public class GainSettings : INotifyPropertyChanged
    {
        private byte[] _PreselGains;
        public byte[] PreselGains
        {
            get => _PreselGains;
            set
            {
                if (_PreselGains == value)
                    return;
                _PreselGains = value;
                OnPropertyChanged();
            }
        }

        private byte[] _ReceiverGains;
        public byte[] ReceiverGains
        {
            get => _ReceiverGains;
            set
            {
                if (_ReceiverGains == value)
                    return;
                _ReceiverGains = value;
                OnPropertyChanged();
            }
        }

        public GainSettings()
        {

        }

        public GainSettings(int NumberOfBands)
        {
            _PreselGains = new byte[NumberOfBands];
            _PreselGains = _PreselGains.Select(x => x = 30).ToArray();

            _ReceiverGains = new byte[NumberOfBands];
            _ReceiverGains = _ReceiverGains.Select(x => x = 30).ToArray();
        }

        public static GainSettings Load(Yaml innerYaml, int NumberOfBands, string NameDotYaml)
        {
            GainSettings innerGainSettings = innerYaml.YamlLoad<GainSettings>(NameDotYaml);
            if (innerGainSettings == null || innerGainSettings.PreselGains == null || innerGainSettings.ReceiverGains == null)
            {
                innerGainSettings = new GainSettings(NumberOfBands);
                innerYaml.YamlSave<GainSettings>(innerGainSettings, NameDotYaml);
            }
            return innerGainSettings;
        }

        public void SetDeviceGain(DapDevice Device, byte EPO, byte Gain)
        {
            switch (Device)
            {
                case DapDevice.Preselector:
                    _PreselGains[EPO] = Gain;
                    break;
                case DapDevice.Receiver:
                    _ReceiverGains[EPO] = Gain;
                    break;
            }
        }

        public byte GetDeviceGain(DapDevice Device, byte EPO)
        {
            switch (Device)
            {
                case DapDevice.Preselector:
                    return _PreselGains[EPO];
                case DapDevice.Receiver:
                    return _ReceiverGains[EPO];
                default: return 30;
            }
        }

        #region INotify 
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}
