﻿using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

using UDP_Cuirasse;

using DAPServerClient2;
using DspDataModel;


using YamlDotNet.Serialization;
using YamlDotNet.Core;
using WpfMapRastr;
using WorkPort;
using WorkPortNew;
using Nito.AsyncEx;

namespace DAPServer
{
    public partial class MainWindow
    {
        CancellationTokenSource c1;
        CancellationTokenSource c2;
        CancellationTokenSource c3;
        CancellationTokenSource c4;

        private async Task TokenAwater(CancellationToken ctoken)
        {
            while (true)
            {
                if (ctoken.IsCancellationRequested)
                {
                    return;
                }
                await Task.Delay(1);
            }
        }

        Port port1 = new Port();
        Port port2 = new Port();
        Port port3 = new Port();
        Port port4 = new Port();

        private void InitPreselEvents()
        {
            //Для инициализации
            port1.OnGain_02 += Port1_OnGain_02;
            port2.OnGain_02 += Port2_OnGain_02;
            port3.OnGain_02 += Port3_OnGain_02;
            port4.OnGain_02 += Port4_OnGain_02;

            port1.OnOnOffPreamp_04 += Port1_OnOnOffPreamp_04;
            port2.OnOnOffPreamp_04 += Port2_OnOnOffPreamp_04;
            port3.OnOnOffPreamp_04 += Port3_OnOnOffPreamp_04;
            port4.OnOnOffPreamp_04 += Port4_OnOnOffPreamp_04;

            port1.OnOnOffOpticalTransmitter_10 += Port1_OnOnOffOpticalTransmitter_10;
            port2.OnOnOffOpticalTransmitter_10 += Port2_OnOnOffOpticalTransmitter_10;
            port3.OnOnOffOpticalTransmitter_10 += Port3_OnOnOffOpticalTransmitter_10;
            port4.OnOnOffOpticalTransmitter_10 += Port4_OnOnOffOpticalTransmitter_10;

            //Для запросов
            port1.OnSetFreqGain_06 += Port1_OnSetFreqGain_06;
            port2.OnSetFreqGain_06 += Port2_OnSetFreqGain_06;
            port3.OnSetFreqGain_06 += Port3_OnSetFreqGain_06;
            port4.OnSetFreqGain_06 += Port4_OnSetFreqGain_06;

            //Для запросов
            port1.OnFreq_01 += Port1_OnFreq_01;
            port2.OnFreq_01 += Port2_OnFreq_01;
            port3.OnFreq_01 += Port3_OnFreq_01;
            port4.OnFreq_01 += Port4_OnFreq_01;
        }

        private void Port1_OnFreq_01(object sender, short e)
        {
            c1.Cancel();
        }
        private void Port2_OnFreq_01(object sender, short e)
        {
            c2.Cancel();
        }
        private void Port3_OnFreq_01(object sender, short e)
        {
            c3.Cancel();
        }
        private void Port4_OnFreq_01(object sender, short e)
        {
            c4.Cancel();
        }

        private void Port1_OnSetFreqGain_06(object sender, WorkPort.FreqGainEventArgs e)
        {
            c1.Cancel();
        }
        private void Port2_OnSetFreqGain_06(object sender, WorkPort.FreqGainEventArgs e)
        {
            c2.Cancel();
        }
        private void Port3_OnSetFreqGain_06(object sender, WorkPort.FreqGainEventArgs e)
        {
            c3.Cancel();
        }
        private void Port4_OnSetFreqGain_06(object sender, WorkPort.FreqGainEventArgs e)
        {
            c4.Cancel();
        }

        private void Port1_OnGain_02(object sender, byte e)
        {
            c1.Cancel();
        }
        private void Port2_OnGain_02(object sender, byte e)
        {
            c2.Cancel();
        }
        private void Port3_OnGain_02(object sender, byte e)
        {
            c3.Cancel();
        }
        private void Port4_OnGain_02(object sender, byte e)
        {
            c4.Cancel();
        }

        private void Port1_OnOnOffPreamp_04(object sender, Port.OnOffPreamp e)
        {
            c1.Cancel();
        }
        private void Port2_OnOnOffPreamp_04(object sender, Port.OnOffPreamp e)
        {
            c2.Cancel();
        }
        private void Port3_OnOnOffPreamp_04(object sender, Port.OnOffPreamp e)
        {
            c3.Cancel();
        }
        private void Port4_OnOnOffPreamp_04(object sender, Port.OnOffPreamp e)
        {
            c4.Cancel();
        }

        private void Port1_OnOnOffOpticalTransmitter_10(object sender, Port.OnOffSwitch e)
        {
            c1.Cancel();
        }
        private void Port2_OnOnOffOpticalTransmitter_10(object sender, Port.OnOffSwitch e)
        {
            c2.Cancel();
        }
        private void Port3_OnOnOffOpticalTransmitter_10(object sender, Port.OnOffSwitch e)
        {
            c3.Cancel();
        }
        private void Port4_OnOnOffOpticalTransmitter_10(object sender, Port.OnOffSwitch e)
        {
            c4.Cancel();
        }

        private void InitPresel(Settings innerSettings)
        {
            switch (innerSettings.PreselectorVersion)
            {
                case 0:
                    //Task.Run(() => InitPreselWorkСonsequentially(innerSettings));
                    Task.Run(() => InitPreselWorkСonsequentially2(innerSettings));
                    break;
                case 1:
                    //Task.Run(() => InitPreselWorkNewСonsequentially(innerSettings));
                    Task.Run(() => InitPreselWorkNewСonsequentially2(innerSettings));
                    break;
            }
        }

        private async Task InitPreselWorkСonsequentially(Settings innerSettings)
        {
            InitPreselEvents();

            var b1 = port1.OpenPort(innerSettings.OpticRS232Settings1.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings1.PortSpeed));
            var b2 = port2.OpenPort(innerSettings.OpticRS232Settings2.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings2.PortSpeed));
            var b3 = port3.OpenPort(innerSettings.OpticRS232Settings3.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings3.PortSpeed));
            var b4 = port4.OpenPort(innerSettings.OpticRS232Settings4.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings4.PortSpeed));

            bool[] b = new bool[] { b1, b2, b3, b4 };

            if (b.All(x => x == true))
            {
                //Установка усиления, команда 02
                {
                    port1.SetGainCode2(31, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);

                    SW sw = new SW();

                    var task = InitPreselAwait(2);
                    port2.SetGainCode2(31, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();

                    sw = new SW();

                    task = InitPreselAwait(3);
                    port3.SetGainCode2(31, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();

                    sw = new SW();

                    task = InitPreselAwait(4);
                    port4.SetGainCode2(31, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();
                }

                //Включение предусилителя, команда 04
                {
                    port1.SetOnOffCode4(Port.FlagOnOff.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);

                    SW sw = new SW();

                    var task = InitPreselAwait(2);
                    port2.SetOnOffCode4(Port.FlagOnOff.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();

                    sw = new SW();

                    task = InitPreselAwait(3);
                    port3.SetOnOffCode4(Port.FlagOnOff.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();

                    sw = new SW();

                    task = InitPreselAwait(4);
                    port4.SetOnOffCode4(Port.FlagOnOff.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();
                }

                //Включение оптики
                //Питание 12В для оптического передатчика или питание 5В для дополнительного РЧ-усилителя
                //Команда 10
                {
                    port1.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);

                    SW sw = new SW();

                    var task = InitPreselAwait(2);
                    port2.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();

                    sw = new SW();

                    task = InitPreselAwait(3);
                    port3.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();

                    sw = new SW();

                    task = InitPreselAwait(4);
                    port4.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    sw.Stop();
                }
                Console.WriteLine("Presel INI OK");
            }
            else
            {
                for (int i = 0; i < b.Count(); i++)
                {
                    if (b[i])
                        Console.WriteLine($"Presel COM №{i + 1} is Open");
                    else
                        Console.WriteLine($"Presel COM №{i + 1} is not Open");
                }
            }
        }

        private void ConsoleLog(bool FlagCheck, string Str)
        {
            if (FlagCheck)
            {
                Console.ForegroundColor = ConsoleColor.Gray;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
            Console.WriteLine(Str);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private void ConsoleLog(byte byteCheck, string Str)
        {
            switch (byteCheck)
            {
                case 0:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case 1:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }
            Console.WriteLine(Str);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private byte _PreamplifierState = 1;
        public byte PreamplifierState
        {
            get => _PreamplifierState;
            set
            {
                if (_PreamplifierState == value)
                    return;
                _PreamplifierState = value;
            }
        }

        private async Task InitPreselWorkСonsequentially2(Settings innerSettings)
        {
            InitPreselEvents();

            var b1 = port1.OpenPort(innerSettings.OpticRS232Settings1.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings1.PortSpeed));
            var b2 = port2.OpenPort(innerSettings.OpticRS232Settings2.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings2.PortSpeed));
            var b3 = port3.OpenPort(innerSettings.OpticRS232Settings3.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings3.PortSpeed));
            var b4 = port4.OpenPort(innerSettings.OpticRS232Settings4.ComPort, (Port.BaudRate)((int)innerSettings.OpticRS232Settings4.PortSpeed));

            bool[] b = new bool[] { b1, b2, b3, b4 };

            if (b.All(x => x == true))
            {
                //Установка усиления, команда 02
                {
                    short Freq = 0;
                    byte Gain = 30;

                    int Cmd = 2;

                    b1 = await AttemptCountCmd(Cmd, 1, (short)Freq, Gain);
                    if (b[0] == true && b1 == false) b[0] = b1;
                    ConsoleLog(b1, $"Result: PortNumber: {1} - Cmd{Cmd} - {b1}");

                    b2 = await AttemptCountCmd(Cmd, 2, (short)Freq, Gain);
                    if (b[1] == true && b2 == false) b[1] = b2;
                    ConsoleLog(b2, $"Result: PortNumber: {2} - Cmd{Cmd} - {b2}");

                    b3 = await AttemptCountCmd(Cmd, 3, (short)Freq, Gain);
                    if (b[2] == true && b3 == false) b[2] = b3;
                    ConsoleLog(b3, $"Result: PortNumber: {3} - Cmd{Cmd} - {b3}");

                    b4 = await AttemptCountCmd(Cmd, 4, (short)Freq, Gain);
                    if (b[3] == true && b4 == false) b[3] = b4;
                    ConsoleLog(b4, $"Result: PortNumber: {4} - Cmd{Cmd} - {b4}");
                }

                //Включение предусилителя, команда 04
                {
                    short Freq = 0;
                    byte Gain = PreamplifierState;

                    int Cmd = 4;

                    b1 = await AttemptCountCmd(Cmd, 1, (short)Freq, Gain);
                    if (b[0] == true && b1 == false) b[0] = b1;
                    ConsoleLog(b1, $"Result: PortNumber: {1} - Cmd{Cmd} - {b1}");

                    b2 = await AttemptCountCmd(Cmd, 2, (short)Freq, Gain);
                    if (b[1] == true && b2 == false) b[1] = b2;
                    ConsoleLog(b2, $"Result: PortNumber: {2} - Cmd{Cmd} - {b2}");

                    b3 = await AttemptCountCmd(Cmd, 3, (short)Freq, Gain);
                    if (b[2] == true && b3 == false) b[2] = b3;
                    ConsoleLog(b3, $"Result: PortNumber: {3} - Cmd{Cmd} - {b3}");

                    b4 = await AttemptCountCmd(Cmd, 4, (short)Freq, Gain);
                    if (b[3] == true && b4 == false) b[3] = b4;
                    ConsoleLog(b4, $"Result: PortNumber: {4} - Cmd{Cmd} - {b4}");
                }

                //Включение оптики
                //Питание 12В для оптического передатчика или питание 5В для дополнительного РЧ-усилителя
                //Команда 10
                {
                    short Freq = 0;
                    byte Gain = 30;

                    int Cmd = 10;

                    b1 = await AttemptCountCmd(Cmd, 1, (short)Freq, Gain);
                    if (b[0] == true && b1 == false) b[0] = b1;
                    ConsoleLog(b1, $"Result: PortNumber: {1} - Cmd{Cmd} - {b1}");

                    b2 = await AttemptCountCmd(Cmd, 2, (short)Freq, Gain);
                    if (b[1] == true && b2 == false) b[1] = b2;
                    ConsoleLog(b2, $"Result: PortNumber: {2} - Cmd{Cmd} - {b2}");

                    b3 = await AttemptCountCmd(Cmd, 3, (short)Freq, Gain);
                    if (b[2] == true && b3 == false) b[2] = b3;
                    ConsoleLog(b3, $"Result: PortNumber: {3} - Cmd{Cmd} - {b3}");

                    b4 = await AttemptCountCmd(Cmd, 4, (short)Freq, Gain);
                    if (b[3] == true && b4 == false) b[3] = b4;
                    ConsoleLog(b4, $"Result: PortNumber: {4} - Cmd{Cmd} - {b4}");
                }
                ConsoleLog(2, "All Presel INI Commands passed");
            }
            else
            {
                for (int i = 0; i < b.Count(); i++)
                {
                    if (b[i])
                    {
                        ConsoleLog(true, $"Presel COM №{i + 1} is Open");
                    }
                    else
                    {
                        ConsoleLog(false, $"Presel COM №{i + 1} is not Open");
                    }
                }
            }
        }

        private async Task PreamplifierChangeCmd4()
        {
            bool[] b = new bool[] { true, true, true, true, };

            //Включение предусилителя, команда 04
            {
                short Freq = 0;
                byte Gain = PreamplifierState;

                int Cmd = 4;

                var b1 = await AttemptCountCmd(Cmd, 1, (short)Freq, Gain);
                if (b[0] == true && b1 == false) b[0] = b1;
                ConsoleLog(b1, $"Result: PortNumber: {1} - Cmd{Cmd} - {b1}");

                var b2 = await AttemptCountCmd(Cmd, 2, (short)Freq, Gain);
                if (b[1] == true && b2 == false) b[1] = b2;
                ConsoleLog(b2, $"Result: PortNumber: {2} - Cmd{Cmd} - {b2}");

                var b3 = await AttemptCountCmd(Cmd, 3, (short)Freq, Gain);
                if (b[2] == true && b3 == false) b[2] = b3;
                ConsoleLog(b3, $"Result: PortNumber: {3} - Cmd{Cmd} - {b3}");

                var b4 = await AttemptCountCmd(Cmd, 4, (short)Freq, Gain);
                if (b[3] == true && b4 == false) b[3] = b4;
                ConsoleLog(b4, $"Result: PortNumber: {4} - Cmd{Cmd} - {b4}");
            }
        }

        PortNew portNew1 = new PortNew();
        PortNew portNew2 = new PortNew();
        PortNew portNew3 = new PortNew();
        PortNew portNew4 = new PortNew();

        private void InitPreselNewEvents()
        {
            //Для инициализации
            portNew1.OnGain_02_3b += PortNew1_OnGain_02_3b;
            portNew2.OnGain_02_3b += PortNew2_OnGain_02_3b;
            portNew3.OnGain_02_3b += PortNew3_OnGain_02_3b;
            portNew4.OnGain_02_3b += PortNew4_OnGain_02_3b;

            portNew1.OnOnOffPreamp_04 += PortNew1_OnOnOffPreamp_04;
            portNew2.OnOnOffPreamp_04 += PortNew2_OnOnOffPreamp_04;
            portNew3.OnOnOffPreamp_04 += PortNew3_OnOnOffPreamp_04;
            portNew4.OnOnOffPreamp_04 += PortNew4_OnOnOffPreamp_04;

            portNew1.OnOnOffOpticalTransmitter_10 += PortNew1_OnOnOffOpticalTransmitter_10;
            portNew2.OnOnOffOpticalTransmitter_10 += PortNew2_OnOnOffOpticalTransmitter_10;
            portNew3.OnOnOffOpticalTransmitter_10 += PortNew3_OnOnOffOpticalTransmitter_10;
            portNew4.OnOnOffOpticalTransmitter_10 += PortNew4_OnOnOffOpticalTransmitter_10;

            //Для запрсов
            portNew1.OnSetFreqGain_06 += PortNew1_OnSetFreqGain_06;
            portNew2.OnSetFreqGain_06 += PortNew2_OnSetFreqGain_06;
            portNew3.OnSetFreqGain_06 += PortNew3_OnSetFreqGain_06;
            portNew4.OnSetFreqGain_06 += PortNew4_OnSetFreqGain_06;

            //Для запросов
            portNew1.OnFreq_01 += PortNew1_OnFreq_01;
            portNew2.OnFreq_01 += PortNew2_OnFreq_01;
            portNew3.OnFreq_01 += PortNew3_OnFreq_01;
            portNew4.OnFreq_01 += PortNew4_OnFreq_01;

        }

        private void PortNew1_OnFreq_01(object sender, short e)
        {
            c1.Cancel();
        }
        private void PortNew2_OnFreq_01(object sender, short e)
        {
            c2.Cancel();
        }
        private void PortNew3_OnFreq_01(object sender, short e)
        {
            c3.Cancel();
        }
        private void PortNew4_OnFreq_01(object sender, short e)
        {
            c4.Cancel();
        }

        private void PortNew1_OnSetFreqGain_06(object sender, WorkPortNew.FreqGainEventArgs e)
        {
            c1.Cancel();
        }
        private void PortNew2_OnSetFreqGain_06(object sender, WorkPortNew.FreqGainEventArgs e)
        {
            c2.Cancel();
        }
        private void PortNew3_OnSetFreqGain_06(object sender, WorkPortNew.FreqGainEventArgs e)
        {
            c3.Cancel();
        }
        private void PortNew4_OnSetFreqGain_06(object sender, WorkPortNew.FreqGainEventArgs e)
        {
            c4.Cancel();
        }

        private void PortNew1_OnGain_02_3b(object sender, byte e)
        {
            c1.Cancel();
        }
        private void PortNew2_OnGain_02_3b(object sender, byte e)
        {
            c2.Cancel();
        }
        private void PortNew3_OnGain_02_3b(object sender, byte e)
        {
            c3.Cancel();
        }
        private void PortNew4_OnGain_02_3b(object sender, byte e)
        {
            c4.Cancel();
        }

        private void PortNew1_OnOnOffPreamp_04(object sender, PortNew.PreselectorOnOff e)
        {
            c1.Cancel();
        }
        private void PortNew2_OnOnOffPreamp_04(object sender, PortNew.PreselectorOnOff e)
        {
            c2.Cancel();
        }
        private void PortNew3_OnOnOffPreamp_04(object sender, PortNew.PreselectorOnOff e)
        {
            c3.Cancel();
        }
        private void PortNew4_OnOnOffPreamp_04(object sender, PortNew.PreselectorOnOff e)
        {
            c4.Cancel();
        }

        private void PortNew1_OnOnOffOpticalTransmitter_10(object sender, PortNew.OnOffSwitch e)
        {
            c1.Cancel();
        }
        private void PortNew2_OnOnOffOpticalTransmitter_10(object sender, PortNew.OnOffSwitch e)
        {
            c2.Cancel();
        }
        private void PortNew3_OnOnOffOpticalTransmitter_10(object sender, PortNew.OnOffSwitch e)
        {
            c3.Cancel();
        }
        private void PortNew4_OnOnOffOpticalTransmitter_10(object sender, PortNew.OnOffSwitch e)
        {
            c4.Cancel();
        }


        private async Task InitPreselWorkNewСonsequentially(Settings innerSettings)
        {
            InitPreselNewEvents();

            var b1 = portNew1.OpenPort(innerSettings.OpticRS232Settings1.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings1.PortSpeed));
            var b2 = portNew2.OpenPort(innerSettings.OpticRS232Settings2.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings2.PortSpeed));
            var b3 = portNew3.OpenPort(innerSettings.OpticRS232Settings3.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings3.PortSpeed));
            var b4 = portNew4.OpenPort(innerSettings.OpticRS232Settings4.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings4.PortSpeed));

            bool[] b = new bool[] { b1, b2, b3, b4 };

            if (b.All(x => x == true))
            {
                //Установка усиления, команда 02
                {
                    var task = InitPreselAwait(1);
                    portNew1.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(2);
                    portNew2.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(3);
                    portNew3.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(4);
                    portNew4.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;
                }

                //Включение предусилителя, команда 04
                {
                    var task = InitPreselAwait(1);
                    portNew1.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(2);
                    portNew2.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(3);
                    portNew3.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(4);
                    portNew4.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;
                }

                //Включение оптики
                //Питание 12В для оптического передатчика или питание 5В для дополнительного РЧ-усилителя
                //Команда 10
                {
                    var task = InitPreselAwait(1);
                    portNew1.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(2);
                    portNew2.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(3);
                    portNew3.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;

                    task = InitPreselAwait(4);
                    portNew4.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    await Task.Delay(PreselCmdDelay);
                    await task;
                }
            }
            else
            {
                for (int i = 0; i < b.Count(); i++)
                {
                    if (b[i])
                        Console.WriteLine($"PreselNew COM №{i + 1} is Open");
                    else
                        Console.WriteLine($"PreselNew COM №{i + 1} is not Open");
                }
            }
        }

        private async Task InitPreselWorkNewСonsequentially2(Settings innerSettings)
        {
            InitPreselNewEvents();

            var b1 = portNew1.OpenPort(innerSettings.OpticRS232Settings1.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings1.PortSpeed));
            var b2 = portNew2.OpenPort(innerSettings.OpticRS232Settings2.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings2.PortSpeed));
            var b3 = portNew3.OpenPort(innerSettings.OpticRS232Settings3.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings3.PortSpeed));
            var b4 = portNew4.OpenPort(innerSettings.OpticRS232Settings4.ComPort, (PortNew.BaudRate)((int)innerSettings.OpticRS232Settings4.PortSpeed));

            bool[] b = new bool[] { b1, b2, b3, b4 };

            if (b.All(x => x == true))
            {
                //Установка усиления, команда 02
                {
                    short Freq = 0;
                    byte Gain = 30;

                    int Cmd = 2;

                    b1 = await AttemptCountCmdNew(Cmd, 1, (short)Freq, Gain);
                    if (b[0] == true && b1 == false) b[0] = b1;
                    Console.WriteLine($"Result: PortNumber: {1} - Cmd{Cmd} - {b1}");

                    b2 = await AttemptCountCmdNew(Cmd, 2, (short)Freq, Gain);
                    if (b[1] == true && b2 == false) b[1] = b2;
                    Console.WriteLine($"Result: PortNumber: {2} - Cmd{Cmd} - {b2}");

                    b3 = await AttemptCountCmdNew(Cmd, 3, (short)Freq, Gain);
                    if (b[2] == true && b3 == false) b[2] = b3;
                    Console.WriteLine($"Result: PortNumber: {3} - Cmd{Cmd} - {b3}");

                    b4 = await AttemptCountCmdNew(Cmd, 4, (short)Freq, Gain);
                    if (b[3] == true && b4 == false) b[3] = b4;
                    Console.WriteLine($"Result: PortNumber: {4} - Cmd{Cmd} - {b4}");
                }

                //Включение предусилителя, команда 04
                {
                    short Freq = 0;
                    byte Gain = 30;

                    int Cmd = 4;

                    b1 = await AttemptCountCmdNew(Cmd, 1, (short)Freq, Gain);
                    if (b[0] == true && b1 == false) b[0] = b1;
                    Console.WriteLine($"Result: PortNumber: {1} - Cmd{Cmd} - {b1}");

                    b2 = await AttemptCountCmdNew(Cmd, 2, (short)Freq, Gain);
                    if (b[1] == true && b2 == false) b[1] = b2;
                    Console.WriteLine($"Result: PortNumber: {2} - Cmd{Cmd} - {b2}");

                    b3 = await AttemptCountCmdNew(Cmd, 3, (short)Freq, Gain);
                    if (b[2] == true && b3 == false) b[2] = b3;
                    Console.WriteLine($"Result: PortNumber: {3} - Cmd{Cmd} - {b3}");

                    b4 = await AttemptCountCmdNew(Cmd, 4, (short)Freq, Gain);
                    if (b[3] == true && b4 == false) b[3] = b4;
                    Console.WriteLine($"Result: PortNumber: {4} - Cmd{Cmd} - {b4}");
                }

                //Включение оптики
                //Питание 12В для оптического передатчика или питание 5В для дополнительного РЧ-усилителя
                //Команда 10
                {
                    short Freq = 0;
                    byte Gain = 30;

                    int Cmd = 10;

                    b1 = await AttemptCountCmdNew(Cmd, 1, (short)Freq, Gain);
                    if (b[0] == true && b1 == false) b[0] = b1;
                    Console.WriteLine($"Result: PortNumber: {1} - Cmd{Cmd} - {b1}");

                    b2 = await AttemptCountCmdNew(Cmd, 2, (short)Freq, Gain);
                    if (b[1] == true && b2 == false) b[1] = b2;
                    Console.WriteLine($"Result: PortNumber: {2} - Cmd{Cmd} - {b2}");

                    b3 = await AttemptCountCmdNew(Cmd, 3, (short)Freq, Gain);
                    if (b[2] == true && b3 == false) b[2] = b3;
                    Console.WriteLine($"Result: PortNumber: {3} - Cmd{Cmd} - {b3}");

                    b4 = await AttemptCountCmdNew(Cmd, 4, (short)Freq, Gain);
                    if (b[3] == true && b4 == false) b[3] = b4;
                    Console.WriteLine($"Result: PortNumber: {4} - Cmd{Cmd} - {b4}");
                }
            }
            else
            {
                for (int i = 0; i < b.Count(); i++)
                {
                    if (b[i])
                        Console.WriteLine($"PreselNew COM №{i + 1} is Open");
                    else
                        Console.WriteLine($"PreselNew COM №{i + 1} is not Open");
                }
            }
        }



        private async Task SetPreselFreqGainAsyncСonsistently(int Version, int Freq, byte Gain)
        {
            switch (Version)
            {
                case 0:
                    //await SetPreselFreqGainOldAsyncСonsistently2(Freq, Gain);
                    await SetPreselFreqGainOldAsyncСonsistently3(Freq, Gain);
                    break;
                case 1:
                    //await SetPreselFreqGainNewAsyncСonsistently((short)Freq, Gain);
                    await SetPreselFreqGainNewAsyncСonsistently2((short)Freq, Gain);
                    break;
            }
        }

        private async Task<bool[]> SetPreselFreqGainAsyncСonsistently5(int Version, int Freq, byte Gain)
        {
            switch (Version)
            {
                case 0:
                    var b0 = await SetPreselFreqGainOldAsyncСonsistently5(Freq, Gain);
                    return b0;
                case 1:
                    var b1 = await SetPreselFreqGainNewAsyncСonsistently5((short)Freq, Gain);
                    return b1;
                default:
                    return new bool[] { false, false, false, false }; ;
            }
        }
        private async Task<bool[]> SetPreselFreqGainAsyncСonsistently6(int Version, int Freq, byte Gain)
        {
            switch (Version)
            {
                case 0:
                    var b0 = await SetPreselFreqGainOldAsyncСonsistently6(Freq, Gain);
                    return b0;
                case 1:
                    var b1 = await SetPreselFreqGainNewAsyncСonsistently6((short)Freq, Gain);
                    return b1;
                default:
                    return new bool[] { false, false, false, false }; ;
            }
        }


        private async Task SetPreselFreqGainOldAsyncСonsistently(int Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainOldAsyncСonsistently Start");

            //Инициализация ожидания преселектора 1
            //var task = InitPreselAwait(1);
            //Установка частоты-усиления, команда 06
            port1.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            //await task;

            //Инициализация ожидания преселектора 2
            var task = InitPreselAwait(2);
            //Установка частоты-усиления, команда 06
            port2.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            //Инициализация ожидания преселектора 3
            task = InitPreselAwait(3);
            //Установка частоты-усиления, команда 06
            port3.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            //Инициализация ожидания преселектора 4
            task = InitPreselAwait(4);
            //Установка частоты-усиления, команда 06
            port4.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;
        }
        private async Task SetPreselFreqGainOldAsyncСonsistently2(int Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainOldAsyncСonsistently2 Start");

            //Настройка преселектора на частоту, команда 01
            port1.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);

            //Инициализация ожидания преселектора 2
            //var task = InitPreselAwait(2);
            port2.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            //await task;

            //Инициализация ожидания преселектора 3
            //task = InitPreselAwait(3);
            port3.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            //await task;

            //Инициализация ожидания преселектора 4
            //task = InitPreselAwait(4);
            port4.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            //await task;

            //Настройка усиления преселектора
            port1.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);

            //Инициализация ожидания преселектора 2
            //task = InitPreselAwait(2);
            port2.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            //await task;

            //Инициализация ожидания преселектора 3
            //task = InitPreselAwait(3);
            port3.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            //await task;

            //Инициализация ожидания преселектора 4
            //task = InitPreselAwait(4);
            port4.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector); 
            await Task.Delay(PreselCmdDelay);
            //await task;
        }
        private async Task SetPreselFreqGainOldAsyncСonsistently3(int Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainOldAsyncСonsistently3 Start");

            //Настройка преселектора на частоту, команда 01
            port1.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);

            SW sw = new SW();

            //Инициализация ожидания преселектора 2
            var task = InitPreselAwait(2);
            port2.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            sw.Stop();

            sw = new SW();

            //Инициализация ожидания преселектора 3
            task = InitPreselAwait(3);
            port3.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            sw.Stop();

            sw = new SW();

            //Инициализация ожидания преселектора 4
            task = InitPreselAwait(4);
            port4.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            sw.Stop();

            //Настройка усиления преселектора
            port1.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);

            sw = new SW();

            //Инициализация ожидания преселектора 2
            task = InitPreselAwait(2);
            port2.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            sw.Stop();

            sw = new SW();

            //Инициализация ожидания преселектора 3
            task = InitPreselAwait(3);
            port3.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            sw.Stop();

            sw = new SW();

            //Инициализация ожидания преселектора 4
            task = InitPreselAwait(4);
            port4.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            await Task.Delay(PreselCmdDelay);
            await task;

            sw.Stop();
        }
        private async Task SetPreselFreqGainOldAsyncСonsistently4(int Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainOldAsyncСonsistently4 Start");

            //Настройка преселектора на частоту, команда 01
            {
                var b1 = await AttemptCountCmd(1, 1, (short)Freq, Gain);
                var b2 = await AttemptCountCmd(1, 2, (short)Freq, Gain);
                var b3 = await AttemptCountCmd(1, 3, (short)Freq, Gain);
                var b4 = await AttemptCountCmd(1, 4, (short)Freq, Gain);
            }

            //Настройка усиления преселектора, команда 02
            {
                var b1 = await AttemptCountCmd(2, 1, (short)Freq, Gain);
                var b2 = await AttemptCountCmd(2, 2, (short)Freq, Gain);
                var b3 = await AttemptCountCmd(2, 3, (short)Freq, Gain);
                var b4 = await AttemptCountCmd(2, 4, (short)Freq, Gain);
            }
        }
        private async Task<bool[]> SetPreselFreqGainOldAsyncСonsistently5(int Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainOldAsyncСonsistently5 Start");

            //Настройка преселектора на частоту, команда 01
            {
                var b1 = await AttemptCountCmd(1, 1, (short)Freq, Gain);
                Console.WriteLine($"Result: PortNumber: {1} - Cmd{1} - {b1}");
                var b2 = await AttemptCountCmd(1, 2, (short)Freq, Gain);
                Console.WriteLine($"Result: PortNumber: {2} - Cmd{1} - {b2}");
                var b3 = await AttemptCountCmd(1, 3, (short)Freq, Gain);
                Console.WriteLine($"Result: PortNumber: {3} - Cmd{1} - {b3}");
                var b4 = await AttemptCountCmd(1, 4, (short)Freq, Gain);
                Console.WriteLine($"Result: PortNumber: {4} - Cmd{1} - {b4}");

                bool[] b = new bool[] { b1, b2, b3, b4 };
                if (b.Any(x => x == false)) return b;
            }

            //Настройка усиления преселектора, команда 02
            {
                var b1 = await AttemptCountCmd(2, 1, (short)Freq, Gain);
                var b2 = await AttemptCountCmd(2, 2, (short)Freq, Gain);
                var b3 = await AttemptCountCmd(2, 3, (short)Freq, Gain);
                var b4 = await AttemptCountCmd(2, 4, (short)Freq, Gain);

                bool[] b = new bool[] { b1, b2, b3, b4 };
                if (b.Any(x => x == false)) return b;
            }
            return new bool[] { true, true, true, true };
        }
        private async Task<bool[]> SetPreselFreqGainOldAsyncСonsistently6(int Freq, byte Gain)
        {
            Console.WriteLine($"SetPreselFreqGainOldAsyncСonsistently6 Freq: {Freq} Gain: {Gain}");


            bool[] b = new bool[] { true, true, true, true };


            //Настройка преселектора на частоту, команда 01
            var b1 = await AttemptCountCmd(1, 1, (short)Freq, Gain);
            if (b[0] == true && b1 == false) b[0] = b1;
            ConsoleLog(b1, $"Result: PortNumber: {1} - Cmd{1} - {b1}");

            var b2 = await AttemptCountCmd(1, 2, (short)Freq, Gain);
            if (b[1] == true && b2 == false) b[1] = b2;
            ConsoleLog(b2, $"Result: PortNumber: {2} - Cmd{1} - {b2}");

            var b3 = await AttemptCountCmd(1, 3, (short)Freq, Gain);
            if (b[2] == true && b3 == false) b[2] = b3;
            ConsoleLog(b3, $"Result: PortNumber: {3} - Cmd{1} - {b3}");

            var b4 = await AttemptCountCmd(1, 4, (short)Freq, Gain);
            if (b[3] == true && b4 == false) b[3] = b4;
            ConsoleLog(b4, $"Result: PortNumber: {4} - Cmd{1} - {b4}");


            //Настройка усиления преселектора, команда 02
            b1 = await AttemptCountCmd(2, 1, (short)Freq, Gain);
            if (b[0] == true && b1 == false) b[0] = b1;
            ConsoleLog(b1, $"Result: PortNumber: {1} - Cmd{2} - {b1}");

            b2 = await AttemptCountCmd(2, 2, (short)Freq, Gain);
            if (b[1] == true && b2 == false) b[1] = b2;
            ConsoleLog(b2, $"Result: PortNumber: {2} - Cmd{2} - {b2}");

            b3 = await AttemptCountCmd(2, 3, (short)Freq, Gain);
            if (b[2] == true && b3 == false) b[2] = b3;
            ConsoleLog(b3, $"Result: PortNumber: {3} - Cmd{2} - {b3}");

            b4 = await AttemptCountCmd(2, 4, (short)Freq, Gain);
            if (b[3] == true && b4 == false) b[3] = b4;
            ConsoleLog(b4, $"Result: PortNumber: {4} - Cmd{2} - {b4}");


            return b;
        }

        private async Task Cmd1(int portNumber, short Freq)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            switch (portNumber)
            {
                case 1:
                    //Установка частоты, команда 01
                    port1.SetFreqCode1(Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Установка частоты, команда 01
                    port2.SetFreqCode1(Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Установка частоты, команда 01
                    port3.SetFreqCode1(Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Установка частоты, команда 01
                    port4.SetFreqCode1(Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdSetDelay);
            await task;
        }
        private async Task Cmd2(int portNumber, byte Gain)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            switch (portNumber)
            {
                case 1:
                    //Установка усиления, команда 02
                    port1.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Установка усиления, команда 02
                    port2.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Установка усиления, команда 02
                    port3.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Установка усиления, команда 02
                    port4.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdSetDelay);
            await task;
        }
        private async Task Cmd4(int portNumber, byte Gain)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            Port.FlagOnOff FlagOnOff = (Port.FlagOnOff)Enum.GetValues(typeof(Port.FlagOnOff)).GetValue(Gain);
            switch (portNumber)
            {
                case 1:
                    //Включение предусилителя, команда 04
                    port1.SetOnOffCode4(FlagOnOff, Port.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Включение предусилителя, команда 04
                    port2.SetOnOffCode4(FlagOnOff, Port.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Включение предусилителя, команда 04
                    port3.SetOnOffCode4(FlagOnOff, Port.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Включение предусилителя, команда 04
                    port4.SetOnOffCode4(FlagOnOff, Port.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdSetDelay);
            await task;
        }
        private async Task Cmd10(int portNumber)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            switch (portNumber)
            {
                case 1:
                    //Включение оптики, команда 10
                    port1.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Включение оптики, команда 10
                    port2.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Включение оптики, команда 10
                    port3.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Включение оптики, команда 10
                    port4.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdSetDelay);
            await task;
        }

        private async Task<bool> AttemptCountCmd(int cmdNumber, int portNumber, short Freq, byte Gain, int count = 3)
        {
            var flag = false;
            int counter = 0;
            do
            {
                switch (cmdNumber)
                {
                    case 1:
                        //Console.WriteLine($"PortNumber: {portNumber} - Cmd{cmdNumber}");
                        flag = await TaskTimeChecker(Cmd1(portNumber, Freq), 200);
                        break;
                    case 2:
                        //Console.WriteLine($"PortNumber: {portNumber} - Cmd{cmdNumber}");
                        flag = await TaskTimeChecker(Cmd2(portNumber, Gain), 200);
                        break;
                    case 4:
                        //Console.WriteLine($"PortNumber: {portNumber} - Cmd{cmdNumber}");
                        flag = await TaskTimeChecker(Cmd4(portNumber, Gain), 200);
                        break;
                    case 10:
                        //Console.WriteLine($"PortNumber: {portNumber} - Cmd{cmdNumber}");
                        flag = await TaskTimeChecker(Cmd10(portNumber), 200);
                        break;
                }
                if (flag == false) counter++;
                if (counter == count) return false;
            }
            while (!flag);
            return true;
        }


        private async Task SetPreselFreqGainNewAsyncСonsistently(short Freq, byte Gain)
        {
            //Инициализация ожидания преселектора 1
            var task = InitPreselAwait(1);
            //Установка частоты-усиления, команда 06
            portNew1.SetFreqGainCode6((short)Freq, Gain, PortNew.PreselectorNumber.AllPreselector);
            await task;

            //Инициализация ожидания преселектора 1
            task = InitPreselAwait(2);
            portNew2.SetFreqGainCode6((short)Freq, Gain, PortNew.PreselectorNumber.AllPreselector);
            await task;

            //Инициализация ожидания преселектора 1
            task = InitPreselAwait(2);
            portNew3.SetFreqGainCode6((short)Freq, Gain, PortNew.PreselectorNumber.AllPreselector);
            await task;

            //Инициализация ожидания преселектора 1
            task = InitPreselAwait(4);
            portNew4.SetFreqGainCode6((short)Freq, Gain, PortNew.PreselectorNumber.AllPreselector);
            await task;
        }
        private async Task SetPreselFreqGainNewAsyncСonsistently2(short Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainNewAsyncСonsistently2 Start");


            //Инициализация ожидания преселектора 1
            var task = InitPreselAwait(1);
            //Настройка преселектора на частоту, команда 01
            portNew1.SetFreqCode1((short)Freq, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);

            //Инициализация ожидания преселектора 2
            task = InitPreselAwait(2);
            portNew2.SetFreqCode1((short)Freq, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);
            await task;

            //Инициализация ожидания преселектора 3
            task = InitPreselAwait(3);
            portNew3.SetFreqCode1((short)Freq, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);
            await task;

            //Инициализация ожидания преселектора 4
            task = InitPreselAwait(4);
            portNew4.SetFreqCode1((short)Freq, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);
            await task;


            //Инициализация ожидания преселектора 1
            task = InitPreselAwait(1);
            //Настройка усиления преселектора
            portNew1.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);
            await task;

            //Инициализация ожидания преселектора 2
            task = InitPreselAwait(2);
            portNew2.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);
            await task;

            //Инициализация ожидания преселектора 3
            task = InitPreselAwait(3);
            portNew3.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);
            await task;

            //Инициализация ожидания преселектора 4
            task = InitPreselAwait(4);
            portNew4.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
            await Task.Delay(1);
            await task;
        }
        private async Task<bool[]> SetPreselFreqGainNewAsyncСonsistently5(int Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainNewAsyncСonsistently5 Start");

            //Настройка преселектора на частоту, команда 01
            {
                var b1 = await AttemptCountCmdNew(1, 1, (short)Freq, Gain);
                var b2 = await AttemptCountCmdNew(1, 2, (short)Freq, Gain);
                var b3 = await AttemptCountCmdNew(1, 3, (short)Freq, Gain);
                var b4 = await AttemptCountCmdNew(1, 4, (short)Freq, Gain);

                bool[] b = new bool[] { b1, b2, b3, b4 };
                if (b.Any(x => x == false)) return b;
            }

            //Настройка усиления преселектора, команда 02
            {
                var b1 = await AttemptCountCmdNew(2, 1, (short)Freq, Gain);
                var b2 = await AttemptCountCmdNew(2, 2, (short)Freq, Gain);
                var b3 = await AttemptCountCmdNew(2, 3, (short)Freq, Gain);
                var b4 = await AttemptCountCmdNew(2, 4, (short)Freq, Gain);

                bool[] b = new bool[] { b1, b2, b3, b4 };
                if (b.Any(x => x == false)) return b;
            }
            return new bool[] { true, true, true, true };
        }
        private async Task<bool[]> SetPreselFreqGainNewAsyncСonsistently6(int Freq, byte Gain)
        {
            Console.WriteLine("SetPreselFreqGainNewAsyncСonsistently6 Start");


            bool[] b = new bool[] { true, true, true, true };


            //Настройка преселектора на частоту, команда 01
            var b1 = await AttemptCountCmdNew(1, 1, (short)Freq, Gain);
            if (b[0] == true && b1 == false) b[0] = b1;
            Console.WriteLine($"Result: PortNumber: {1} - Cmd{1} - {b1}");

            var b2 = await AttemptCountCmdNew(1, 2, (short)Freq, Gain);
            if (b[1] == true && b2 == false) b[1] = b2;
            Console.WriteLine($"Result: PortNumber: {2} - Cmd{1} - {b2}");

            var b3 = await AttemptCountCmdNew(1, 3, (short)Freq, Gain);
            if (b[2] == true && b3 == false) b[2] = b3;
            Console.WriteLine($"Result: PortNumber: {3} - Cmd{1} - {b3}");

            var b4 = await AttemptCountCmdNew(1, 4, (short)Freq, Gain);
            if (b[3] == true && b4 == false) b[3] = b4;
            Console.WriteLine($"Result: PortNumber: {4} - Cmd{1} - {b4}");


            //Настройка усиления преселектора, команда 02
            b1 = await AttemptCountCmdNew(2, 1, (short)Freq, Gain);
            if (b[0] == true && b1 == false) b[0] = b1;
            Console.WriteLine($"Result: PortNumber: {1} - Cmd{2} - {b1}");

            b2 = await AttemptCountCmdNew(2, 2, (short)Freq, Gain);
            if (b[1] == true && b2 == false) b[1] = b2;
            Console.WriteLine($"Result: PortNumber: {2} - Cmd{2} - {b2}");

            b3 = await AttemptCountCmdNew(2, 3, (short)Freq, Gain);
            if (b[2] == true && b3 == false) b[2] = b3;
            Console.WriteLine($"Result: PortNumber: {3} - Cmd{2} - {b3}");

            b4 = await AttemptCountCmdNew(2, 4, (short)Freq, Gain);
            if (b[3] == true && b4 == false) b[3] = b4;
            Console.WriteLine($"Result: PortNumber: {4} - Cmd{2} - {b4}");


            return b;
        }

        private async Task Cmd1New(int portNumber, short Freq)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            switch (portNumber)
            {
                case 1:
                    //Установка частоты, команда 01
                    portNew1.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Установка частоты, команда 01
                    portNew2.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Установка частоты, команда 01
                    portNew3.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Установка частоты, команда 01
                    portNew4.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdDelay);
            await task;
        }
        private async Task Cmd2New(int portNumber, byte Gain)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            switch (portNumber)
            {
                case 1:
                    //Установка усиления, команда 02
                    portNew1.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Установка усиления, команда 02
                    portNew1.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Установка усиления, команда 02
                    portNew1.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Установка усиления, команда 02
                    portNew1.SetGainCode2_3b(Gain, PortNew.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdDelay);
            await task;
        }
        private async Task Cmd4New(int portNumber)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            switch (portNumber)
            {
                case 1:
                    //Включение предусилителя, команда 04
                    portNew1.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Включение предусилителя, команда 04
                    portNew2.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Включение предусилителя, команда 04
                    portNew3.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Включение предусилителя, команда 04
                    portNew4.SetOnOffCode4(PortNew.PreselectorOnOff.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdSetDelay);
            await task;
        }
        private async Task Cmd10New(int portNumber)
        {
            //Инициализация ожидания преселектора portNumber
            var task = InitPreselAwait(portNumber);
            switch (portNumber)
            {
                case 1:
                    //Включение оптики, команда 10
                    portNew1.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 2:
                    //Включение оптики, команда 10
                    portNew2.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 3:
                    //Включение оптики, команда 10
                    portNew3.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                case 4:
                    //Включение оптики, команда 10
                    portNew4.SetOpticalPowerCode10(PortNew.OnOffSwitch.ON, PortNew.PreselectorNumber.AllPreselector);
                    break;
                default: return;
            }
            await Task.Delay(PreselCmdSetDelay);
            await task;
        }

        private async Task<bool> AttemptCountCmdNew(int cmdNumber, int portNumber, short Freq, byte Gain, int count = 3)
        {
            var flag = false;
            int counter = 0;
            do
            {
                switch (cmdNumber)
                {
                    case 1:
                        flag = await TaskTimeChecker(Cmd1New(portNumber, Freq), 200);
                        break;
                    case 2:
                        flag = await TaskTimeChecker(Cmd2New(portNumber, Gain), 200);
                        break;
                    case 4:
                        flag = await TaskTimeChecker(Cmd4New(portNumber), 200);
                        break;
                    case 10:
                        flag = await TaskTimeChecker(Cmd10New(portNumber), 200);
                        break;
                }
                if (flag == false) counter++;
                if (counter == count) return false;
            }
            while (!flag);
            return true;
        }

        private Task InitPreselAwait(int Number)
        {
            switch (Number)
            {
                case 1:
                    c1 = new CancellationTokenSource();
                    CancellationToken ctoken1 = c1.Token;
                    return TokenAwater(ctoken1);
                case 2:
                    c2 = new CancellationTokenSource();
                    CancellationToken ctoken2 = c2.Token;
                    return TokenAwater(ctoken2);
                case 3:
                    c3 = new CancellationTokenSource();
                    CancellationToken ctoken3 = c3.Token;
                    return TokenAwater(ctoken3);
                case 4:
                    c4 = new CancellationTokenSource();
                    CancellationToken ctoken4 = c4.Token;
                    return TokenAwater(ctoken4);
                default:
                    return null;
            };
        }
      
    }
}