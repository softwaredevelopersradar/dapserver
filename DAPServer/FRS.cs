﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAPServer
{
    public class FRS
    {
        public double FreqkHz;
        public float BandWidthkHz;
        public List<KirasaModelsDBLib.Coord> Coords;

        public FRS()
        {
            FreqkHz = 0;
            BandWidthkHz = 0;
            Coords = new List<KirasaModelsDBLib.Coord>();
        }
    }

    public struct Coord
    {
        public double Lat;
        public double Lon;
        public double Alt;
    }
}
