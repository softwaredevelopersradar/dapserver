﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAPServer
{
    public partial class MainWindow
    {
        private List<double> approximationParabellum_returnValue(List<double> x, List<double> y)
        {
            List<double> x2 = new List<double>();
            List<double> x3 = new List<double>();
            List<double> x4 = new List<double>();
            List<double> xy = new List<double>();
            List<double> x2y = new List<double>();

            int n = x.Count();

            for (int i = 0; i < n; i++)
            {
                x2.Add(Math.Pow(x[i], 2));
                x3.Add(Math.Pow(x[i], 3));
                x4.Add(Math.Pow(x[i], 4));
                xy.Add(x[i] * y[i]);
                x2y.Add(Math.Pow(x[i], 2) * y[i]);
            }

            double sumX = x.Sum();
            double sumY = y.Sum();
            double sumX2 = x2.Sum();
            double sumX3 = x3.Sum();
            double sumX4 = x4.Sum();
            double sumXY = xy.Sum();
            double sumX2Y = x2y.Sum();

            double delta = determinant(sumX2, sumX, n, sumX3, sumX2, sumX, sumX4, sumX3, sumX2);

            double delta_a = determinant(sumY, sumX, n, sumXY, sumX2, sumX, sumX2Y, sumX3, sumX2);
            double delta_b = determinant(sumX2, sumY, n, sumX3, sumXY, sumX, sumX4, sumX2Y, sumX2);
            double delta_c = determinant(sumX2, sumX, sumY, sumX3, sumX2, sumXY, sumX4, sumX3, sumX2Y);

            double COEF_A = delta_a / delta;
            double COEF_B = delta_b / delta;
            double COEF_C = delta_c / delta;

            return new List<double>() { COEF_A, COEF_B, COEF_C };
        }

        private double determinant(double a1, double a2, double a3,
                                         double a4, double a5, double a6,
                                         double a7, double a8, double a9)
        {
            return a1 * (a5 * a9 - a8 * a6) - a2 * (a4 * a9 - a7 * a6) + a3 * (a4 * a8 - a7 * a5);
        }

        private float corrThreshold = 0.1f;
        public float CorrThreshold
        {
            get => corrThreshold;
            set
            {
                if (corrThreshold != value)
                {
                    corrThreshold = value;
                    settings.CorrThreshold = value;
                }
            }
        }

        public float CorrDivide { get; set; } = 0.5f;

        public double BandWidthMHz { get; set; } = 62.5d;

        private (double tau1, double tau2, double tau3) CalcTauFromCorrFuncs(List<List<double>> CorrDoubleList)
        {
            List<double> tupl = new List<double>() { -1, -1, -1 };
            if (CorrDoubleList.Count() == 3)
            {
                List<double> lMaxes = CorrDoubleList.Select(x => x.Max()).ToList();

                if (lMaxes.All(x => x >= CorrThreshold))
                {
                    for (int w = 0; w < CorrDoubleList.Count(); w++)
                    {
                        double localMax = (lMaxes[w] * CorrDivide);

                        int maxindex = CorrDoubleList[w].IndexOf(lMaxes[w]);
                        int lbindex = -1;
                        int hbindex = -1;
                        for (int i = 0; i < CorrDoubleList[w].Count(); i++)
                        {
                            if (i < maxindex && CorrDoubleList[w][i] >= localMax && lbindex == -1)
                            {
                                lbindex = i;
                            }
                            if (i > maxindex && CorrDoubleList[w][i] <= localMax && lbindex != -1 && hbindex == -1)
                            {
                                hbindex = i;
                                break;
                            }
                        }

                        if (lbindex != -1 && hbindex != -1)
                        {
                            List<double> subRangeX = new List<double>();
                            for (int j = lbindex; j < hbindex; j++)
                                subRangeX.Add(j);

                            List<double> subRangeY = CorrDoubleList[w].GetRange(lbindex, hbindex - lbindex);

                            var ABC = approximationParabellum_returnValue(subRangeX, subRangeY);

                            tupl[w] = ((-1d) * (ABC[1] / (2d * ABC[0]))) * (1d / (BandWidthMHz * 10e6)) * 10e9;
                        }
                    }
                }
            }
            return ValueTuple.Create(tupl[0], tupl[1], tupl[2]);
        }
    }
}
