﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WorkPort;


namespace DAPServer
{
    public partial class MainWindow
    {
        Port port = new Port();

        public void InitOptcEvents()
        {
            port.OnWriteB += port_OnWriteB;
            port.OnReadB += port_OnReadB;

            port.OnPreselectorNumber += port_OnPreselectorNumber;

            port.OnState_00 += port_OnState_00; // Текущее состояние преселектора
            port.OnFreq_01 += port_OnFreq_01;  // Получение установленной частоты
            port.OnGain_02 += port_OnGain_02; // Получение установленного усиления
            port.OnModePreamp_03 += port_OnModePreamp_03; //- Получение установленного режимы работы предусилителя
            port.OnOnOffPreamp_04 += port_OnOnOffPreamp_04; // Получения состояния предусилителя
            port.OnSetPreampRange_05 += port_OnSetPreampRange_05;  // Получение настройки предусилителя по номеру диапозона
            port.OnSetFreqGain_06 += port_OnSetFreqGain_06; // Получение частоты и усиления на этой частоте
            port.OnModeExternalSwitches_07 += port_OnModeExternalSwitches_07; // Получение режима работы внешних коммутаторов
            port.OnRFChannel_08 += port_OnRFChannel_08; //- Получение номера РЧ-Канала
            port.OnGain_09 += port_OnGain_09; // Получение уровня аттенюатора предусилителя
            port.OnAttenuatorLevelLow_0A += port_OnAttenuatorLevelLow_0A; // Получение уровня аттенюатора предусилителя в нижнем диапозоне
            port.OnAttenuatorLevelHigh_0B += port_OnAttenuatorLevelHigh_0B; // Получение уровня аттенюатора предусилителя в верхнем диапозоне
            port.OnOnOffSwitch_0F += port_OnOnOffSwitch_0F; // Получение состояния питания для внешних коммутаторов
            port.OnOnOffOpticalTransmitter_10 += port_OnOnOffOpticalTransmitter_10; // Получение состояния питания для оптического передатчика
            port.OnModeAttenuator_11 += port_OnModeAttenuator_11; // Получение режима работы аттенюатора в канале предусилителя
            port.OnConfigurationChanels_12 += port_OnConfigurationChanels_12; // Получение конфигурирования каналов на внешних коммутаторах

            port.OnSave_13 += port_OnSave_13;
            port.OnReset_14 += port_OnReset_14;

        }

        private void port_OnWriteB(object sender, string e)
        {
            Console.WriteLine("Write bytes: " + e);
        }
        private void port_OnReadB(object sender, string e)
        {
            Console.WriteLine("Read bytes: " + e);
        }

        private void port_OnPreselectorNumber(object sender, Port.PreselectorNumber e)
        {
            Console.WriteLine("PreselectorNumber: " + e);
        }

        private void port_OnState_00(object sender, StateEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                TB1.Text =
                  e.GainLO1.ToString() + "\r\n" +
                  e.GainHI1.ToString() + "\r\n" +
                  e.GainPreamp1.ToString() + "\r\n" +
                  e.StateFilterLO1.ToString() + "\r\n" +
                  e.StateFilterHI1.ToString() + "\r\n" +
                  e.NumberFilter1.ToString() + "\r\n" +
                  e.FlagPreamp1.ToString() + "\r\n" +
                  e.LOorHI1.ToString() + "\r\n" +
                  e.PowerPreamp1.ToString() + "\r\n" +
                  e.LOorHI2.ToString() + "\r\n" +
                  e.PowerPreamp2.ToString() + "\r\n" +
                  e.GainLO2.ToString() + "\r\n" +
                  e.GainHI2.ToString() + "\r\n" +
                  e.GainPreamp2.ToString() + "\r\n" +
                  e.StateFilterLO2.ToString() + "\r\n" +
                  e.StateFilterHI2.ToString() + "\r\n" +
                  e.NumberFilter2.ToString() + "\r\n" +
                  e.FlagPreamp2.ToString() + "\r\n" +
                  e.ExternalSwitch1.ToString() + "\r\n" +
                  e.ExternalSwitch2.ToString() + "\r\n";
            });
        }
        private void port_OnFreq_01(object sender, short e)
        {
            Console.WriteLine("GetFreq : " + e);
        }
        private void port_OnGain_02(object sender, byte e)
        {
            Console.WriteLine("GetGain : " + e);
        }
        private void port_OnModePreamp_03(object sender, Port.ModePreamp e)
        {
            Console.WriteLine("GetPreamp" + e);
        }
        private void port_OnOnOffPreamp_04(object sender, Port.OnOffPreamp e)
        {
            Console.WriteLine("GetPreampOnOff" + e);
        }
        private void port_OnSetPreampRange_05(object sender, Port.SetPreampRange e)
        {
            Console.WriteLine("GetPreampRange" + e);
        }
        private void port_OnSetFreqGain_06(object sender, FreqGainEventArgs e)
        {
            Console.WriteLine("Port_OnGetFreqGain : " + "Freq: "+ e.Freq + "Gain: " + e.Gain);
        }
        private void port_OnModeExternalSwitches_07(object sender, Port.ModePreamp e)
        {
            Console.WriteLine("GetModeExternalSwitches" + e);
        }
        private void port_OnRFChannel_08(object sender, Port.RFChannel e)
        {
            Console.WriteLine("OnGetRFChanel" + e);
        }
        private void port_OnGain_09(object sender, byte e)
        {
            Console.WriteLine("GetGain" + e);
        }
        private void port_OnAttenuatorLevelLow_0A(object sender, byte e)
        {
            Console.WriteLine("GetAttenuatorLevelLow_0A" + e);
        }
        private void port_OnAttenuatorLevelHigh_0B(object sender, byte e)
        {
            Console.WriteLine("GetAttenuatorLevelHigh_0B: " + e);
        }
        private void port_OnOnOffSwitch_0F(object sender, Port.OnOffSwitch e)
        {
            Console.WriteLine("GetOnOffSwitch: " + e);
        }
        private void port_OnOnOffOpticalTransmitter_10(object sender, Port.OnOffSwitch e)
        {
            Console.WriteLine("GetOnOrOffOpticalTransmitter: " + e);
        }
        private void port_OnModeAttenuator_11(object sender, Port.ModePreamp e)
        {
            Console.WriteLine("GetOnModeAttenuator_11: " + e);
        }
        private void port_OnConfigurationChanels_12(object sender, short[] e)
        {
            Console.WriteLine("GetConfigurationChanels" + "\r\n" + e);
        }
        private void port_OnSave_13(object sender, bool e)
        {
            Console.WriteLine("Port_OnSave" + "\r\n" + e);
        }
        private void port_OnReset_14(object sender, bool e)
        {
            Console.WriteLine("Port_OnReset" + "\r\n" + e);
        }

    }
}
