﻿using Arction.Wpf.SemibindableCharting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAPServer
{
    public partial class MainWindow
    {
        public void PlotCorrFunc(double[] dataArray, int Number)
        {
            //int pointCounter = dataArray.Length;

            //var min = chart.ViewXY.XAxes[0].Minimum;
            //var max = chart.ViewXY.XAxes[0].Maximum;

            //var data = new SeriesPoint[pointCounter];
            //for (int i = 0; i < pointCounter; i++)
            //{
            //    data[i].X = min + i * (max - min) / pointCounter;
            //    data[i].Y = dataArray[i];
            //}

            //switch (Number)
            //{
            //    case 0:
            //        pointSeries.Points = data;
            //        break;
            //    case 1:
            //        pointSeries1.Points = data;
            //        break;
            //    case 2:
            //        pointSeries2.Points = data;
            //        break;
            //}
        }



        public void PlotSpectrum(double[] dataArray, int Number)
        {
            //int pointCounter = dataArray.Length;

            //double min = 0;
            //double max = 0;

            //switch (Number)
            //{
            //    case 0:
            //        min = sChart0.ViewXY.XAxes[0].Minimum;
            //        max = sChart0.ViewXY.XAxes[0].Maximum;
            //        break;
            //    case 1:
            //        min = sChart0_Copy.ViewXY.XAxes[0].Minimum;
            //        max = sChart0_Copy.ViewXY.XAxes[0].Maximum;
            //        break;
            //    case 2:
            //        min = sChart0_Copy1.ViewXY.XAxes[0].Minimum;
            //        max = sChart0_Copy1.ViewXY.XAxes[0].Maximum;
            //        break;
            //    case 3:
            //        min = sChart0_Copy2.ViewXY.XAxes[0].Minimum;
            //        max = sChart0_Copy2.ViewXY.XAxes[0].Maximum;
            //        break;
            //    case 4:
            //        min = sChart0_Copy3.ViewXY.XAxes[0].Minimum;
            //        max = sChart0_Copy3.ViewXY.XAxes[0].Maximum;
            //        break;
            //}

            //var data = new SeriesPoint[pointCounter];
            //for (int i = 0; i < pointCounter; i++)
            //{
            //    data[i].X = min + i * (max - min) / pointCounter;
            //    data[i].Y = dataArray[i];
            //}

            //switch (Number)
            //{
            //    case 0:
            //        spointSeries0.Points = data;
            //        break;
            //    case 1:
            //        spointSeries1.Points = data;
            //        break;
            //    case 2:
            //        spointSeries2.Points = data;
            //        break;
            //    case 3:
            //        spointSeries3.Points = data;
            //        break;
            //    case 4:
            //        spointSeries4.Points = data;
            //        break;
            //}
        }
        public void PlotSpectrum(float[] dataArray, int Number)
        {
        //    int pointCounter = dataArray.Length;

        //    double min = 0;
        //    double max = 0;

        //    switch (Number)
        //    {
        //        case 0:
        //            min = sChart0.ViewXY.XAxes[0].Minimum;
        //            max = sChart0.ViewXY.XAxes[0].Maximum;
        //            break;
        //        case 1:
        //            min = sChart0_Copy.ViewXY.XAxes[0].Minimum;
        //            max = sChart0_Copy.ViewXY.XAxes[0].Maximum;
        //            break;
        //        case 2:
        //            min = sChart0_Copy1.ViewXY.XAxes[0].Minimum;
        //            max = sChart0_Copy1.ViewXY.XAxes[0].Maximum;
        //            break;
        //        case 3:
        //            min = sChart0_Copy2.ViewXY.XAxes[0].Minimum;
        //            max = sChart0_Copy2.ViewXY.XAxes[0].Maximum;
        //            break;
        //        case 4:
        //            min = sChart0_Copy3.ViewXY.XAxes[0].Minimum;
        //            max = sChart0_Copy3.ViewXY.XAxes[0].Maximum;
        //            break;
        //    }

        //    var data = new SeriesPoint[pointCounter];
        //    for (int i = 0; i < pointCounter; i++)
        //    {
        //        data[i].X = min + i * (max - min) / pointCounter;
        //        data[i].Y = dataArray[i];
        //    }

        //    switch (Number)
        //    {
        //        case 0:
        //            spointSeries0.Points = data;
        //            break;
        //        case 1:
        //            spointSeries1.Points = data;
        //            break;
        //        case 2:
        //            spointSeries2.Points = data;
        //            break;
        //        case 3:
        //            spointSeries3.Points = data;
        //            break;
        //        case 4:
        //            spointSeries4.Points = data;
        //            break;
        //    }
        }


        public void PlotABit(float[] dataArray)
        {
            int pointCounter = dataArray.Length;
            var data = new SeriesPoint[pointCounter];

            double startFreq = 2368.75;
            double endFreq = 2431.25;

            for (int i = 0; i < pointCounter; i++)
            {
                data[i].X = startFreq + i * (endFreq - startFreq) / (pointCounter - 1);
                data[i].Y = dataArray[i];
            }

            //spointSeries0.Points = data;
        }

        public void PlotABit(double startFreq, double endFreq, float[] dataArray)
        {
            int pointCounter = dataArray.Length;
            var data = new SeriesPoint[pointCounter];

            for (int i = 0; i < pointCounter; i++)
            {
                data[i].X = startFreq + i * (endFreq - startFreq) / (pointCounter - 1);
                data[i].Y = dataArray[i];
            }

           // spointSeries0.Points = data;
        }

    }
}
