﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using UDP_Cuirasse;

namespace DAPServer
{
    public partial class MainWindow
    {
        UDP uDP = new UDP();

        public void InitUDP()
        {
            uDP = new UDP();
        }

        public void InitUDP(IPAddress myIp, int myPort, IPAddress remoteIp, int remotePort, byte addrSender, byte addrRecipient, Mod mod)
        {
            uDP = new UDP(myIp, myPort, remoteIp, remotePort, addrSender, addrRecipient, mod, ReceiveMod.Receiver);
        }

        public void InitOptcDtEvents()
        {
            uDP.OnConnect += UDP_OnConnect; // проверка подключения(true - успешно) 
            uDP.OnDisconnect += UDP_OnDisconnect; // закрывает соединение UDP(true - успешно)
            uDP.OnSend += UDP_OnSend; // возвращает отправленный byte[] содержащий данные дейтаграммы
            uDP.OnReceive += UDP_OnReceive; // возвращает byte[] содержащий данные дейтаграммы
            //uDP.OnGetState += UDP_OnGetState;//запрос состояния
            //uDP.OnSetFreq += UDP_OnSetFreq; // установить параметры
            //uDP.OnGetSpectrum += UDP_OnGetSpectrum; // запрос спектра
            //uDP.OnGetCorrelationFunc += UDP_OnGetCorrelationFunc; // запрос корреляционной функции
        }

        private void UDP_OnConnect(object sender, OpenCloseEventArgs e)
        {
            Console.WriteLine("UDP_OnConnect " + e.Flag);
        }

        private void UDP_OnDisconnect(object sender, OpenCloseEventArgs e)
        {
            Console.WriteLine("UDP_OnDisconnect " + e.Flag);
        }

        private void UDP_OnSend(object sender, SendEventArgs e)
        {
            //Console.WriteLine("UDP_OnSend" + e.bytes);
        }

        private void UDP_OnReceive(object sender, ReceiveEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void UDP_OnGetState(object sender, GetStateEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void UDP_OnSetFreq(object sender, SetFreqEventArgs e)
        {
            Console.WriteLine(e);
        }

        private void UDP_OnGetSpectrum(object sender, GetSpectrumEventArgs e)
        {
            Console.WriteLine(e);
        }

        private void UDP_OnGetCorrelationFunc(object sender, GetCorrelationFuncEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
