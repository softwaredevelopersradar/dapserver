﻿using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

using UDP_Cuirasse;

using DAPServerClient2;
using DspDataModel;


using YamlDotNet.Serialization;
using YamlDotNet.Core;
using WpfMapRastr;
using WorkPort;
using WorkPortNew;
using Nito.AsyncEx;
using DAPprotocols;

namespace DAPServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Yaml yaml = new Yaml();
        Settings settings;

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        ClientDB clientDB;

        Server MyDapServer = new Server();

        GainSettings _GainSettings;

        private void InitDeploymentKey()
        {
            //Set Deployment Key for Arction components             
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi - bindable chart, if you use it
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        public MainWindow()
        {
            InitDeploymentKey();

            InitializeComponent();


            MessageLogger.SetLogger(new NLogLogger());

            InitOptcEvents();
            InitOptcDtEvents();

            settings = yaml.YamlLoad<Settings>("Settings.yaml");
            //yaml.YamlSave<Settings>(settings, "Settings.yaml");

            InitDivide(settings);

            LoadForm(settings);

            _GainSettings = GainSettings.Load(yaml, _GlobalNumberOfBands, "GainSettings.yaml");


            CorrelationParser.CorrParser.TheardEnter();

            PreInitTrackDefinitions();

            string Name = "DAPServer";
            //string endPoint = "127.0.0.1:8302";
            string endPoint = GenerateDBendPoint(settings.DBSettings);

            clientDB = new ClientDB(Name, endPoint);
            InitClientDB();
            clientDB?.ConnectAsync();

            InitUDP(
               IPAddress.Parse(settings.OpticUDPSettings.myIpAdress),
               settings.OpticUDPSettings.myPort,
               IPAddress.Parse(settings.OpticUDPSettings.remoteIpAdress),
               settings.OpticUDPSettings.remotePort,
               0,
               0,
               Mod.RealTimeWork
               );

            UDPClass.Emulator.OnCheckLoadToList += Emulator_OnCheckLoadToList;
            UDPClass.Emulator.TheardEnter("All");

            uDP.Connect();

            InitDapServerEvents();


            config = yaml.YamlLoad<Config>(Config.ConfigPath);


            dataProcessor0 = new DataProcessor.DataProcessor(config);
            //dataProcessor1 = new DataProcessor.DataProcessor(config);
            //dataProcessor2 = new DataProcessor.DataProcessor(config);
            //dataProcessor3 = new DataProcessor.DataProcessor(config);
            //dataProcessor4 = new DataProcessor.DataProcessor(config);

            RecalcDivide();
            GenerateSpectrumStorage();

            GenerateBigSpectrumStorage();

            InitSomeOneRanges();


            InitComboBox(settings.workMode);


            //Инициализация преселектора
            InitPresel(settings);

            MyDapServer.ServerStart(settings.ServerSettings.IpAddress, settings.ServerSettings.Port);

            InitDictionariesForPreSelAndRecGain();
        }

        Dictionary<int, short> DictionaryPreSelGain = new Dictionary<int, short>();
        Dictionary<int, short> DictionaryRecGain = new Dictionary<int, short>();

        private void InitDictionariesForPreSelAndRecGain()
        {
            for (int i = 0; i < _GlobalNumberOfBands; i++)
            {
                DictionaryPreSelGain.Add(i, -1);
                DictionaryRecGain.Add(i, -1);
            }
        }

        private void InitDapServerEvents()
        {
            MyDapServer.ResponseIsNeeded += MyDapServer_ResponseIsNeeded;
            MyDapServer.SpectrumResponseIsNeeded += MyDapServer_SpectrumResponseIsNeeded;
            MyDapServer.CorrFuncResponseIsNeeded += MyDapServer_CorrFuncResponseIsNeeded;
            MyDapServer.DeviceGainMessageResponseIsNeeded += MyDapServer_DeviceGainMessageResponseIsNeeded;
            MyDapServer.FiltersResponseIsNeeded += MyDapServer_FiltersResponseIsNeeded;
        }

        private void InitComboBox(WorkMode currentWorkMode)
        {
            var enumCount = Enum.GetNames(typeof(WorkMode)).Length;

            for (int i = 0; i < enumCount; i++)
            {
                WorkMode tempWorkMode = (WorkMode)Enum.GetValues(typeof(WorkMode)).GetValue((byte)i);
                CBworkMode.Items.Add(tempWorkMode.ToString());
            }

            CBworkMode.SelectedIndex = (byte)currentWorkMode;
        }


        List<TrackAirObject> _listTrackAirObject = new List<TrackAirObject>();

        TracksDefinition _tracksDefinition;

        private void UpdateTrackDefinitionsParameters(List<GlobalProperties> TableGlobalProperties)
        {
            if (TableGlobalProperties.Count > 0)
            {
                // Максимально допустимое время ожидания отметки
                _tracksDefinition.twait = TableGlobalProperties[0].UpdateSourceTime;
                // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                _tracksDefinition.twaitAirObject = TableGlobalProperties[0].UpdateAirObjTime;
                // Допустимая скорость
                _tracksDefinition.Vdop = TableGlobalProperties[0].Speed;
                // СКО ускорения на плоскости
                _tracksDefinition.CKOA_pl = TableGlobalProperties[0].RmsAccelerationPlane;
                // СКО ускорения по высоте
                _tracksDefinition.CKOA_H = TableGlobalProperties[0].RmsAccelerationAltitude;
                // СКО по координатам для формирования матрицы D 
                // ошибок измерения в данный момент времени
                _tracksDefinition.CKO_X = TableGlobalProperties[0].RmsX;
                _tracksDefinition.CKO_Y = TableGlobalProperties[0].RmsY;
                _tracksDefinition.CKO_Z = TableGlobalProperties[0].RmsZ;
                // Количество точек для инициализации фильтра
                _tracksDefinition.numbInit = TableGlobalProperties[0].MinAirObjPoints;
                //Статическая матрица ошибок
                _tracksDefinition.flagStatDyn = TableGlobalProperties[0].StaticErrorMatrix;
                // Отсеивание по высоте
                _tracksDefinition.Hmax = settings.HMax;
            }
        }

        private void PreInitTrackDefinitions()
        {
            DispatchIfNecessary(() =>
            {
                //Станции по умолчанию
                if (cbDS.IsChecked.Value)
                {
                    //Заполенение листа координат станций и их задержек
                    List<Station> listStations = new List<Station>();

                    Station station0 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.9312,
                            Long: 27.635555556,
                            Alt: 15,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 0,
                    };
                    station0.Position.ID = 0;
                    listStations.Add(station0);

                    Station station1 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.930997222,
                            Long: 27.6349,
                            Alt: 23.01,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 1E-9,
                    };
                    station1.Position.ID = 1;
                    listStations.Add(station1);

                    Station station2 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.931444444,
                            Long: 27.636638889,
                            Alt: 22.63,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 1E-9,
                    };
                    station2.Position.ID = 2;
                    listStations.Add(station2);

                    Station station3 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.932327778,
                            Long: 27.63485,
                            Alt: 14.86,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 1E-9,
                    };
                    station3.Position.ID = 3;
                    listStations.Add(station3);

                    _tracksDefinition = new TracksDefinition(ref _listTrackAirObject)
                    {
                        listStations = listStations
                    };
                }
                //Параметры по умолчанию
                if (cbDP.IsChecked.Value)
                {
                    // Максимально допустимое время ожидания отметки
                    _tracksDefinition.twait = 5; //сек
                    // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                    _tracksDefinition.twaitAirObject = 5; //сек
                    // Допустимая скорость
                    _tracksDefinition.Vdop = 40; //м/с
                    // СКО ускорения на плоскости
                    _tracksDefinition.CKOA_pl = 0.12; // м/с*с
                    // СКО ускорения по высоте
                    _tracksDefinition.CKOA_H = 0.8; // м/с*с
                    // СКО по координатам для формирования матрицы D 
                    // ошибок измерения в данный момент времени
                    _tracksDefinition.CKO_X = 30; //м
                    _tracksDefinition.CKO_Y = 15; //м
                    _tracksDefinition.CKO_Z = 30; //м
                    // Количество точек для инициализации фильтра
                    _tracksDefinition.numbInit = 10;
                    // использовать динамическую матрицу ошибок измерений
                    _tracksDefinition.flagStatDyn = false;
                    // Отсеивание по высоте
                    _tracksDefinition.Hmax = settings.HMax;
                }
            });
        }

        private void InitTrackDefinitions()
        {
            if (tableLocalPoints != null && tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
            {
                List<Station> listStations = new List<Station>();

                //Функция порядочка и определения базовой стации в листе по индексу
                List<int> GeneratesListOfisOwnIndexes()
                {
                    List<int> lintisOwn = new List<int>();
                    for (int k = 0; k < tableLocalPoints.Count; k++)
                    {
                        if (tableLocalPoints[k].IsCetral == true)
                        {
                            lintisOwn.Insert(0, k);
                        }
                        else
                        {
                            lintisOwn.Add(k);
                        }
                    }
                    return lintisOwn;
                }
                List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                //Заполенение листа координат станций и их задержек
                for (int k = 0; k < 4; k++)
                {
                    Station station = new Station()
                    {
                        Position = new MarkCoord(
                            tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                            tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                            tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,

                            tableLocalPoints[lintIsOwn[0]].Coordinates.Latitude,
                            tableLocalPoints[lintIsOwn[0]].Coordinates.Longitude,
                            tableLocalPoints[lintIsOwn[0]].Coordinates.Altitude
                            ),

                        ErrorTime = tableLocalPoints[lintIsOwn[k]].TimeError * 1e-9,
                    };
                    listStations.Add(station);
                }

                _tracksDefinition = new TracksDefinition(ref _listTrackAirObject)
                {
                    listStations = listStations
                };
            }

            UpdateTrackDefinitionsParameters(lTableGlobalProperties);
        }

        private void InitSomeOneRanges()
        {
            DspDataModel.FrequencyRange[] frequencyRanges = new DspDataModel.FrequencyRange[5];

            frequencyRanges[0] = new DspDataModel.FrequencyRange(2400f, 2462.5f);
            frequencyRanges[1] = new DspDataModel.FrequencyRange(2437.5f, 2500f);
            frequencyRanges[2] = new DspDataModel.FrequencyRange(5720f, 5782.5f);
            frequencyRanges[3] = new DspDataModel.FrequencyRange(5760f, 5822.5f);
            frequencyRanges[4] = new DspDataModel.FrequencyRange(5807f, 5870.5f);
        }

        public static Config config = new Config();

        private List<List<float>> ListSpectrumStorage = new List<List<float>>();

        private void GenerateSpectrumStorage()
        {
            for (int i = 0; i < _GlobalNumberOfBands; i++)
            {
                //List<float> innerInitList = new List<float>(_GlobalDotsPerBandCount);
                List<float> innerInitList = new float[_GlobalDotsPerBandCount].ToList<float>();
                innerInitList = innerInitList.Select(x => x = -130f).ToList<float>();
                ListSpectrumStorage.Add(innerInitList);
            }
        }

        private List<List<List<float>>> BigListSpectrumStorage = new List<List<List<float>>>();
        // 0 - 1 канал
        // 1 - 2 канал
        // 2 - 3 канал
        // 3 - 4 канал
        // 4 - Max
        // 5 - Median
        private string ChanelName(int innerValue)
        {
            switch (innerValue)
            {
                case 0: return "1";
                case 1: return "2";
                case 2: return "3";
                case 3: return "4";
                case 4: return "Max";
                case 5: return "Median";
                default: return "?";
            }
        }
        private int _GlobalN = 6;
        private void GenerateBigSpectrumStorage()
        {
            for (int w = 0; w < _GlobalN; w++)
            {
                List<List<float>> innerListSpectrumStorage = new List<List<float>>();
                for (int i = 0; i < _GlobalNumberOfBands; i++)
                {
                    List<float> innerInitList = new float[_GlobalDotsPerBandCount].ToList<float>();
                    innerInitList = innerInitList.Select(x => x = -130f).ToList<float>();
                    innerListSpectrumStorage.Add(innerInitList);
                }
                BigListSpectrumStorage.Add(innerListSpectrumStorage);
            }
        }

        private void MyDapServer_ResponseIsNeeded(int Response, int extraCode)
        {
            switch (Response)
            {
                case 1:
                    GenerateResponse1(extraCode);
                    break;
                case 2:
                    GenerateResponse2(extraCode);
                    break;
                case 3:
                    GenerateResponse3(extraCode);
                    break;
                case 4:
                    GenerateResponse4(extraCode);
                    break;
                case 5:
                    GenerateResponse5();
                    break;
                case 6:
                    GenerateResponse6(extraCode);
                    break;
                case 7:
                    GenerateResponse7();
                    break;
                case 8:
                    GenerateResponse8(extraCode);
                    break;
                case 9:
                    GenerateResponse9(extraCode);
                    break;
            }
        }

        private async void MyDapServer_SpectrumResponseIsNeeded(int Response, DAPprotocols.GetSpectrumRequest getSpectrumRequest)
        {
            byte PostNumber = getSpectrumRequest.PostNumber;
            float MinFrequencyMHz = getSpectrumRequest.MinFrequencyMHz;
            float MaxFrequencyMHz = getSpectrumRequest.MaxFrequencyMHz;
            ChannelPicker Picker = getSpectrumRequest.Picker;

            //Log($"Spectrum Request for PostNumber: {getSpectrumRequest.PostNumber}");

            //var vs1 = NiceCalcSpectrum(getSpectrumRequest, ListSpectrumStorage);
            if ((int)Picker < _GlobalN)
            {
                var vs1 = NiceCalcSpectrum(getSpectrumRequest, BigListSpectrumStorage[(int)Picker]);

                var answer = await MyDapServer.ServerSendSpectrumResponse(PostNumber, Picker, MinFrequencyMHz, MaxFrequencyMHz, vs1);
            }
        }

        private float[] NiceCalcSpectrum(DAPprotocols.GetSpectrumRequest innerGetSpectrumRequest, List<List<float>> innerListSpectrumStorage)
        {
            var Indexes = CalcLikePro(innerGetSpectrumRequest.MinFrequencyMHz, innerGetSpectrumRequest.MaxFrequencyMHz);

            List<List<float>> resultSpectrumList = new List<List<float>>();

            for (int i = Indexes.indexStart; i <= Indexes.indexEnd; i++)
            {
                List<float> temp = new List<float>(innerListSpectrumStorage[i]);

                if (i == Indexes.indexStart)
                {
                    //Оберзка - вырезка

                    if (innerGetSpectrumRequest.MinFrequencyMHz != Divide[Indexes.indexStart])
                    //будем обрезать
                    {
                        double localBandWidthMHz = Divide[Indexes.indexStart + 1] - innerGetSpectrumRequest.MinFrequencyMHz;

                        int localPointsCount = (int)((localBandWidthMHz * _GlobalDotsPerBandCount) / _GlobalBandWidthMHz);

                        temp.RemoveRange(0, _GlobalDotsPerBandCount - localPointsCount);
                    }
                }

                if (i == Indexes.indexEnd)
                {
                    //Оберзка - вырезка

                    if (innerGetSpectrumRequest.MaxFrequencyMHz != Divide[Indexes.indexEnd + 1])
                    //будем обрезать
                    {
                        double localBandWidthMHz = Math.Abs(Divide[Indexes.indexEnd] - innerGetSpectrumRequest.MaxFrequencyMHz);

                        int localPointsCount = (int)((localBandWidthMHz * _GlobalDotsPerBandCount) / _GlobalBandWidthMHz);

                        temp.RemoveRange(temp.Count() - (_GlobalDotsPerBandCount - localPointsCount), _GlobalDotsPerBandCount - localPointsCount);
                    }
                }

                resultSpectrumList.Add(temp);
            }

            var vs1 = new float[RequeredCapacity(resultSpectrumList)];

            int RequeredCapacity(List<List<float>> SpectrumList)
            {
                int requeredCapacity = 0;
                for (int k = 0; k < SpectrumList.Count(); k++)
                {
                    requeredCapacity += SpectrumList[k].Count();
                }
                return requeredCapacity;
            }

            int CapacityFromIndex(int index, List<List<float>> SpectrumList)
            {
                int capacityFromIndex = 0;

                for (int k = 0; k < index; k++)
                {
                    capacityFromIndex += SpectrumList[k].Count;
                }

                return capacityFromIndex;
            }

            if (vs1.Count() > innerGetSpectrumRequest.PointCount)
            {
                for (int i = 0; i < resultSpectrumList.Count; i++)
                {
                    resultSpectrumList[i].CopyTo(0, vs1, CapacityFromIndex(i, resultSpectrumList), resultSpectrumList[i].Count);
                }

                //Ужатие Спектра от Феди
                var segment = new ArraySegment<float>(vs1, 0, vs1.Count());
                var spectrum = segment.StrechSpectrum(innerGetSpectrumRequest.PointCount);
                vs1 = spectrum.Amplitudes;
            }
            else
            {
                resultSpectrumList[0].CopyTo(0, vs1, 0, resultSpectrumList[0].Count);
            }

            return vs1;
        }

        float FilterMinFrequencyMHz = -1;
        float FilterMaxFrequencyMHz = -1;

        AsyncLock _lock = new AsyncLock();
        static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        private void MyDapServer_CorrFuncResponseIsNeeded(int Response, DAPprotocols.GetCorrFuncRequest getCorrFuncRequest)
        {
            FilterMinFrequencyMHz = getCorrFuncRequest.MinFrequencyMHz;
            FilterMaxFrequencyMHz = getCorrFuncRequest.MaxFrequencyMHz;
        }

        private async void MyDapServer_CorrFuncResponseIsNeededFull(int Response, DAPprotocols.GetCorrFuncRequest getCorrFuncRequest)
        {
            //await semaphoreSlim.WaitAsync();
            using (await _lock.LockAsync())
            {
                if (getCorrFuncRequest != null)
                {
                    FilterMinFrequencyMHz = getCorrFuncRequest.MinFrequencyMHz;
                    FilterMaxFrequencyMHz = getCorrFuncRequest.MaxFrequencyMHz;

                    Log($"CorrFunc Request for FuncNumber: {getCorrFuncRequest.FuncNumber}");

                    if (settings.workMode == WorkMode.Main)
                    {
                        ctsMain.Cancel();
                        await MainTask;

                        //Перестройка приёмника для получения Корреляционной функции

                        var centralFreqKhz = (getCorrFuncRequest.MaxFrequencyMHz + getCorrFuncRequest.MinFrequencyMHz) / 2 * 1000;
                        var bandwidthKhz = (getCorrFuncRequest.MaxFrequencyMHz - getCorrFuncRequest.MinFrequencyMHz) * 1000;

                        //Перестройка преселекторов
                        SetPreselFreqGain(settings.PreselectorVersion, (int)(centralFreqKhz / 1000), 10);

                        //Перестройка приемника
                        //var answerCorrSetFreq = await uDP.SetFreq((int)(centralFreqKhz), (int)(bandwidthKhz), 0);
                        //var answerCorrSetFreq = await uDP.SetFreq((int)(centralFreqKhz), (byte)(1), 0);
                        //var answerCorrSetFreq = await uDP.SetFreq((int)(centralFreqKhz), BandWidthCodeFromBandWidthkHz(bandwidthKhz), 30);
                        var answerCorrSetFreq = await uDP.SetFreq((int)(centralFreqKhz), 30);

                        //Ожидание приёмника
                        await Task.Delay(_ReceiverDelay);
                        CurrentFreqkHz = centralFreqKhz;

                        //установка параметров для корреляционной кривой
                        var answerCorrFuncParamSet = await uDP.SetParam(BandWidthCodeFromBandWidthkHz(bandwidthKhz), 0, Alpha1, Alpha2);
                        await Task.Delay(1);

                        //Запрос корреляционной функции
                        //var answerCor = await uDP.GetCorrelationFunc(0); // запрос с устройства №0
                        var answerCor = await uDP.GetCorrelationFunc(); // запрос с устройства №0

                        //Ортонормирование
                        var corrFuncTuple = OrthoNormalizationToDoubleTuple(answerCor);

                        //Отправка корреляциооной функции в АРМ Кираса
                        var answer = await MyDapServer.ServerSendCorrFuncResponse(
                            (byte)getCorrFuncRequest.FuncNumber,
                            (int)centralFreqKhz, (int)bandwidthKhz,
                            corrFuncTuple.corrFunc0,
                            corrFuncTuple.corrFunc1,
                            corrFuncTuple.corrFunc2,
                             corrFuncTuple.corrFunc3
                            );

                        MainReLoopIfNeeded();
                    }
                    else
                    {
                        if (settings.workMode == WorkMode.LoadSpectrum)
                        {
                            cts.Cancel();
                            await LoadSpectrumTask;
                        }

                        var centralFreqKhz = (getCorrFuncRequest.MaxFrequencyMHz + getCorrFuncRequest.MinFrequencyMHz) / 2 * 1000;
                        var bandwidthKhz = (getCorrFuncRequest.MaxFrequencyMHz - getCorrFuncRequest.MinFrequencyMHz) * 1000;

                        var CorrDoubleList = CorrelationParser.CorrParser.GetCorrFunc(getCorrFuncRequest.FuncNumber);

                        double[] corrFunc0 = CorrDoubleList[0].Select(x => x).ToArray<double>();
                        double[] corrFunc1 = CorrDoubleList[1].Select(x => x).ToArray<double>();
                        double[] corrFunc2 = CorrDoubleList[2].Select(x => x).ToArray<double>();
                        double[] corrFunc3 = CorrDoubleList[0].Select(x => x / 2.0d).ToArray<double>();

                        var answer = await MyDapServer.ServerSendCorrFuncResponse(
                            (byte)getCorrFuncRequest.FuncNumber,
                            (int)centralFreqKhz, (int)bandwidthKhz,
                            corrFunc0, corrFunc1, corrFunc2, corrFunc3);

                        if (settings.workMode == WorkMode.LoadSpectrum)
                        {
                            ReLoopLoadSpectrum();
                        }
                    }
                }
            }
            //semaphoreSlim.Release();
        }

        private async void MyDapServer_DeviceGainMessageResponseIsNeeded(int Response, DeviceGainMessage deviceGainMessage)
        {
            //set
            if (Response == 12)
            {
                switch (deviceGainMessage.Device)
                {
                    case DapDevice.Preselector:
                    case DapDevice.Receiver:
                        _GainSettings.SetDeviceGain(deviceGainMessage.Device, deviceGainMessage.EPO, deviceGainMessage.Gain);
                        yaml.YamlSave<GainSettings>(_GainSettings, "GainSettings.yaml");
                        break;
                    case DapDevice.Preamplifier:
                        PreamplifierState = deviceGainMessage.Gain;
                        await PreamplifierChangeCmd4();
                        break;
                }
                await MyDapServer.ServerAnswerSetDeviceGain(deviceGainMessage.Device, deviceGainMessage.EPO, deviceGainMessage.Gain);
            }
            //get
            if (Response == 13)
            {
                byte Gain = 0;
                switch (deviceGainMessage.Device)
                {
                    case DapDevice.Preselector:
                    case DapDevice.Receiver:
                        Gain = _GainSettings.GetDeviceGain(deviceGainMessage.Device, deviceGainMessage.EPO);
                        break;
                    case DapDevice.Preamplifier:
                        Gain = PreamplifierState;
                        break;
                }
                await MyDapServer.ServerAnswerGetDeviceGain(deviceGainMessage.Device, deviceGainMessage.EPO, Gain);
            }
        }

        private async void MyDapServer_FiltersResponseIsNeeded(int Response, FiltersMessage filtersMessage)
        {
            //set
            if (Response == 6)
            {
                if (filtersMessage != null)
                    switch (filtersMessage.Type)
                    {
                        case ThresholdType.RIThreshold:
                            Threshold = filtersMessage.Threshold;
                            var answer1 = await MyDapServer.ServerAnswerSetFilterMessage((short)Threshold, ThresholdType.RIThreshold);
                            break;
                        case ThresholdType.CorrThreshold:
                            CorrThreshold = filtersMessage.Threshold / 100f;
                            var answer2 = await MyDapServer.ServerAnswerSetFilterMessage((short)(CorrThreshold * 100), ThresholdType.CorrThreshold);
                            break;
                    }
            }
            //get
            if (Response == 7)
            {
                if (filtersMessage != null)
                    switch (filtersMessage.Type)
                    {
                        case ThresholdType.RIThreshold:
                            var answer1 = await MyDapServer.ServerAnswerSetFilterMessage((short)Threshold, ThresholdType.RIThreshold);
                            break;
                        case ThresholdType.CorrThreshold:
                            var answer2 = await MyDapServer.ServerAnswerSetFilterMessage((short)(CorrThreshold * 100), ThresholdType.CorrThreshold);
                            break;
                    }
            }
        }


        private async Task GenerateModeChange(int extraCode)
        {
            if (extraCode == 1)
            {
                Mode = extraCode;
            }
            else
            {
                Mode = 0;
                if (MainTask != null)
                    await MainTask;
            }
        }
        private async void GenerateResponse1(int extraCode)
        {
            if (extraCode == 1)
            {
                Mode = extraCode;
            }
            else
            {
                await StopLocal();

                Mode = 0;
                if (MainTask != null)
                    await MainTask;
            }

            DapServerMode mode = (DapServerMode)Enum.GetValues(typeof(DapServerMode)).GetValue((byte)Mode);
            var answer = await MyDapServer.ServerSendModeMessage(mode);
        }

        private async Task EndRI()
        {
            await StopLocal();

            Mode = 0;
            if (MainTask != null)
                await MainTask;

            DapServerMode mode = (DapServerMode)Enum.GetValues(typeof(DapServerMode)).GetValue((byte)Mode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(mode);
        }

        private async Task ReStartRI()
        {
            Mode = 0;
            if (MainTask != null)
                await MainTask;

            DapServerMode mode = (DapServerMode)Enum.GetValues(typeof(DapServerMode)).GetValue((byte)Mode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(mode);

            Mode = 1;
            mode = (DapServerMode)Enum.GetValues(typeof(DapServerMode)).GetValue((byte)Mode);
            answer = await MyDapServer.ServerSendExtraordinaryModeMessage(mode);
        }

        private async void GenerateResponse2(int extraCode)
        {
            //Set Freq from extraCode

            var answer = await MyDapServer.ServerSendFrequencyMessage();
        }

        private async void GenerateResponse3(int extraCode)
        {
            float[] spectrum = new float[0];
            switch (extraCode)
            {
                case 0:
                    spectrum = lastSavedSpectrum0;
                    break;
                case 1:
                    spectrum = lastSavedSpectrum1;
                    break;
                case 2:
                    // spectrum = lastSavedSpectrum2;
                    break;
                case 3:
                    // spectrum = lastSavedSpectrum3;
                    break;
                case 4:
                    //  spectrum = lastSavedSpectrum4;
                    break;
                case 5:
                    spectrum = MaxUnionPrevandNext(1, 0, lastSavedSpectrum0, lastSavedSpectrum1);
                    break;
                case 6:
                    //  var tempresult = MaxUnionPrevandNext(3, 2, lastSavedSpectrum2, lastSavedSpectrum3);
                    //  spectrum = MaxUnionPrevandNext(4, 3, tempresult, lastSavedSpectrum4);
                    break;
            }

            //var answer = await MyDapServer.ServerSendSpectrumResponse((byte)extraCode, spectrum);
        }

        private float willy = 7.8125f;

        private float[] MaxUnionPrevandNext(int indexNext, int indexPrev, float[] ch1, float[] ch2)
        {
            DspDataModel.FrequencyRange[] frs = new DspDataModel.FrequencyRange[5];

            frs[0] = new DspDataModel.FrequencyRange(2400f, 2462.5f);
            frs[1] = new DspDataModel.FrequencyRange(2437.5f, 2500f);
            frs[2] = new DspDataModel.FrequencyRange(5720f, 5782.5f);
            frs[3] = new DspDataModel.FrequencyRange(5760f, 5822.5f);
            frs[4] = new DspDataModel.FrequencyRange(5807f, 5870.5f);

            int pointNumber = Math.Abs((int)((frs[indexNext].StartFrequencyKhz * 1000f - frs[indexPrev].EndFrequencyKhz * 1000f) / willy));

            float[] result = new float[ch1.Count() - pointNumber + pointNumber + ch2.Count() - pointNumber];

            for (int i = 0; i < ch1.Count() - pointNumber; i++)
            {
                result[i] = ch1[i];
            }

            for (int i = ch1.Count() - pointNumber, j = 0; i < ch1.Count(); i++, j++)
            {
                result[i] = Math.Max(ch1[i], ch2[j]);
            }

            for (int i = result.Count() - (ch2.Count() - pointNumber), j = pointNumber; j < ch2.Count(); i++, j++)
            {
                result[i] = ch2[j];
            }

            return result;
        }

        private async void GenerateResponse4(int extraCode)
        {
            var CorrDoubleList = CorrelationParser.CorrParser.GetCorrFunc(extraCode);

            double[] corrFunc0 = CorrDoubleList[0].Select(x => x).ToArray<double>();
            double[] corrFunc1 = CorrDoubleList[1].Select(x => x).ToArray<double>();
            double[] corrFunc2 = CorrDoubleList[2].Select(x => x).ToArray<double>();
            double[] corrFunc3 = CorrDoubleList[0].Select(x => x / 2.0d).ToArray<double>();

            var answer = await MyDapServer.ServerSendCorrFuncResponse((byte)extraCode, 0, 0, corrFunc0, corrFunc1, corrFunc2, corrFunc3);
        }

        private async void GenerateResponse5()
        {
            var mode = DAPprotocols.DapServerMode.Stop;
            switch (Mode)
            {
                case 0:
                    mode = DAPprotocols.DapServerMode.Stop;
                    break;
                case 1:
                    mode = DAPprotocols.DapServerMode.RadioIntelligence;
                    break;
                default:
                    mode = DAPprotocols.DapServerMode.Stop;
                    break;
            }
            var answer = await MyDapServer.ServerAnswerModeMessage(mode);
        }

        private async void GenerateResponse6(int extracode)
        {
            //Threshold = (short)extracode;
            //Console.WriteLine(Threshold);
            //var answer = await MyDapServer.ServerAnswerSetFilterMessage((short)Threshold);
        }

        private async void GenerateResponse7()
        {
            //var answer = await MyDapServer.ServerAnswerFiltersMessage((short)Threshold);
        }

        private async void GenerateResponse8(int extracode)
        {
            if (Convert.ToBoolean(extracode) == false)
            {
                ctsLocalSpectrum.Cancel();
                if (LocalSpectrumTask != null)
                    await LocalSpectrumTask;

                ctsLocalCorrFunc.Cancel();
                if (LocalCorrFuncTask != null)
                    await LocalCorrFuncTask;

                ApplyFilter = Convert.ToBoolean(extracode);

                _taskAddEvent.Set();
            }

            _AsyncAutoResetEvent4LocalSpectrum = new AsyncAutoResetEvent();
            _AsyncAutoResetEvent4LocalCorr = new AsyncAutoResetEvent();

            ApplyFilter = Convert.ToBoolean(extracode);

            var answer = await MyDapServer.ServerAnswerAppleExFilterMessage(ApplyFilter);
        }

        private async Task StopLocal()
        {
            ctsLocalSpectrum.Cancel();
            if (LocalSpectrumTask != null)
                await LocalSpectrumTask;

            ctsLocalCorrFunc.Cancel();
            if (LocalCorrFuncTask != null)
                await LocalCorrFuncTask;

            ApplyFilter = false;

            _taskAddEvent.Set();

            _AsyncAutoResetEvent4LocalSpectrum = new AsyncAutoResetEvent();
            _AsyncAutoResetEvent4LocalCorr = new AsyncAutoResetEvent();

            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.Stop);
        }

        private async void GenerateResponse9(int extraCode)
        {
            StartStopEventArgs answerSetStartStop = await AttemptCountCmdUdp<StartStopEventArgs>(6, (byte)(extraCode), 0);
            if (answerSetStartStop == null)
            {
                ConsoleLog(false, "UDP UDPSetStartStop Error!");
            }
            RecordStartStop = Convert.ToBoolean(extraCode);
            try
            {
                var answer = await MyDapServer.ServerAnswerRecordStartStopMessage(RecordStartStop);
            }
            catch { }
        }

        public bool SpectrumEmulatorIsReady = false;

        private void Emulator_OnCheckLoadToList(object sender, bool e)
        {
            //Console.WriteLine(e);
            SpectrumEmulatorIsReady = e;
        }

        private bool ComIsOpen = false;
        private void Optc1_Click(object sender, RoutedEventArgs e)
        {
            if (ComIsOpen == false)
            {
                var statusport = port.OpenPort("COM" + TB1ComConnection.Text, (Port.BaudRate)115200);
                if (statusport)
                {
                    TB1ComConnection.Background = new SolidColorBrush(Colors.Green);
                    ComIsOpen = true;
                }
            }
            else
            {
                var statusport = port.ClosePort();
                if (statusport == false)
                {
                    TB1ComConnection.Background = new SolidColorBrush(Colors.Red);
                    ComIsOpen = false;
                }
            }
        }

        private void Optc2_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Optc3_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Optc4_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Optc5_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Optc6_Click(object sender, RoutedEventArgs e)
        {

        }

        private bool UDPIsOpen = false;
        private void OptcDt1_Click(object sender, RoutedEventArgs e)
        {
            if (UDPIsOpen == false)
            {
                //uDP = new UDPClass.UDP(IPAddress.Parse("192.168.1.104"), 10000, IPAddress.Parse(TB2IP.Text), 10001, 0, 1, Mod.RealTimeWork);
                //uDP = new UDPClass.UDP(IPAddress.Parse("192.168.1.1"), 25007, IPAddress.Parse("192.168.1.20"), 25005, 0, 1, Mod.RealTimeWork);
                //uDP = new UDP(IPAddress.Parse(myIP.Text), Convert.ToInt32(myPort.Text), IPAddress.Parse(remIP.Text), Convert.ToInt32(remPort.Text), 0, 0, Mod.RealTimeWork, ReceiveMod.Receiver);

                InitOptcDtEvents();

                uDP.Connect();
                UDPIsOpen = true;
            }
            else
            {
                uDP.Disconnect();
                UDPIsOpen = false;
            }
        }

        private async void OptcDt2_Click(object sender, RoutedEventArgs e)
        {
            GetStateEventArgs a = await uDP.GetState();
        }

        private async void OptcDt3_Click(object sender, RoutedEventArgs e)
        {
            //SetFreqEventArgs a = await uDP.SetFreq(Convert.ToInt32(myFreq.Text), Convert.ToByte(myFilter.Text), Convert.ToByte(myGain.Text));
            SetFreqEventArgs a = await uDP.SetFreq(Convert.ToInt32(myFreq.Text), Convert.ToByte(myGain.Text));
        }

        private async void OptcDt4_Click(object sender, RoutedEventArgs e)
        {
            var answer = await uDP.GetSpectrum(0);

            PlotABit(Convert.ToDouble(tbStartFreq.Text), Convert.ToDouble(tbEndFreq.Text), answer.Ampl.ToArray());
        }

        private async void OptcDt5_Click(object sender, RoutedEventArgs e)
        {
            var answer = await uDP.GetCorrelationFunc();
        }

        private async void OptcDt6_Click(object sender, RoutedEventArgs e)
        {
            //var answer = await uDP.SetParam(Convert.ToByte(myFilter.Text), Convert.ToInt32(myOffset.Text), Convert.ToInt32(myAlpha1.Text), Convert.ToInt32(myAlpha2.Text));
            var answer = await uDP.SetParam(
                Convert.ToInt32(myOffset.Text),
                Convert.ToByte(myFilter.Text),
                Convert.ToInt32(myAlpha1.Text),
                Convert.ToInt32(myAlpha2.Text));

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button0_Click(object sender, RoutedEventArgs e)
        {
            port.GetStatePreselectorCode0();
        }
        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            port.SetFreqCode1(35, Port.PreselectorNumber.AllPreselector);
        }
        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            port.SetGainCode2(15, Port.PreselectorNumber.AllPreselector);
        }
        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            port.SetPreampCode3(Port.ModePreamp.AllRange, Port.PreselectorNumber.AllPreselector);
        }
        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            port.SetOnOffCode4(Port.FlagOnOff.ON, Port.PreselectorNumber.AllPreselector);
        }
        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            port.SetPreselectorSettingCode5(Port.SetPreampRange.Range120_210MHz, Port.PreselectorNumber.AllPreselector);
        }
        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            port.SetFreqGainCode6(35, 15, Port.PreselectorNumber.AllPreselector);
        }
        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            port.SetOperationModCode7(Port.ModePreamp.AllRange, Port.PreselectorNumber.AllPreselector);
        }
        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            port.SetLinkCode8(Port.RFChannel.AllRFChannelsOFF, 0);
        }
        private void Button9_Click(object sender, RoutedEventArgs e)
        {
            port.SetAttenuatorLevelCode9(31, Port.PreselectorNumber.AllPreselector);
        }
        private void Button0A_Click(object sender, RoutedEventArgs e)
        {
            port.SetAttenuatorLowLevelCodeA(31, Port.PreselectorNumber.AllPreselector);
        }
        private void Button0B_Click(object sender, RoutedEventArgs e)
        {
            port.SetAttenuatorHighLevelCodeB(31, Port.PreselectorNumber.AllPreselector);
        }
        private void Button0F_Click(object sender, RoutedEventArgs e)
        {
            port.SetPowerCodeF(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
        }
        private void Button10_Click(object sender, RoutedEventArgs e)
        {
            port.SetOpticalPowerCode10(Port.OnOffSwitch.ON, Port.PreselectorNumber.AllPreselector);
        }
        private void Button11_Click(object sender, RoutedEventArgs e)
        {
            port.SetGainOperationModeCode11(Port.ModePreamp.AllRange, Port.PreselectorNumber.AllPreselector);
        }
        private void Button12_Click(object sender, RoutedEventArgs e)
        {
            port.SetExternalSwitchCode12("41322314", Port.PreselectorNumber.AllPreselector);
        }
        private void Button13_Click(object sender, RoutedEventArgs e)
        {
            port.SetSaveCode13();
        }
        private void Button14_Click(object sender, RoutedEventArgs e)
        {
            port.ResetCode14();
        }

        private void Button_Mode_0_Click(object sender, RoutedEventArgs e)
        {
            Mode = 0;
        }

        private void Button_Mode_1_Click(object sender, RoutedEventArgs e)
        {
            Mode = 1;
        }

        private void LoadForm(Settings innerSettings)
        {
            CorrThreshold = innerSettings.CorrThreshold;

            d1.Text = innerSettings.Delay1ms.ToString();
            d2.Text = innerSettings.Delay2ms.ToString();

            myReceiverDelay.Text = innerSettings.ReceiverDelay.ToString();
            myReceiverDelaySolo.Text = innerSettings.ReceiverDelaySolo.ToString();

            mytbAlpha1.Text = innerSettings.Alpha1.ToString();
            mytbAlpha2.Text = innerSettings.Alpha2.ToString();

            myPreSelValue.Text = innerSettings.PreSelValue.ToString();

            ReceiverDelay = innerSettings.ReceiverDelay;
            ReceiverDelaySolo = innerSettings.ReceiverDelaySolo;

            Alpha1 = innerSettings.Alpha1;
            Alpha2 = innerSettings.Alpha2;

            PreSelValue = innerSettings.PreSelValue;

            myFilterMinBandWidthkHz.Text = innerSettings.FilterMinBandWidthkHz.ToString();
            myFilterMaxBandWidthkHz.Text = innerSettings.FilterMaxBandWidthkHz.ToString();

            FilterMinBandWidthkHz = innerSettings.FilterMinBandWidthkHz;
            FilterMaxBandWidthkHz = innerSettings.FilterMaxBandWidthkHz;

            DefaultChannel = innerSettings.DefaultChannel;
            ChanelNumber = (int)DefaultChannel;

            myChanelNumber.Text = ChanelNumber.ToString();

            PreselCmdDelay = innerSettings.PreselCmdDelay;
            PreselCmdSetDelay = innerSettings.PreselCmdSetDelay;
            GetAutoCorrFuncDelay = innerSettings.GetAutoCorrFuncDelay;
        }


        private Channel _DefaultChannel;
        public Channel DefaultChannel
        {
            get => _DefaultChannel;
            set
            {
                if (_DefaultChannel == value)
                    return;
                _DefaultChannel = value;
            }
        }

        private int _PreselCmdDelay;
        public int PreselCmdDelay
        {
            get => _PreselCmdDelay;
            set
            {
                if (_PreselCmdDelay == value)
                    return;
                _PreselCmdDelay = value;
            }
        }

        public int PreselCmdSetDelay { get; set; }
        public int GetAutoCorrFuncDelay { get; set; }

        private void apply_Click(object sender, RoutedEventArgs e)
        {
            settings.Delay1ms = Convert.ToInt32(d1.Text);
            settings.Delay2ms = Convert.ToInt32(d2.Text);

            settings.ReceiverDelay = Convert.ToInt32(myReceiverDelay.Text);
            settings.ReceiverDelaySolo = Convert.ToInt32(myReceiverDelaySolo.Text);

            ReceiverDelay = Convert.ToInt32(myReceiverDelay.Text);
            ReceiverDelaySolo = Convert.ToInt32(myReceiverDelaySolo.Text);

            settings.Alpha1 = Convert.ToInt32(mytbAlpha1.Text);
            settings.Alpha2 = Convert.ToInt32(mytbAlpha2.Text);

            Alpha1 = Convert.ToInt32(mytbAlpha1.Text);
            Alpha2 = Convert.ToInt32(mytbAlpha2.Text);

            ChanelNumber = Convert.ToInt32(myChanelNumber.Text);

            PreSelValue = Convert.ToInt32(myPreSelValue.Text);

            settings.PreSelValue = Convert.ToInt32(myPreSelValue.Text);

            settings.FilterMinBandWidthkHz = Convert.ToSingle(myFilterMinBandWidthkHz.Text);
            settings.FilterMaxBandWidthkHz = Convert.ToSingle(myFilterMaxBandWidthkHz.Text);

            FilterMinBandWidthkHz = Convert.ToSingle(myFilterMinBandWidthkHz.Text);
            FilterMaxBandWidthkHz = Convert.ToSingle(myFilterMaxBandWidthkHz.Text);

            yaml.YamlSave<Settings>(settings, "Settings.yaml");
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            yaml.YamlSave<Settings>(settings, "Settings.yaml");
            yaml.YamlSave<GainSettings>(_GainSettings, "GainSettings.yaml");
        }

        private void cbRecordStartStop_Click(object sender, RoutedEventArgs e)
        {
            RecordStartStop = cbRecordStartStop.IsChecked.Value;
        }


    }
}
