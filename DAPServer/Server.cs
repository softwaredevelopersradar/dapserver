﻿using DspDataModel.DataProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;

using UDP_Cuirasse;

using DspDataModel;
using WpfMapRastr;
using WorkPortNew;
using YamlTestCorrFunc;
using YamlReverseExpertise;
using Nito.AsyncEx;

namespace DAPServer
{
    public partial class MainWindow
    {
        CancellationTokenSource cts = new CancellationTokenSource();
        CancellationTokenSource ctsMain = new CancellationTokenSource();
        CancellationTokenSource ctsTable = new CancellationTokenSource();
        CancellationTokenSource ctsTest = new CancellationTokenSource();

        private int _Mode = 0;
        public int Mode
        {
            get { return _Mode; }
            set
            {
                if (_Mode != value)
                {
                    _Mode = value;
                    if (value == 0)
                    {
                        cts.Cancel();

                        ctsMain.Cancel();

                        ctsTable.Cancel();

                        ctsTest.Cancel();

                        //_ = StopLocal();
                    }
                    if (value == 1)
                    {
                        DispatchIfNecessary(() =>
                        {
                            settings.workMode = (WorkMode)Enum.GetValues(typeof(WorkMode)).GetValue((byte)CBworkMode.SelectedIndex);
                        });
                        if (settings.workMode == WorkMode.LoadSpectrum)
                        {
                            cts = new CancellationTokenSource();
                            CancellationToken token = cts.Token;
                            //Task.Run(() => LoadSpectrumLoop(token));
                            LoadSpectrumTask = LoadSpectrumLoop(token);
                        }
                        if (settings.workMode == WorkMode.Emu)
                        {
                            cts = new CancellationTokenSource();
                            CancellationToken token = cts.Token;
                            Task.Run(() => EmuLoop(token));
                            Task.Run(() => EmuCorrFuncLoop(token));
                        }
                        if (settings.workMode == WorkMode.Work)
                        {
                            cts = new CancellationTokenSource();
                            CancellationToken token = cts.Token;
                            //Task.Run(() => Loop(token));
                            //Task.Run(() => Looop(token));
                            Task.Run(() => SpectrumLooop(token));
                        }
                        if (settings.workMode == WorkMode.Main)
                        {
                            ctsMain = new CancellationTokenSource();
                            CancellationToken tokenMain = ctsMain.Token;
                            //Task.Run(() => MainLoop(tokenMain));
                            MainTask = MainLoop(tokenMain);
                        }
                        if (settings.workMode == WorkMode.Table)
                        {
                            ctsTable = new CancellationTokenSource();
                            CancellationToken token = ctsTable.Token;
                            Task.Run(() => Loop1(token));
                            Task.Run(() => Loop2(token));
                            Task.Run(() => PointToARM(token));
                        }
                        if (settings.workMode == WorkMode.TableAero)
                        {
                            ctsTable = new CancellationTokenSource();
                            CancellationToken token = ctsTable.Token;

                            Task.Run(() => Loop1Aero(token));
                            Task.Run(() => Loop2Aero(token));
                        }
                        if (settings.workMode == WorkMode.TableMix)
                        {
                            ctsTable = new CancellationTokenSource();
                            CancellationToken token = ctsTable.Token;

                            Task.Run(() => Loop1(token));
                            Task.Run(() => Loop2(token));
                            Task.Run(() => PointToARM(token));

                            Task.Run(() => Loop1Aero(token));
                            Task.Run(() => Loop2Aero(token));
                        }
                        if (settings.workMode == WorkMode.FileLenaTest)
                        {
                            CancellationTokenSource ctsTest = new CancellationTokenSource();
                            CancellationToken tokenTest = ctsTest.Token;

                            Task.Run(() => LenaTestLoop(tokenTest));
                        }
                    }
                }
            }
        }

        private async Task PointToARM(CancellationToken token)
        {
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested) return;

                var resultMark = GeneratePosition();

                //Отправка точки в АРМ
                var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                await Task.Delay(100);
            }
        }
        private (double Latitude, double Longitude, float Altitude) GeneratePosition()
        {
            Random random = new Random();

            double Latitude = settings.Latitude + RandomSign() * random.NextDouble() / settings.RandomCoordsCoef;
            double Longitude = settings.Longitude + RandomSign() * random.NextDouble() / settings.RandomCoordsCoef;
            float Altitude = (float)(50f) + (float)RandomSign();
                
            return (Latitude, Longitude, Altitude);

            double RandomSign()
            {
                return random.NextDouble() <= 0.5 ? -1 : 1;
            }
        }

        //double[] MinFreqs = new double[] { 2385d, 5697.5d };
        //double[] MaxFreqs = new double[] { 2510d, 5885 };

        double[] MinFreqs = new double[] { 2385d };
        double[] MaxFreqs = new double[] { 2447, 5d };

        double[] KnownMinFreqs = new double[] { 90d, 100d };
        double[] KnownMaxFreqs = new double[] { 120d, 140d };

        float[] lastSavedSpectrum0;
        float[] lastSavedSpectrum1;
        //float[] lastSavedSpectrum2;
        //float[] lastSavedSpectrum3;
        //float[] lastSavedSpectrum4;



        private void UpdateRROrKnownMinMaxFreqs(NameTable nameTable)
        {
            List<double> ltempMinFreqs = new List<double>();
            List<double> ltempMaxFreqs = new List<double>();
            switch (nameTable)
            {
                case NameTable.TableFreqRangesRecon:
                    //Инициализации диапазонов Радиоразведки
                    if (lTablesFreqRangesRecon.Count() > 0)
                    {
                        for (int i = 0; i < lTablesFreqRangesRecon.Count(); i++)
                        {
                            if (lTablesFreqRangesRecon[i].IsActive == true)
                            {
                                ltempMinFreqs.Add(lTablesFreqRangesRecon[i].FreqMinKHz / 1000d);
                                ltempMaxFreqs.Add(lTablesFreqRangesRecon[i].FreqMaxKHz / 1000d);
                            }
                        }
                        MinFreqs = ltempMinFreqs.ToArray();
                        MaxFreqs = ltempMaxFreqs.ToArray();

                        var Row = DoIt(MinFreqs, MaxFreqs);
                        Row.Sort();
                        GlobalRow = new List<int>(Row.Distinct().ToList());

                        if (GlobalRow.Count > 0)
                        {
                            _AsyncAutoResetEvent4GlobalRow.Set();
                        }
                    }
                    break;
                case NameTable.TableFreqKnown:
                    //Инициализации известных частот
                    if (lTablesFreqKnowns.Count() > 0)
                    {
                        for (int i = 0; i < lTablesFreqKnowns.Count(); i++)
                        {
                            if (lTablesFreqKnowns[i].IsActive == true)
                            {
                                ltempMinFreqs.Add(lTablesFreqKnowns[i].FreqMinKHz / 1000d);
                                ltempMaxFreqs.Add(lTablesFreqKnowns[i].FreqMaxKHz / 1000d);
                            }
                        }
                        KnownMinFreqs = ltempMinFreqs.ToArray();
                        KnownMaxFreqs = ltempMaxFreqs.ToArray();
                    }
                    break;
            }
        }

        private bool isFRSonListKnown(double Freq)
        {
            double[] localKnownMinFreqs;
            lock (KnownMinFreqs)
            {
                localKnownMinFreqs = new double[KnownMinFreqs.Count()];
                KnownMinFreqs.CopyTo(localKnownMinFreqs, 0);
            }
         
            double[] localKnownMaxFreqs;
            lock (KnownMaxFreqs)
            {
                localKnownMaxFreqs = new double[KnownMaxFreqs.Count()];
                KnownMaxFreqs.CopyTo(localKnownMaxFreqs, 0);
            }

            int Count = Math.Min(localKnownMinFreqs.Count(), localKnownMaxFreqs.Count());
            for (int i = 0; i < Count; i++)
            {
                if (Freq >= localKnownMinFreqs[i] && Freq <= localKnownMaxFreqs[i])
                    return false;
            }
            return true;
        }

        private bool FRSonRangeRI(double CenterFreq)
        {
            double[] localMinFreqs;
            lock (MinFreqs)
            {
                localMinFreqs = new double[MinFreqs.Count()];
                MinFreqs.CopyTo(localMinFreqs, 0);
            }

            double[] localMaxFreqs;
            lock (MaxFreqs)
            {
                localMaxFreqs = new double[MaxFreqs.Count()];
                MaxFreqs.CopyTo(localMaxFreqs, 0);
            }

            int Count = Math.Min(localMinFreqs.Count(), localMaxFreqs.Count());
            for (int i = 0; i < Count; i++)
            {
                if (CenterFreq >= localMinFreqs[i] && CenterFreq <= localMaxFreqs[i])
                    return true;
            }
            return false;
        }

        Random r = new Random();
        private bool isNew()
        {
            return Convert.ToBoolean(r.Next(0, 2));
        }

        //DataProcessor.DataProcessor dataProcessor0 = new DataProcessor.DataProcessor(config);
        //DataProcessor.DataProcessor dataProcessor1 = new DataProcessor.DataProcessor(config);
        //DataProcessor.DataProcessor dataProcessor2 = new DataProcessor.DataProcessor(config);
        //DataProcessor.DataProcessor dataProcessor3 = new DataProcessor.DataProcessor(config);
        //DataProcessor.DataProcessor dataProcessor4 = new DataProcessor.DataProcessor(config);

        DataProcessor.DataProcessor dataProcessor0;
        //DataProcessor.DataProcessor dataProcessor1;
        //DataProcessor.DataProcessor dataProcessor2;
        //DataProcessor.DataProcessor dataProcessor3;
        //DataProcessor.DataProcessor dataProcessor4;


        private short _Threshold = -80;
        public short Threshold
        {
            get { return _Threshold; }
            set
            {
                if (_Threshold != value)
                {
                    _Threshold = value;
                }
            }
        }

        bool _ApplyFilter = false;
        public bool ApplyFilter
        {
            get { return _ApplyFilter; }
            set
            {
                if (_ApplyFilter != value)
                {
                    _ApplyFilter = value;
                }
            }
        }


        DateTime TimeStart;
        private bool _RecordStartStop = false;
        public bool RecordStartStop
        {
            get { return _RecordStartStop; }
            set
            {
                if (_RecordStartStop != value)
                {
                    if (value)
                    {
                        TimeStart = DateTime.Now;
                        FileName = $"{TimeStart.Day.ToString("00")}-{TimeStart.Month.ToString("00")}-{TimeStart.Year} {TimeStart.Hour.ToString("00")}_{TimeStart.Minute.ToString("00")}_{TimeStart.Second.ToString("00")}";
                        _RecordStartStop = value;
                    }
                    else
                    {
                        _RecordStartStop = value;
                        WriteAllRecords();
                        WriteAllExpertises();
                    }
                }
            }
        }

        List<Expertise> expertises = new List<Expertise>();
        private void WriteAllExpertises()
        {
            ReverseExpertise reverseExpertise = new ReverseExpertise(expertises);

            string nameDotYaml = $"ReverseExpertise {TimeStart.Hour.ToString("00")}-{TimeStart.Minute.ToString("00")}-{TimeStart.Second.ToString("00")}.yaml";

            yaml.YamlSavePro<ReverseExpertise>(reverseExpertise, nameDotYaml);

            expertises.Clear();
        }

        Task LoadSpectrumTask;
        private void ReLoopLoadSpectrum()
        {
            //cts.Cancel();
            cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;
            LoadSpectrumTask = LoadSpectrumLoop(token);
        }

        Task MainTask;
        private void MainReLoopIfNeeded()
        {
            if (Mode == 1 && settings.workMode == WorkMode.Main)
            {
                //cts.Cancel();
                ctsMain = new CancellationTokenSource();
                CancellationToken tokenMain = ctsMain.Token;
                //Task.Run(() => MainLoop(token));
                MainTask = MainLoop(tokenMain);
            }
        }


        List<int> GlobalRow = new List<int>();


        private byte BandWidthCodeFromBandWidthkHz(double BandWidthkHz)
        {
            if (BandWidthkHz / 1000 < 1d)
            {
                return 1;
            }
            else
            {
                return (byte)((int)(BandWidthkHz / 1000));
            }
        }


        private void SetPreselFreqGain(int Version, int Freq, byte Gain)
        {
            switch (Version)
            {
                case 0:
                    SetPreselFreqGainOld(Freq, Gain);
                    break;
                case 1:
                    SetPreselFreqGainNew((short)Freq, Gain);
                    break;
            }
        }

        private void SetPreselFreqGainOld(int Freq, byte Gain)
        {
            //Настройка преселектора на частоту, команда 01
            port1.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            port2.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            port3.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);
            port4.SetFreqCode1((short)Freq, WorkPort.Port.PreselectorNumber.AllPreselector);

            //Настройка усиления преселектора
            port1.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            port2.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            port3.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            port4.SetGainCode2(Gain, WorkPort.Port.PreselectorNumber.AllPreselector);

            //или сразу установка частоты-усиления
            {
                //port1.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
                //port2.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
                //port3.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
                //port4.SetFreqGainCode6((short)Freq, Gain, WorkPort.Port.PreselectorNumber.AllPreselector);
            }
        }
        private void SetPreselFreqGainNew(short Freq, byte Gain)
        {
            //Установка частоты
            //Команда 1
            portNew1.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);
            portNew2.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);
            portNew3.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);
            portNew4.SetFreqCode1(Freq, PortNew.PreselectorNumber.AllPreselector);

            //Установка усиления
            //Команда 2
            portNew1.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
            portNew2.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
            portNew3.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
            portNew4.SetGainCode2_3b(31, PortNew.PreselectorNumber.AllPreselector);
        }

        private List<List<double>> OrthoNormalization(GetCorrelationFuncEventArgs answerCor)
        {
            //Ортонормировать корреляционные функции
            List<List<double>> CorrDoubleList = new List<List<double>>();

            double OrthoValue1 = Math.Sqrt(answerCor.Corr_1.sCorr * answerCor.Corr_1.eCorr);
            List<double> OrthoList1 = answerCor.Corr_1.Corr.Select(x => x / OrthoValue1).ToList<double>();
            CorrDoubleList.Add(OrthoList1);

            double OrthoValue2 = Math.Sqrt(answerCor.Corr_2.sCorr * answerCor.Corr_2.eCorr);
            List<double> OrthoList2 = answerCor.Corr_2.Corr.Select(x => x / OrthoValue2).ToList<double>();
            CorrDoubleList.Add(OrthoList2);

            double OrthoValue3 = Math.Sqrt(answerCor.Corr_3.sCorr * answerCor.Corr_3.eCorr);
            List<double> OrthoList3 = answerCor.Corr_3.Corr.Select(x => x / OrthoValue3).ToList<double>();
            CorrDoubleList.Add(OrthoList3);

            double OrthoValue4 = Math.Sqrt(answerCor.Corr_4.sCorr * answerCor.Corr_4.eCorr);
            List<double> OrthoList4 = answerCor.Corr_4.Corr.Select(x => x / OrthoValue4).ToList<double>();
            CorrDoubleList.Add(OrthoList4);

            return CorrDoubleList;
        }

        private (double[] corrFunc0, double[] corrFunc1, double[] corrFunc2, double [] corrFunc3) OrthoNormalizationToDoubleTuple(GetCorrelationFuncEventArgs answerCor)
        {
            //Ортонормировать корреляционные функции

            double OrthoValue1 = Math.Sqrt(answerCor.Corr_1.sCorr * answerCor.Corr_1.eCorr);
            double[] corrFunc0 = answerCor.Corr_1.Corr.Select(x => x / OrthoValue1).ToArray<double>();

            double OrthoValue2 = Math.Sqrt(answerCor.Corr_2.sCorr * answerCor.Corr_2.eCorr);
            double[] corrFunc1 = answerCor.Corr_2.Corr.Select(x => x / OrthoValue2).ToArray<double>();

            double OrthoValue3 = Math.Sqrt(answerCor.Corr_3.sCorr * answerCor.Corr_3.eCorr);
            double[] corrFunc2 = answerCor.Corr_3.Corr.Select(x => x / OrthoValue3).ToArray<double>();

            double OrthoValue4 = Math.Sqrt(answerCor.Corr_4.sCorr * answerCor.Corr_4.eCorr);
            double[] corrFunc3 = answerCor.Corr_4.Corr.Select(x => x / OrthoValue4).ToArray<double>();

            return (corrFunc0, corrFunc1, corrFunc2, corrFunc3);
        }

        Dictionary<ISignal, DateTime> SignalTime = new Dictionary<ISignal, DateTime>();

        private int _Alpha1 = 999000;
        public int Alpha1
        {
            get { return _Alpha1; }
            set
            {
                if (_Alpha1 != value)
                {
                    _Alpha1 = value;
                }
            }
        }

        private int _Alpha2 = 0;
        public int Alpha2
        {
            get { return _Alpha2; }
            set
            {
                if (_Alpha2 != value)
                {
                    _Alpha2 = value;
                }
            }
        }

        private int _ReceiverDelaySolo = 100;
        public int ReceiverDelaySolo
        {
            get { return _ReceiverDelaySolo; }
            set
            {
                if (_ReceiverDelaySolo != value)
                {
                    _ReceiverDelaySolo = value;
                }
            }
        }

        private int _ReceiverDelay = 300;
        public int ReceiverDelay
        {
            get { return _ReceiverDelay; }
            set
            {
                if (_ReceiverDelay != value)
                {
                    _ReceiverDelay = value;
                }
            }
        }

        private float _FilterMinBandWidthkHz = 4000;
        public float FilterMinBandWidthkHz
        {
            get => _FilterMinBandWidthkHz;
            set
            {
                if (_FilterMinBandWidthkHz == value)
                    return;
                _FilterMinBandWidthkHz = value;
            }
        }

        private float _FilterMaxBandWidthkHz = 20000;
        public float FilterMaxBandWidthkHz
        {
            get => _FilterMaxBandWidthkHz;
            set
            {
                if (_FilterMaxBandWidthkHz == value)
                    return;
                _FilterMaxBandWidthkHz = value;
            }
        }


        private double CurrentFreqkHz = 0;


        private int _ChanelNumber = 4;
        public int ChanelNumber
        {
            get { return _ChanelNumber; }
            set
            {
                if (_ChanelNumber != value)
                {
                    _ChanelNumber = value;
                }
            }
        }

        private int _PreSelValue = 30;
        public int PreSelValue
        {
            get { return _PreSelValue; }
            set
            {
                if (_PreSelValue != value)
                {
                    _PreSelValue = value;
                }
            }
        }

        private int _SaveIndex = 0;
        private async Task MainLoop0(CancellationToken token)
        {
            //Получаем список ЕПО где ведём Радиоразведку
            var Row = DoIt(MinFreqs, MaxFreqs);
            Row.Sort();
            GlobalRow = new List<int>(Row.Distinct().ToList());

            while (_Mode != 0)
            {
                for (int i = _SaveIndex; i < GlobalRow.Count; i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        Console.WriteLine("Операция прервана токеном");
                        return;
                    }

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * GlobalRow[i];

                    //Перестройка преселекторов старая
                    //SetPreselFreqGain(settings.PreselectorVersion, (int)centerFreqMHz, (byte)_PreSelValue);
                    //await Task.Delay(1);
                    //Перестройка преселекторов новая псевдо-паралельная
                    //await SetPreselFreqGainAsync(settings.PreselectorVersion, (int)centerFreqMHz, (byte)_PreSelValue);
                    //Перестройка преселекторов новая последовательная
                    await SetPreselFreqGainAsyncСonsistently(settings.PreselectorVersion, (int)centerFreqMHz, (byte)_PreSelValue);

                    //Настройка РПУ на текущую ЕПО по частоте
                    var answerSetFreq = await uDP.SetFreq((int)(centerFreqMHz * 1000), 30);

                    if (CurrentFreqkHz == centerFreqMHz * 1000)
                    {
                        await Task.Delay(_ReceiverDelaySolo);
                    }
                    else
                    {
                        await Task.Delay(_ReceiverDelay);
                        CurrentFreqkHz = centerFreqMHz * 1000;
                    }

                    {
                        //стопвотч
                        //System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
                        //stopWatch.Start();
                        //stopWatch.Stop();
                        //TimeSpan ts = stopWatch.Elapsed;
                        //string elapsedTime = String.Format("{0:00}.{1:000}", ts.Seconds, ts.Milliseconds);
                        //Console.WriteLine("RunTime " + elapsedTime);
                    }

                    //Чтение данных

                    //Для большого хранилища
                    List<GetSpectrumEventArgs> answersGetSpectrum = new List<GetSpectrumEventArgs>();
                    for (int ww = 0; ww < _GlobalN; ww++)
                    {
                        GetSpectrumEventArgs tempAnswerGetSpectrum = await uDP.GetSpectrum((byte)ww);
                        answersGetSpectrum.Add(tempAnswerGetSpectrum);
                    }

                    //var answerGetSpectrum = await uDP.GetSpectrum((byte)_ChanelNumber);
                    var answerGetSpectrum = answersGetSpectrum[_ChanelNumber];

                    if (answerGetSpectrum.Ampl.Count == 8001) answerGetSpectrum.Ampl.RemoveAt(0);

                    if (RecordStartStop)
                    {
                        DateTime Time = DateTime.Now;
                        int ShiftDelay = (int)(Math.Abs(Time.TimeOfDay.TotalMilliseconds - TimeStart.TimeOfDay.TotalMilliseconds));
                        SaveRecord(ShiftDelay, GlobalRow[i], answerGetSpectrum.Ampl.ToArray());
                    }

                    //Массив для ужатия спектра
                    //float[] vs1 = answerGetSpectrum.Ampl.ToArray();
                    //Массив для поиска частот
                    lastSavedSpectrum0 = answerGetSpectrum.Ampl.ToArray();

                    //Тестовоя отрисовка спектра
                    DispatchIfNecessary(() =>
                       {
                           PlotABit(lastSavedSpectrum0);
                       });

                    ////Ужатие Спектра от Феди
                    //var segment = new ArraySegment<float>(vs1, 0, vs1.Count());
                    //var spectrum = segment.StrechSpectrum(_GlobalDotsPerBandCount);
                    //vs1 = spectrum.Amplitudes;

                    //Обновляем хранилище спектра
                    //ListSpectrumStorage[Row[i]] = vs1.ToList();
                    ListSpectrumStorage[Row[i]] = new List<float>(answerGetSpectrum.Ampl);

                    //Для большого хранилища
                    for (int ww = 0; ww < _GlobalN; ww++)
                    {
                        BigListSpectrumStorage[ww][Row[i]] = new List<float>(answersGetSpectrum[ww].Ampl);
                    }

                    //Обнаружение ИРИ от Феди
                    DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(lastSavedSpectrum0, GlobalRow[i], DateTime.Now, 0);

                    //Параметр для поиска сигналов по порогу
                    ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(_Threshold);

                    //Результаты сигналов
                    var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

                    //Ассоциация сигнал-время
                    if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
                    {
                        //Console.WriteLine(result0.Signals.Count);
                        SignalTime.Clear();
                        for (int qq = 0; qq < result0.Signals.Count; qq++)
                        {
                            SignalTime.Add(result0.Signals[qq], DateTime.Now);
                        }
                    }

                    //Исполнительный фильт
                    if (ApplyFilter && FilterMinFrequencyMHz != -1 && FilterMaxFrequencyMHz != -1)
                    {
                        var indexFilter = ConvertValueToIndexes(new double[] { FilterMinFrequencyMHz, FilterMaxFrequencyMHz });
                        if (indexFilter[0]== GlobalRow[i] && indexFilter[0] == indexFilter[1])
                        {
                            double FilterCenterFrequencyMHz = Math.Min(FilterMinFrequencyMHz, FilterMaxFrequencyMHz) + Math.Abs((FilterMaxFrequencyMHz - FilterMinFrequencyMHz) / 2d);

                            //установка параметров для корреляционной кривой
                            byte filtervalue = BandWidthCodeFromBandWidthkHz(Math.Abs((FilterMaxFrequencyMHz- FilterMinFrequencyMHz)*1000));
                            int offset = (int)(FilterCenterFrequencyMHz * 1000 - CurrentFreqkHz);
                            var answerCorrFuncParamSet = await uDP.SetParam(offset, filtervalue, Alpha1, Alpha2);
                            //var answerCorrFuncParamSet = await uDP.SetParam(filtervalue, offset, Alpha1, Alpha2);
                            await Task.Delay(1);

                            //Запрос корреляционной функции
                            var answerCor0 = await uDP.GetCorrelationFunc();

                            //Ортонормирование
                            var corrFuncTuple = OrthoNormalizationToDoubleTuple(answerCor0);
                            //Отправка корреляциооной функции в АРМ Кираса Manual
                            var answer = await MyDapServer.ServerSendCorrFunc(
                                0, 
                                (int)(FilterCenterFrequencyMHz * 1000), 
                                (int)(Math.Abs((FilterMaxFrequencyMHz - FilterMinFrequencyMHz) * 1000)), 
                                corrFuncTuple.corrFunc0, 
                                corrFuncTuple.corrFunc1, 
                                corrFuncTuple.corrFunc2,
                                corrFuncTuple.corrFunc3
                                );

                        }

                        //Фильтр по выделенной области
                        {
                            var signals = new List<ISignal>();

                            for (int v = 0; v < result0.Signals.Count; v++)
                            {
                                if (result0.Signals[v].FrequencyKhz >= FilterMinFrequencyMHz && result0.Signals[v].FrequencyKhz <= FilterMaxFrequencyMHz)
                                {
                                    signals.Add(result0.Signals[v]);
                                }
                            }
                            result0 = new ProcessResult(signals);
                        }
                    }

                    //Фильтр по ширине
                    {
                        var signals = new List<ISignal>();

                        for (int v = 0; v < result0.Signals.Count; v++)
                        {
                            if (result0.Signals[v].BandwidthKhz >= _FilterMinBandWidthkHz && result0.Signals[v].BandwidthKhz <= _FilterMaxBandWidthkHz)
                            {
                                signals.Add(result0.Signals[v]);
                            }
                        }
                        result0 = new ProcessResult(signals);
                    }

                    //Обнаружение ИРИ
                    List<double> Freqs = result0.Signals.Select(x => (double)x.FrequencyKhz).ToList<double>();

                    //В цикле по ИРИ
                    //for (int j = 0; j < Freqs.Count(); j++)
                    for (int j = 0; j < result0.Signals.Count(); j++)
                    {
                        if (token.IsCancellationRequested)
                        {
                            Console.WriteLine("Операция прервана токеном");
                            return;
                        }

                        if (isFRSonListKnown(Freqs[j]))
                        {
                            //Уже не нужна перестройка преселекторов
                            //SetPreselFreqGain(settings.PreselectorVersion, (int)(result0.Signals[j].CentralFrequencyKhz / 1000), 10);

                            //Уже не нужна перестойка приёмников
                            //var answerCorrSetFreq = await uDP.SetFreq((int)(result0.Signals[j].CentralFrequencyKhz), 30);

                            //установка параметров для корреляционной кривой
                            byte filtervalue = BandWidthCodeFromBandWidthkHz(result0.Signals[j].BandwidthKhz);
                            int offset = (int)(result0.Signals[j].CentralFrequencyKhz - CurrentFreqkHz);
                            var answerCorrFuncParamSet = await uDP.SetParam(offset, filtervalue, Alpha1, Alpha2);
                            //var answerCorrFuncParamSet = await uDP.SetParam(filtervalue, offset, Alpha1, Alpha2);
                            await Task.Delay(1);

                            //Запрос корреляционной функции
                            var answerCor0 = await uDP.GetCorrelationFunc(); // запрос с устройства №0

                            //Ортонормировать корреляционные функции
                            List<List<double>> CorrDoubleList = OrthoNormalization(answerCor0);

                            //Ортонормирование
                            var corrFuncTuple = OrthoNormalizationToDoubleTuple(answerCor0);
                            //Отправка корреляциооной функции в АРМ Кираса
                            var answer = await MyDapServer.ServerSendCorrFunc(
                                1, (int)(result0.Signals[j].CentralFrequencyKhz), (int)result0.Signals[j].BandwidthKhz, 
                                corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3);

                            

                            //var tau123 = CalcTauFromCorrFuncs(CorrDoubleList);
                            var dListTau123 = CalcTauFromCorrFuncsByEgor(CorrDoubleList);
                            //dListTau123 = SignMirror(dListTau123, 0); //0-standart
                            //dListTau123 = SignMirror(dListTau123, 3); //3-Egor
                            dListTau123 = SignMirror(dListTau123, 1, 1, 1); //new

                            (double tau1, double tau2, double tau3) tau123 = (dListTau123[0], dListTau123[1], dListTau123[2]);

                            double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды

                            double[] tau = new double[] { 0, arrtau123[0] * 1e-9, arrtau123[1] * 1e-9, arrtau123[2] * 1e-9 }; // массив задержек всех станций //Секунды

                            tau = SignMirror(tau, 1, 1, 1);

                            bool isTau = false;

                            Console.WriteLine(tau123);

                            if (arrtau123.All(x => x != -1))
                            {
                                //Console.WriteLine(tau123); // в наносекундах
                                isTau = true;
                            }
                            if (tau.Any(x=> Double.IsInfinity(x)) || tau.Any(x => Double.IsNaN(x)))
                            {
                                isTau = false;
                            }

                            bool CalcDelaytest = false;
                            //Если есть положительные задержки то запишем задежки и корр функции
                            if (CalcDelaytest && isTau)
                            {
                                if (counter < 10)
                                {
                                    TestCorrFunc testCorrFunc =
                                        new TestCorrFunc(
                                                        tau123.tau1, tau123.tau2, tau123.tau3,
                                                        answerCor0.Corr_1.eCorr, answerCor0.Corr_1.sCorr, answerCor0.Corr_2.sCorr, answerCor0.Corr_3.sCorr,
                                                        answerCor0.Corr_1.Corr.ToArray(), answerCor0.Corr_2.Corr.ToArray(), answerCor0.Corr_3.Corr.ToArray());

                                    yaml.YamlSave(testCorrFunc, $"CorrfuncSaveTest{counter}.yaml");

                                    counter++;
                                }
                            }

                            //Проверочка по задержкам
                            if (isTau)
                            {
                                //Класс Расчета координат
                                fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                                //Инициализация листа координат станций и их задержек
                                List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                                //Важная проверочка для рассчета координат
                                if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                                {
                                    //Функция порядочка и определения базовой стации в листе по индексу
                                    List<int> GeneratesListOfisOwnIndexes()
                                    {
                                        List<int> lintisOwn = new List<int>();
                                        for (int k = 0; k < tableLocalPoints.Count; k++)
                                        {
                                            if (tableLocalPoints[k].IsCetral == true)
                                            {
                                                lintisOwn.Insert(0, k);
                                            }
                                            else
                                            {
                                                lintisOwn.Add(k);
                                            }
                                        }
                                        return lintisOwn;
                                    }
                                    List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                                    //Заполенение листа координат станций и их задержек
                                    for (int k = 0; k < 4; k++)
                                    {
                                        fDRM.ClassObject classObject = new fDRM.ClassObject()
                                        {
                                            Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                            Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                            Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                            BaseStation = (k == 0) ? true : false,
                                            IndexStation = k + 1,
                                            tau = tau[k]
                                        };

                                        lstClassObject.Add(classObject);
                                    }

                                    //Рассчёт координат источника
                                    fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject);
                                    Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                                    //Отправка точки в АРМ
                                    var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                                    //Запись в файл времён, задержек, координат
                                    if (RecordStartStop)
                                    {
                                        expertises.Add(
                                            new Expertise(
                                            SignalTime[result0.Signals[j]],
                                            result0.Signals[j].CentralFrequencyKhz, result0.Signals[j].BandwidthKhz,
                                            tau[1], tau[2], tau[3],
                                            resultMark.Latitude, resultMark.Longitude, resultMark.Altitude
                                            ));
                                    }

                                    //Проверка принадлежности ИРИ (по дальности)
                                    double D = 40000; // 40 km

                                    //Проверка по одной станции (опорная/центральная)
                                    //var d = Bearing.ClassBearing.f_D_2Points(lt[0], ln[0], lat, lon, 1);

                                    //Рассчет расстояния между двумя точками
                                    var d = classDRM_Dll.f_D_2Points(
                                        tableLocalPoints[lintIsOwn[0]].Coordinates.Latitude,
                                        tableLocalPoints[lintIsOwn[0]].Coordinates.Longitude,
                                        resultMark.Latitude,
                                        resultMark.Longitude,
                                        1);


                                    //Проверка на аномалию
                                    bool isCheck = (d < D) ? true : false;

                                    isCheck = true; //для теста чтоль

                                    //double Time = 0;
                                    double Time = SignalTime[result0.Signals[j]].TimeOfDay.TotalSeconds;
                                    InputPointTrack inputPointTrack = new InputPointTrack()
                                    {
                                        LatMark = resultMark.Latitude,
                                        LongMark = resultMark.Longitude,
                                        HMark = resultMark.Altitude,
                                        Time = Time,
                                        Freq = result0.Signals[j].CentralFrequencyKhz,
                                        dFreq = result0.Signals[j].BandwidthKhz,
                                        _time = SignalTime[result0.Signals[j]]
                                    };
                                    _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                    //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                    //Отработка по Дронам
                                    LenaForMain();

                                }
                                else
                                {
                                    //Заполнения листа месторасположения станций
                                    //List<Station> stations = DefaultStations();
                                    List<Station> stations = DefaultStationsWithEgorHeights();

                                    //Рассчёт координат источника
                                    fDRM.ClassObjectTmp resultMark = CalcCoordsByEgor(dListTau123, 1, 1, 1, stations); //new
                                    Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Altitude.ToString("F1")}");

                                    //Отправка точки в АРМ
                                    var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                                    //Запись в файл времён, задержек, координат
                                    if (RecordStartStop)
                                    {
                                        expertises.Add(
                                            new Expertise(
                                            SignalTime[result0.Signals[j]],
                                            result0.Signals[j].CentralFrequencyKhz, result0.Signals[j].BandwidthKhz,
                                            tau[1], tau[2], tau[3],
                                            resultMark.Latitude, resultMark.Longitude, resultMark.Altitude
                                            ));
                                    }

                                    //Проверка принадлежности ИРИ (по дальности)
                                    double D = 40000; // 40 km

                                    var d = classDRM_Dll.f_D_2Points(
                                    stations[0].Position.Latitude,
                                    stations[0].Position.Longitude,
                                    resultMark.Latitude,
                                    resultMark.Longitude,
                                    1);

                                    //Проверка на аномалию
                                    bool isCheck = (d < D) ? true : false;

                                    isCheck = true; //для теста чтоль

                                    //double Time = 0;
                                    double Time = SignalTime[result0.Signals[j]].TimeOfDay.TotalSeconds;
                                    InputPointTrack inputPointTrack = new InputPointTrack()
                                    {
                                        LatMark = resultMark.Latitude,
                                        LongMark = resultMark.Longitude,
                                        HMark = resultMark.Altitude,
                                        Time = Time,
                                        Freq = result0.Signals[j].CentralFrequencyKhz,
                                        dFreq = result0.Signals[j].BandwidthKhz,
                                        _time = SignalTime[result0.Signals[j]]
                                    };
                                    _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                    //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                    //Отработка по Дронам
                                    LenaForMain();
                                }
                            }
                        }
                    }

                    await Task.Delay(1);
                }
                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }
        int counter = 0;
        private async Task MainLoop2(CancellationToken token)
        {
            //Получаем список ЕПО где ведём Радиоразведку
            var Row = DoIt(MinFreqs, MaxFreqs);
            Row.Sort();
            GlobalRow = new List<int>(Row.Distinct().ToList());

            while (_Mode != 0)
            {
                for (int i = _SaveIndex; i < GlobalRow.Count; i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        Console.WriteLine("Операция прервана токеном");
                        return;
                    }

                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * GlobalRow[i];

                    //Перестройка преселекторов
                    SetPreselFreqGain(settings.PreselectorVersion, (int)centerFreqMHz, (byte)_PreSelValue);

                    //Настройка РПУ на текущую ЕПО по частоте; Freq in kHz, Gain: 0-31
                    var answerSetFreq = await uDP.SetFreq((int)(centerFreqMHz * 1000), 30);

                    if (CurrentFreqkHz == centerFreqMHz * 1000)
                    {
                        await Task.Delay(_ReceiverDelaySolo);
                    }
                    else
                    {
                        await Task.Delay(_ReceiverDelay);
                        CurrentFreqkHz = centerFreqMHz * 1000;
                    }

                    //Чтение данных спектра с выбранного канала _ChanelNumber
                    var answerGetSpectrum = await uDP.GetSpectrum((byte)_ChanelNumber);

                    //Запись спектра в файл
                    if (RecordStartStop)
                    {
                        DateTime Time = DateTime.Now;
                        int ShiftDelay = (int)(Math.Abs(Time.TimeOfDay.TotalMilliseconds - TimeStart.TimeOfDay.TotalMilliseconds));
                        SaveRecord(ShiftDelay, GlobalRow[i], answerGetSpectrum.Ampl.ToArray());
                    }

                    //Исполнительный фильтр
                    if (ApplyFilter && FilterMinFrequencyMHz != -1 && FilterMaxFrequencyMHz != -1)
                    {
                        var indexFilter = ConvertValueToIndexes(new double[] { FilterMinFrequencyMHz, FilterMaxFrequencyMHz });
                        if (indexFilter[0] == GlobalRow[i] && indexFilter[0] == indexFilter[1])
                        {
                            double FilterCenterFrequencyMHz = Math.Min(FilterMinFrequencyMHz, FilterMaxFrequencyMHz) + Math.Abs((FilterMaxFrequencyMHz - FilterMinFrequencyMHz) / 2d);

                            //Ассоциация сигнал-время
                            SignalTime.Clear();
                            var signal = dataProcessor0.GetISignal((float)(FilterCenterFrequencyMHz * 1000), (float)Math.Abs((FilterMaxFrequencyMHz - FilterMinFrequencyMHz) * 1000));
                            SignalTime.Add(signal, DateTime.Now);

                            //установка параметров для корреляционной кривой
                            byte filtervalue = BandWidthCodeFromBandWidthkHz(Math.Abs((FilterMaxFrequencyMHz - FilterMinFrequencyMHz) * 1000));
                            int offset = (int)(FilterCenterFrequencyMHz * 1000 - CurrentFreqkHz);
                            var answerCorrFuncParamSet = await uDP.SetParam(offset, filtervalue, Alpha1, Alpha2);
                            //var answerCorrFuncParamSet = await uDP.SetParam(filtervalue, offset, Alpha1, Alpha2);
                            await Task.Delay(1);

                            //Запрос корреляционной функции
                            var answerCor0 = await uDP.GetCorrelationFunc();

                            //Ортонормирование
                            var corrFuncTuple = OrthoNormalizationToDoubleTuple(answerCor0);
                            //Отправка корреляциооной функции в АРМ Кираса Manual
                            var answer = await MyDapServer.ServerSendCorrFunc(
                                0,
                                (int)(FilterCenterFrequencyMHz * 1000),
                                (int)(Math.Abs((FilterMaxFrequencyMHz - FilterMinFrequencyMHz) * 1000)),
                                corrFuncTuple.corrFunc0,
                                corrFuncTuple.corrFunc1,
                                corrFuncTuple.corrFunc2,
                                corrFuncTuple.corrFunc3
                                );

                            //Ортонормировать корреляционные функции
                            List<List<double>> CorrDoubleList = OrthoNormalization(answerCor0);

                            //Получение задержек из корр-листа
                            var tau123 = CalcTauFromCorrFuncs(CorrDoubleList);
                            double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды
                            double[] tau = new double[] { 0, arrtau123[0] / 10e9, arrtau123[1] / 10e9, arrtau123[2] / 10e9 }; // массив задержек всех станций //Секунды

                            Console.WriteLine(tau123);

                            bool isTau = false;
                            if (arrtau123.All(x => x > 0))
                            {
                                //Console.WriteLine(tau123); // в наносекундах
                                isTau = true;
                            }
                            //Проверочка по задержкам
                            if (isTau)
                            {
                                //Класс Расчета координат
                                fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                                //Инициализация листа координат станций и их задержек
                                List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                                //Важная проверочка для рассчета координат
                                if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                                {
                                    //Функция порядочка и определения базовой стации в листе по индексу
                                    List<int> GeneratesListOfisOwnIndexes()
                                    {
                                        List<int> lintisOwn = new List<int>();
                                        for (int k = 0; k < tableLocalPoints.Count; k++)
                                        {
                                            if (tableLocalPoints[k].IsCetral == true)
                                            {
                                                lintisOwn.Insert(0, k);
                                            }
                                            else
                                            {
                                                lintisOwn.Add(k);
                                            }
                                        }
                                        return lintisOwn;
                                    }
                                    List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                                    //Заполенение листа координат станций и их задержек
                                    for (int k = 0; k < 4; k++)
                                    {
                                        fDRM.ClassObject classObject = new fDRM.ClassObject()
                                        {
                                            Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                            Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                            Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                            BaseStation = (k == 0) ? true : false,
                                            IndexStation = k + 1,
                                            tau = tau[k]
                                        };

                                        lstClassObject.Add(classObject);
                                    }

                                    //Рассчёт координат источника
                                    fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject);
                                    Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                                    //Проверка принадлежности ИРИ (по дальности)
                                    double D = 40000; // 40 km

                                    //Проверка по одной станции (опорная/центральная)
                                    //var d = Bearing.ClassBearing.f_D_2Points(lt[0], ln[0], lat, lon, 1);

                                    //Рассчет расстояния между двумя точками
                                    var d = classDRM_Dll.f_D_2Points(
                                        tableLocalPoints[lintIsOwn[0]].Coordinates.Latitude,
                                        tableLocalPoints[lintIsOwn[0]].Coordinates.Longitude,
                                        resultMark.Latitude,
                                        resultMark.Longitude,
                                        1);


                                    //Проверка на аномалию
                                    bool isCheck = (d < D) ? true : false;

                                    isCheck = true; //для теста чтоль

                                    //double Time = 0;
                                    double Time = SignalTime[signal].TimeOfDay.TotalSeconds;
                                    InputPointTrack inputPointTrack = new InputPointTrack()
                                    {
                                        LatMark = resultMark.Latitude,
                                        LongMark = resultMark.Longitude,
                                        HMark = resultMark.Altitude,
                                        Time = Time,
                                        Freq = signal.CentralFrequencyKhz,
                                        dFreq = signal.BandwidthKhz,
                                        _time = SignalTime[signal]
                                    };
                                    _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                    //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                    //Отработка по Дронам
                                    LenaForMain();

                                }
                                else
                                {
                                    List<Station> stations = DefaultStations();
                                    //Заполенение листа координат станций и их задержек тестовая версия
                                    for (int k = 0; k < 4; k++)
                                    {
                                        fDRM.ClassObject classObject = new fDRM.ClassObject()
                                        {
                                            Latitude = stations[k].Position.Latitude,
                                            Longitude = stations[k].Position.Longitude,
                                            Altitude = stations[k].Position.Altitude,
                                            BaseStation = (k == 0) ? true : false,
                                            IndexStation = k + 1,
                                            tau = tau[k]
                                        };

                                        lstClassObject.Add(classObject);
                                    }

                                    //Рассчёт координат источника
                                    fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject);
                                    Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                                    //Проверка принадлежности ИРИ (по дальности)
                                    double D = 40000; // 40 km

                                    var d = classDRM_Dll.f_D_2Points(
                                    stations[0].Position.Latitude,
                                    stations[0].Position.Longitude,
                                    resultMark.Latitude,
                                    resultMark.Longitude,
                                    1);

                                    //Проверка на аномалию
                                    bool isCheck = (d < D) ? true : false;

                                    isCheck = true; //для теста чтоль

                                    //double Time = 0;
                                    double Time = SignalTime[signal].TimeOfDay.TotalSeconds;
                                    InputPointTrack inputPointTrack = new InputPointTrack()
                                    {
                                        LatMark = resultMark.Latitude,
                                        LongMark = resultMark.Longitude,
                                        HMark = resultMark.Altitude,
                                        Time = Time,
                                        Freq = signal.CentralFrequencyKhz,
                                        dFreq = signal.BandwidthKhz,
                                        _time = SignalTime[signal]
                                    };
                                    _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                    //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                    //Отработка по Дронам
                                    LenaForMain();
                                }
                            }

                        }
                    }
                    else
                    {
                        //Массив для ужатия спектра
                        //float[] vs1 = answerGetSpectrum.Ampl.ToArray();
                        //Массив для поиска частот
                        lastSavedSpectrum0 = answerGetSpectrum.Ampl.ToArray();

                        ////Ужатие Спектра от Феди
                        //var segment = new ArraySegment<float>(vs1, 0, vs1.Count());
                        //var spectrum = segment.StrechSpectrum(_GlobalDotsPerBandCount);
                        //vs1 = spectrum.Amplitudes;

                        //Обновляем хранилище спектра
                        //ListSpectrumStorage[Row[i]] = vs1.ToList();
                        ListSpectrumStorage[Row[i]] = new List<float>(answerGetSpectrum.Ampl);

                        //Обнаружение ИРИ от Феди
                        DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(lastSavedSpectrum0, GlobalRow[i], DateTime.Now, 0);

                        //Параметр для поиска сигналов по порогу
                        ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(_Threshold);

                        //Результаты сигналов
                        var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

                        //Ассоциация сигнал-время
                        if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
                        {
                            //Console.WriteLine(result0.Signals.Count);
                            SignalTime.Clear();
                            for (int qq = 0; qq < result0.Signals.Count; qq++)
                            {
                                SignalTime.Add(result0.Signals[qq], DateTime.Now);
                            }
                        }

                        //Фильтр по ширине
                        {
                            var signals = new List<ISignal>();

                            for (int v = 0; v < result0.Signals.Count; v++)
                            {
                                if (result0.Signals[v].BandwidthKhz >= settings.FilterMinBandWidthkHz && result0.Signals[v].BandwidthKhz <= settings.FilterMaxBandWidthkHz)
                                {
                                    signals.Add(result0.Signals[v]);
                                }
                            }
                            result0 = new ProcessResult(signals);
                        }

                        //Обнаружение ИРИ
                        List<double> Freqs = result0.Signals.Select(x => (double)x.CentralFrequencyKhz).ToList<double>();

                        //В цикле по ИРИ
                        for (int j = 0; j < result0.Signals.Count(); j++)
                        {
                            if (token.IsCancellationRequested)
                            {
                                Console.WriteLine("Операция прервана токеном");
                                return;
                            }

                            if (isFRSonListKnown(Freqs[j]))
                            {
                                //Уже не нужна перестройка преселекторов
                                //SetPreselFreqGain(settings.PreselectorVersion, (int)(result0.Signals[j].CentralFrequencyKhz / 1000), 10);

                                //Уже не нужна перестойка приёмников
                                //var answerCorrSetFreq = await uDP.SetFreq((int)(result0.Signals[j].CentralFrequencyKhz), 30);

                                //установка параметров для корреляционной кривой
                                byte filtervalue = BandWidthCodeFromBandWidthkHz(result0.Signals[j].BandwidthKhz);
                                int offset = (int)(result0.Signals[j].CentralFrequencyKhz - CurrentFreqkHz);
                                var answerCorrFuncParamSet = await uDP.SetParam(offset, filtervalue, Alpha1, Alpha2);
                                await Task.Delay(1);

                                //Запрос корреляционной функции
                                var answerCor0 = await uDP.GetCorrelationFunc();

                                //Ортонормировать корреляционные функции
                                List<List<double>> CorrDoubleList = OrthoNormalization(answerCor0);

                                //Ортонормирование
                                var corrFuncTuple = OrthoNormalizationToDoubleTuple(answerCor0);
                                //Отправка корреляциооной функции в АРМ Кираса
                                var answer = await MyDapServer.ServerSendCorrFunc(
                                    1, (int)(result0.Signals[j].CentralFrequencyKhz), (int)result0.Signals[j].BandwidthKhz, 
                                    corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3);

                                //Получение задержек из корр-листа
                                var tau123 = CalcTauFromCorrFuncs(CorrDoubleList);

                                double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды

                                double[] tau = new double[] { 0, arrtau123[0] / 1e9, arrtau123[1] / 1e9, arrtau123[2] / 1e9 }; // массив задержек всех станций //Секунды

                                bool isTau = false;

                                Console.WriteLine(tau123);

                                if (arrtau123.All(x => x > 0))
                                {
                                    //Console.WriteLine(tau123); // в наносекундах
                                    isTau = true;
                                }

                                //Проверочка по задержкам
                                if (isTau)
                                {
                                    //Класс Расчета координат
                                    fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                                    //Инициализация листа координат станций и их задержек
                                    List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                                    //Важная проверочка для рассчета координат
                                    if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                                    {
                                        //Функция порядочка и определения базовой стации в листе по индексу
                                        List<int> GeneratesListOfisOwnIndexes()
                                        {
                                            List<int> lintisOwn = new List<int>();
                                            for (int k = 0; k < tableLocalPoints.Count; k++)
                                            {
                                                if (tableLocalPoints[k].IsCetral == true)
                                                {
                                                    lintisOwn.Insert(0, k);
                                                }
                                                else
                                                {
                                                    lintisOwn.Add(k);
                                                }
                                            }
                                            return lintisOwn;
                                        }
                                        List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                                        //Заполенение листа координат станций и их задержек
                                        for (int k = 0; k < 4; k++)
                                        {
                                            fDRM.ClassObject classObject = new fDRM.ClassObject()
                                            {
                                                Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                                Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                                Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                                BaseStation = (k == 0) ? true : false,
                                                IndexStation = k + 1,
                                                tau = tau[k]
                                            };

                                            lstClassObject.Add(classObject);
                                        }

                                        //Рассчёт координат источника
                                        fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject);
                                        Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                                        //Проверка принадлежности ИРИ (по дальности)
                                        double D = 40000; // 40 km

                                        //Проверка по одной станции (опорная/центральная)
                                        //var d = Bearing.ClassBearing.f_D_2Points(lt[0], ln[0], lat, lon, 1);

                                        //Рассчет расстояния между двумя точками
                                        var d = classDRM_Dll.f_D_2Points(
                                            tableLocalPoints[lintIsOwn[0]].Coordinates.Latitude,
                                            tableLocalPoints[lintIsOwn[0]].Coordinates.Longitude,
                                            resultMark.Latitude,
                                            resultMark.Longitude,
                                            1);


                                        //Проверка на аномалию
                                        bool isCheck = (d < D) ? true : false;

                                        isCheck = true; //для теста чтоль

                                        //double Time = 0;
                                        double Time = SignalTime[result0.Signals[j]].TimeOfDay.TotalSeconds;
                                        InputPointTrack inputPointTrack = new InputPointTrack()
                                        {
                                            LatMark = resultMark.Latitude,
                                            LongMark = resultMark.Longitude,
                                            HMark = resultMark.Altitude,
                                            Time = Time,
                                            Freq = result0.Signals[j].FrequencyKhz,
                                            dFreq = result0.Signals[j].BandwidthKhz,
                                            _time = SignalTime[result0.Signals[j]]
                                        };
                                        _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                        //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                        //Отработка по Дронам
                                        LenaForMain();

                                    }
                                    else
                                    {
                                        List<Station> stations = DefaultStations();
                                        //Заполенение листа координат станций и их задержек тестовая версия
                                        for (int k = 0; k < 4; k++)
                                        {
                                            fDRM.ClassObject classObject = new fDRM.ClassObject()
                                            {
                                                Latitude = stations[k].Position.Latitude,
                                                Longitude = stations[k].Position.Longitude,
                                                Altitude = stations[k].Position.Altitude,
                                                BaseStation = (k == 0) ? true : false,
                                                IndexStation = k + 1,
                                                tau = tau[k]
                                            };

                                            lstClassObject.Add(classObject);
                                        }

                                        //Рассчёт координат источника
                                        fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject);
                                        Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                                        //Проверка принадлежности ИРИ (по дальности)
                                        double D = 40000; // 40 km

                                        var d = classDRM_Dll.f_D_2Points(
                                        stations[0].Position.Latitude,
                                        stations[0].Position.Longitude,
                                        resultMark.Latitude,
                                        resultMark.Longitude,
                                        1);

                                        //Проверка на аномалию
                                        bool isCheck = (d < D) ? true : false;

                                        isCheck = true; //для теста чтоль

                                        //double Time = 0;
                                        double Time = SignalTime[result0.Signals[j]].TimeOfDay.TotalSeconds;
                                        InputPointTrack inputPointTrack = new InputPointTrack()
                                        {
                                            LatMark = resultMark.Latitude,
                                            LongMark = resultMark.Longitude,
                                            HMark = resultMark.Altitude,
                                            Time = Time,
                                            Freq = result0.Signals[j].FrequencyKhz,
                                            dFreq = result0.Signals[j].BandwidthKhz,
                                            _time = SignalTime[result0.Signals[j]]
                                        };
                                        _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                        //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                        //Отработка по Дронам
                                        LenaForMain();
                                    }
                                }
                            }
                        }
                    }
                }

                _SaveIndex = 0;
            }
        }

        private int countSpectrum = 0;
        private async Task LoadSpectrumLoop(CancellationToken token)
        {
            try
            {
                Console.WriteLine("LoadSpectrumLoop Start");

                //await LoadSpectrum();

                int maxShiftDelay = 0;
                for (int i = 0; i < lRecords.Count - 1; i++)
                {
                    if (lRecords[i + 1].ShiftDelay - lRecords[i].ShiftDelay > maxShiftDelay) maxShiftDelay = lRecords[i + 1].ShiftDelay - lRecords[i].ShiftDelay;
                }

                while (_Mode != 0)
                {
                    if (token.IsCancellationRequested)
                    {
                        Console.WriteLine("Операция прервана токеном");
                        return;
                    }

                    if (countSpectrum == lRecords.Count)
                    {
                        countSpectrum = 0;
                    }

                    ListSpectrumStorage[lRecords[countSpectrum].EPO] = lRecords[countSpectrum].Spectrum.ToList();

                    var recordSpectrum = lRecords[countSpectrum].Spectrum.ToArray();

                    //Обнаружение ИРИ от Феди
                    DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(recordSpectrum, lRecords[countSpectrum].EPO, DateTime.Now, 0);

                    //Параметр для поиска сигналов по порогу
                    ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(50);

                    //Результаты сигналов
                    var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

                    for (int j = 0; j < result0.Signals.Count(); j++)
                    {
                        if (result0.Signals[j].BandwidthKhz > 1000)
                        {

                            if (token.IsCancellationRequested)
                            {
                                Console.WriteLine("Операция прервана токеном");
                                return;
                            }

                            int testN = 1;
                            for (int ii = 0; ii < testN; ii++)
                            {
                                //Корр функция с эмулятора
                                var CorrDoubleList = CorrelationParser.CorrParser.GetCorrFunc(1);

                                float[] corrFunc0 = CorrDoubleList[0].Select(x => (float)x).ToArray<float>();
                                float[] corrFunc1 = CorrDoubleList[1].Select(x => (float)x).ToArray<float>();
                                float[] corrFunc2 = CorrDoubleList[2].Select(x => (float)x).ToArray<float>();

                                //Отправка корреляциооной функции в АРМ Кираса
                                //var answer = await MyDapServer.ServerSendCorrFunc(1, (int)(result0.Signals[j].CentralFrequencyKhz), (int)result0.Signals[j].BandwidthKhz, corrFunc0, corrFunc1, corrFunc2);

                                await Task.Delay(1);
                            }
                        }
                    }

                    int Ti = countSpectrum;
                    int Ti1 = countSpectrum + 1;
                    if (Ti1 == lRecords.Count) Ti1 = 0;

                    int delay = Math.Abs(lRecords[Ti1].ShiftDelay - lRecords[Ti].ShiftDelay);
                    if (delay > maxShiftDelay) delay = maxShiftDelay;

                    await Task.Delay(delay);

                    countSpectrum++;
                }
            }
            catch (Exception e)
            {

            }
        }

        int EmuCounter = 0;
        private async void EmuLoop(CancellationToken token)
        {
            //Получаем список ЕПО где ведём Радиоразведку
            var Row = DoIt(MinFreqs, MaxFreqs);
            GlobalRow = new List<int>(Row.Distinct().ToList());

            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Операция прервана токеном");
                    return;
                }

                //var Ranges = InitRanges0();
                var Ranges = InitRanges();

                for (int w = 0; w < Ranges.Count(); w++)
                {
                    for (int i = 0; i < EmulatorGetCount(w); i++)
                    {
                        //Получение данных от Эмулятора
                        List<float> ld0 = new List<float>(UDPClass.Emulator.GetSpectrumByBandNumber(w));

                        //Убрать лишнее из данных от симулятора
                        if (ld0.Count == 8001) ld0.RemoveAt(0);

                        var indexes = CalcLikePro(Ranges[w].StartFrequencyKhz, Ranges[w].EndFrequencyKhz);

                        double startEPO = Divide[indexes.indexStart];
                        double endEPO = Divide[indexes.indexEnd];


                        float[] fillone = new float[8000];

                        double sum = 0;
                        int exindex = 0;
                        for (int j = 0; j < 8000; j++)
                        {
                            if (startEPO + sum < Ranges[w].StartFrequencyKhz)
                            {
                                fillone[j] = -130;
                            }
                            else
                            {
                                fillone[j] = ld0[exindex++];
                            }
                            sum = (_GlobalBandWidthMHz / 8000d) * j;
                        }

                        ListSpectrumStorage[indexes.indexStart] = new List<float>(fillone);

                        float[] filloneCopy = new float[fillone.Count()];
                        fillone.CopyTo(filloneCopy, 0);
                        //Некое нормирование
                        for (int v=0; v< filloneCopy.Count(); v++)
                        {
                            if (filloneCopy[v] < -130) filloneCopy[v] = -130;
                            if (filloneCopy[v] > 0) filloneCopy[v] = 0;
                        }

                        //Для большого хранилища
                        for (int ww = 0; ww < _GlobalN; ww++)
                        {
                            BigListSpectrumStorage[ww][indexes.indexStart] = new List<float>(filloneCopy);
                        }

                        //Обнаружение ИРИ от Феди
                        DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(fillone, indexes.indexStart, DateTime.Now, 0);

                        //Параметр для поиска сигналов по порогу
                        ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(25);

                        //Результаты сигналов
                        var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

                        if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
                        {
                            var widthSignals = result0.Signals.Where(x => x.BandwidthKhz >= 10000).ToList();
                            if (widthSignals.Count > 0)
                            {

                            }
                        }


                        await Task.Delay(5);

                        float[] filltwo = new float[8000];

                        int twoindex = 0;
                        for (int j = exindex; j < 8000; j++)
                        {
                            filltwo[twoindex++] = ld0[j];
                        }

                        for (int j = twoindex; j < 8000; j++)
                        {
                            filltwo[j] = -130;
                        }

                        ListSpectrumStorage[indexes.indexEnd] = new List<float>(filltwo);

                        float[] filltwoCopy = new float[filltwo.Count()];
                        filltwo.CopyTo(filltwoCopy, 0);

                        //Некое нормирование
                        for (int v = 0; v < filltwoCopy.Count(); v++)
                        {
                            if (filltwoCopy[v] < -130) filltwoCopy[v] = -130;
                            if (filltwoCopy[v] > 0) filltwoCopy[v] = 0;
                        }

                        //Для большого хранилища
                        for (int ww = 0; ww < _GlobalN; ww++)
                        {
                            BigListSpectrumStorage[ww][indexes.indexEnd] = new List<float>(filltwoCopy);
                        }

                        //Обнаружение ИРИ от Феди
                        DspDataModel.Data.IAmplitudeScan amplitudeScan1 = new DspDataModel.Data.AmplitudeScan(filltwo, indexes.indexEnd, DateTime.Now, 0);

                        //Параметр для поиска сигналов по порогу
                        ScanProcessConfig scanProcessConfig1 = ScanProcessConfig.CreateConfigWithoutDf(25);

                        //Результаты сигналов
                        var result1 = dataProcessor0.GetSignals(amplitudeScan1, scanProcessConfig1);

                        if (result1.RawSignals.Count != 0 || result1.Signals.Count != 0)
                        {
                            var widthSignals = result1.Signals.Where(x => x.BandwidthKhz >= 10000).ToList();
                            if (widthSignals.Count > 0)
                            {

                            }
                        }

                        await Task.Delay(5);
                    }

                }

                await Task.Delay(5);
            }

            DspDataModel.FrequencyRange[] InitRanges0()
            {
                DspDataModel.FrequencyRange[] frequencyRanges = new DspDataModel.FrequencyRange[1];
                frequencyRanges[0] = new DspDataModel.FrequencyRange(2400f, 2462.5f);
                return frequencyRanges;
            }
            DspDataModel.FrequencyRange[] InitRanges()
            {
                DspDataModel.FrequencyRange[] frequencyRanges = new DspDataModel.FrequencyRange[5];

                frequencyRanges[0] = new DspDataModel.FrequencyRange(2400f, 2462.5f);
                frequencyRanges[1] = new DspDataModel.FrequencyRange(2437.5f, 2500f);
                frequencyRanges[2] = new DspDataModel.FrequencyRange(5720f, 5782.5f);
                frequencyRanges[3] = new DspDataModel.FrequencyRange(5760f, 5822.5f);
                frequencyRanges[4] = new DspDataModel.FrequencyRange(5807f, 5870.5f);

                return frequencyRanges;
            }

            int EmulatorGetCount(int Band)
            {
                switch (Band)
                {
                    case 0:
                        return UDPClass.Emulator.zeroBand.Count();
                    case 1:
                        return UDPClass.Emulator.firstBand.Count();
                    case 2:
                        return UDPClass.Emulator.secondBand.Count();
                    case 3:
                        return UDPClass.Emulator.thirdBand.Count();
                    case 4:
                        return UDPClass.Emulator.fourthBand.Count();
                    default:
                        return 0;
                }
                return -1;
            }

        }
        private async void EmuCorrFuncLoop(CancellationToken token)
        {
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Операция прервана токеном");
                    return;
                }

                //Corr 0
                {
                    var CorrDoubleList = CorrelationParser.CorrParser.GetCorrFunc(0);

                    if (CorrDoubleList != null && 
                        CorrDoubleList.Count == 3 &&
                        CorrDoubleList[0].Count == 121 &&
                        CorrDoubleList[1].Count == 121 &&
                        CorrDoubleList[2].Count == 121)
                    {
                        double[] corrFunc0 = CorrDoubleList[0].Select(x => x).ToArray<double>();
                        double[] corrFunc1 = CorrDoubleList[1].Select(x => x).ToArray<double>();
                        double[] corrFunc2 = CorrDoubleList[2].Select(x => x).ToArray<double>();
                        double[] corrFunc3 = CorrDoubleList[0].Select(x => x / 2.0d).ToArray<double>();

                        var Freq = (float)(2412000d + r.NextDouble() * 1000d);
                        var dFreq = 10000f + (float)(r.NextDouble() * 12f);

                        var answer = await MyDapServer.ServerSendCorrFunc(0, (int)Freq, (int)dFreq, corrFunc0, corrFunc1, corrFunc2, corrFunc3);
                    }
                }

                //Corr1
                {
                    var CorrDoubleList = CorrelationParser.CorrParser.GetCorrFunc(1);

                    if (CorrDoubleList != null &&
                       CorrDoubleList.Count == 3 &&
                       CorrDoubleList[0].Count == 121 &&
                       CorrDoubleList[1].Count == 121 &&
                       CorrDoubleList[2].Count == 121)
                    {
                        double[] corrFunc0 = CorrDoubleList[0].Select(x => x).ToArray<double>();
                        double[] corrFunc1 = CorrDoubleList[1].Select(x => x).ToArray<double>();
                        double[] corrFunc2 = CorrDoubleList[2].Select(x => x).ToArray<double>();
                        double[] corrFunc3 = CorrDoubleList[0].Select(x => x / 2.0d).ToArray<double>();

                        var Freq = (float)(5735000d + r.NextDouble() * 1000d);
                        var dFreq = 20000f + (float)(r.NextDouble() * 24f);

                        var answer = await MyDapServer.ServerSendCorrFunc(1, (int)Freq, (int)dFreq, corrFunc0, corrFunc1, corrFunc2, corrFunc3);
                    }
                }

                await Task.Delay(16);
            }
        }

        private async void SpectrumLooop(CancellationToken token)
        {
            //Получаем список ЕПО где ведём Радиоразведку
            var Row = DoIt(MinFreqs, MaxFreqs);
            GlobalRow = new List<int>(Row.Distinct().ToList());

            while (_Mode != 0)
            {
                for (int i = 0; i < GlobalRow.Count; i++)
                {
                    if (token.IsCancellationRequested)
                    {
                        Console.WriteLine("Операция прервана токеном");
                        return;
                    }

                    //Получение данных от Эмулятора
                    List<float> ld0 = new List<float>(UDPClass.Emulator.GetSpectrumByBandNumber(EmuCounter));
                    EmuCounter++;
                    if (EmuCounter == 5) EmuCounter = 0;

                    await Task.Delay(5);

                    //Убрать лишнее из данных от симулятора
                    ld0.RemoveAt(0);

                    lastSavedSpectrum0 = ld0.ToArray();

                    ListSpectrumStorage[GlobalRow[i]] = new List<float>(ld0);

                    await Task.Delay(5);
                }
            }

        }

        private async void Loop1(CancellationToken token)
        {

            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Операция прервана токеном");
                    return;
                }

                Random random = new Random();
                //int r = ;
                switch (random.Next(0, 2))
                {
                    //добавить
                    case 0:

                        if (lTablesUAVRes.Count() < settings.MaxDroneCount)
                        {
                            var Row = DoIt(MinFreqs, MaxFreqs);
                            GlobalRow = new List<int>(Row.Distinct().ToList());
                            double centerFreq = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * GlobalRow[random.Next(0, GlobalRow.Count())];

                            FRS frsNew = new FRS()
                            {
                                FreqkHz = centerFreq * 1000d,
                                BandWidthkHz = random.Next(1, 13)
                            };
                            KirasaModelsDBLib.Coord KirasaCoordSource = new KirasaModelsDBLib.Coord()
                            {
                                Latitude = settings.Latitude + random.NextDouble() / settings.RandomCoordsCoef,
                                Longitude = settings.Longitude + random.NextDouble() / settings.RandomCoordsCoef,
                                Altitude = (float)(50f)
                            };
                            frsNew.Coords.Add(KirasaCoordSource);

                            Log("Add");
                            //NewAddTableUAVs(frsNew);
                            //NewAddTableUAVsWithStrob(frsNew);
                            NewAddTableUAVsWithTypeWithStrobAndHead(frsNew);
                        }

                        break;

                    //обновить состояния
                    case 1:

                        if (lTablesUAVRes.Count() > 0)
                        {
                            //выбираем случайный дрон из списка
                            int droneIndex = random.Next(0, lTablesUAVRes.Count());

                            var record = lTablesUAVRes[droneIndex];

                            record.State = reverse(record.State);

                            byte reverse(byte value)
                            {
                                if (value == 0)
                                {
                                    return 1;
                                }
                                else
                                {
                                    return 0;
                                }
                            }

                            Log("Change");
                            try { clientDB?.Tables[NameTable.TableUAVRes].Change(record); } catch { }
                        }

                        break;

                    //удалить
                    case 2:

                        if (lTablesUAVRes.Count() > 0)
                        {
                            //выбираем случайный дрон из списка
                            int droneIndex = random.Next(0, lTablesUAVRes.Count());

                            var record = lTablesUAVRes[droneIndex];

                            Log("Delete");
                            try { clientDB?.Tables[NameTable.TableUAVRes].Delete(record); } catch { }
                        }

                        break;
                }

                await Task.Delay(settings.Delay1ms);
            }
        }

        private async void Loop1Aero(CancellationToken token)
        {

            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Операция прервана токеном");
                    return;
                }

                Random random = new Random();
                //int r = ;
                //switch (random.Next(0, 2))
                switch (0)
                {
                    //добавить
                    case 0:

                        if (lTablesAeroscopes.Count() < settings.MaxDroneCount)
                        {
                            var Row = DoIt(MinFreqs, MaxFreqs);
                            GlobalRow = new List<int>(Row.Distinct().ToList());
                            double centerFreq = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * GlobalRow[random.Next(0, GlobalRow.Count())];

                            FRS frsNew = new FRS()
                            {
                                FreqkHz = centerFreq * 1000d,
                                BandWidthkHz = random.Next(1, 13)
                            };
                            KirasaModelsDBLib.Coord KirasaCoordSource = new KirasaModelsDBLib.Coord()
                            {
                                Latitude = settings.Latitude + random.NextDouble() / settings.RandomCoordsCoef,
                                Longitude = settings.Longitude + random.NextDouble() / settings.RandomCoordsCoef,
                                Altitude = (float)(50f)
                            };
                            frsNew.Coords.Add(KirasaCoordSource);

                            Log("Add");
                            NewAddTableAeroScopes(frsNew);
                        }

                        break;

                    //обновить состояния
                    case 1:

                        if (lTablesAeroscopes.Count() > 0)
                        {
                            //выбираем случайный дрон из списка
                            int droneIndex = random.Next(0, lTablesAeroscopes.Count());

                            var record = lTablesAeroscopes[droneIndex];

                            Log("Change");
                            try { clientDB?.Tables[NameTable.TableАeroscope].Change(record); } catch { }
                        }

                        break;

                    //удалить
                    case 2:

                        if (lTablesAeroscopes.Count() > 0)
                        {
                            //выбираем случайный дрон из списка
                            int droneIndex = random.Next(0, lTablesAeroscopes.Count());

                            var record = lTablesAeroscopes[droneIndex];

                            Log("Delete");
                            try { clientDB?.Tables[NameTable.TableАeroscope].Delete(record); } catch { }
                        }

                        break;
                }

                await Task.Delay(settings.Delay1ms);
            }
        }

        private async void Loop2(CancellationToken token)
        {
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Операция прервана токеном");
                    return;
                }

                //добавить координаты любому дрону в списке
                if (lTablesUAVRes.Count() > 0)
                {
                    //выбираем любой дрон
                    Random random = new Random();
                    int droneIndex = random.Next(0, lTablesUAVRes.Count());
                    int droneID = lTablesUAVRes[droneIndex].Id;

                    var selectedFromTableUAVTrajectoryList = lTableUAVTrajectory.Where(x => x.TableUAVResId == droneID).ToList();
                    short numcount = (short)selectedFromTableUAVTrajectoryList.Count();

                    if (numcount > 0)
                    {
                        var last = selectedFromTableUAVTrajectoryList.Last();

                        //var filtered = selectedFromTableUAVTrajectoryList.OrderBy(x => x.Num).ToList();


                        FRS frsNew = new FRS()
                        {
                            FreqkHz = last.FrequencyKHz,
                            BandWidthkHz = last.BandKHz
                        };

                        double coef()
                        {
                            if (random.NextDouble() <= 0.5)
                                return -1;
                            else return 1;
                        }

                        KirasaModelsDBLib.Coord KirasaCoordSource = new KirasaModelsDBLib.Coord()
                        {
                            Latitude = last.Coordinates.Latitude + coef() * random.NextDouble() / settings.RandomCoordsCoef,
                            Longitude = last.Coordinates.Longitude + coef() * random.NextDouble() / settings.RandomCoordsCoef,
                            Altitude = (float)(last.Coordinates.Altitude + coef())
                        };


                        frsNew.Coords.Add(KirasaCoordSource);

                        Log("Add TableUAVTrajectory");
                        //AnotherOneAddTableUAVs(droneID, numcount, frsNew);
                        AnotherOneAddTableUAVsWithStrobAndHead(droneID, numcount, frsNew);
                    }
                }

                await Task.Delay(settings.Delay2ms);
            }
        }

        private async void Loop2Aero(CancellationToken token)
        {
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Операция прервана токеном");
                    return;
                }

                //добавить координаты любому дрону в списке
                if (lTablesAeroscopes.Count() > 0)
                {
                    //выбираем любой дрон
                    Random random = new Random();
                    int droneIndex = random.Next(0, lTablesAeroscopes.Count());
                    string droneSerialNumber = lTablesAeroscopes[droneIndex].SerialNumber;

                    var selectedFromTableUAVTrajectoryList = lTableAeroscopeTrajectory.Where(x => x.SerialNumber == droneSerialNumber).ToList();
                    short numcount = (short)selectedFromTableUAVTrajectoryList.Count();

                    if (numcount > 0)
                    {
                        var last = selectedFromTableUAVTrajectoryList.Last();

                        //var filtered = selectedFromTableUAVTrajectoryList.OrderBy(x => x.Num).ToList();


                        FRS frsNew = new FRS()
                        {
                            FreqkHz = 0,
                            BandWidthkHz = 0
                        };

                        double coef()
                        {
                            if (random.NextDouble() <= 0.5)
                                return -1;
                            else return 1;
                        }

                        KirasaModelsDBLib.Coord KirasaCoordSource = new KirasaModelsDBLib.Coord()
                        {
                            Latitude = last.Coordinates.Latitude + coef() * random.NextDouble() / settings.RandomCoordsCoef,
                            Longitude = last.Coordinates.Longitude + coef() * random.NextDouble() / settings.RandomCoordsCoef,
                            Altitude = (float)(last.Coordinates.Altitude + coef())
                        };


                        frsNew.Coords.Add(KirasaCoordSource);

                        Log("Add TableAeroTrajectory");
                        AnotherOneAddTableAeroScopes(droneSerialNumber, numcount, frsNew);
                    }
                }

                await Task.Delay(settings.Delay2ms);
            }
        }
    }
}
