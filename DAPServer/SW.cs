﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAPServer
{
    public class SW
    {
        System.Diagnostics.Stopwatch stopWatch;

        public SW()
        {
            //стопвотч
            stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();
        }

        public void Stop()
        {
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}.{1:000}", ts.Seconds, ts.Milliseconds);
            Console.WriteLine("RunTime " + elapsedTime);
        }
    }
}
