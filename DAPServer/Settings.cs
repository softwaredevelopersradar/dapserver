﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using YamlDotNet.Serialization;

namespace DAPServer
{
    public class Settings : INotifyPropertyChanged
    {
        int _NumberOfBands;
        double _BandwidthMHz;
        double _RangeXMin;
        int _DotsPerBand;

        WorkMode _workMode;
        int _MaxDroneCount;
        int _Delay1ms;
        int _Delay2ms;
        EndPointConnection _ServerSettings;
        EndPointConnection _DBSettings;

        UDPEndPointConnection _OpticUDPSettings;

        UDPEndPointConnection _EgorTestUDPSettings;

        ComConnection _OpticRS232Settings1;
        ComConnection _OpticRS232Settings2;
        ComConnection _OpticRS232Settings3;
        ComConnection _OpticRS232Settings4;

        double _Latitude;
        double _Longitude;
        double _RandomCoordsCoef;

        int _PreselectorVersion;

        int _ReceiverDelay;
        int _ReceiverDelaySolo;

        int _Alpha1;
        int _Alpha2;

        float _FilterMinBandWidthkHz;
        float _FilterMaxBandWidthkHz;

        int _PreSelValue;

        Channel _DefaultChannel;

        int _PreselCmdDelay;
        int _PreselCmdSetDelay;
        int _GetAutoCorrFuncDelay;

        float _CorrThreshold;

        int _HMax;

        public Settings()
        {
            _NumberOfBands = 96;
            _BandwidthMHz = 62.5d;
            _RangeXMin = 10.0d;
            _DotsPerBand = 8000;

            _workMode = WorkMode.Main;
            _MaxDroneCount = 50;
            _Delay1ms = 4000;
            _Delay2ms = 500;
            _ServerSettings = new EndPointConnection(10003);
            _DBSettings = new EndPointConnection(8302);

            _OpticUDPSettings = new UDPEndPointConnection();

            _EgorTestUDPSettings = new UDPEndPointConnection();

            _OpticRS232Settings1 = new ComConnection();
            _OpticRS232Settings2 = new ComConnection();
            _OpticRS232Settings3 = new ComConnection();
            _OpticRS232Settings4 = new ComConnection();

            _Latitude = 53.93d;
            _Longitude = 27.63d;
            _RandomCoordsCoef = 1000;

            _PreselectorVersion = 0;

            _ReceiverDelay = 300;
            _ReceiverDelaySolo = 100;

            _Alpha1 = 999000;
            _Alpha2 = 0;

            _FilterMinBandWidthkHz = 4000;
            _FilterMaxBandWidthkHz = 20000;

            _PreSelValue = 30;

            _DefaultChannel = Channel.ChannelMax;

            _PreselCmdDelay = 100;
            _PreselCmdSetDelay = 10;
            _GetAutoCorrFuncDelay = 1;

            _CorrThreshold = 0.1f;

            _HMax = 100;
        }

        #region INotify 
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        public int NumberOfBands { get => _NumberOfBands; set { _NumberOfBands = value; OnPropertyChanged(); } }
        public double BandwidthMHz { get => _BandwidthMHz; set { _BandwidthMHz = value; OnPropertyChanged(); } }
        public double RangeXMin { get => _RangeXMin; set { _RangeXMin = value; OnPropertyChanged(); } }
        public int DotsPerBand { get => _DotsPerBand; set { _DotsPerBand = value; OnPropertyChanged(); } }

        public WorkMode workMode
        {
            get => _workMode;
            set
            {
                if (_workMode == value)
                    return;
                _workMode = value;
                OnPropertyChanged();
            }
        }
        public int MaxDroneCount
        {
            get => _MaxDroneCount;
            set
            {
                if (_MaxDroneCount == value)
                    return;
                _MaxDroneCount = value;
                OnPropertyChanged();
            }
        }
        public int Delay1ms
        {
            get => _Delay1ms;
            set
            {
                if (_Delay1ms == value)
                    return;
                _Delay1ms = value;
                OnPropertyChanged();
            }
        }
        public int Delay2ms
        {
            get => _Delay2ms;
            set
            {
                if (_Delay2ms == value)
                    return;
                _Delay2ms = value;
                OnPropertyChanged();
            }
        }

        public EndPointConnection ServerSettings
        {
            get => _ServerSettings;
            set
            {
                if (_ServerSettings == value)
                    return;
                _ServerSettings = value;
                OnPropertyChanged();
            }
        }
        public EndPointConnection DBSettings
        {
            get => _DBSettings;
            set
            {
                if (_DBSettings == value)
                    return;
                _DBSettings = value;
                OnPropertyChanged();
            }
        }

        public UDPEndPointConnection OpticUDPSettings
        {
            get => _OpticUDPSettings;
            set
            {
                if (_OpticUDPSettings == value)
                    return;
                _OpticUDPSettings = value;
                OnPropertyChanged();
            }
        }

        public UDPEndPointConnection EgorTestUDPSettings
        {
            get => _EgorTestUDPSettings;
            set
            {
                if (_EgorTestUDPSettings == value)
                    return;
                _EgorTestUDPSettings = value;
                OnPropertyChanged();
            }
        }

        public ComConnection OpticRS232Settings1
        {
            get => _OpticRS232Settings1;
            set
            {
                if (_OpticRS232Settings1 == value)
                    return;
                _OpticRS232Settings1 = value;
                OnPropertyChanged();
            }
        }
        public ComConnection OpticRS232Settings2
        {
            get => _OpticRS232Settings2;
            set
            {
                if (_OpticRS232Settings2 == value)
                    return;
                _OpticRS232Settings2 = value;
                OnPropertyChanged();
            }
        }
        public ComConnection OpticRS232Settings3
        {
            get => _OpticRS232Settings3;
            set
            {
                if (_OpticRS232Settings3 == value)
                    return;
                _OpticRS232Settings3 = value;
                OnPropertyChanged();
            }
        }
        public ComConnection OpticRS232Settings4
        {
            get => _OpticRS232Settings4;
            set
            {
                if (_OpticRS232Settings4 == value)
                    return;
                _OpticRS232Settings4 = value;
                OnPropertyChanged();
            }
        }

        public double Latitude
        {
            get => _Latitude;
            set
            {
                if (_Latitude == value)
                    return;
                _Latitude = value;
                OnPropertyChanged();
            }
        }
        public double Longitude
        {
            get => _Longitude;
            set
            {
                if (_Longitude == value)
                    return;
                _Longitude = value;
                OnPropertyChanged();
            }
        }
        public double RandomCoordsCoef
        {
            get => _RandomCoordsCoef;
            set
            {
                if (_RandomCoordsCoef == value)
                    return;
                _RandomCoordsCoef = value;
                OnPropertyChanged();
            }
        }

        public int PreselectorVersion
        {
            get => _PreselectorVersion;
            set
            {
                if (_PreselectorVersion == value)
                    return;
                _PreselectorVersion = value;
                OnPropertyChanged();
            }
        }

        public int ReceiverDelay
        {
            get => _ReceiverDelay;
            set
            {
                if (_ReceiverDelay == value)
                    return;
                _ReceiverDelay = value;
                OnPropertyChanged();
            }
        }

        public int ReceiverDelaySolo
        {
            get => _ReceiverDelaySolo;
            set
            {
                if (_ReceiverDelaySolo == value)
                    return;
                _ReceiverDelaySolo = value;
                OnPropertyChanged();
            }
        }

        public int Alpha1
        {
            get => _Alpha1;
            set
            {
                if (_Alpha1 == value)
                    return;
                _Alpha1 = value;
                OnPropertyChanged();
            }
        }

        public int Alpha2
        {
            get => _Alpha2;
            set
            {
                if (_Alpha2 == value)
                    return;
                _Alpha2 = value;
                OnPropertyChanged();
            }
        }

        public float FilterMinBandWidthkHz
        {
            get => _FilterMinBandWidthkHz;
            set
            {
                if (_FilterMinBandWidthkHz == value)
                    return;
                _FilterMinBandWidthkHz = value;
                OnPropertyChanged();
            }
        }

        public float FilterMaxBandWidthkHz
        {
            get => _FilterMaxBandWidthkHz;
            set
            {
                if (_FilterMaxBandWidthkHz == value)
                    return;
                _FilterMaxBandWidthkHz = value;
                OnPropertyChanged();
            }
        }

        public int PreSelValue
        {
            get => _PreSelValue;
            set
            {
                if (_PreSelValue == value)
                    return;
                _PreSelValue = value;
                OnPropertyChanged();
            }
        }

        public Channel DefaultChannel
        {
            get => _DefaultChannel;
            set
            {
                if (_DefaultChannel == value)
                    return;
                _DefaultChannel = value;
                OnPropertyChanged();
            }
        }

        public int PreselCmdDelay
        {
            get => _PreselCmdDelay;
            set
            {
                if (_PreselCmdDelay == value)
                    return;
                _PreselCmdDelay = value;
                OnPropertyChanged();
            }
        }

        public int PreselCmdSetDelay
        {
            get => _PreselCmdSetDelay;
            set
            {
                if (_PreselCmdSetDelay == value)
                    return;
                _PreselCmdSetDelay = value;
                OnPropertyChanged();
            }
        }

        public int GetAutoCorrFuncDelay
        {
            get => _GetAutoCorrFuncDelay;
            set
            {
                if (_GetAutoCorrFuncDelay == value)
                    return;
                _GetAutoCorrFuncDelay = value;
                OnPropertyChanged();
            }
        }

        public float CorrThreshold
        {
            get => _CorrThreshold;
            set
            {
                if (_CorrThreshold == value)
                    return;
                _CorrThreshold = value;
                OnPropertyChanged();
            }
        }

        public int HMax
        {
            get => _HMax;
            set
            {
                if (_HMax == value)
                    return;
                _HMax = value;
                OnPropertyChanged();
            }
        }

    }

    public interface IMethod<T> where T : class
    {
        T Clone();
        void Update(T data);
        bool Compare(T data);
    }

    public class EndPointConnection : INotifyPropertyChanged, IMethod<EndPointConnection>
    {

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string ipAdress;// = "127.0.0.1";
        private int port;

        #endregion

        #region Properties

        public EndPointConnection()
        {
            ipAdress = "127.0.0.1"; //127.0.0.1
            port = 10000;
        }

        public EndPointConnection(int port)
        {
            ipAdress = "127.0.0.1"; //127.0.0.1
            this.port = port;
        }

        [NotifyParentProperty(true)]
        [DisplayName("IP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get { return ipAdress; }
            set
            {
                if (ipAdress == value)
                    return;
                ipAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1, 1000000)]
        public int Port
        {
            get { return port; }
            set
            {
                if (port == value)
                    return;
                port = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return ipAdress + " : " + port;
        }

        #region IMethod

        public EndPointConnection Clone()
        {
            return new EndPointConnection()
            {
                IpAddress = this.ipAdress,
                Port = this.port
            };
        }

        public void Update(EndPointConnection endPoint)
        {
            IpAddress = endPoint.IpAddress;
            Port = endPoint.Port;
        }

        public bool Compare(EndPointConnection data)
        {
            if (ipAdress != data.IpAddress || port != data.Port)
                return false;
            return true;
        }

        #endregion
    }

    public class UDPEndPointConnection : INotifyPropertyChanged, IMethod<UDPEndPointConnection>
    {

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private string _myIpAdress;// = "127.0.0.1";
        private int _myPort;

        private string _remoteIpAdress;// = "127.0.0.1";
        private int _remotePort;

        #endregion

        #region Properties

        public UDPEndPointConnection()
        {
            _myIpAdress = "127.0.0.1"; //127.0.0.1
            _myPort = 10000;

            _remoteIpAdress = "127.0.0.1"; //127.0.0.1
            _remotePort = 10001;
        }

        public UDPEndPointConnection(int myPort, int remotePort)
        {
            _myIpAdress = "127.0.0.1"; //127.0.0.1
            this._myPort = myPort;

            _remoteIpAdress = "127.0.0.1"; //127.0.0.1
            this._remotePort = remotePort;
        }

        [NotifyParentProperty(true)]
        [DisplayName("myIP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string myIpAdress
        {
            get { return _myIpAdress; }
            set
            {
                if (_myIpAdress == value)
                    return;
                _myIpAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1, 1000000)]
        public int myPort
        {
            get { return _myPort; }
            set
            {
                if (_myPort == value)
                    return;
                _myPort = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [DisplayName("remoteIP")]
        [Required]
        [RegularExpression(@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string remoteIpAdress
        {
            get { return _remoteIpAdress; }
            set
            {
                if (_remoteIpAdress == value)
                    return;
                _remoteIpAdress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        //[Required]
        [Range(1, 1000000)]
        public int remotePort
        {
            get { return _remotePort; }
            set
            {
                if (_remotePort == value)
                    return;
                _remotePort = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return myIpAdress + " : " + myPort + " / " + remoteIpAdress + " : " + remotePort;
        }

        #region IMethod

        public UDPEndPointConnection Clone()
        {
            return new UDPEndPointConnection()
            {
                myIpAdress = this._myIpAdress,
                myPort = this._myPort,
                remoteIpAdress = this._remoteIpAdress,
                remotePort = this._remotePort
            };
        }

        public void Update(UDPEndPointConnection udpEndPoint)
        {
            myIpAdress = udpEndPoint.myIpAdress;
            myPort = udpEndPoint.myPort;
            remoteIpAdress = udpEndPoint.remoteIpAdress;
            remotePort = udpEndPoint.remotePort;
        }

        public bool Compare(UDPEndPointConnection data)
        {
            if (
            myIpAdress != data.myIpAdress ||
            myPort != data.myPort ||
            remoteIpAdress != data.remoteIpAdress ||
            remotePort != data.remotePort
            )
                return false;
            return true;
        }

        #endregion
    }

    public class ComConnection : INotifyPropertyChanged, IMethod<ComConnection>
    {
        #region Private

        private string comPort;
        private SpeedPorts portSpeed;

        #endregion

        public ComConnection()
        {
            comPort = "COM1";
            PortSpeed = (SpeedPorts)115200;
        }

        #region Properties
        [NotifyParentProperty(true)]
        public string ComPort
        {
            get => comPort;
            set
            {
                if (comPort == value) return;
                comPort = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public SpeedPorts PortSpeed
        {
            get => portSpeed;
            set
            {
                if (portSpeed == value) return;
                portSpeed = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public override string ToString()
        {
            return comPort + " / " + (int)PortSpeed;
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region IMethod

        public ComConnection Clone()
        {
            return new ComConnection()
            {
                ComPort = this.comPort,
                PortSpeed = this.portSpeed,
            };
        }

        public void Update(ComConnection comConnection)
        {
            ComPort = comConnection.ComPort;
            PortSpeed = comConnection.PortSpeed;
        }

        public bool Compare(ComConnection data)
        {
            if (comPort != data.ComPort || PortSpeed != data.PortSpeed)
                return false;
            return true;
        }

        #endregion
    }

    public enum SpeedPorts : int
    {
        [Description("2400")]
        Speed_2400 = 2400,
        [Description("4800")]
        Speed_4800 = 4800,
        [Description("9600")]
        Speed_9600 = 9600,
        [Description("19200")]
        Speed_19200 = 19200,
        [Description("38400")]
        Speed_38400 = 38400,
        [Description("57600")]
        Speed_57600 = 57600,
        [Description("115200")]
        Speed_115200 = 115200
    }

    public enum WorkMode : byte
    {
        LoadSpectrum,
        Emu,
        Work,
        Main,
        Table,
        TableAero,
        TableMix,
        FileLenaTest
    }

    public enum Channel
    {
        Channel1,
        Channel2,
        Channel3,
        Channel4,
        ChannelMax,
        ChannelMedian
    }
}
