﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System.Threading;
using System.Collections.ObjectModel;

namespace DAPServer
{
    public partial class MainWindow
    {
        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;

            (clientDB.Tables[NameTable.TableUAVRes] as ITableUpRecord<TableUAVRes>).OnDeleteRecord += OnDeleteRecord_TableUAVRes;
            (clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord += OnDeleteRecord_TableAeroscope;
        }
       

        async void HandlerConnect(object obj, EventArgs eventArgs)
        {
            //DB connect
            await InitTables2();
        }


        List<TableLocalPoints> tableLocalPoints;
        List<TableRemotePoints> tableRemotePoints;

        List<TableFreqRangesRecon> lTablesFreqRangesRecon = new List<TableFreqRangesRecon>();

        List<TableFreqKnown> lTablesFreqKnowns = new List<TableFreqKnown>();

        List<TableUAVRes> lTablesUAVRes = new List<TableUAVRes>();

        List<TableUAVTrajectory> lTableUAVTrajectory = new List<TableUAVTrajectory>();

        int lastIDTableUAVRes = 0;
        int lastIDTableUAVTrajectory = 0;

        List<TableAeroscope> lTablesAeroscopes = new List<TableAeroscope>();

        List<TableAeroscopeTrajectory> lTableAeroscopeTrajectory = new List<TableAeroscopeTrajectory>();

        int lastIDTablesAeroscopes = 0;
        int lastTableAeroscopeTrajectory = 0;

        List<GlobalProperties> lTableGlobalProperties = new List<GlobalProperties>();

        private async Task InitTables2()
        {
            tableLocalPoints = await clientDB?.Tables[NameTable.TableLocalPoints].LoadAsync<KirasaModelsDBLib.TableLocalPoints>();

            tableRemotePoints = await clientDB?.Tables[NameTable.TableRemotePoints].LoadAsync<KirasaModelsDBLib.TableRemotePoints>();


            lTablesFreqKnowns = await clientDB?.Tables[NameTable.TableFreqKnown].LoadAsync<KirasaModelsDBLib.TableFreqKnown>();
            UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqKnown);

            lTablesFreqRangesRecon = await clientDB?.Tables[NameTable.TableFreqRangesRecon].LoadAsync<KirasaModelsDBLib.TableFreqRangesRecon>();
            UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqRangesRecon);


            lTablesUAVRes = await clientDB?.Tables[NameTable.TableUAVRes].LoadAsync<KirasaModelsDBLib.TableUAVRes>();
            lastIDTableUAVRes = (lTablesUAVRes.Count > 0) ? lTablesUAVRes.Max(x => x.Id) + 1 : lTablesUAVRes.Count;

            Log($"Load data from Db. {(NameTable.TableUAVRes).ToString()} count records - {lTablesUAVRes.Count}");


            lTableUAVTrajectory = await clientDB?.Tables[NameTable.TableUAVTrajectory].LoadAsync<KirasaModelsDBLib.TableUAVTrajectory>();
            lastIDTableUAVTrajectory = (lTableUAVTrajectory.Count > 0) ? lTableUAVTrajectory.Max(x => x.Id) + 1 : lTableUAVTrajectory.Count;

            Log($"Load data from Db. {(NameTable.TableUAVTrajectory).ToString()} count records - {lTableUAVTrajectory.Count}");


            lTablesAeroscopes = await clientDB?.Tables[NameTable.TableАeroscope].LoadAsync<KirasaModelsDBLib.TableAeroscope>();
            //Console.WriteLine($"Load data from Db. {(NameTable.TableАeroscope).ToString()} count records - {table6.Count} \n");
            lastIDTablesAeroscopes = (lTablesAeroscopes.Count > 0) ? lTablesAeroscopes.Max(x => x.Id) + 1 : lTablesAeroscopes.Count;

            lTableAeroscopeTrajectory = await clientDB?.Tables[NameTable.TableАeroscopeTrajectory].LoadAsync<KirasaModelsDBLib.TableAeroscopeTrajectory>();
            //Console.WriteLine($"Load data from Db. {(NameTable.TableАeroscopeTrajectory).ToString()} count records - {table7.Count} \n");
            lastTableAeroscopeTrajectory = (lTableAeroscopeTrajectory.Count > 0) ? lTableAeroscopeTrajectory.Max(x => x.Id) + 1 : lTableAeroscopeTrajectory.Count;

            lTableGlobalProperties = await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<KirasaModelsDBLib.GlobalProperties>();

            InitTrackDefinitions();
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            //DB disconnect
            if (eventArgs.GetMessage != "")
            {
                Console.WriteLine(eventArgs.GetMessage);
            }
            clientDB = null;
        }

        async void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Console.WriteLine($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            //});

            switch (eventArgs.Name)
            {
                case NameTable.TableLocalPoints:
                    tableLocalPoints = new List<TableLocalPoints>(eventArgs.AbstractData.ToList<TableLocalPoints>());
                    InitTrackDefinitions();
                    break;

                case NameTable.TableRemotePoints:
                    tableRemotePoints = new List<TableRemotePoints>(eventArgs.AbstractData.ToList<TableRemotePoints>());
                    break;

                case NameTable.TableFreqKnown:
                    lTablesFreqKnowns = new List<TableFreqKnown>(eventArgs.AbstractData.ToList<TableFreqKnown>());
                    UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqKnown);
                    break;

                case NameTable.TableFreqRangesRecon:
                    lTablesFreqRangesRecon = new List<TableFreqRangesRecon>(eventArgs.AbstractData.ToList<TableFreqRangesRecon>());
                    //ctsMain.Cancel();
                    //await MainTask;
                    UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqRangesRecon);
                    //MainReLoopIfNeeded();
                    break;

                case NameTable.TableUAVRes:
                    lTablesUAVRes = new List<TableUAVRes>(eventArgs.AbstractData.ToList<TableUAVRes>());
                    Log($"lTablesUAVRes.Count(): {lTablesUAVRes.Count}");
                    //lastIDTableUAVRes = lTablesUAVRes.Count();

                    if (lTablesUAVRes.Count == 0) _listTrackAirObject.Clear();

                    break;

                case NameTable.TableUAVTrajectory:
                    lTableUAVTrajectory = new List<TableUAVTrajectory>(eventArgs.AbstractData.ToList<TableUAVTrajectory>());
                    Log($"lTableUAVTrajectory.Count(): {lTableUAVTrajectory.Count}");
                    //lastIDTableUAVTrajectory = lTableUAVTrajectory.Count();
                    break;

                case NameTable.TableАeroscope:
                    lTablesAeroscopes = new List<TableAeroscope>(eventArgs.AbstractData.ToList<TableAeroscope>());
                    //Log($"lTablesAeroscopes.Count(): {lTablesAeroscopes.Count}");
                    break;

                case NameTable.TableАeroscopeTrajectory:
                    lTableAeroscopeTrajectory = new List<TableAeroscopeTrajectory>(eventArgs.AbstractData.ToList<TableAeroscopeTrajectory>());
                    //Log($"lTableAeroscopeTrajectory.Count(): {lTableAeroscopeTrajectory.Count}");
                    break;

                case NameTable.GlobalProperties:
                    lTableGlobalProperties = new List<GlobalProperties>(eventArgs.AbstractData.ToList<GlobalProperties>());
                    Log($"lTableGlobalProperties.Count(): {lTableGlobalProperties.Count}");
                    UpdateTrackDefinitionsParameters(lTableGlobalProperties);
                    break;
            }

        }

        private void OnDeleteRecord_TableUAVRes(object sender, TableUAVRes e)
        {
            lTablesUAVRes.Remove(e);

            var removeIndex = _listTrackAirObject.FindIndex(x => x.ID == e.Id);
            if (removeIndex != -1) 
                _listTrackAirObject.RemoveAt(removeIndex);
        }

        private void OnDeleteRecord_TableAeroscope(object sender, TableAeroscope e)
        {
            lTablesAeroscopes.Remove(e);
        }
       

        void HandlerErrorDataBase(object obj, OperationTableEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                Console.WriteLine(eventArgs.GetMessage);
            });
        }




        private ObservableCollection<TableUAVTrajectory> GetTrajectory(double Freq, List<Coord> Coords)
        {
            ObservableCollection<TableUAVTrajectory> tableUAVTrajectories = new ObservableCollection<TableUAVTrajectory>();

            for (int i = 0; i < Coords.Count; i++)
            {
                tableUAVTrajectories.Add(new TableUAVTrajectory()
                {
                    Time = DateTime.Now,
                    FrequencyKHz = Freq,
                    Coordinates = new KirasaModelsDBLib.Coord()
                    {
                        Altitude = (float)Coords[i].Alt,
                        Latitude = Coords[i].Lat,
                        Longitude = Coords[i].Lon
                    },

                    Elevation = 0,
                    Num = 0
                }) ;
            }
            return tableUAVTrajectories;
        }

        //private void AddTableUAVs(FRS frsNew)
        //{
        //    var record = new TableUAVRes
        //    {
        //        FrequencyKHz = frsNew.Freq,
        //        Elevation = 0,
        //        Coordinates = new KirasaModelsDBLib.Coord()
        //        {
        //            Altitude = (float)frsNew.Coords[frsNew.Coords.Count - 1].Alt,
        //            Latitude = frsNew.Coords[frsNew.Coords.Count - 1].Lat,
        //            Longitude = frsNew.Coords[frsNew.Coords.Count - 1].Lon,
        //        },
        //        BandKHz = 0,
        //        Points = (short)frsNew.Coords.Count,
        //        Note = DateTime.Now.ToString(),
        //        Trajectory = GetTrajectory(frsNew.Coords)
        //    };

        //    clientDB?.Tables[NameTable.TableUAVRes].Add(record);
        //}

        bool randomUAV = false;
        private void AddTableUAVs(int ID, byte State)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = TypeUAVRes.Unknown;

            if (randomUAV)
            {
                switch (random.Next(0, 5))
                {
                    case 0:
                        tempTypeUAVRes = TypeUAVRes.Unknown;
                        break;
                    case 1:
                        tempTypeUAVRes = TypeUAVRes.Lightbridge;
                        break;
                    case 2:
                        tempTypeUAVRes = TypeUAVRes.Ocusinc;
                        break;
                    case 3:
                        tempTypeUAVRes = TypeUAVRes.WiFi;
                        break;
                    case 4:
                        tempTypeUAVRes = TypeUAVRes.G3;
                        break;
                }
            }

            //tempTypeUAVRes = TypeUAVRes.Ocusinc;

            var record = new TableUAVRes
            {
                Type = tempTypeUAVRes,
                Id = ID,
                State = State,
                IsActive = true,
                //Note = DateTime.Now.Millisecond.ToString()
            };

            try { clientDB?.Tables[NameTable.TableUAVRes].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }
        }

        private void AddTableUAVTrajectories(int ID, short num, FRS frsNew)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = 0,
                //Elevation = frsNew.Coords[0].Altitude,
                Num = num,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++
            };

            //recordtraj.Coordinates.Altitude = 255f + (float)(r.NextDouble() * 5f);

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AddTableUAVTrajectories(int ID, short num, FRS frsNew, DateTime dateTime)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                //Elevation = 0,
                Elevation = frsNew.Coords[0].Altitude,
                Num = num,
                Time = dateTime,
                Id = lastIDTableUAVTrajectory++
            };

            recordtraj.Coordinates.Altitude = 0;

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AddTableUAVTrajectoriesWithStrob(int ID, short num, FRS frsNew, DateTime dateTime, (double RadiusA, double RadiusB) Strob)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                //Elevation = 0,
                Elevation = frsNew.Coords[0].Altitude,
                Num = num,
                Time = dateTime,
                Id = lastIDTableUAVTrajectory++,
                RadiusA = (float)Strob.RadiusA,
                RadiusB = (float)Strob.RadiusB
            };

            recordtraj.Coordinates.Altitude = 0;

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void NewAddTableUAVs(FRS frsNew)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = TypeUAVRes.Unknown;

            switch (random.Next(0, 5))
            {
                case 0:
                    tempTypeUAVRes = TypeUAVRes.Unknown;
                    break;
                case 1:
                    tempTypeUAVRes = TypeUAVRes.Lightbridge;
                    break;
                case 2:
                    tempTypeUAVRes = TypeUAVRes.Ocusinc;
                    break;
                case 3:
                    tempTypeUAVRes = TypeUAVRes.WiFi;
                    break;
                case 4:
                    tempTypeUAVRes = TypeUAVRes.G3;
                    break;
            }


            var record = new TableUAVRes
            {
                //Type = 0,
                Type = tempTypeUAVRes,
                //Note = DateTime.Now.ToString(),
                Note = DateTime.Now.Millisecond.ToString(),
                Id = lastIDTableUAVRes++,
                //IsActive = Convert.ToBoolean(lastIDTableUAVRes % 2)
                State = 1
            };

            try { clientDB?.Tables[NameTable.TableUAVRes].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }

            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = record.Id,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = frsNew.Coords[0].Altitude,
                Num = 0,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++
            };

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void NewAddTableUAVsWithStrob(FRS frsNew)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = TypeUAVRes.Unknown;

            switch (random.Next(0, 5))
            {
                case 0:
                    tempTypeUAVRes = TypeUAVRes.Unknown;
                    break;
                case 1:
                    tempTypeUAVRes = TypeUAVRes.Lightbridge;
                    break;
                case 2:
                    tempTypeUAVRes = TypeUAVRes.Ocusinc;
                    break;
                case 3:
                    tempTypeUAVRes = TypeUAVRes.WiFi;
                    break;
                case 4:
                    tempTypeUAVRes = TypeUAVRes.G3;
                    break;
            }


            var record = new TableUAVRes
            {
                //Type = 0,
                Type = tempTypeUAVRes,
                //Note = DateTime.Now.ToString(),
                Note = DateTime.Now.Millisecond.ToString(),
                Id = lastIDTableUAVRes++,
                //IsActive = Convert.ToBoolean(lastIDTableUAVRes % 2)
                State = 1
            };

            try { clientDB?.Tables[NameTable.TableUAVRes].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }

            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = record.Id,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = frsNew.Coords[0].Altitude,
                Num = 0,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++,
                RadiusA = (float)(50 + random.NextDouble() * 100),
                RadiusB = (float)(100 + random.NextDouble() * 100)
            };

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void NewAddTableUAVsWithTypeWithStrobAndHead(FRS frsNew)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = TypeUAVRes.Unknown;

            if (randomUAV)
            {
                switch (random.Next(0, 5))
                {
                    case 0:
                        tempTypeUAVRes = TypeUAVRes.Unknown;
                        break;
                    case 1:
                        tempTypeUAVRes = TypeUAVRes.Lightbridge;
                        break;
                    case 2:
                        tempTypeUAVRes = TypeUAVRes.Ocusinc;
                        break;
                    case 3:
                        tempTypeUAVRes = TypeUAVRes.WiFi;
                        break;
                    case 4:
                        tempTypeUAVRes = TypeUAVRes.G3;
                        break;
                }
            }

            TypeL TypeL = (Convert.ToBoolean(random.Next(0, 2))) ? TypeL.Friend : TypeL.Enemy;
            TypeM TypeM = (Convert.ToBoolean(random.Next(0, 2))) ? TypeM.Drone : TypeM.Unknown;

            var record = new TableUAVRes
            {
                //Type = 0,
                Type = tempTypeUAVRes,
                TypeL = TypeL,
                TypeM = TypeM,
                //Note = DateTime.Now.ToString(),
                Note = DateTime.Now.Millisecond.ToString(),
                Id = lastIDTableUAVRes++,
                //IsActive = Convert.ToBoolean(lastIDTableUAVRes % 2)
                State = 1,
            };

            try { clientDB?.Tables[NameTable.TableUAVRes].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }

            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = record.Id,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = frsNew.Coords[0].Altitude,
                Num = 0,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++,
                RadiusA = (float)(50 + random.NextDouble() * 100),
                RadiusB = (float)(100 + random.NextDouble() * 100),
                Head = (short)random.Next(0, 360)
            };

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void NewAddTableAeroScopes(FRS frsNew)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = TypeUAVRes.Unknown;

            if (randomUAV)
            {
                switch (random.Next(0, 5))
                {
                    case 0:
                        tempTypeUAVRes = TypeUAVRes.Unknown;
                        break;
                    case 1:
                        tempTypeUAVRes = TypeUAVRes.Lightbridge;
                        break;
                    case 2:
                        tempTypeUAVRes = TypeUAVRes.Ocusinc;
                        break;
                    case 3:
                        tempTypeUAVRes = TypeUAVRes.WiFi;
                        break;
                    case 4:
                        tempTypeUAVRes = TypeUAVRes.G3;
                        break;
                }
            }

            var record = new TableAeroscope
            {
                Type = tempTypeUAVRes.ToString(),
                Id = lastIDTablesAeroscopes++,
                SerialNumber = (lastIDTablesAeroscopes - 1).ToString(),
                HomeLatitude = frsNew.Coords[0].Latitude,
                HomeLongitude = frsNew.Coords[0].Longitude,
            };

            try { clientDB?.Tables[NameTable.TableАeroscope].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }

            var recordtraj = new TableAeroscopeTrajectory
            {
                Id = lastTableAeroscopeTrajectory++,
                Num = 0,
                SerialNumber = record.SerialNumber,
                Coordinates = frsNew.Coords[0]
            };
           

            try { clientDB?.Tables[NameTable.TableАeroscopeTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AnotherOneAddTableUAVs(int ID, short num, FRS frsNew)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = 0,
                Num = num,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++
            };

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AnotherOneAddTableUAVsWithStrobAndHead(int ID, short num, FRS frsNew)
        {
            Random random = new Random();
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = 0,
                Num = num,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++,
                RadiusA = (float)(50 + random.NextDouble() * 100),
                RadiusB = (float)(100 + random.NextDouble() * 100),
                Head = (short)random.Next(0, 360)
            };

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AnotherOneAddTableAeroScopes(string ID, short num, FRS frsNew)
        {
            var recordtraj = new TableAeroscopeTrajectory
            {
                Id = lastTableAeroscopeTrajectory++,
                SerialNumber = ID,
                Num = num,
                Coordinates = frsNew.Coords[0]
            };

            try { clientDB?.Tables[NameTable.TableАeroscopeTrajectory].Add(recordtraj); }
            catch { }
        }


        private TableUAVRes GenerateChangeTableUAVs(FRS frsNew, int nufOfRec)
        {
            var record = new TableUAVRes
            {
                Id = nufOfRec,
                //Points = (short)frsNew.Coords.Count,
                Note = DateTime.Now.ToString(),
                //Trajectory = GetTrajectory(frsNew.Freq, frsNew.Coords)
            };
            return record;
        }
        
    }
}
