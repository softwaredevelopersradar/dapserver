﻿using KirasaModelsDBLib;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WpfMapRastr;

namespace DAPServer
{
    public partial class MainWindow
    {
        List<AeroScopeEmulator.Emulator.AeroScope> aeroScopes = new List<AeroScopeEmulator.Emulator.AeroScope>();

        private void LoadDroneFile(object sender, RoutedEventArgs e)
        {
            aeroScopes.Clear();

            string temp = "";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = ".txt|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                temp = openFileDialog.FileName;
            }

            try
            {
                aeroScopes = AeroScopeEmulator.Emulator.GetSamples(temp);
            }
            catch { }
        }

        private int countAero = 0;

        private List<Station> DefaultStations()
        {
            //Заполенение листа координат станций и их задержек
            List<Station> listStations = new List<Station>();

            Station station0 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.9312,
                    Long: 27.635555556,
                    Alt: 15,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 15
                    ),
                ErrorTime = 0,
            };
            station0.Position.ID = 0;
            listStations.Add(station0);

            Station station1 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.930997222,
                    Long: 27.6349,
                    Alt: 23.01,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 15
                    ),
                ErrorTime = 1E-9,
            };
            station1.Position.ID = 1;
            listStations.Add(station1);

            Station station2 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.931444444,
                    Long: 27.636638889,
                    Alt: 22.63,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 15
                    ),
                ErrorTime = 1E-9,
            };
            station2.Position.ID = 2;
            listStations.Add(station2);

            Station station3 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.932327778,
                    Long: 27.63485,
                    Alt: 14.86,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 15
                    ),
                ErrorTime = 1E-9,
            };
            station3.Position.ID = 3;
            listStations.Add(station3);

            return listStations;
        }

        private async void LenaTestLoop(CancellationToken token)
        {
            DispatchIfNecessary(() =>
            {
                //Станции по умолчанию
                if (cbDS.IsChecked.Value)
                {
                    //Заполенение листа координат станций и их задержек
                    List<Station> listStations = new List<Station>();

                    Station station0 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.9312,
                            Long: 27.635555556,
                            Alt: 15,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 0,
                    };
                    station0.Position.ID = 0;
                    listStations.Add(station0);

                    Station station1 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.930997222,
                            Long: 27.6349,
                            Alt: 23.01,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 1E-9,
                    };
                    station1.Position.ID = 1;
                    listStations.Add(station1);

                    Station station2 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.931444444,
                            Long: 27.636638889,
                            Alt: 22.63,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 1E-9,
                    };
                    station2.Position.ID = 2;
                    listStations.Add(station2);

                    Station station3 = new Station()
                    {
                        Position = new MarkCoord(
                            Lat: 53.932327778,
                            Long: 27.63485,
                            Alt: 14.86,
                            LatRLSBase: 53.9312,
                            LongRLSBase: 27.635555556,
                            AltRLSBase: 15
                            ),
                        ErrorTime = 1E-9,
                    };
                    station3.Position.ID = 3;
                    listStations.Add(station3);

                    _tracksDefinition.listStations = listStations;
                }
                //Параметры по умолчанию
                if (cbDP.IsChecked.Value)
                {
                    // Максимально допустимое время ожидания отметки
                    _tracksDefinition.twait = 5; //сек
                    // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                    _tracksDefinition.twaitAirObject = 5; //сек
                    // Допустимая скорость
                    _tracksDefinition.Vdop = 40; //м/с
                    // СКО ускорения на плоскости
                    _tracksDefinition.CKOA_pl = 0.12; // м/с*с
                    // СКО ускорения по высоте
                    _tracksDefinition.CKOA_H = 0.8; // м/с*с
                    // СКО по координатам для формирования матрицы D 
                    // ошибок измерения в данный момент времени
                    _tracksDefinition.CKO_X = 30; //м
                    _tracksDefinition.CKO_Y = 15; //м
                    _tracksDefinition.CKO_Z = 30; //м
                    // Количество точек для инициализации фильтра
                    _tracksDefinition.numbInit = 10;
                    // использовать динамическую матрицу ошибок измерений
                    _tracksDefinition.flagStatDyn = false;
                    // Отсеивание по высоте
                    _tracksDefinition.Hmax = settings.HMax;
                }
            });

            Dictionary<int, int> IDcountTrajOld = new Dictionary<int, int>();

            Dictionary<int, int> IDcountTrajNew = new Dictionary<int, int>();

            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("LenaTestLoop прерван токеном");
                    return;
                }

                if (countAero == aeroScopes.Count)
                {
                    countAero = 0;
                    clientDB.Tables[NameTable.TableUAVRes].Clear();
                    clientDB.Tables[NameTable.TableUAVTrajectory].Clear();

                    _listTrackAirObject.Clear();
                }

                float x = aeroScopes[countAero].latitude; //широта
                float z = aeroScopes[countAero].longitude; //долгота
                float y = (float)aeroScopes[countAero].altitude; // высота

                double time = aeroScopes[countAero].time / 1000d;
                //time = time * 4;

                int Ti = countAero;
                int Ti1 = countAero + 1;
                if (Ti1 == aeroScopes.Count)
                    Ti1 = 0;

                int delay = Math.Abs(aeroScopes[Ti1].time - aeroScopes[Ti].time);
                if (delay > 10000) delay = 5000;


                InputPointTrack inputPointTrack = new InputPointTrack()
                { 
                    LatMark = x,
                    LongMark = z,
                    HMark = y,
                    Time = time,
                    Freq = (float)(2412000d + r.NextDouble() * 1000d),
                    dFreq = 10000f + (float)(r.NextDouble() * 12f),
                    _time = DateTime.Now
                };
                _tracksDefinition.f_TracksDefinition(inputPointTrack);
                //_tracksDefinition.f_TracksDefinition(x, z, y, time);
                //_tracksDefinition.f_TracksDefinition(x, z, y, delay);


                //new
                IDcountTrajNew.Clear();
                for (int w = 0; w < _listTrackAirObject.Count(); w++)
                {
                    //IDcountTrajNew.Add(_listTrackAirObject[w].ID, _listTrackAirObject[w].Marks.Count());
                    IDcountTrajNew.Add(_listTrackAirObject[w].ID, _listTrackAirObject[w].Marks.Where(mark => mark.Skip == false).Count());
                }

                //Если изменилось кол-во Дронов
                if (IDcountTrajOld.Count != IDcountTrajNew.Count)
                {
                    int dif = IDcountTrajNew.Count - IDcountTrajOld.Count;

                    for (int q = 0; q < dif; q++)
                    {
                        //получаем нужный ID для записи
                        int getID = IDcountTrajNew.ElementAt(IDcountTrajNew.Count - (dif - q)).Key;

                        //получаем по этому ID нужную запись из листа Лены
                        var record = _listTrackAirObject.Where(el => el.ID == getID).FirstOrDefault();

                        //Пишем в базу источник
                        AddTableUAVs(
                            ID: record.ID,
                            State: record.State
                            );

                        var skipMarks = record.Marks.Where(mark => mark.Skip == false).ToList();

                        //Пишем все его избранные траектории
                        for (int v = 0; v < skipMarks.Count(); v++)
                        {
                            FRS tempFRS = new FRS();
                            tempFRS.FreqkHz = skipMarks[v].CoordPaint.Freq;
                            tempFRS.BandWidthkHz = skipMarks[v].CoordPaint.dFreq;
                            tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                            tempFRS.Coords.Add
                                (new KirasaModelsDBLib.Coord()
                                {
                                    Latitude = skipMarks[v].CoordPaint.Latitude,
                                    Longitude = skipMarks[v].CoordPaint.Longitude,
                                    Altitude = (float)skipMarks[v].CoordPaint.Altitude
                                }
                                );

                            AddTableUAVTrajectories
                                (ID: record.ID,
                                num: (short)v,
                                tempFRS,
                                skipMarks[v].CoordPaint._time
                                );
                        }

                        //Пишем все его траектории
                        {
                            //for (int v = 0; v < record.Marks.Count(); v++)
                            //{
                            //    FRS tempFRS = new FRS();
                            //    tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                            //    tempFRS.Coords.Add
                            //        (new KirasaModelsDBLib.Coord()
                            //        {
                            //            Latitude = record.Marks[v].CoordReal.Latitude,
                            //            Longitude = record.Marks[v].CoordReal.Longitude,
                            //            Altitude = (float)record.Marks[v].CoordReal.Altitude
                            //        }
                            //        );

                            //    AddTableUAVTrajectories
                            //        (ID: record.ID,
                            //        num: (short)v,
                            //        tempFRS
                            //        );
                            //}
                        }
                    }
                }
                else
                {
                    //получаем нужный ID для записи
                    if (IDcountTrajOld.Count > 0 || IDcountTrajNew.Count > 0)
                    {
                        List<int> getIDs = Search4Changes();
                        for (int q = 0; q < getIDs.Count(); q++)
                        {
                            //получаем по этому ID нужную запись из листа Лены
                            var record = _listTrackAirObject.Where(el => el.ID == getIDs[q]).FirstOrDefault();

                            var skipMarks = record.Marks.Where(mark => mark.Skip == false).ToList();

                            //Дописываем в базу крайнюю избранную точку его траектории
                            if (record != null && skipMarks.Count > 0)
                            {
                                int v = skipMarks.Count - 1;

                                var lastMark = skipMarks[skipMarks.Count - 1];

                                FRS tempFRS = new FRS();
                                tempFRS.FreqkHz = lastMark.CoordPaint.Freq;
                                tempFRS.BandWidthkHz = lastMark.CoordPaint.dFreq;
                                tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                                tempFRS.Coords.Add
                                    (new KirasaModelsDBLib.Coord()
                                    {
                                        Latitude = lastMark.CoordPaint.Latitude,
                                        Longitude = lastMark.CoordPaint.Longitude,
                                        Altitude = (float)lastMark.CoordPaint.Altitude
                                    }
                                    );

                                AddTableUAVTrajectories
                                    (ID: record.ID,
                                    num: (short)v,
                                    tempFRS,
                                    lastMark.CoordPaint._time
                                    );
                            }

                            //Дописываем в базу крайнюю точку его траектории
                            {
                                //if (record != null && record.Marks.Count > 0)
                                //{
                                //    int v = record.Marks.Count - 1;

                                //    var lastMark = record.Marks[record.Marks.Count - 1];

                                //    FRS tempFRS = new FRS();
                                //    tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                                //    tempFRS.Coords.Add
                                //        (new KirasaModelsDBLib.Coord()
                                //        {
                                //            Latitude = lastMark.CoordReal.Latitude,
                                //            Longitude = lastMark.CoordReal.Longitude,
                                //            Altitude = (float)lastMark.CoordReal.Altitude
                                //        }
                                //        );

                                //    AddTableUAVTrajectories
                                //        (ID: record.ID,
                                //        num: (short)v,
                                //        tempFRS
                                //        );
                                //}
                            }
                        }
                    }
                    List<int> Search4Changes()
                    {
                        List<int> temp = new List<int>();
                        for (int l = 0; l < IDcountTrajOld.Count(); l++)
                        {
                            if (IDcountTrajOld.ElementAt(l).Value != IDcountTrajNew.ElementAt(l).Value)
                            {
                                temp.Add(IDcountTrajOld.ElementAt(l).Key);
                            }
                        }
                        return temp;
                    }
                }

                //TestDictionaries(IDcountTrajOld, IDcountTrajNew);

                IDcountTrajOld.Clear();
                IDcountTrajOld = new Dictionary<int, int>(IDcountTrajNew);
                IDcountTrajNew.Clear();


                //await Task.Delay(delay);
                await Task.Delay((int)time);

                countAero++;
            }
        }

        Dictionary<int, int> IDcountTrajOldForMain = new Dictionary<int, int>();
        Dictionary<int, int> IDcountTrajNewForMain = new Dictionary<int, int>();
        private void LenaForMain()
        {
            //new
            IDcountTrajNewForMain.Clear();
            for (int w = 0; w < _listTrackAirObject.Count(); w++)
            {
                IDcountTrajNewForMain.Add(_listTrackAirObject[w].ID, _listTrackAirObject[w].Marks.Where(mark => mark.Skip == false).Count());
            }

            //Если изменилось кол-во Дронов
            if (IDcountTrajOldForMain.Count != IDcountTrajNewForMain.Count)
            {
                int dif = IDcountTrajNewForMain.Count - IDcountTrajOldForMain.Count;

                for (int q = 0; q < dif; q++)
                {
                    //получаем нужный ID для записи
                    int getID = IDcountTrajNewForMain.ElementAt(IDcountTrajNewForMain.Count - (dif - q)).Key;

                    //получаем по этому ID нужную запись из листа Лены
                    var record = _listTrackAirObject.Where(el => el.ID == getID).FirstOrDefault();
                    //Получаем по этой записи строб
                    (double RadiusA, double RadiusB) Strob = (record.StrobX, record.StrobZ);

                    //Пишем в базу источник
                    AddTableUAVs(
                        ID: record.ID,
                        State: record.State
                        );

                    var skipMarks = record.Marks.Where(mark => mark.Skip == false).ToList();

                    //Пишем все его избранные траектории
                    for (int v = 0; v < skipMarks.Count(); v++)
                    {
                        FRS tempFRS = new FRS();
                        tempFRS.FreqkHz = skipMarks[v].CoordPaint.Freq;
                        tempFRS.BandWidthkHz = skipMarks[v].CoordPaint.dFreq;
                        tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                        tempFRS.Coords.Add
                            (new KirasaModelsDBLib.Coord()
                            {
                                Latitude = skipMarks[v].CoordPaint.Latitude,
                                Longitude = skipMarks[v].CoordPaint.Longitude,
                                Altitude = (float)skipMarks[v].CoordPaint.Altitude
                            }
                            );

                        //AddTableUAVTrajectories
                        //    (ID: record.ID,
                        //    num: (short)v,
                        //    tempFRS,
                        //    skipMarks[v].CoordPaint._time
                        //    );

                        //Пишем его траекторию и добавляем к ней строб
                        AddTableUAVTrajectoriesWithStrob
                            (ID: record.ID,
                            num: (short)v,
                            tempFRS,
                            skipMarks[v].CoordPaint._time,
                            Strob
                            );
                    }
                }
            }
            else
            {
                //получаем нужный ID для записи
                if (IDcountTrajOldForMain.Count > 0 || IDcountTrajNewForMain.Count > 0)
                {
                    List<int> getIDs = Search4Changes();
                    for (int q = 0; q < getIDs.Count(); q++)
                    {
                        //получаем по этому ID нужную запись из листа Лены
                        var record = _listTrackAirObject.Where(el => el.ID == getIDs[q]).FirstOrDefault();
                        //Получаем по этой записи строб
                        (double RadiusA, double RadiusB) Strob = (record.StrobX, record.StrobZ);

                        var skipMarks = record.Marks.Where(mark => mark.Skip == false).ToList();

                        //Дописываем в базу крайнюю избранную точку его траектории
                        if (record != null && skipMarks.Count > 0)
                        {
                            int v = skipMarks.Count - 1;

                            var lastMark = skipMarks[skipMarks.Count - 1];

                            FRS tempFRS = new FRS();
                            tempFRS.FreqkHz = lastMark.CoordPaint.Freq;
                            tempFRS.BandWidthkHz = lastMark.CoordPaint.dFreq;
                            tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                            tempFRS.Coords.Add
                                (new KirasaModelsDBLib.Coord()
                                {
                                    Latitude = lastMark.CoordPaint.Latitude,
                                    Longitude = lastMark.CoordPaint.Longitude,
                                    Altitude = (float)lastMark.CoordPaint.Altitude
                                }
                                );

                            //AddTableUAVTrajectories
                            //    (ID: record.ID,
                            //    num: (short)v,
                            //    tempFRS,
                            //    lastMark.CoordPaint._time
                            //    );
                            //Дописываем в базу крайнюю избранную точку его траектории и добавляем к ней строб
                            AddTableUAVTrajectoriesWithStrob
                               (ID: record.ID,
                               num: (short)v,
                               tempFRS,
                               lastMark.CoordPaint._time,
                               Strob
                               );
                        }
                    }
                }
                List<int> Search4Changes()
                {
                    List<int> temp = new List<int>();
                    for (int l = 0; l < IDcountTrajOldForMain.Count(); l++)
                    {
                        if (IDcountTrajOldForMain.ElementAt(l).Value != IDcountTrajNewForMain.ElementAt(l).Value)
                        {
                            temp.Add(IDcountTrajOldForMain.ElementAt(l).Key);
                        }
                    }
                    return temp;
                }
            }

            //TestDictionaries(IDcountTrajOldForMain, IDcountTrajNewForMain);

            IDcountTrajOldForMain.Clear();
            IDcountTrajOldForMain = new Dictionary<int, int>(IDcountTrajNewForMain);
            IDcountTrajNewForMain.Clear();
        }

        private void TestDictionaries(Dictionary<int, int> IDcountTrajOld, Dictionary<int, int> IDcountTrajNew)
        {

            for (int o = 0; o < IDcountTrajOld.Count(); o++)
            {
                if (IDcountTrajNew.ContainsKey(o))
                    if (IDcountTrajNew[o] - IDcountTrajNew[o] > 1)
                        Console.Beep();
            }

            //проверяем различие по кол-ву
            if (IDcountTrajOld.Count == 0 && IDcountTrajNew.Count > 0)
            {
                for (int w = IDcountTrajOld.Count(); w < IDcountTrajNew.Count(); w++)
                {
                    slog($"Была добавленна запись {w} и в ней {IDcountTrajNew[w]} точек");
                }
            }
            if (IDcountTrajOld.Count > 0 && IDcountTrajNew.Count > 0)
                if (IDcountTrajOld.Count != IDcountTrajNew.Count)
                {
                    //Есть различие по кол-ву
                    slog($"Изменилось кол-во! До:{IDcountTrajOld.Count} После:{IDcountTrajNew.Count}");

                    for (int w = 0; w < IDcountTrajNew.Count(); w++)
                    {
                        //проверяем различие по кол-ву точек в каждом
                        if (IDcountTrajOld[w] != IDcountTrajNew[w])
                        {
                            //Кол-во точек в записи w не равно
                            slog($"Кол-во точек в записи {w} не равно, До:{ IDcountTrajOld[w]} После: {IDcountTrajNew[w]}");
                        }
                        else
                        {
                            slog($"Кол-во точек в записи {w} равно, До:{ IDcountTrajOld[w]} После: {IDcountTrajOld[w]}");
                        }
                    }

                    for (int w = IDcountTrajOld.Count(); w < IDcountTrajNew.Count(); w++)
                    {
                        slog($"Была добавленна запись {w} и в ней {IDcountTrajNew[w]} точек");
                    }
                }
                else
                {
                    //Нет различия по кол-ву
                    for (int w = 0; w < IDcountTrajNew.Count(); w++)
                    {
                        //проверяем различие по кол-ву точек в каждом
                        if (IDcountTrajOld[w] != IDcountTrajNew[w])
                        {
                            //Кол-во точек в записи w не равно
                            slog($"Кол-во точек в записи {w} не равно, До:{ IDcountTrajOld[w]} После: {IDcountTrajNew[w]}");
                        }
                        else
                        {
                            slog($"Кол-во точек в записи {w} равно, До:{ IDcountTrajOld[w]} После: {IDcountTrajOld[w]}");
                        }
                    }

                    for (int w = IDcountTrajOld.Count(); w < IDcountTrajNew.Count(); w++)
                    {
                        slog($"Была добавленна запись {w} и в ней {IDcountTrajNew[w]} точек");
                    }
                }

        }


    }
}
