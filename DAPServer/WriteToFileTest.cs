﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System.Threading;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.IO.Compression;
using CompressFile;
using Microsoft.Win32;

namespace DAPServer
{
    public partial class MainWindow
    {
        List<byte> lData = new List<byte>();

        private void record0(object sender, RoutedEventArgs e)
        {
            var fullFilePathDotExp = GetFullFilePathDotExp("File", "Record", ".txt");

            int temp = 0;

            int EPO = 0;
            float[] Amplitudes = new float[8000];
            Amplitudes = Amplitudes.Select(x => x = temp++).ToArray();

            string value = ConvertToString(EPO, Amplitudes);

            WriteToFile(fullFilePathDotExp, value);

            RecordToData(EPO, Amplitudes);

            Example();
        }

        private void record(object sender, RoutedEventArgs e)
        {
            int temp = 0;

            int EPO = 0;
            float[] Amplitudes = new float[8000];
            Amplitudes = Amplitudes.Select(x => x = temp++).ToArray();
            RecordToData(EPO, Amplitudes);
        }

        private async void RecordToData0(int EPO, float[] Amplitudes)
        {
            Record record = new Record(0, EPO, Amplitudes);

            lData.AddRange(record.GetBytes());

            await WriteToCompress(GetFullFilePathDotExp("Compress"), lData.ToArray());

            var result = await ReadFromCompress(GetFullFilePathDotExp("Compress"));

            var result2 = await DecompressFromFileAsync(GetFullFilePathDotExp("Compress"));

            lRecords.Add(Record.Parse(result2));
        }

        private async void RecordToData(int EPO, float[] Amplitudes)
        {
            Record record = new Record(0, EPO, Amplitudes);

            lData.AddRange(record.GetBytes());
            lData.AddRange(record.GetBytes());

            await WriteToCompress(GetFullFilePathDotExp("Compress"), lData.ToArray());

            lRecords.Clear();

            var result2 = await DecompressFromFileAsync(GetFullFilePathDotExp("Compress"));

            int count = result2.Length / Record.BinarySize;

            for (int i = 0; i < count; i++)
            {
                Record temp = Record.Parse(result2, i * Record.BinarySize);
                lRecords.Add(temp);
            }

        }

        private async void WriteToFile(string fullFilePathDotExp, string value)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(fullFilePathDotExp, true, System.Text.Encoding.Default))
                {
                    await sw.WriteLineAsync(value);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private string ConvertToString(int EPO, float[] Amplitudes)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(EPO.ToString());
            for (int i = 0; i < Amplitudes.Count(); i++)
            {
                sb.Append(" " + Amplitudes[i].ToString());
            }

            return sb.ToString();
        }

        private void neMain()
        {
            string sourceFile = "D://test/book.pdf"; // исходный файл
            string compressedFile = "D://test/book.gz"; // сжатый файл
            string targetFile = "D://test/book_new.pdf"; // восстановленный файл

            // создание сжатого файла
            Compress(sourceFile, compressedFile);
            // чтение из сжатого файла
            Decompress(compressedFile, targetFile);

            Console.ReadLine();
        }

        public async Task<FileStream> ReadFromCompress(string compressedFile)
        {
            // поток для чтения из сжатого файла
            using (FileStream sourceStream = new FileStream(compressedFile, FileMode.OpenOrCreate))
            {
                // поток разархивации
                using (GZipStream decompressionStream = new GZipStream(sourceStream, CompressionMode.Decompress))
                {
                    await decompressionStream.CopyToAsync(sourceStream);
                    decompressionStream.Close();

                    sourceStream.Close();
                    return sourceStream;
                }
            }
        }

        private void Example()
        {
            var inputString = "64684684684684";
            int gg = 33;
            byte[] compressed;
            string output;

            using (var outStream = new MemoryStream())
            {
                using (var tinyStream = new GZipStream(outStream, CompressionMode.Compress))
                //using (var mStream = new MemoryStream(Encoding.UTF8.GetBytes(inputString)))
                using (var mStream = new MemoryStream(BitConverter.GetBytes(gg)))
                    mStream.CopyTo(tinyStream);

                compressed = outStream.ToArray();
            }

            // “compressed” now contains the compressed string.
            // Also, all the streams are closed and the above is a self-contained operation.

            using (var inStream = new MemoryStream(compressed))
            using (var bigStream = new GZipStream(inStream, CompressionMode.Decompress))
            using (var bigStreamOut = new MemoryStream())
            {
                bigStream.CopyTo(bigStreamOut);
                output = Encoding.UTF8.GetString(bigStreamOut.ToArray());

                var bytes = bigStreamOut.ToArray();
            }

            // “output” now contains the uncompressed string.
            Console.WriteLine(output);

        }

        public void Compress(string sourceFile, string compressedFile)
        {
            // поток для чтения исходного файла
            using (FileStream sourceStream = new FileStream(sourceFile, FileMode.OpenOrCreate))
            {
                // поток для записи сжатого файла
                using (FileStream targetStream = File.Create(compressedFile))
                {
                    // поток архивации
                    using (GZipStream compressionStream = new GZipStream(targetStream, CompressionMode.Compress))
                    {
                        sourceStream.CopyTo(compressionStream); // копируем байты из одного потока в другой
                        Console.WriteLine("Сжатие файла {0} завершено. Исходный размер: {1}  сжатый размер: {2}.",
                            sourceFile, sourceStream.Length.ToString(), targetStream.Length.ToString());
                    }
                }
            }
        }

        public void Decompress(string compressedFile, string targetFile)
        {
            // поток для чтения из сжатого файла
            using (FileStream sourceStream = new FileStream(compressedFile, FileMode.OpenOrCreate))
            {
                // поток для записи восстановленного файла
                using (FileStream targetStream = File.Create(targetFile))
                {
                    // поток разархивации
                    using (GZipStream decompressionStream = new GZipStream(sourceStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(targetStream);
                        Console.WriteLine("Восстановлен файл: {0}", targetFile);
                    }
                }
            }
        }
    }
}
