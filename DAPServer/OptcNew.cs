﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WorkPortNew;


namespace DAPServer
{
    public partial class MainWindow
    {
        PortNew portNew = new PortNew();

        public void InitOptcEventsNew()
        {
            portNew.OnWriteB += Port_OnWriteB;
            portNew.OnReadB += Port_OnReadB;

            portNew.OnPreselectorNumber += Port_OnPreselectorNumber;

            portNew.OnState_00 += Port_OnState_00; //Текущее состояния преселектора 
            portNew.OnFreq_01 += Port_OnFreq_01; //Получение установленной частоты
            portNew.OnGain_02_3b += PortNew_OnGain_02_3b; //Получение установленного усиления 3-х байтная команда
            portNew.OnGain_02_4b += PortNew_OnGain_02_4b; //Получение установленного усиления 4-х байтная команда
            portNew.OnModePreamp_03 += Port_OnModePreamp_03; //Получение установленного режимы работы предусилителя
            portNew.OnOnOffPreamp_04 += PortNew_OnOnOffPreamp_04; //Получения состояния предусилителя Включен/выключен
            portNew.OnSetPreampRange_05 += Port_OnSetPreampRange_05; //Получение настройки предусилителя по номеру диапозона
            portNew.OnSetFreqGain_06 += Port_OnSetFreqGain_06;  //Получение частоты и усиления на этой частоте
            portNew.OnModeExternalSwitches_07 += Port_OnModeExternalSwitches_07; // Получение режима работы внешних коммутаторов
            portNew.OnRFChannel_08 += Port_OnRFChannel_08; //Ответ от системы  коммутации с внешними коммутаторами 
            portNew.OnGain_09_3b += PortNew_OnGain_09_3b;  //Получение уровня аттенюатора предусилителя 3-х байтная команда
            portNew.OnGain_09_4b += PortNew_OnGain_09_4b; //Получение уровня аттенюатора предусилителя 4-х байтная команда
            portNew.OnAttenuatorLevelLow_0A += Port_OnAttenuatorLevelLow_0A; // Получение уровня аттенюатора предусилителя в нижнем диапозоне
            portNew.OnAttenuatorLevelHigh_0B += Port_OnAttenuatorLevelHigh_0B; // Получение уровня аттенюатора предусилителя в верхнем диапозоне
            portNew.OnFeedChannel_0E += Port_OnFeedChannel_0E; // Получение состояния питания для внешних коммутаторов
            portNew.OnOnOffSwitch_0F += Port_OnOnOffSwitch_0F;
            portNew.OnOnOffOpticalTransmitter_10 += Port_OnOnOffOpticalTransmitter_10; // Получение состояния питания для оптического передатчика
            portNew.OnModeAttenuator_11 += Port_OnModeAttenuator_11; // Получение режима работы аттенюатора в канале предусилителя
            portNew.OnConfigurationChanels_12 += Port_OnConfigurationChanels_12; // Получение конфигурирования каналов на внешних коммутаторах
            portNew.OnSave_13 += Port_OnSave_13;
            portNew.OnReset_14 += Port_OnReset_14;
            portNew.OnGain_15 += Port_OnGain_15;
            portNew.OnAdditionalState_16 += Port_OnAdditionalState_16;
            portNew.OnFeedLaser_17 += Port_OnFeedLaser_17;
        }

       

        private void Port_OnWriteB(object sender, string e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnReadB(object sender, string e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnPreselectorNumber(object sender, PortNew.PreselectorNumber e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnState_00(object sender, StateEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnFreq_01(object sender, short e)
        {
            throw new NotImplementedException();
        }

        private void PortNew_OnGain_02_3b(object sender, byte e)
        {
            throw new NotImplementedException();
        }

        private void PortNew_OnGain_02_4b(object sender, GainEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnModePreamp_03(object sender, PortNew.ModePreamp e)
        {
            throw new NotImplementedException();
        }

        private void PortNew_OnOnOffPreamp_04(object sender, PortNew.PreselectorOnOff e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnSetPreampRange_05(object sender, PortNew.SetPreampRange e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnSetFreqGain_06(object sender, FreqGainEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnModeExternalSwitches_07(object sender, PortNew.ModePreamp e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnRFChannel_08(object sender, PortNew.RFChannel e)
        {
            throw new NotImplementedException();
        }

        private void PortNew_OnGain_09_3b(object sender, byte e)
        {
            throw new NotImplementedException();
        }

        private void PortNew_OnGain_09_4b(object sender, GainEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnAttenuatorLevelLow_0A(object sender, byte e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnAttenuatorLevelHigh_0B(object sender, byte e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnFeedChannel_0E(object sender, PortNew.FeedChannels e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnOnOffSwitch_0F(object sender, PortNew.OnOffSwitch e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnOnOffOpticalTransmitter_10(object sender, PortNew.OnOffSwitch e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnModeAttenuator_11(object sender, PortNew.ModePreamp e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnConfigurationChanels_12(object sender, short[] e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnSave_13(object sender, bool e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnReset_14(object sender, bool e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnGain_15(object sender, byte e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnAdditionalState_16(object sender, AdditionalStateEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Port_OnFeedLaser_17(object sender, PortNew.OnOffSwitch e)
        {
            throw new NotImplementedException();
        }

    }
}
