﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;

namespace Filter3DEstimation
{
    /// <summary>
    /// Хранит в себе настройки фильтра
    /// </summary>
    static class TTFSettings
    {
        public static double SigmaThreeError = 8;  // default 8  // 3sigma ~ ns
        public static double AccelerationError = 4;  // default 4  // СКО шума
        public static double DelthaTimeMax = 3;  // default 3     // время устраревания\накопления отсчетов возможной трассы
        public static double DelthaTimeMaxReset = 3;  // default 3       // масимальное время сброса трассы
        public static double NMarkMinimal = 5; // default 5       // Минимально необходимое количество точек для завязки трассы
    }

    /// <summary>
    /// Класс, который рассчитывает и фильтрует входные задержки.
    /// </summary>
    class ThreeTauFilter
    {
        //System.Diagnostics.Stopwatch clock = new System.Diagnostics.Stopwatch();  // счетчик временной

        private int mainIndex = -1;
        private int currentSample = -1;

        private bool state1 = false;
        private bool state2 = false;
        private double[,] Psi_previous = new double[6, 6];
        private double[,] S_previous = new double[6, 1];

        private List<List<double[]>> Tracks_Old = new List<List<double[]>>();
        private List<List<double[]>> Tracks_Actual = new List<List<double[]>>();
        private List<double> currentTimeOffset_Old = new List<double>();
        private List<double> currentTimeOffset_Actual = new List<double>();
        private List<double[]> currentTimeStamps = new List<double[]>();
        private List<List<double>> TimeSamples_Old = new List<List<double>>();
        private List<List<double>> TimeSamples_Actual = new List<List<double>>();

        private List<double[,]> acquiredPsi = new List<double[,]>();
        private List<double[,]> acquiredS = new List<double[,]>();

        private double[,] H_relationship_matrix = new double[3, 6]
            {
            { 1, 0, 0, 0, 0, 0},
            { 0, 0, 1, 0, 0, 0},
            { 0, 0, 0, 0, 1, 0}
            };

        /// <summary>
        /// Возвращает количество сопровождаемых трасс по задержкам
        /// </summary>
        /// <returns></returns>
        public int GetActualTraces()
        {
            return Tracks_Actual.Count;
        }

        /// <summary>
        /// Возвращает количество завязываемых трасс.
        /// </summary>
        /// <returns></returns>
        public int GetOldTraces()
        {
            return Tracks_Old.Count;
        }

        /// <summary>
        /// Расчет фильтрованных задержек.
        /// </summary>
        /// <param name="time">Время прихода текущей отметки</param>
        /// <param name="tau1">Задержка пары корреляторов 12</param>
        /// <param name="tau2">Задержка пары корреляторов 13</param>
        /// <param name="tau3">Задержка пары корреляторов 14</param>
        /// <param name="ttfSettings">Состоит из [SigmaThreeError, AccelerationError, DelthaTimeMax, DelthaTimeMaxReset, NMarkMinimal]</param>
        /// <returns></returns>
        public TauContainer AcquireFilteredValues(double time, double tau1, double tau2, double tau3)
        {
            double R_distance_i_k = 6 * TTFSettings.SigmaThreeError;

            currentSample++;
            state1 = false;
            state2 = false;

            bool isExtrapolated = false;
            bool condition0 = false;
            bool condition1 = false;
            bool condition2 = false;
            bool condition3 = false;

            //double res_d1 = 0;
            double res_d1 = tau1;
            //double res_d2 = 0;
            double res_d2 = tau2;
            //double res_d3 = 0;            
            double res_d3 = tau3;

            //Logger.print("\n----Прием новой отметки--------------------------");
            //Logger.print("Текущий номер отметки: " + currentSample);
            //Logger.print("Текущее время приема отметки: " + time);
            //Logger.print($"Вектор пришедших задержек [{tau1}, {tau2}, {tau3}]");

            //Logger.print($"Количество возможных начал трасс = {Tracks_Old.Count}");
            //Logger.print($"Количество трасс = {Tracks_Actual.Count}");

            if (Tracks_Actual.Count > 0)
            {
                //Logger.print("Этап идентификации с существующими трассами:");
                //этап идентификации с существующими трассами

                //контейнер, хранящий значения экстраполированных задержек
                List<double[,]> container_Sn = new List<double[,]>();
                for (int cnid = 0; cnid < Tracks_Actual.Count; cnid++)
                {
                    container_Sn.Add(new double[6, 1]);
                }
                //список, хранящий информацию о трассах, значения задержек которых не совпадают по одной из них с пришедшими
                List<double[]> containerExtrapolatedVAlues = new List<double[]>();

                for (int j = 0; j < Tracks_Actual.Count; j++)
                {
                    //время, рассчитанное в соответствии с учетом обнуления временной метки для каждой трассы
                    double recalculateTime = Math.Abs(time - currentTimeOffset_Actual[j]);
                    //время необходимое для сравнения с постоянной максимального времени сброса трассы
                    var deltha_Tmax_reset_tracking_time = Math.Abs(recalculateTime - TimeSamples_Actual[j].Last());

                    //Logger.print($"   Первый элемент трассы, время: {TimeSamples_Actual[j].First()}; метка: {currentTimeOffset_Actual[j]}, разница между текущим временем и меткой составляет {recalculateTime}");
                    //Logger.print($"   разница по времени между пришедшим отсчетом и последним отсчетом в трассе {j} равна {deltha_Tmax_reset_tracking_time}");

                    if (deltha_Tmax_reset_tracking_time > TTFSettings.DelthaTimeMaxReset/* || Math.Abs(recalculateTime - currentTimeStamps[j].Min()) > DelthaTimeMaxReset*/)
                    {
                        //Logger.print($"      Превышено время максимального сброса трассы: {DelthaTimeMaxReset} c. Удаление трассы {j}, сброс индекса.");

                        mainIndex = -1;

                        //Logger.print("------------------------");
                        Tracks_Actual.RemoveAt(j);
                        TimeSamples_Actual.RemoveAt(j);
                        currentTimeOffset_Actual.RemoveAt(j);
                        acquiredS.RemoveAt(j);
                        acquiredPsi.RemoveAt(j);

                        j--;

                        continue;
                    }

                    //инициализация матриц

                    double time_matrix_element = recalculateTime - TimeSamples_Actual[j].Last();

                    double[,] dynamic_commutation_matrix = new double[6, 6]
                    {
                        { 1, time_matrix_element, 0, 0, 0, 0},
                        { 0,                   1, 0, 0, 0, 0},
                        { 0, 0, 1, time_matrix_element, 0, 0},
                        { 0, 0, 0,                   1, 0, 0},
                        { 0, 0, 0, 0, 1, time_matrix_element},
                        { 0, 0, 0, 0, 0,                   1}
                    };

                    double[,] dynamic_perturbation_matrix = new double[6, 3]
                        {
                            { Math.Pow(time_matrix_element, 2)/2, 0, 0},
                            {                time_matrix_element, 0, 0},
                            { 0, Math.Pow(time_matrix_element, 2)/2, 0},
                            { 0,                time_matrix_element, 0},
                            { 0, 0, Math.Pow(time_matrix_element, 2)/2},
                            { 0, 0,                time_matrix_element}
                        };
                    //случайные гауссовские: матожид + дисперсия*случчисло
                    double[,] random_acceleration_matrix = new double[3, 1]
                    {
                        { 0 },
                        { 0 },
                        { 0 }
                    };

                    //переопределение массива со случайными гауссовскими значениями
                    /*
                    Random rand = new Random();
                    for (int i = 0; i < 3; i++)
                    {                  
                        double u1 = 1.0 - rand.NextDouble();
                        double u2 = 1.0 - rand.NextDouble();
                        double randStdNormal = Math.Sqrt(-2 * Math.Log(u1)) * Math.Sin(2.0 * Math.PI * u2);
                        random_acceleration_matrix[i,0] = 0 + AccelerationError * randStdNormal;
                    }
                    */

                    double[,] Y_calculated_delay = new double[3, 1]
                        {
                            {tau1 },
                            {tau2 },
                            {tau3 }
                        };

                    double[,] Da_dispersion_acceleration_matrix = new double[6, 6]
                    {
                        { Math.Pow(TTFSettings.AccelerationError,2), 0, 0, 0, 0, 0},
                        { 0, Math.Pow(TTFSettings.AccelerationError,2), 0, 0, 0, 0},
                        { 0, 0, Math.Pow(TTFSettings.AccelerationError,2), 0, 0, 0},
                        { 0, 0, 0, Math.Pow(TTFSettings.AccelerationError,2),0 , 0},
                        { 0, 0, 0, 0, Math.Pow(TTFSettings.AccelerationError,2), 0},
                        { 0, 0, 0, 0, 0, Math.Pow(TTFSettings.AccelerationError,2)}
                    };

                    double[,] D_prior_meas_errors = new double[3, 3]
                        {
                            { Math.Pow(TTFSettings.SigmaThreeError, 2), 0, 0},
                            { 0, Math.Pow(TTFSettings.SigmaThreeError, 2), 0},
                            { 0, 0, Math.Pow(TTFSettings.SigmaThreeError, 2)}
                        };

                    //конец блока инициализации матриц

                    //Logger.print($"    -номер рабочей траектории: {j}; Количество элементов в рабочей траектории: {Tracks_Actual[j].Count}");
                    //Logger.print("       -Обновление соответствующего фильтра сопровождения:");
                    //обновление соответствующего фильтра сопровождения

                    //расчет экстраполированного значения состояния траектории в текущий момент времени
                    var Se_extrapol_vector0 = MatrixEqualizer.Sum(MatrixEqualizer.Multiplication(dynamic_commutation_matrix, acquiredS[j]), MatrixEqualizer.Multiplication(dynamic_perturbation_matrix, random_acceleration_matrix));
                    container_Sn[j] = Se_extrapol_vector0;

                    var deltha_Se_tau1 = Math.Abs(Se_extrapol_vector0[0, 0] - tau1);
                    var deltha_Se_tau2 = Math.Abs(Se_extrapol_vector0[2, 0] - tau2);
                    var deltha_Se_tau3 = Math.Abs(Se_extrapol_vector0[4, 0] - tau3);

                    bool dt1bool = deltha_Se_tau1 <= R_distance_i_k;
                    bool dt2bool = deltha_Se_tau2 <= R_distance_i_k;
                    bool dt3bool = deltha_Se_tau3 <= R_distance_i_k;

                    condition0 = dt1bool && dt2bool && dt3bool;
                    condition1 = !dt1bool && dt2bool && dt3bool;
                    condition2 = dt1bool && !dt2bool && dt3bool;
                    condition3 = dt1bool && dt2bool && !dt3bool;

                    List<bool> conditionsChoice = new List<bool>()
                    {
                        condition0,
                        condition1,
                        condition2,
                        condition3
                    };

                    int currentCondition = -1;

                    for (int con = 0; con < conditionsChoice.Count; con++)
                    {
                        //Logger.print($"     -условие: {con}; результат: {conditionsChoice[con]}");

                        if (conditionsChoice[con])
                        {
                            currentCondition = con;
                            break;
                        }
                    }

                    //Logger.print($"\n      deltha_Se_tau = [{deltha_Se_tau1}, {deltha_Se_tau2}, {deltha_Se_tau3}], " +
                    //                $"\n      tauExtrapol_N-1 = [{acquiredS[j][0, 0]},{acquiredS[j][2, 0]},{acquiredS[j][4, 0]}]" +
                    //                $"\n      tauExtrapol_N = [{Se_extrapol_vector0[0, 0]},{Se_extrapol_vector0[2, 0]},{Se_extrapol_vector0[4, 0]}]\n");

                    // расчет экстраполированной корреляционной матрицы ошибок оценки траектории
                    var multiplication_Fn_PsiN1 = MatrixEqualizer.Multiplication(dynamic_commutation_matrix, acquiredPsi[j]);
                    var multiplication_mult_Fn_PsiN1_FnT = MatrixEqualizer.Multiplication(multiplication_Fn_PsiN1, MatrixEqualizer.Transpose(dynamic_commutation_matrix));

                    var multiplication_Gn_GnT = MatrixEqualizer.Multiplication(dynamic_perturbation_matrix, MatrixEqualizer.Transpose(dynamic_perturbation_matrix));
                    var multiplication_mult_Gn_GnT_Da = MatrixEqualizer.Multiplication(multiplication_Gn_GnT, Da_dispersion_acceleration_matrix);

                    var PsiE_extra_corr_matrix = MatrixEqualizer.Sum(multiplication_mult_Fn_PsiN1_FnT, multiplication_mult_Gn_GnT_Da); //n 11

                    // расчет коэффициента усиления фильтра Кn на основе пси, Н и D
                    var multiplication_PsiE_HT = MatrixEqualizer.Multiplication(PsiE_extra_corr_matrix, MatrixEqualizer.Transpose(H_relationship_matrix));
                    var multiplication_H_PsiE = MatrixEqualizer.Multiplication(H_relationship_matrix, PsiE_extra_corr_matrix);
                    var multiplication_mult_H_PsiE_HT = MatrixEqualizer.Multiplication(multiplication_H_PsiE, MatrixEqualizer.Transpose(H_relationship_matrix));
                    var sum_mult_H_PsiE_HT_D = MatrixEqualizer.Sum(multiplication_mult_H_PsiE_HT, D_prior_meas_errors);
                    var reversed_sum_mult_H_PsiE_HT_D = sum_mult_H_PsiE_HT_D.Inverse();

                    var Kn_coefficient = MatrixEqualizer.Multiplication(multiplication_PsiE_HT, reversed_sum_mult_H_PsiE_HT_D); //n 12
                    //Logger.print($"        коэффициент усиления фильтра, пара12: {Kn_coefficient[0, 0]}, {Kn_coefficient[1, 0]}");

                    //расчет оценки Sn 
                    var multiplication_H_Se = MatrixEqualizer.Multiplication(H_relationship_matrix, Se_extrapol_vector0);
                    var difference_Y_multiplication_H_Se = MatrixEqualizer.Diff(Y_calculated_delay, multiplication_H_Se);
                    var multiplication_Kn_diff_Y_mult_H_Se = MatrixEqualizer.Multiplication(Kn_coefficient, difference_Y_multiplication_H_Se);

                    var Sn_estimation = MatrixEqualizer.Sum(Se_extrapol_vector0, multiplication_Kn_diff_Y_mult_H_Se);//n 13

                    //Расчет корреляционной матрицы ошибок оценки траектории
                    var multiplication_Kn_mult_H_PsiE = MatrixEqualizer.Multiplication(Kn_coefficient, multiplication_H_PsiE);
                    var PsiN_error_matrix = MatrixEqualizer.Diff(PsiE_extra_corr_matrix, multiplication_Kn_mult_H_PsiE);//n 14

                    //Logger.print($"             текущие задержки: tau1 = {tau1}, tau2 = {tau2}, tau3 = {tau3}");
                    //Logger.print($"          фильтрованные задержки: tau1' = {Sn_estimation[0, 0]}, tau2' = {Sn_estimation[2, 0]}, tau3' = {Sn_estimation[4, 0]}");

                    double d1 = 0, d2 = 0, d3 = 0;

                    //обновление сопровождаемой трассы и получение задержек фильтрованных
                    switch (currentCondition)
                    {
                        case 0:
                            d1 = Sn_estimation[0, 0];
                            d2 = Sn_estimation[2, 0];
                            d3 = Sn_estimation[4, 0];

                            //обновление векторов экстраполированных значений. ОТЛАДИТЬ
                            acquiredS[j] = Sn_estimation;
                            acquiredPsi[j] =
                                PsiN_error_matrix;
                            //PsiE_extra_corr_matrix;

                            mainIndex = j;

                            //Logger.print($"           Используем фильтрованные значения.");

                            //Logger.print("+++++++++++++++++++++++");
                            Tracks_Actual[j].Add(new double[3] { d1, d2, d3 });
                            TimeSamples_Actual[j].Add(recalculateTime);

                            //Logger.print($"            -Этап идентификации пройден, mainindex = {mainIndex}");
                            //Logger.print($"              результативный вектор задержек: tau1' = {d1}, tau2' = {d2}, tau3' = {d3}");
                            //Logger.print("----Конец работы--------------------------\n");

                            return new TauContainer 
                            { 
                                tau12 = d1, 
                                tau13 = d2, 
                                tau14 = d3 
                            };

                        case 1:
                            //d1 = acquiredS[j][0, 0];
                            d1 = Se_extrapol_vector0[0, 0];
                            d2 = Sn_estimation[2, 0];
                            d3 = Sn_estimation[4, 0];

                            //Logger.print($"           Используем экстраполированное значение для задержки номер 1");

                            acquiredS[j] = Sn_estimation;
                            acquiredS[j][0, 0] = Se_extrapol_vector0[0, 0];
                            acquiredS[j][1, 0] = Se_extrapol_vector0[1, 0];

                            acquiredPsi[j] =
                                PsiN_error_matrix;
                            //PsiE_extra_corr_matrix;

                            acquiredPsi[j][0, 0] = PsiE_extra_corr_matrix[0, 0];
                            acquiredPsi[j][1, 0] = PsiE_extra_corr_matrix[1, 0];

                            //Logger.print("+++++++++++++++++++++++");
                            //Tracks_Actual[j].Add(new double[3] { d1, d2, d3 });
                            // TimeSamples_Actual[j].Add(recalculateTime);

                            //res_d1 = d1;
                            //res_d2 = d2;
                            //res_d3 = d3;

                            containerExtrapolatedVAlues.Add(new double[6] { j, recalculateTime, d1, d2, d3, currentCondition });

                            isExtrapolated = true;

                            break;
                        case 2:
                            d1 = Sn_estimation[0, 0];
                            //d2 = acquiredS[j][2, 0];
                            d2 = Se_extrapol_vector0[2, 0];
                            d3 = Sn_estimation[4, 0];

                            //Logger.print($"           Используем экстраполированное значение для задержки номер 2");

                            acquiredS[j] = Sn_estimation;
                            acquiredS[j][2, 0] = Se_extrapol_vector0[2, 0];
                            acquiredS[j][3, 0] = Se_extrapol_vector0[3, 0];
                            acquiredPsi[j] =
                                PsiN_error_matrix;
                            //PsiE_extra_corr_matrix;
                            acquiredPsi[j][2, 0] = PsiE_extra_corr_matrix[2, 0];
                            acquiredPsi[j][3, 0] = PsiE_extra_corr_matrix[3, 0];

                            //Logger.print("+++++++++++++++++++++++");
                            //Tracks_Actual[j].Add(new double[3] { d1, d2, d3 });
                            // TimeSamples_Actual[j].Add(recalculateTime);

                            //res_d1 = d1;
                            //res_d2 = d2;
                            //res_d3 = d3;

                            containerExtrapolatedVAlues.Add(new double[6] { j, recalculateTime, d1, d2, d3, currentCondition });

                            isExtrapolated = true;

                            break;
                        case 3:
                            d1 = Sn_estimation[0, 0];
                            d2 = Sn_estimation[2, 0];
                            d3 = Se_extrapol_vector0[4, 0];
                            //d3 = acquiredS[j][4, 0];

                            //Logger.print($"           Используем экстраполированное значение для задержки номер 3");

                            acquiredS[j] = Sn_estimation;
                            acquiredS[j][4, 0] = Se_extrapol_vector0[4, 0];
                            acquiredS[j][5, 0] = Se_extrapol_vector0[5, 0];
                            acquiredPsi[j] =
                                PsiN_error_matrix;
                            //PsiE_extra_corr_matrix;
                            acquiredPsi[j][4, 0] = PsiE_extra_corr_matrix[4, 0];
                            acquiredPsi[j][5, 0] = PsiE_extra_corr_matrix[5, 0];

                            //Tracks_Actual[j].Add(new double[3] { d1, d2, d3 });
                            //TimeSamples_Actual[j].Add(recalculateTime);

                            containerExtrapolatedVAlues.Add(new double[6] { j, recalculateTime, d1, d2, d3, currentCondition });

                            isExtrapolated = true;

                            break;
                        default:
                            break;
                    }

                    //Logger.print($"                -трасса {j} не попадает в строб.");
                }

                if (isExtrapolated && containerExtrapolatedVAlues.Count != 0)
                {
                    double[] minimalDistances = new double[containerExtrapolatedVAlues.Count];
                    int indexOfMinimal = 0;
                    for (int mm = 0; mm < containerExtrapolatedVAlues.Count; mm++)
                    {
                        minimalDistances[mm] = Math.Sqrt(Math.Pow(tau1 - containerExtrapolatedVAlues[mm][2], 2) + Math.Pow(tau2 - containerExtrapolatedVAlues[mm][3], 2) + Math.Pow(tau3 - containerExtrapolatedVAlues[mm][4], 2));
                        if (minimalDistances[indexOfMinimal] > minimalDistances[mm])
                        {
                            indexOfMinimal = mm;
                        }
                    }

                    res_d1 = containerExtrapolatedVAlues[indexOfMinimal][2];
                    res_d2 = containerExtrapolatedVAlues[indexOfMinimal][3];
                    res_d3 = containerExtrapolatedVAlues[indexOfMinimal][4];



                    Tracks_Actual[(int)containerExtrapolatedVAlues[indexOfMinimal][0]].Add(new double[3] { res_d1, res_d2, res_d3 });
                    TimeSamples_Actual[(int)containerExtrapolatedVAlues[indexOfMinimal][0]].Add(containerExtrapolatedVAlues[indexOfMinimal][1]);

                    switch (containerExtrapolatedVAlues[indexOfMinimal].Last())
                    {
                        case 1:
                            currentTimeStamps[(int)containerExtrapolatedVAlues[indexOfMinimal][0]][1] = containerExtrapolatedVAlues[indexOfMinimal][1];
                            currentTimeStamps[(int)containerExtrapolatedVAlues[indexOfMinimal][0]][2] = containerExtrapolatedVAlues[indexOfMinimal][1];
                            break;
                        case 2:
                            currentTimeStamps[(int)containerExtrapolatedVAlues[indexOfMinimal][0]][0] = containerExtrapolatedVAlues[indexOfMinimal][1];
                            currentTimeStamps[(int)containerExtrapolatedVAlues[indexOfMinimal][0]][2] = containerExtrapolatedVAlues[indexOfMinimal][1];
                            break;
                        case 3:
                            currentTimeStamps[(int)containerExtrapolatedVAlues[indexOfMinimal][0]][0] = containerExtrapolatedVAlues[indexOfMinimal][1];
                            currentTimeStamps[(int)containerExtrapolatedVAlues[indexOfMinimal][0]][1] = containerExtrapolatedVAlues[indexOfMinimal][1];
                            break;

                    }


                    //Logger.print($"_Обноwление трассы номер {(int)containerExtrapolatedVAlues[indexOfMinimal][0]}, отсчет {currentSample}_");
                }

                if (Tracks_Actual.Count != 0 && !isExtrapolated)
                {

                    int inx = mainIndex == -1 ? 0 : mainIndex;

                    res_d1 = container_Sn[inx][0, 0];
                    res_d2 = container_Sn[inx][2, 0];
                    res_d3 = container_Sn[inx][4, 0];

                    //Logger.print($"_Задействована подстановка трех экстраполированных значений, отсчет {currentSample}_");
                    Tracks_Actual[inx].Add(new double[3] { res_d1, res_d2, res_d3 });
                    TimeSamples_Actual[inx].Add(time - currentTimeOffset_Actual[inx]);
                }
            }

            //Logger.print("{{{");

            FindingNewTracesStage(time, R_distance_i_k, tau1, tau2, tau3);

            //Logger.print("}}}");

            //Logger.print($"__Возвращаем результирующий вектор: {res_d1}, {res_d2}, {res_d3}");
            //Logger.print("----Конец работы--------------------------\n");

            //выдача результатов по запросу о сопровождаемых трассах
            return new TauContainer 
            { 
                tau12 = res_d1, 
                tau13 = res_d2, 
                tau14 = res_d3 
            };

        }

        /// <summary>
        /// Этап поиска новых трасс.
        /// </summary>
        /// <param name="time">Время прихода текущей отметки</param>
        /// <param name="Rij">Строб 3 сигма</param>
        /// <param name="tau1">Задержка корреляционной пары 12</param>
        /// <param name="tau2">Задержка корреляционной пары 13</param>
        /// <param name="tau3">Задержка корреляционной пары 14</param>
        private void FindingNewTracesStage(double time, double Rij, double tau1, double tau2, double tau3)
        {

            //Logger.print($".Этап поиска новых трасс");

            if (Tracks_Old.Count == 0)
            {
                mainIndex = -1;
                //Logger.print("   -Начало работы. Отметок 0.");
                Tracks_Old.Add(new List<double[]> { new double[3] { tau1, tau2, tau3 } });
                TimeSamples_Old.Add(new List<double>() { 0 });
                currentTimeOffset_Old.Add(time);
                //Logger.print($"    ДОБАВЛЕНИЕ {Tracks_Old.IndexOf(Tracks_Old.Last())}-ой ВОЗМОЖНОЙ ТРАЕКТОРИИ, время завязки трассы: {time}");
            }
            else
            {
                for (int k = 0; k < Tracks_Old.Count; k++)
                {
                    double recalculatedTime = Math.Abs(time - currentTimeOffset_Old[k]);
                    double deltha_t_ki = Math.Abs(TimeSamples_Old[k][0] - recalculatedTime);

                    //Logger.print($"    номер возможной трассы: {k}, deltha_t_ki = {deltha_t_ki}");

                    if (deltha_t_ki > TTFSettings.DelthaTimeMax)
                    {
                        //Logger.print($"      deltha_t_ki > {DelthaTimeMax}, устаревание.");
                        if (Tracks_Old[k].Count < TTFSettings.NMarkMinimal)
                        {
                            //Logger.print($"         Количество элементов в {k}-ой возможной трассе: {Tracks_Old[k].Count}, Количество элементов в трассе меньше, чем {NMarkMinimal}");
                            //Logger.print("------------------------");
                            Tracks_Old[k].RemoveAt(0);
                            TimeSamples_Old[k].RemoveAt(0);

                            if (Tracks_Old[k].Count == 0)
                            {
                                //Logger.print($"           -Удаление пустой возможной трассы {k}");
                                Tracks_Old.RemoveAt(k);
                                TimeSamples_Old.RemoveAt(k);
                                currentTimeOffset_Old.RemoveAt(k);
                                k--;
                            }
                            else
                            {
                                //обновление массива currentTimeOffset_Old
                                double diffusor = TimeSamples_Old[k].First();
                                currentTimeOffset_Old[k] += diffusor;

                                for (int kl = 0; kl < TimeSamples_Old[k].Count; kl++)
                                {
                                    TimeSamples_Old[k][kl] -= diffusor;
                                }
                            }
                        }
                        else
                        {
                            //Инициализация траекторного фильтра по фиксированной выборке данных объемом N_mark

                            //Logger.print($"      Количество элементов в трассе равняется {Tracks_Old[k].Count} > {NMarkMinimal}, Инициализация траекторного фильтра по фиксированной выборке данных объемом {Tracks_Old[k].Count}");

                            //Вектор измерений 3N*1
                            double[,] Y_dimension_vec = new double[3 * Tracks_Old[k].Count, 1];
                            int op = 0;
                            for (int p = 0; p < 3; p++)
                            {
                                for (int j = 0; j < Tracks_Old[k].Count; j++)
                                {
                                    Y_dimension_vec[op++, 0] = Tracks_Old[k][j][p];
                                }
                            }

                            //Матрица Вандермота 3N*6
                            double[,] A_Vandermoth = new double[3 * Tracks_Old[k].Count, 6];

                            for (int n1 = 0; n1 < 3; n1++)
                            {
                                for (int n2 = 0; n2 < Tracks_Old[k].Count; n2++)
                                {
                                    if (n1 == 0)
                                    {
                                        A_Vandermoth[n2 + n1 * Tracks_Old[k].Count, 0] = 1;
                                        A_Vandermoth[n2 + n1 * Tracks_Old[k].Count, 1] = TimeSamples_Old[k][n2];
                                    }
                                    else if (n1 == 1)
                                    {
                                        A_Vandermoth[n2 + n1 * Tracks_Old[k].Count, 2] = 1;
                                        A_Vandermoth[n2 + n1 * Tracks_Old[k].Count, 3] = TimeSamples_Old[k][n2];
                                    }
                                    else if (n1 == 2)
                                    {
                                        A_Vandermoth[n2 + n1 * Tracks_Old[k].Count, 4] = 1;
                                        A_Vandermoth[n2 + n1 * Tracks_Old[k].Count, 5] = TimeSamples_Old[k][n2];
                                    }
                                }
                            }

                            //Матрица ошибок измерения 3N*3N
                            double[,] R0_matrix_estimation_error = new double[3 * TimeSamples_Old[k].Count, 3 * TimeSamples_Old[k].Count];

                            for (int j = 0; j < 3 * TimeSamples_Old[k].Count; j++)
                            {
                                for (int p = 0; p < 3 * TimeSamples_Old[k].Count; p++)
                                {
                                    R0_matrix_estimation_error[j, p] = (j == p) ? Math.Pow(TTFSettings.SigmaThreeError, 2) : 0;
                                }
                            }


                            //расчет корреляционной матрицы ошибок оценок параметров траектории 6*6 
                            var multiplication_At_R01 = MatrixEqualizer.Multiplication(MatrixEqualizer.Transpose(A_Vandermoth), R0_matrix_estimation_error.Inverse());
                            var multiplication_mult_At_R01_A = MatrixEqualizer.Multiplication(multiplication_At_R01, A_Vandermoth);
                            var Psi_matrix_Estimation_Error = multiplication_mult_At_R01_A.Inverse();
                            Psi_previous = Psi_matrix_Estimation_Error;

                            //расчет матрицы оценок параметров траектории, page 15
                            var multiplication_Psi_At = MatrixEqualizer.Multiplication(Psi_matrix_Estimation_Error, MatrixEqualizer.Transpose(A_Vandermoth));
                            var multiplication_mult_Psi_At_R01 = MatrixEqualizer.Multiplication(multiplication_Psi_At, R0_matrix_estimation_error.Inverse());
                            var S_Estimation_characteristics_trajectory_matrix = MatrixEqualizer.Multiplication(multiplication_mult_Psi_At_R01, Y_dimension_vec);
                            S_previous = S_Estimation_characteristics_trajectory_matrix;

                            //переинициализация вектора эксраполированных значений.
                            for (int v = 0; v < 3; v++)
                            {
                                S_previous[v * 2, 0] += S_previous[v * 2 + 1, 0] * deltha_t_ki;
                            }

                            Tracks_Actual.Add(new List<double[]>());
                            TimeSamples_Actual.Add(new List<double>());

                            //Logger.print("+++++++++++++++++++++++");
                            for (int sind = 0; sind < Tracks_Old[k].Count; sind++)
                            {
                                //Logger.print($"Tracks_Old.Count = {Tracks_Old.Count}, Tracks_Old[{k}].Count = {Tracks_Old[k].Count}, Tracks_Old[{k}][{sind}].Count = {Tracks_Old[k][sind].Count()}");
                                Tracks_Actual.Last().Add(Tracks_Old[k][sind]);
                                TimeSamples_Actual.Last().Add(TimeSamples_Old[k][sind]);
                            }

                            currentTimeStamps.Add(new double[3] { TimeSamples_Old[k].Last(), TimeSamples_Old[k].Last(), TimeSamples_Old[k].Last() });
                            currentTimeOffset_Actual.Add(currentTimeOffset_Old[k]);
                            //Logger.print($"      Создание траектории, время создания траектории: {currentTimeOffset_Actual.Last()}");

                            //Logger.print($"      -Создание {Tracks_Actual.Count}-ой ФИЛЬТРОВАННОЙ ТРАЕКТОРИИ.");
                            //Logger.print("------------------------");
                            Tracks_Old.RemoveAt(k);
                            TimeSamples_Old.RemoveAt(k);
                            currentTimeOffset_Old.RemoveAt(k);
                            acquiredPsi.Add(Psi_previous);
                            acquiredS.Add(S_previous);
                            break;
                        }
                    }
                    else
                    {
                        state1 = true;
                        //Logger.print($"      deltha_t_ki < {DelthaTimeMax}");
                        double deltha_tau1 = Math.Abs(tau1 - Tracks_Old[k].Last()[0]);
                        double deltha_tau2 = Math.Abs(tau2 - Tracks_Old[k].Last()[1]);
                        double deltha_tau3 = Math.Abs(tau3 - Tracks_Old[k].Last()[2]);

                        if (deltha_tau1 <= Rij && deltha_tau2 <= Rij && deltha_tau3 <= Rij)
                        {
                            Tracks_Old[k].Add(new double[] { tau1, tau2, tau3 });
                            TimeSamples_Old[k].Add(recalculatedTime);

                            //Logger.print($"         -попадение в строб, ДОБАВЛЕНИЕ ОТСЧЕТОВ В ТРАЕКТОРИЮ {k} tracks_old, количество элементов траектории на данный момент времени: {Tracks_Old[k].Count}");

                            state2 = true;
                            break;
                        }
                    }
                }

                if (state1 && !state2)
                {
                    Tracks_Old.Add(new List<double[]> { new double[] { tau1, tau2, tau3 } });
                    TimeSamples_Old.Add(new List<double> { 0 });
                    currentTimeOffset_Old.Add(time);
                    //Logger.print($"         -не попало в строб, ДОБАВЛЕНИЕ {Tracks_Old.IndexOf(Tracks_Old.Last())}-ой ТРАЕКТОРИИ");
                }

                //для перестраховки
                state1 = false;
                state2 = false;

            }


        }

    }

    /// <summary>
    /// Класс работы с матрицами.
    /// </summary>
    static class MatrixEqualizer
    {
        /// <summary>
        /// Вывод матрицы в консоль.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="msg"></param>
        public static void DisplayMatrix(double[,] m, string msg)
        {
            Logger.print("------" + msg + "------");
            for (int i = 0; i < m.GetLength(0); i++)
            {
                Console.Write($" ");
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    Console.Write($"{m[i, j]} ");
                }
                Console.Write("\n");
            }
            Logger.print("------");
        }
        /// <summary>
        /// Вывод матрицы в консоль.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="msg"></param>
        public static void DisplayMatrixPair(double[][] m, string msg)
        {
            Logger.print("------" + msg + "------");
            for (int i = 0; i < m.Length; i++)
            {
                Console.Write($" ");
                for (int j = 0; j < m[i].Length; j++)
                {
                    Console.Write($"{m[i][j]} ");
                }
                Console.Write("\n");
            }
            Logger.print("------");
        }
        /// <summary>
        /// Произведение матриц.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] Multiplication(double[,] a, double[,] b)
        {
            if (a.GetLength(1) != b.GetLength(0))
            {
                throw new Exception("Matrix multiplication error");
            }

            double[,] resultMultiplication = new double[a.GetLength(0), b.GetLength(1)];

            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < b.GetLength(1); j++)
                {
                    for (int k = 0; k < b.GetLength(0); k++)
                    {
                        resultMultiplication[i, j] += a[i, k] * b[k, j];
                    }
                }
            }
            return resultMultiplication;
        }
        /// <summary>
        /// Транспонирование матрицы.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static double[,] Transpose(double[,] t)
        {
            double[,] resultTransposed = new double[t.GetLength(1), t.GetLength(0)];

            for (int i = 0; i < t.GetLength(1); i++)
            {
                for (int j = 0; j < t.GetLength(0); j++)
                {
                    resultTransposed[i, j] = t[j, i];
                }
            }

            return resultTransposed;
        }
        /// <summary>
        /// Суммирование двух матриц.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] Sum(double[,] a, double[,] b)
        {
            if ((a.GetLength(0) != b.GetLength(0)) || (a.GetLength(1) != b.GetLength(1)))
            {
                throw new Exception("Matrix sum error");
            }

            double[,] resultSummary = new double[a.GetLength(0), b.GetLength(1)];

            for (int i = 0; i < a.GetLength(1); i++)
            {
                for (int j = 0; j < b.GetLength(0); j++)
                {
                    resultSummary[j, i] = a[j, i] + b[j, i];
                }
            }

            return resultSummary;
        }
        /// <summary>
        /// Разность двух матриц.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[,] Diff(double[,] a, double[,] b)
        {
            if ((a.GetLength(0) != b.GetLength(0)) || (a.GetLength(1) != b.GetLength(1)))
            {
                throw new Exception("Matrix diff error");
            }

            double[,] resultDifference = new double[a.GetLength(0), b.GetLength(1)];

            for (int i = 0; i < a.GetLength(1); i++)
            {
                for (int j = 0; j < b.GetLength(0); j++)
                {
                    resultDifference[j, i] = a[j, i] - b[j, i];
                }
            }

            return resultDifference;
        }

        public static double[] VectorDivideByNumber(double[] a, double b)
        {
            if (b == 0)
            {
                throw new Exception("Something goes WRONG at VectorDivideByNumber");
            }

            for (int i = 0; i < a.Length; i++)
            {
                a[i] /= b;
            }

            return a;
        }
            
        public static double[] VectorDiff(double[] a, double[] b)
        {
            if (a.Length != b.Length)
            {
                throw new Exception("Something goes WRONG at vectordiff");
            }

            double[] c = new double[a.Length];

            for (int i = 0; i < a.Length; i++)
            {
                c[i] = a[i] - b[i];
            }

            return c;
        }

        /// <summary>
        /// Произведение вектора строки на вектор столбец.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double Multiplication_VecRow_VecColumn(double[,] a, double[,] b)
        {
            if ((a.GetLength(0) != 1) || (b.GetLength(1) != 1))
            {
                throw new Exception("Something goes WRONG at multiplication vecRow and vecColumn");
            }

            double result = 0;

            for (int i = 0; i < a.GetLength(1); i++)
            {
                result += a[0, i] * b[i, 0];
            }

            return result;
        }
        /// <summary>
        /// Нахождение определителя матрицы 3 степени.
        /// </summary>
        /// <param name="a1"></param>
        /// <param name="a2"></param>
        /// <param name="a3"></param>
        /// <param name="a4"></param>
        /// <param name="a5"></param>
        /// <param name="a6"></param>
        /// <param name="a7"></param>
        /// <param name="a8"></param>
        /// <param name="a9"></param>
        /// <returns></returns>
        public static double determinant3deg(double a1, double a2, double a3,
                                 double a4, double a5, double a6,
                                 double a7, double a8, double a9)
        {
            return a1 * determinant2deg(a5, a6, a8, a9) - a2 * determinant2deg(a4, a6, a7, a9) + a3 * determinant2deg(a4, a5, a7, a8);
        }
        /// <summary>
        /// Нахождение определителя матрицы 2 степени.
        /// </summary>
        /// <param name="a1"></param>
        /// <param name="a2"></param>
        /// <param name="a3"></param>
        /// <param name="a4"></param>
        /// <returns></returns>
        public static double determinant2deg(double a1, double a2,
                                        double a3, double a4)
        {
            return (a1 * a4) - (a2 * a3);
        }
        
    }

}
