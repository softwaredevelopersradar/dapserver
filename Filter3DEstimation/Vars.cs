﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filter3DEstimation
{
    public static class Vars
    {
        /// <summary>
        /// Порог принятия корреляционных кривых на обработку
        /// </summary>
        public static double PassThroughCorrelationThreshold = 0.1;

        /// <summary>
        /// Порог при расчете высшей и низшей границ выборки для аппроксимации по параболе
        /// </summary>
        public static double CoefficientBorderDelayEstimation = 0.5;

        //public static double GlobalDistance = 4222; // m from base
        public static double HeightUpperBoard = 500; // максимальная высота

        /// <summary>
        /// Список станций
        /// </summary>
        public static List<Point> Stations;

        /// <summary>
        /// Список поправочных задержек
        /// </summary>
        public static TauContainer AdditionalCorrelationCorrectionValues = new TauContainer
        {
            tau12 = 0,
            tau13 = 0,
            tau14 = 0
        };

        public static readonly double SpeedOfLight = 299792458;

        /// <summary>
        /// Частота дискретизации приемника
        /// </summary>
        public static readonly double SamplingFrequency = 125e6;

        /// <summary>
        /// Значение задержки в одном Гц в секундах
        /// </summary>
        public static readonly double Delay_1Hz_s = 1 / SamplingFrequency; //При 125е6 значение задержки равно 8е-9 с.

        /// <summary>
        /// Значение задержки в одном Гц в наносекундах
        /// </summary>
        public static readonly double Delay_1Hz_ns = 1e9 / SamplingFrequency; //При 125е6 значение задержки равно 8 нс.

        public static double[] test_counter = new double[5];

        public static bool isPrintAvailable = true;
    }
}
