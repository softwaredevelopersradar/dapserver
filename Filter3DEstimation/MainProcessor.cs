﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fDRM;

namespace Filter3DEstimation
{
    public class MainProcessor
    {
        //private ThreeTauFilter ttf = new ThreeTauFilter();
        //private ThreeTauFilterUPD ttfUPD = new ThreeTauFilterUPD();
        private TauEstimation ten = new TauEstimation();

        public ClassDRM_dll drmPoint;
        public PlaneCalculation2D ffdrm;
        private List<ClassObject> lstClassObject;

        Dictionary<int, ThreeTauFilter> TTFDictionary = new Dictionary<int, ThreeTauFilter>();
        Dictionary<int, GeodesicCoordinates> CCDictionary = new Dictionary<int, GeodesicCoordinates>();

        public MainProcessor()
        {
            drmPoint = new ClassDRM_dll();
            ffdrm = new PlaneCalculation2D();
        }
        /// <summary>
        /// Установка настроек рабочей базы РДМ комплекса.
        /// </summary>
        /// <param name="base_0"></param>
        /// <param name="point_1"></param>
        /// <param name="point_2"></param>
        /// <param name="point_3"></param>
        public void SetBaseConfiguration(Point base_0, Point point_1, Point point_2, Point point_3)
        {
            Logger.print("Установка базы.");

            Vars.Stations = new List<Point>()
            {
                base_0,
                point_1,
                point_2,
                point_3
            };

            CSRecalculation.SphericalTransferToLocalTerestrial_Base(ref Vars.Stations);

            lstClassObject = (new List<ClassObject>() {
                new ClassObject()
                {
                    Latitude = Vars.Stations[0].geodesic.Latitude,
                    Longitude = Vars.Stations[0].geodesic.Longitude,
                    Altitude = Vars.Stations[0].geodesic.Altitude,
                    BaseStation = true,
                    IndexStation = 1
                },
                new ClassObject()
                {
                    Latitude = Vars.Stations[1].geodesic.Latitude,
                    Longitude = Vars.Stations[1].geodesic.Longitude,
                    Altitude = Vars.Stations[1].geodesic.Altitude,
                    BaseStation = false,
                    IndexStation = 2
                },
                new ClassObject()
                {
                    Latitude = Vars.Stations[2].geodesic.Latitude,
                    Longitude = Vars.Stations[2].geodesic.Longitude,
                    Altitude = Vars.Stations[2].geodesic.Altitude,
                    BaseStation = false,
                    IndexStation = 3
                },
                new ClassObject()
                {
                    Latitude = Vars.Stations[3].geodesic.Latitude,
                    Longitude = Vars.Stations[3].geodesic.Longitude,
                    Altitude = Vars.Stations[3].geodesic.Altitude,
                    BaseStation = false,
                    IndexStation = 4
                }
            });

        }

        public void SetHeightApproximationSettings(double approximationAlpha = 0.95, double startHeight = 50, bool isInvertedImaginaryInDRM = true)
        {
            Logger.print($"Настройка высотных коэффициентов: \n\tальфа: {approximationAlpha.ToString("f2")}\n\tначальная высота: {startHeight.ToString("f2")}\n\tинвертация мнимого в DRM: {isInvertedImaginaryInDRM}");
            
            HeightSettings.approximationAlpha = approximationAlpha;
            HeightSettings.approximationBeta = 1 - approximationAlpha;
            HeightSettings.pointHeight = startHeight;
            HeightSettings.isInvertedImaginaryInDRM = isInvertedImaginaryInDRM;
        }

        /// <summary>
        /// Установка значений поправочных коэффициентов корреляции.
        /// </summary>
        public void SetCorrelationCorrectionValues(TauContainer tc)
        {
            Logger.print($"Настройка поправочных коэффициентов:\n\ttau12 = {tc.tau12.ToString("f2")}\n\ttau13 = {tc.tau13.ToString("f2")}\n\ttau14 = {tc.tau14.ToString("f2")}");

            Vars.AdditionalCorrelationCorrectionValues = tc;
        }
        /// <summary>
        /// Устанавливает коэффициенты, необходимые для расчета задержек
        /// </summary>
        /// <param name="PassThroughCorrelationThreshold">Первичная проверка на наличие пиков, превышающих данное значение</param>
        /// <param name="CoefficientBorderDelayEstimation">Коэффициент, учавствующий в нахождении максимума и минимума границ при формировании массивов, которые впоследствие будут аппроксимированы полиномом второй степени.</param>
        public void SetCoefficientThreshold(double PassThroughCorrelationThreshold = 0.15, double CoefficientBorderDelayEstimation = 0.5)
        {
            Logger.print($"Настройка порогов:\n\tпорог корреляционный: {PassThroughCorrelationThreshold.ToString("f2")}\n\tкоэффициент для расчета границы корреляционной выборки: {CoefficientBorderDelayEstimation.ToString("f2")}");

            Vars.PassThroughCorrelationThreshold = PassThroughCorrelationThreshold;
            Vars.CoefficientBorderDelayEstimation = CoefficientBorderDelayEstimation;
            //Vars.CoefficientBorderDelayEstimation = 0.4;
            //Vars.PassThroughCorrelationThreshold = 0.4;
        }

        public void RemoveTTFDictionary_ID(int ID)
        {
            Logger.print($"Удален коррелятор с ID №{ID}");
            TTFDictionary.Remove(ID);
            CCDictionary.Remove(ID);
        }

        public void RemoveTTFDictionary_All()
        {
            Logger.print($"Удалены все корреляторы");
            TTFDictionary.Clear();
            CCDictionary.Clear();
        }

        private ThreeTauFilter CheckAndReturnIDPresenceTTF(int ID)
        {
            if (!TTFDictionary.ContainsKey(ID))
            {
                TTFDictionary.Add(ID, new ThreeTauFilter());
            }

            return TTFDictionary[ID];
        }

        private GeodesicCoordinates CheckAndReturnIDPresenceCC(int ID)
        {
            if (!CCDictionary.ContainsKey(ID))
            {
                return null;
            }

            return CCDictionary[ID];
        }

        public void SetIDPresenceCC(int ID, GeodesicCoordinates exraCoord)
        {
            ID++;

            //Logger.print($"\n\n\t\tЭкстраполированные координаты траекторной обработки:\n\t\t\tID = {ID}\n\t\t\textra.lat = {exraCoord.Latitude}\n\t\t\textra.lon = {exraCoord.Longitude}\n\t\t\textra.alt = {exraCoord.Altitude}\n\n");

            if (!CCDictionary.ContainsKey(ID))
            {
                CCDictionary.Add(ID, new GeodesicCoordinates()
                { 
                    Latitude = exraCoord.Latitude,
                    Longitude = exraCoord.Longitude,
                    Altitude = exraCoord.Altitude
                });
            }
            else
            {
                CCDictionary[ID] = exraCoord;
            }
        }

        public void SetTTFSettings(double SigmaThreeError = 8, double AccelerationError = 4, double DelthaTimeMax = 3, double DelthaTimeMaxReset = 3, double NMarkMinimal = 5)
        {
            Logger.print($"Настройка фильтра задержек:" +
                $"\n\tsigma error = {SigmaThreeError.ToString("f2")}" +
                $"\n\tacceleration error = {AccelerationError.ToString("f2")}" +
                $"\n\tdeltha time maximum = {DelthaTimeMax.ToString("f2")}" +
                $"\n\tdeltha time maximum reset = {DelthaTimeMaxReset.ToString("f2")}" +
                $"\n\tN mark minimum = {NMarkMinimal.ToString("f2")}");

            TTFSettings.SigmaThreeError = SigmaThreeError;
            TTFSettings.AccelerationError = AccelerationError;
            TTFSettings.DelthaTimeMax = DelthaTimeMax;
            TTFSettings.DelthaTimeMaxReset = DelthaTimeMaxReset;
            TTFSettings.NMarkMinimal = NMarkMinimal;
        }
        
        /// <summary>
        /// Запрос на получение фильтрованных задержек.
        /// </summary>
        /// <param name="InputCorrelationSamplesList">Состоит из трех списков значений корреляционных кривых</param>
        /// <param name="ttfSettings">Состоит из [SigmaThreeError, AccelerationError, DelthaTimeMax, DelthaTimeMaxReset, NMarkMinimal]</param>
        /// <param name="inputTime">Время прихода текущей отметки</param>
        public ResultBox Get3DEstimation(int ID, List<List<double>> InputCorrelationSamplesList, double inputTime, bool isPrintAvailable = false)
        {
            Vars.isPrintAvailable = isPrintAvailable;
            //Sample.InputCorrelationSamples = InputCorrelationSamplesList;            
            ResultBox resultBox = new ResultBox();
        
            Vars.test_counter[3]++;

            //проверка на пороги корреляционных функций
            if (ten.CheckAvailabilityCorrelationSamplesToPassThroughThreshold(ten.GetListMaximumValue(InputCorrelationSamplesList)))
            {
                Vars.test_counter[4]++;
                Logger.print($"\nВременная метка: {inputTime.ToString("f2")}");
                
                // Dictionary acq vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                Logger.print("Сопоставление данных в словарях:");

                var currentTTF = CheckAndReturnIDPresenceTTF(ID);
                resultBox.ID = ID;

                var currentExtraPlug = CheckAndReturnIDPresenceCC(ID);
                resultBox.extrapolatedCoordinates = currentExtraPlug;

                Logger.print($"\tID = {ID}");
                if (currentExtraPlug != null)
                {
                    Logger.print($"\tЭкстраполированные координаты:" +
                        $"\n\t\tlat = {currentExtraPlug.Latitude}" +
                        $"\n\t\tlng = {currentExtraPlug.Longitude}" +
                        $"\n\t\talt = {currentExtraPlug.Altitude}");

                    Sample.point.geodesic = currentExtraPlug;
                }
                else
                {
                    Logger.print($"\tЭкстраполированные координаты: null");
                }

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                // Блок расчета задержек vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                Logger.print("\nРасчет задержек.");

                Sample.delayValues_pure = new TauContainer
                {
                    tau12 = ten.GetDelayValue(InputCorrelationSamplesList[0]) + Vars.AdditionalCorrelationCorrectionValues.tau12,
                    tau13 = ten.GetDelayValue(InputCorrelationSamplesList[1]) + Vars.AdditionalCorrelationCorrectionValues.tau13,
                    tau14 = ten.GetDelayValue(InputCorrelationSamplesList[2]) + Vars.AdditionalCorrelationCorrectionValues.tau14
                };

                resultBox.PureTau = new TauContainer
                {
                    tau12 = Sample.delayValues_pure.tau12 * 1e-9,
                    tau13 = Sample.delayValues_pure.tau13 * 1e-9,
                    tau14 = Sample.delayValues_pure.tau14 * 1e-9
                };

                // ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                // Блок фильтрации задержек vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                Logger.print("Фильтрация задержек:");

                Sample.delayValues_filtered = currentTTF.AcquireFilteredValues
                (
                    inputTime,
                    Sample.delayValues_pure.tau12,                       
                    Sample.delayValues_pure.tau13,                       
                    Sample.delayValues_pure.tau14                       
                );
                
                resultBox.FilteredTau = new TauContainer()
                {
                    tau12 = Sample.delayValues_filtered.tau12 * 1e-9,
                    tau13 = Sample.delayValues_filtered.tau13 * 1e-9,
                    tau14 = Sample.delayValues_filtered.tau14 * 1e-9
                };

                Logger.print(
                    $"\ttau12 = {Sample.delayValues_filtered.tau12} ns" +
                    $"\n\ttau13 = {Sample.delayValues_filtered.tau13} ns" +
                    $"\n\ttau14 = {Sample.delayValues_filtered.tau14} ns");

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                // Заполнение входного массива фильтрованными задержками vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                Logger.print("Заполнение входного массива фильтрованными задержками.");

                lstClassObject[0].tau = 0;
                lstClassObject[1].tau = Sample.delayValues_filtered.tau12 * 1e-9;
                lstClassObject[2].tau = Sample.delayValues_filtered.tau13 * 1e-9;
                lstClassObject[3].tau = Sample.delayValues_filtered.tau14 * 1e-9;

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            
                // Блок расчета 3D координат vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                Logger.print("\nРасчет 3D:");

                var tp1 = drmPoint.f_DRM
                    (
                        lstClassObject,
                        !HeightSettings.isInvertedImaginaryInDRM,
                        -1
                    );

                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


                // Проверка на наличие решений
                if ((tp1.Latitude != -1 && tp1.Longitude != -1 && tp1.Altitude != -1) && (tp1.Latitude_2 != -1 && tp1.Longitude_2 != -1 && tp1.Altitude_2 != -1))
                {
                    //есть решения по блоку 3д

                    Logger.print(
                        $"\n\troot 1:" +
                        $"\n\t\tlat = {tp1.Latitude}" +
                        $"\n\t\tlng = {tp1.Longitude}" +
                        $"\n\t\talt = {tp1.Altitude}" +
                        $"\n\troot 2:" +
                        $"\n\t\tlat = {tp1.Latitude_2}" +
                        $"\n\t\tlng = {tp1.Longitude_2}" +
                        $"\n\t\talt = {tp1.Altitude_2}");

                    // Блок выбора одного решения vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                    Logger.print($"\n\tВыбор одного решения:");

                    GeodesicCoordinates solo3Dpoint = Decisions.Make3DPointDecision(tp1);

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                    if (solo3Dpoint != null)
                    {
                        Logger.print(
                            $"\n\t\tlat = {solo3Dpoint.Latitude}" +
                            $"\n\t\tlng = {solo3Dpoint.Longitude}" +
                            $"\n\t\talt = {solo3Dpoint.Altitude}");

                        if (currentExtraPlug == null)
                        {
                            currentExtraPlug = solo3Dpoint;
                            Sample.point.geodesic = solo3Dpoint;
                        }

                        // Блок аппроксимации высоты vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                        Logger.print($"\nАппроксимирование высоты:");

                        double apxHeight = HeightSettings.Approximate(solo3Dpoint.Altitude);
                        solo3Dpoint.Altitude = apxHeight;

                        Logger.print($"\tВысота: {apxHeight}");

                        //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                        resultBox.Coordinates = solo3Dpoint;                        
                    }    
                    else
                    {
                        Logger.print(
                            $"\tnull");
                    }
                }
                else
                {

                    Logger.print(
                        $"\tРешений по блоку расчета 3Д нету.\nРасчет 2D:");
                    /*
                    //Проверка на наличие экстраполированного значения
                    if (currentExtraPlug == null)
                    {
                        Logger.print(
                            $"\t\tЭкстраполированного решения нету, возвращаем null.");
                        return null;
                    }
                    else
                    {

                        Logger.print(
                            $"\t\tЭкстраполированное решение есть - переходим к расчету 2Д.");
                    }*/

                    // Блок выбора точки для расчета угла засечки и его пересчет координат в местную земную систему     vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                    //var pointAngle = Decisions.MakeExtra3DPointDecision(solo3Dpoint, Sample.point.geodesic);

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                    // Блок выбора работающих пар корреляторов увеличенной базы (расчет углов засечки) vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                    Logger.print(
                        $"\n\tРасчет углов засечки.");

                    var temp_tuple = Decisions.MakeCorrelationPairDecision(Vars.Stations, Sample.point);
                    //apx.test(lstClassObject, CurrentSample.coordinatesContainer);

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                    // Блок расчета 2D увеличенной базы  vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
                    
                    Logger.print(
                        $"\n\tТестовый блок расчета 2д по увеличенным базам.");

                    var tp2 = ffdrm.F_Coord_2rdmH(Vars.Stations, temp_tuple.Item1, resultBox.FilteredTau, Sample.point.geodesic);

                    //tp2 = null;

                    //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

                    //Проверка на наличие решений у блока 2Д расчета
                    if (tp2 != null)
                    {
                        Logger.print(
                            $"\t\tПодставляем 2Д решение.");
                        // Если есть решение у блока расчета 2Д
                        resultBox.Coordinates.Latitude = tp2.geodesic.Latitude;
                        resultBox.Coordinates.Longitude = tp2.geodesic.Longitude;
                        resultBox.Coordinates.Altitude = HeightSettings.pointHeight;
                    }
                    else
                    {
                        Logger.print(
                            $"\t\tРешений по блоку 2Д расчета нету.");
                        //
                        
                        resultBox.Coordinates.Latitude = -1;
                        resultBox.Coordinates.Longitude = -1;
                        resultBox.Coordinates.Altitude = -1;                        
                    }

                }
                
                                             
                Logger.print($"\nРезультат: " +
                    $"\nlat = {resultBox.Coordinates.Latitude}" +
                    $"\nlng = {resultBox.Coordinates.Longitude}" +
                    $"\nalt = {resultBox.Coordinates.Altitude}" +
                    $"\n---\n");

                //resultBox.FlagRezCalc = tp2.FlagRezCalc;
                resultBox.FlagRezCalc = 0;

                Logger.print(
                    $"\neverything: {Vars.test_counter[3]}" +
                    $"\nvalid: {Vars.test_counter[4]}" +
                    $"\ncount 23-24: {Vars.test_counter[0]}" +
                    $"\ncount 32-34: {Vars.test_counter[1]}" +
                    $"\ncount 42-43: {Vars.test_counter[2]}"
                    );


                return (resultBox.Coordinates.Latitude == -1 || resultBox.Coordinates.Longitude == -1 || resultBox.Coordinates.Altitude == -1) ? null : resultBox;
            }
            return null;
        }

        //------------------------------------------------------------------------------------------------------------------------------------
        //private
        //-------------------------

        
      
    }
}

