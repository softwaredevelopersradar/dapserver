﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filter3DEstimation
{
    public class Point
    {
        public GeodesicCoordinates geodesic = new GeodesicCoordinates();
        public CartesianCoordinates geocentric = new CartesianCoordinates();
        public CartesianCoordinates localTerestrial = new CartesianCoordinates();        
    }
}
