﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fDRM;

namespace Filter3DEstimation
{
    public static class Decisions
    {


        /// <summary>
        /// Функция выбора работающей корреляционной пары по расчету угла засечки
        /// </summary>
        /// <param name="lstClassObject"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Tuple<int, bool[]> MakeCorrelationPairDecision(List<Point> lstBasePoints, Point point)
        {
            bool[] resFCorrelationPairs;

            //расчет углов засечки

            List<double> notchAngles = new List<double>
            {
                //пары: 23 и 24
                DistanceEstimation.CalculateNotchAngle(
                        lstBasePoints[1],
                        lstBasePoints[2],
                        lstBasePoints[3],
                        point
                    ), 
                //пары: 32 и 34
                DistanceEstimation.CalculateNotchAngle(
                        lstBasePoints[2],
                        lstBasePoints[1],
                        lstBasePoints[3],
                        point
                    ), 
                //пары: 42 и 43
                DistanceEstimation.CalculateNotchAngle(
                        lstBasePoints[3],
                        lstBasePoints[1],
                        lstBasePoints[2],
                        point
                    )
            };

            Logger.print(
                $"\tangle_23-24, sin = {notchAngles[0]}" +
                $"\n\tangle_32-34, sin = {notchAngles[1]}" +
                $"\n\tangle_42-43, sin = {notchAngles[2]}");

            int maxNotchAngleIndex = notchAngles.IndexOf(notchAngles.Max());

            //Logger.print($"Максимальный угол засечки: {notchAngles[maxNotchAngleIndex]}, Индекс угла: {maxNotchAngleIndex}");

            switch (maxNotchAngleIndex)
            {
                case 0:
                    resFCorrelationPairs = new bool[3] { true, false, true };
                    Logger.print($"\nNOTCH. Работают корреляционные пары 23 и 24.");

                    Vars.test_counter[0]++;

                    break;
                case 1:
                    resFCorrelationPairs = new bool[3] { true, true, false };
                    Logger.print($"\nNOTCH. Работают корреляционные пары 32 и 34.");

                    Vars.test_counter[1]++;

                    break;
                case 2:
                    resFCorrelationPairs = new bool[3] { false, true, true };
                    Logger.print($"\nNOTCH. Работают корреляционные пары 42 и 43.");

                    Vars.test_counter[2]++;

                    break;
                default:
                    resFCorrelationPairs = new bool[3] { true, true, true };
                    //Logger.print($"\nNOTCH. Работают три пары.");
                    Logger.print($"\nNOTCH. WRONG.");
                    break;
            }

            //return new bool[3] { true, true, true };
            return Tuple.Create(maxNotchAngleIndex, resFCorrelationPairs);
        }

        /// <summary>
        /// Функция выбора решения после блока расчета 3Д
        /// </summary>
        /// <param name="cot"></param>
        /// <returns></returns>
        public static GeodesicCoordinates Make3DPointDecision(ClassObjectTmp cot)
        {
            GeodesicCoordinates res_cc = new GeodesicCoordinates();

            if (cot.Altitude < 0 && cot.Altitude_2 >= 0)
            {
                res_cc.Latitude = cot.Latitude_2;
                res_cc.Longitude = cot.Longitude_2;
                res_cc.Altitude = cot.Altitude_2;
            }
            else if (cot.Altitude >= 0 && cot.Altitude_2 < 0)
            {
                res_cc.Latitude = cot.Latitude;
                res_cc.Longitude = cot.Longitude;
                res_cc.Altitude = cot.Altitude;
            }
            else if (cot.Altitude >= 0 && cot.Altitude_2 >= 0)
            {
                double currentDelthaXSquare1 = Math.Pow(cot.Latitude - Sample.point.geodesic.Latitude, 2);
                double currentDelthaYSquare1 = Math.Pow(cot.Longitude - Sample.point.geodesic.Longitude, 2);
                double currentDelthaZSquare1 = Math.Pow(cot.Altitude - Sample.point.geodesic.Altitude, 2);
                double distance1 = Math.Sqrt(currentDelthaXSquare1 + currentDelthaYSquare1 + currentDelthaZSquare1);

                double currentDelthaXSquare2 = Math.Pow(cot.Latitude_2 - Sample.point.geodesic.Latitude, 2);
                double currentDelthaYSquare2 = Math.Pow(cot.Longitude_2 - Sample.point.geodesic.Longitude, 2);
                double currentDelthaZSquare2 = Math.Pow(cot.Altitude_2 - Sample.point.geodesic.Altitude, 2);
                double distance2 = Math.Sqrt(currentDelthaXSquare2 + currentDelthaYSquare2 + currentDelthaZSquare2);

                double temp_lat = -1;
                double temp_lng = -1;
                double temp_h = -1;

                if (distance1 <= distance2)
                {
                    temp_lat = cot.Latitude;
                    temp_lng = cot.Longitude;
                    temp_h = cot.Altitude;
                }
                else
                {
                    temp_lat = cot.Latitude_2;
                    temp_lng = cot.Longitude_2;
                    temp_h = cot.Altitude_2;
                }

                res_cc.Latitude = temp_lat;
                res_cc.Longitude = temp_lng;
                res_cc.Altitude = temp_h;

            }
            else
            {
                return null;
            }

            return res_cc;
        }

        public static Point MakeExtra3DPointDecision(GeodesicCoordinates p3D, GeodesicCoordinates pExtrapolated)
        {

            Point p = new Point();

            if ((p3D != null && pExtrapolated != null) || (p3D != null && pExtrapolated == null))
            {
                p.geodesic = p3D;
            }
            else if (p3D == null && pExtrapolated != null)
            {
                p.geodesic = pExtrapolated;
            }
            else
            {
                // sho?
            }

            if (p.geodesic.Altitude != -1)
                CSRecalculation.SphericalTransferToLocalTerestrial_Point(ref p, Vars.Stations);

            return p;
        }

    }
}
