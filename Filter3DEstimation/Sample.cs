﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fDRM;

namespace Filter3DEstimation
{
    public static class Sample
    {
        /// <summary>
        /// Список значений корреляционных кривых
        /// </summary>
        //public static List<List<double>> InputCorrelationSamples = new List<List<double>>();

        /// <summary>
        /// Значения расчетных задержек
        /// </summary>
        public static TauContainer delayValues_pure;

        /// <summary>
        /// Значения фильтрованных задержек
        /// </summary>
        public static TauContainer delayValues_filtered;

        public static Point point = new Point();
    }

    
}
