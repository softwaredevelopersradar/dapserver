﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fDRM;
using GeoCalculator;
using System.Threading.Tasks;

namespace Filter3DEstimation
{
    public class PlaneCalculation2D
    {
        public Point F_Coord_2rdmH(List<Point> lstBasePoints, int intCorrPairs, TauContainer taus, GeodesicCoordinates p3D)
        {
            Logger.print(
                $"\tФормирование увеличенных баз:");

            var mainTuple = MakeActualTuple_main(lstBasePoints, taus, intCorrPairs);

            var os = MakeActualList_3pointBaseExtended(mainTuple.Item3, mainTuple.Item2);
            double Height = HeightSettings.pointHeight - mainTuple.Item1;

            double k1 = ((Math.Pow(os[5], 2) + Math.Pow(os[6], 2) + Math.Pow(os[7], 2)) - (Math.Pow(os[2], 2) + Math.Pow(os[3], 2) + Math.Pow(os[4], 2)) - Math.Pow(Vars.SpeedOfLight * os[0], 2)) / 2;
            double k2 = ((Math.Pow(os[8], 2) + Math.Pow(os[9], 2) + Math.Pow(os[10], 2)) - (Math.Pow(os[2], 2) + Math.Pow(os[3], 2) + Math.Pow(os[4], 2)) - Math.Pow(Vars.SpeedOfLight * os[1], 2)) / 2;

            double a00 = os[1] * (os[2] - os[5]) - os[0] * (os[2] - os[8]);
            double a01 = os[1] * (os[3] - os[6]) - os[0] * (os[3] - os[9]);
            double a02 = os[1] * (os[4] - os[7]) - os[0] * (os[4] - os[10]);
            double a03 = os[1] * k1 - os[0] * k2;

            if (a01 != 0)
            {

                Logger.print(
                    $"\tРасчет координат по увеличенным базам.");

                double M = -a00 / a02;
                double N = -1 * (Height * a01 + a03) / a02;
                double kk1 = (os[2] - os[8]) + M * (os[4] - os[10]);
                double kk0 = N * (os[4] - os[10]) + Height * (os[3] - os[9]) + k2;
                double AA = Math.Pow(kk1, 2) - Math.Pow(Vars.SpeedOfLight * os[1], 2) * (1 + Math.Pow(M, 2));
                double BB = 2 * (kk1 * kk0 - Math.Pow(Vars.SpeedOfLight * os[1], 2) * (M * (N - os[4]) - os[2]));
                double CC = Math.Pow(kk0, 2) - Math.Pow(Vars.SpeedOfLight * os[1], 2) * (Math.Pow(os[2], 2) + Math.Pow(N - os[4], 2) + Math.Pow(Height - os[3], 2));

                double DS = Math.Pow(BB, 2) - 4 * AA * CC;
                
                Logger.print(
                    $"\tПроверка дискриминанта.");

                if (DS >= 0)
                {
                    double temp00 = (-BB - Math.Sqrt(DS)) / (2 * AA);
                    double temp01 = (-BB + Math.Sqrt(DS)) / (2 * AA);
                    double temp10 = N + temp00 * M;
                    double temp11 = N + temp01 * M;
                    double temp20 = Height;
                    double temp21 = Height;
                    
                    double[] alpha1 = new double[]
                    {
                        temp00,
                        temp20,
                        temp10
                    };

                    double[] alpha2 = new double[]
                    {
                        temp01,
                        temp21,
                        temp11
                    };

                    Logger.print(
                        $"\tВыбор решения:");

                    /*
                    ///

                    

                    ///
                    */

                    if (intCorrPairs == 0)
                        Logger.print($"");

                    List<double> dec_os = (os.GetRange(2, os.Count() - 2));

                    double[] d_tau_oc1 = CalculateUdiff(dec_os, alpha1);
                    double[] d_tau_oc2 = CalculateUdiff(dec_os, alpha2);

                    //Logger.print($"\td_tau_oc1_1 = {d_tau_oc1[0]}, dtau_oc1_2 = {d_tau_oc1[1]}");
                    //Logger.print($"\td_tau_oc2_1 = {d_tau_oc2[0]}, dtau_oc2_2 = {d_tau_oc2[1]}");
                    
                    if ((Math.Sign(d_tau_oc1[0]) == Math.Sign(os[0]) && Math.Sign(d_tau_oc1[1]) == Math.Sign(os[1])) && (Math.Sign(d_tau_oc2[0]) == Math.Sign(os[0]) && Math.Sign(d_tau_oc2[1]) == Math.Sign(os[1])))
                    {
                        
                        Point a1 = new Point()
                        {
                            localTerestrial = new CartesianCoordinates()
                            {
                                x = alpha1[0],
                                y = alpha1[2],
                                z = alpha1[1]
                            }
                        };

                        CSRecalculation.LocalTerestrialTransferToSpherical(lstBasePoints, ref a1);

                        Point a2 = new Point()
                        {
                            localTerestrial = new CartesianCoordinates()
                            {
                                x = alpha2[0],
                                y = alpha2[2],
                                z = alpha2[1]
                            }
                        };

                        CSRecalculation.LocalTerestrialTransferToSpherical(lstBasePoints, ref a2);

                        double d1 = DistanceEstimation.GetDistanceBWCoordinates(a1.geodesic, p3D);
                        double d2 = DistanceEstimation.GetDistanceBWCoordinates(a2.geodesic, p3D);

                        if (d1 >= d2)
                        {
                            return a2;
                        }
                        else
                        {
                            return a1;
                        }
                        
                        /*
                        Point a = new Point()
                        {
                            localTerestrial = new CartesianCoordinates()
                            {
                                x = alpha1[0],
                                y = alpha1[2],
                                z = alpha1[1]
                            }
                        };

                        CSRecalculation.LocalTerestrialTransferToSpherical(lstBasePoints, ref a);
                        
                        Logger.print(
                            $"\t\tЕсть два решения по расчету 2Д с увеличенными базами. Выбираем первое решение:" +
                            $"\n\t\t\tlat = {a.spherical.Latitude}" +
                            $"\n\t\t\tlng = {a.spherical.Longitude}" +
                            $"\n\t\t\talt = {a.spherical.Altitude}");

                        return a;
                        */
                    }
                    else if ((Math.Sign(d_tau_oc1[0]) == Math.Sign(os[0]) && Math.Sign(d_tau_oc1[1]) == Math.Sign(os[1])) && (Math.Sign(d_tau_oc2[0]) != Math.Sign(os[0]) || Math.Sign(d_tau_oc2[1]) != Math.Sign(os[1])))
                    {

                        Point a = new Point()
                        {
                            localTerestrial = new CartesianCoordinates()
                            {
                                x = alpha1[0],
                                y = alpha1[2],
                                z = alpha1[1]
                            }
                        };

                        CSRecalculation.LocalTerestrialTransferToSpherical(lstBasePoints, ref a);

                        Logger.print(
                            $"\t\tВыбираем первое решение по расчету 2Д с увеличенными базами. " +
                            $"\n\t\t\tlat = {a.geodesic.Latitude}" +
                            $"\n\t\t\tlng = {a.geodesic.Longitude}" +
                            $"\n\t\t\talt = {a.geodesic.Altitude}");

                        return a;
                    }
                    else if ((Math.Sign(d_tau_oc1[0]) != Math.Sign(os[0]) || Math.Sign(d_tau_oc1[1]) != Math.Sign(os[1])) && (Math.Sign(d_tau_oc2[0]) == Math.Sign(os[0]) && Math.Sign(d_tau_oc2[1]) == Math.Sign(os[1])))
                    {
                        Point a = new Point()
                        {
                            localTerestrial = new CartesianCoordinates()
                            {
                                x = alpha2[0],
                                y = alpha2[2],
                                z = alpha2[1]
                            }
                        };

                        CSRecalculation.LocalTerestrialTransferToSpherical(lstBasePoints, ref a);

                        Logger.print(
                            $"\t\tВыбираем второе решение по расчету 2Д с увеличенными базами. " +
                            $"\n\t\t\tlat = {a.geodesic.Latitude}" +
                            $"\n\t\t\tlng = {a.geodesic.Longitude}" +
                            $"\n\t\t\talt = {a.geodesic.Altitude}");

                        return a;
                    }
                    else
                    {
                        Logger.print(
                            $"\t\tЗнаки не совпадают. Решений нет.");
                    }
                }
                else
                {
                    Logger.print(
                        $"\t\tДискриминант меньше 0. Решений нет.");
                }

            }

            return null;
        }


        private List<double> CalculateDsoe_p(List<double> A, double[] alpha)
        {
            double a0 = Math.Sqrt(Math.Pow(A[0] - alpha[0], 2) + Math.Pow(A[1] - alpha[1], 2) + Math.Pow(A[2] - alpha[2], 2));
            double a1 = Math.Sqrt(Math.Pow(A[3] - alpha[0], 2) + Math.Pow(A[4] - alpha[1], 2) + Math.Pow(A[5] - alpha[2], 2));
            double a2 = Math.Sqrt(Math.Pow(A[6] - alpha[0], 2) + Math.Pow(A[7] - alpha[1], 2) + Math.Pow(A[8] - alpha[2], 2));

            return new List<double> { a0, a1, a2 };
        }

        public double[] CalculateUdiff(List<double> lstTemp, double[] alpha)
        {
            List<double> A = CalculateDsoe_p(lstTemp, alpha);

            double d_tau0 = (A[1] - A[0]) / Vars.SpeedOfLight;
            double d_tau1 = (A[2] - A[0]) / Vars.SpeedOfLight;

            return new double[] { d_tau0, d_tau1 };
        }

        public Tuple<double, List<double>, List<Point>> MakeActualTuple_main(List<Point> lstBasePoints, TauContainer taus, int index)
        {
            List<Point> lstCorrPairs = new List<Point>();
            double baseHeight = 0;

            switch (index)
            {
                case 0:
                    //234
                    lstCorrPairs = new List<Point>()
                    {
                        lstBasePoints[1],
                        lstBasePoints[2],
                        lstBasePoints[3]
                    };

                    baseHeight = lstBasePoints[1].geodesic.Altitude;

                    Logger.print($"\t\tВыбор баз 23 и 24.");
                    break;
                case 1:
                    //324
                    lstCorrPairs = new List<Point>()
                    {
                        lstBasePoints[2],
                        lstBasePoints[1],
                        lstBasePoints[3]
                    };
                    Logger.print($"\t\tВыбор баз 32 и 34.");

                    baseHeight = lstBasePoints[2].geodesic.Altitude;

                    break;
                case 2:
                    //423
                    lstCorrPairs = new List<Point>()
                    {
                        lstBasePoints[3],
                        lstBasePoints[1],
                        lstBasePoints[2]
                    };
                    Logger.print($"\t\tВыбор баз 42 и 43.");

                    baseHeight = lstBasePoints[3].geodesic.Altitude;

                    break;
                default:

                    baseHeight = lstBasePoints[0].geodesic.Altitude;

                    //Logger.print($"\nNOTCH. Работают три пары.");
                    //Logger.print($"\nNOTCH. WRONG.");
                    break;
            }

            List<double> lstCorrPairs_tau = new List<double>();

            switch (index)
            {
                case 0:
                    //234

                    double tau23 = taus.tau13 - taus.tau12;
                    double tau24 = taus.tau14 - taus.tau12;

                    Logger.print(
                        $"\t\tИспользуемые задержки:" +
                        $"\n\t\t\ttau23 = {tau23 * 1e9} ns" +
                        $"\n\t\t\ttau24 = {tau24 * 1e9} ns");

                    lstCorrPairs_tau = new List<double>()
                    {
                        tau23,
                        tau24
                    };
                    //Logger.print($"\nNOTCH. Работают корреляционные пары 23 и 24.");
                    break;
                case 1:
                    //324

                    double tau32 = taus.tau12 - taus.tau13;
                    double tau34 = taus.tau14 - taus.tau13;

                    Logger.print(
                        $"\t\tИспользуемые задержки:" +
                        $"\n\t\t\ttau32 = {tau32 * 1e9} ns" +
                        $"\n\t\t\ttau34 = {tau34 * 1e9} ns");

                    lstCorrPairs_tau = new List<double>()
                    {
                        tau32,
                        tau34
                    };
                    //Logger.print($"\nNOTCH. Работают корреляционные пары 32 и 34.");
                    break;
                case 2:
                    //423

                    double tau42 = taus.tau12 - taus.tau14;
                    double tau43 = taus.tau13 - taus.tau14;

                    Logger.print(
                        $"\t\tИспользуемые задержки:" +
                        $"\n\t\t\ttau42 = {tau42 * 1e9} ns" +
                        $"\n\t\t\ttau43 = {tau43 * 1e9} ns");

                    lstCorrPairs_tau = new List<double>()
                    {
                        tau42,
                        tau43
                    };
                    //Logger.print($"\nNOTCH. Работают корреляционные пары 42 и 43.");
                    break;
                default:
                    //Logger.print($"\nNOTCH. Работают три пары.");
                    //Logger.print($"\nNOTCH. WRONG.");
                    break;
            }
        
            return Tuple.Create(baseHeight, lstCorrPairs_tau, lstCorrPairs);
        }

        private List<double> MakeActualList_3pointBaseExtended(List<Point> lstBasepointsExtended, List<double> taus)
        {
            return new List<double>()
            {
                taus[0],
                taus[1],
                lstBasepointsExtended[0].localTerestrial.x,
                lstBasepointsExtended[0].localTerestrial.z,
                lstBasepointsExtended[0].localTerestrial.y,
                lstBasepointsExtended[1].localTerestrial.x,
                lstBasepointsExtended[1].localTerestrial.z,
                lstBasepointsExtended[1].localTerestrial.y,
                lstBasepointsExtended[2].localTerestrial.x,
                lstBasepointsExtended[2].localTerestrial.z,
                lstBasepointsExtended[2].localTerestrial.y,
            };
        }
    }
}
