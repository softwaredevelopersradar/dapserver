﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filter3DEstimation
{
    public class GeodesicCoordinates
    {
        public double Latitude = -1;
        public double Longitude = -1;
        public double Altitude = -1;
    }
}
