﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fDRM;
using GeoCalculator;
using System.IO;

namespace Filter3DEstimation
{
    public static class CSRecalculation
    {
        public static void LocalTerestrialTransferToSpherical(List<Point> lstBasePoints, ref Point point)
        {
            ClassGeoCalculator.MZSK_Geoc(
                // Базовая
                lstBasePoints[0].geodesic.Latitude * Math.PI / 180, // rad
                lstBasePoints[0].geodesic.Longitude * Math.PI / 180,
                // Базовая в ГЦСК
                lstBasePoints[0].geocentric.x,
                lstBasePoints[0].geocentric.y,
                lstBasePoints[0].geocentric.z,
                // ИРИ в МЗСК
                point.localTerestrial.x,
                point.localTerestrial.y,
                point.localTerestrial.z,
                // ИРИ в ГЦСК
                ref point.geocentric.x,
                ref point.geocentric.y,
                ref point.geocentric.z
            );

            ClassGeoCalculator.f_XYZ_BLH_84_1(
                // ИРИ в ГЦСК
                point.geocentric.x,
                point.geocentric.y,
                point.geocentric.z,
                // ИРИ -> долгота,широта,высота
                ref point.geodesic.Latitude,
                ref point.geodesic.Longitude,
                ref point.geodesic.Altitude
            );
        }

        //
        public static void SphericalTransferToLocalTerestrial_Base(ref List<Point> lstBasePoints)
        {

            for (int i = 0; i < lstBasePoints.Count; i++)
            {
                ClassGeoCalculator.f_BLH_XYZ_84_1(
                    lstBasePoints[i].geodesic.Latitude,
                    lstBasePoints[i].geodesic.Longitude,
                    lstBasePoints[i].geodesic.Altitude,
                    ref lstBasePoints[i].geocentric.x,
                    ref lstBasePoints[i].geocentric.y,
                    ref lstBasePoints[i].geocentric.z
                );
            }

            for (int i = 1; i < lstBasePoints.Count; i++)
            {
                ClassGeoCalculator.Geoc_MZSK(

                    // Base station
                    lstBasePoints[0].geodesic.Latitude * Math.PI / 180, // rad
                    lstBasePoints[0].geodesic.Longitude * Math.PI / 180,
                    lstBasePoints[0].geocentric.x,
                    lstBasePoints[0].geocentric.y,
                    lstBasePoints[0].geocentric.z,

                    // Станции 2,3,4
                    lstBasePoints[i].geocentric.x,
                    lstBasePoints[i].geocentric.y,
                    lstBasePoints[i].geocentric.z,

                    ref lstBasePoints[i].localTerestrial.x,
                    ref lstBasePoints[i].localTerestrial.y,
                    ref lstBasePoints[i].localTerestrial.z
                );
            }
        }

        public static void SphericalTransferToLocalTerestrial_Point(ref Point point, List<Point> lstBasePoints)
        {
            ClassGeoCalculator.f_BLH_XYZ_84_1(
                point.geodesic.Latitude,
                point.geodesic.Longitude,
                point.geodesic.Altitude,
                ref point.geocentric.x,
                ref point.geocentric.y,
                ref point.geocentric.z
            );

            ClassGeoCalculator.Geoc_MZSK(

                // Base station
                lstBasePoints[0].geodesic.Latitude * Math.PI / 180, // rad
                lstBasePoints[0].geodesic.Longitude * Math.PI / 180,
                lstBasePoints[0].geocentric.x,
                lstBasePoints[0].geocentric.y,
                lstBasePoints[0].geocentric.z,

                // geo point
                point.geocentric.x,
                point.geocentric.y,
                point.geocentric.z,

                ref point.localTerestrial.x,
                ref point.localTerestrial.y,
                ref point.localTerestrial.z
            );

        }
        //
    }

    public static class Logger
    {
        public static void record(TauContainer taus, double height, double time)
        {
            using (StreamWriter sw = new StreamWriter(@"..\1.yaml", true, Encoding.Default))
            {
                sw.WriteLine($"{taus.tau12.ToString("f2")} {taus.tau13.ToString("f2")} {taus.tau14.ToString("f2")} {height.ToString("f2")} {time.ToString("f2")}");
            }
        }

        public static void print(string s)
        {
            if (Vars.isPrintAvailable)
            {
                Console.WriteLine(s);
            }
        }
    }

    public static class HeightSettings
    {
        public static double approximationAlpha = 0.95;
        public static double approximationBeta = 0.05;
        public static double pointHeight = 50;    //аппроксимированная высота
        public static bool isInvertedImaginaryInDRM = true;    // инвертация значений при расчете в модуле лены

        /// <summary>
        /// Применение аппроксимации на высоту
        /// </summary>
        /// <param name="inputHeight"></param>
        /// <returns>Текущая высота</returns>
        public static double Approximate(double inputHeight)
        {
            if (inputHeight > 0 && inputHeight < Vars.HeightUpperBoard)
                pointHeight = approximationBeta * (inputHeight) + approximationAlpha * pointHeight;

            return pointHeight;
        }
    }

    public static class DistanceEstimation
    {
        /// <summary>
        /// Радианы в градусы
        /// </summary>
        /// <param name="rad"></param>
        /// <returns></returns>
        public static double rad2deg(double rad)
        {
            return rad / Math.PI * 180;
        }

        public static double CalculateDistanceBW2Points(Point p1, Point p2)
        {
            double sqDelthaLat = Math.Pow(p2.localTerestrial.x - p1.localTerestrial.x, 2);
            double sqDelthaLng = Math.Pow(p2.localTerestrial.y - p1.localTerestrial.y, 2);
            double sqDelthaAlt = Math.Pow(p2.localTerestrial.z - p1.localTerestrial.z, 2);
            double distance = Math.Sqrt(sqDelthaLat + sqDelthaLng + sqDelthaAlt);

            return distance;
        }
        
        public static double CalculateNotchAngle(Point p0, Point p1, Point p2, Point pX)
        {
            double phi01 = CalculateAnglePhi(p0, p1, pX);
            double phi02 = CalculateAnglePhi(p0, p2, pX);
            double notchAngle = (phi01 + phi02) / 2;

            return Math.Sin(notchAngle);
        }

        public static double CalculateAnglePhi(Point p1, Point p2, Point pX)
        {
            double sqDistBW1X = Math.Pow(CalculateDistanceBW2Points(p2, pX), 2);
            double sqDistBW0X = Math.Pow(CalculateDistanceBW2Points(p1, pX), 2);
            double sqDistBW01 = Math.Pow(CalculateDistanceBW2Points(p1, p2), 2);
            double denominator = 2 * CalculateDistanceBW2Points(p1, pX) * CalculateDistanceBW2Points(p2, pX);
            double phi = Math.Acos((sqDistBW1X + sqDistBW0X - sqDistBW01) / denominator);

            return phi;
        }

        /// <summary>
        /// Градусы в радианы
        /// </summary>
        /// <param name="deg"></param>
        /// <returns></returns>
        public static double deg2rad(double deg)
        {
            return deg * Math.PI / 180;
        }

        /// <summary>
        /// Расстояние в метрах между координатами на плоскости
        /// </summary>
        /// <param name="cc1"></param>
        /// <param name="cc2"></param>
        /// <returns></returns>
        public static double GetDistanceBWCoordinates(GeodesicCoordinates cc1, GeodesicCoordinates cc2)
        {
            if (cc1.Latitude == cc2.Latitude && cc1.Longitude == cc2.Longitude)
                return 0;
            else
            {
                double temp_sin_mult = Math.Sin(deg2rad(cc1.Latitude)) * Math.Sin(deg2rad(cc2.Latitude));
                double temp_cos_mult = Math.Cos(deg2rad(cc1.Latitude)) * Math.Cos(deg2rad(cc2.Latitude)) * Math.Cos(deg2rad(cc1.Longitude - cc2.Longitude));
                double temp_value = rad2deg(Math.Acos(temp_sin_mult + temp_cos_mult));
                return temp_value * 60 * 1.1515 * 1.609344 * 1000;
            }

        }
    }

}
