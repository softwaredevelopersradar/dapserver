﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filter3DEstimation
{
    class TauEstimation
    {
        /// <summary>
        /// Возвращает максимальные значения корреляционных кривых
        /// </summary>
        /// <param name="input">Список, состоящий из трех списков отсчетов корреляционных прямых</param>
        /// <returns></returns>
        public double[] GetListMaximumValue(List<List<double>> input)
        {
            return new double[3] { input[0].Max(), input[1].Max(), input[2].Max() };
        }

        /// <summary>
        /// Проверка на то, превышает ли максимальное число одной корреляционной кривой коэффициент минимума.
        /// </summary>
        /// <param name="ar">Массив максимальных значений трех корреляционных кривых</param>
        /// <returns></returns>
        public bool CheckAvailabilityCorrelationSamplesToPassThroughThreshold(double[] ar)
        {
            return
                ar[0] > Vars.PassThroughCorrelationThreshold
                &&
                ar[1] > Vars.PassThroughCorrelationThreshold
                &&
                ar[2] > Vars.PassThroughCorrelationThreshold;
        }

        /// <summary>
        /// Расчет задержки определенной корреляционной пары.
        /// </summary>
        /// <param name="input_pair">Отсчеты корреляционно кривой</param>
        /// <returns></returns>
        public double GetDelayValue(List<double> input_pair)
        {
            double maxValue = input_pair.Max();
            int indexMaxValue = input_pair.IndexOf(maxValue);

            List<int> xValues = new List<int>();
            List<double> xValuesApproximation = new List<double>();
            List<double> yValuesApproximation = new List<double>();

            for (int i = -(input_pair.Count() - 1) / 2; i <= (input_pair.Count() - 1) / 2; i++)
                xValues.Add(i);

            int lowerBorder = getLowerBorder(Vars.CoefficientBorderDelayEstimation * maxValue, indexMaxValue, input_pair);
            int highBorder = getHigherBorder(Vars.CoefficientBorderDelayEstimation * maxValue, indexMaxValue, input_pair);

            for (int i = lowerBorder; i <= highBorder; i++)
            {
                yValuesApproximation.Add(input_pair[i]);
                xValuesApproximation.Add(xValues[i]);
            }

            if (yValuesApproximation.Count() > 2)
            {
                double[] ccs = approximationPoly2deg(xValuesApproximation, yValuesApproximation); //коэффициенты параболы полученные в результате аппроксимации

                double delay = (-ccs[1] / (2 * ccs[0])) * Vars.Delay_1Hz_ns; // 16 = 1/ 62.5 MHz, where 62.5 Mhz - sampling frequency

                return delay;
            }

            return 0;
        }

        /// <summary>
        /// Метод аппроксимации по параболе (полиномом 2ой степени)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public double[] approximationPoly2deg(List<double> x, List<double> y)
        {
            List<double> x2 = new List<double>();
            List<double> x3 = new List<double>();
            List<double> x4 = new List<double>();
            List<double> xy = new List<double>();
            List<double> x2y = new List<double>();

            int n = x.Count();

            for (int i = 0; i < n; i++)
            {
                x2.Add(Math.Pow(x[i], 2));
                x3.Add(Math.Pow(x[i], 3));
                x4.Add(Math.Pow(x[i], 4));
                xy.Add(x[i] * y[i]);
                x2y.Add(Math.Pow(x[i], 2) * y[i]);
            }

            double sumX = x.Sum();
            double sumY = y.Sum();
            double sumX2 = x2.Sum();
            double sumX3 = x3.Sum();
            double sumX4 = x4.Sum();
            double sumXY = xy.Sum();
            double sumX2Y = x2y.Sum();

            double delta = MatrixEqualizer.determinant3deg(sumX2, sumX, n, sumX3, sumX2, sumX, sumX4, sumX3, sumX2);

            double delta_a = MatrixEqualizer.determinant3deg(sumY, sumX, n, sumXY, sumX2, sumX, sumX2Y, sumX3, sumX2);
            double delta_b = MatrixEqualizer.determinant3deg(sumX2, sumY, n, sumX3, sumXY, sumX, sumX4, sumX2Y, sumX2);
            double delta_c = MatrixEqualizer.determinant3deg(sumX2, sumX, sumY, sumX3, sumX2, sumXY, sumX4, sumX3, sumX2Y);

            double COEF_A = delta_a / delta;
            double COEF_B = delta_b / delta;
            double COEF_C = delta_c / delta;

            return new double[] { COEF_A, COEF_B, COEF_C };
        }


        /// <summary>
        /// получение индекса нижней границы.
        /// </summary>
        /// <param name="coef">Граница поиска минимума</param>
        /// <param name="index">Индекс пика корреляционной кривой</param>
        /// <param name="ar">Отсчеты корреляционной кривой</param>
        /// <returns></returns>
        public int getLowerBorder(double coef, int index, List<double> ar)
        {
            int border;

            for (int lb = index; lb >= 0; lb--)
            {
                if (ar[lb] < coef)
                {
                    border = lb + 1;
                    return border;
                }
                else if (lb == 0)
                {
                    border = lb;
                    break;
                }
            }
            return index;
        }

        /// <summary>
        /// получение индекса высшей границы
        /// </summary>
        /// <param name="coef">Граница поиска максимума</param>
        /// <param name="index">Индекс пика корреляционной кривой</param>
        /// <param name="ar">Отсчеты корреляционной кривой</param>
        /// <returns></returns>
        public int getHigherBorder(double coef, int index, List<double> ar)
        {
            int ar_Count = ar.Count();
            int border;

            for (int hb = index; hb < ar_Count; hb++)
            {
                if (ar[hb] < coef)
                {
                    border = hb - 1;
                    return border;
                }
                else if (hb == ar_Count - 1)
                {
                    border = hb;
                    break;
                }
            }
            return index;
        }
    }
}
