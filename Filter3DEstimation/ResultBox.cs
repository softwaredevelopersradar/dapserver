﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filter3DEstimation
{
    public class ResultBox
    {
        public GeodesicCoordinates Coordinates = new GeodesicCoordinates();   // контейнер для координат
        public GeodesicCoordinates extrapolatedCoordinates = new GeodesicCoordinates();   // Экстраполированные координаты, выданные блоком траекторной обработки
        public TauContainer FilteredTau;   //фильтрованные задержки
        public TauContainer PureTau;   //чистые расчетные задержки

        public byte FlagRezCalc = 0;   //флаг, необходимый паше, по которому он с леной работал, всегда стоит 0, чтобы паша ничего лишнего не вырезал
        public int ID = -1;  // ID траектории, частоты и т.п.
    }
}
