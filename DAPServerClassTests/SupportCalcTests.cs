﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAPServerClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAPServerClass.Tests
{
    [TestClass()]
    public class SupportCalcTests
    {

        [TestMethod()]
        public void EjectCiphers9Test()
        {
            // Arrange
            int ID = 9;
            // Act
            var Act = SupportCalc.EjectCiphers(ID);
            // Assert
            Assert.AreEqual<(int, int)>((0, 9), Act);
        }


        [TestMethod()]
        public void EjectCiphers10Test()
        {
            // Arrange
            int ID = 10;
            // Act
            var Act  = SupportCalc.EjectCiphers(ID);
            // Assert
            Assert.AreEqual<(int, int)>((1,0), Act);
        }

        [TestMethod()]
        public void EjectCiphers11Test()
        {
            // Arrange
            int ID = 11;
            // Act
            var Act = SupportCalc.EjectCiphers(ID);
            // Assert
            Assert.AreEqual<(int, int)>((1, 1), Act);
        }

        [TestMethod()]
        public void EjectCiphers20Test()
        {
            // Arrange
            int ID = 20;
            // Act
            var Act = SupportCalc.EjectCiphers(ID);
            // Assert
            Assert.AreEqual<(int, int)>((2, 0), Act);
        }

        [TestMethod()]
        public void EjectCiphers21Test()
        {
            // Arrange
            int ID = 21;
            // Act
            var Act = SupportCalc.EjectCiphers(ID);
            // Assert
            Assert.AreEqual<(int, int)>((2, 1), Act);
        }

        [TestMethod()]
        public void EjectCiphers22Test()
        {
            // Arrange
            int ID = 22;
            // Act
            var Act = SupportCalc.EjectCiphers(ID);
            // Assert
            Assert.AreEqual<(int, int)>((2, 2), Act);
        }
    }
}