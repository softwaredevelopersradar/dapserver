﻿using DAPServerClass;
using System.Diagnostics;
using System.Windows.Forms;
using System;

namespace DAPServerApp
{
    class Program
    {
        private static DapServerClass _DapServerClass = new DapServerClass();

        static void Main(string[] args)
        {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

            MessageLogger.SetLogger(new NLogLogger());

            if (Instance.HasRunningCopy)
            {
                MessageLogger.Log("Another copy of the application is already running", ConsoleColor.Red);
                Console.ReadLine();
                return;
            }

            _DapServerClass.Start();
            Application.Run();
            //Console.ReadLine();
        }
    }
}
