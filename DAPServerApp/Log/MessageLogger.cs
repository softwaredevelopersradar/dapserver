﻿using System;

namespace DAPServerApp
{
    public static class MessageLogger
    {
        public static ConsoleColor ErrorColor
        {
            get => Logger.ErrorColor;
            set => Logger.ErrorColor = value;
        }

        public static ConsoleColor WarningColor
        {
            get => Logger.WarningColor;
            set => Logger.WarningColor = value;
        }
        public static ConsoleColor MessageColor
        {
            get => Logger.MessageColor;
            set => Logger.MessageColor = value;
        }

        public static bool IsReceiversLogEnabled
        {
            get => Logger.IsReceiversLogEnabled;
            set => Logger.IsReceiversLogEnabled = value;
        }

        public static bool IsFpgaLogEnabled
        {
            get => Logger.IsReceiversLogEnabled;
            set => Logger.IsReceiversLogEnabled = value;
        }

        public static bool IsShaperLogEnabled
        {
            get => Logger.IsShaperLogEnabled;
            set => Logger.IsShaperLogEnabled = value;
        }

        public static bool IsServerLogEnabled
        {
            get => Logger.IsServerLogEnabled;
            set => Logger.IsServerLogEnabled = value;
        }

        public static bool IsTraceLogEnabled
        {
            get => Logger.IsTraceLogEnabled;
            set => Logger.IsTraceLogEnabled = value;
        }

        public static IMessageLogger Logger { get; private set; }

        static MessageLogger()
        {
            Logger = new NullLogger();
        }

        public static void SetLogger(IMessageLogger logger)
        {
            Logger = logger ?? throw new ArgumentException("logger can't be null!");
        }

        public static void Log(string message, ConsoleColor color)
        {
            Logger.Log(message, color);
        }

        public static void FpgaLog(string message)
        {
            Logger.FpgaLog(message);
        }

        public static void ReceveirsLog(string seed, byte[] data)
        {
            Logger.ReceveirsLog(seed, data);
        }

        public static void ShaperLog(string message)
        {
            Logger.ShaperLog(message);
        }

        public static void ServerLog(string message)
        {
            Logger.ServerLog(message);
        }

        /// <summary>
        /// Traces inner app methods
        /// </summary>
        public static void Trace(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            Logger.Trace(message, color);
        }

        public static void Log(string message)
        {
            Logger.Log(message, MessageColor);
        }

        public static void Warning(string message)
        {
            Logger.Warning(message);
        }

        public static void Error(string message)
        {
            Logger.Error(message);
        }

        public static void Error(Exception e)
        {
            Logger.Error(e);
        }

        public static void Error(Exception e, string message)
        {
            Logger.Error(e, message);
        }
    }
}
