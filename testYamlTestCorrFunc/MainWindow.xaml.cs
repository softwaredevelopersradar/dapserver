﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfMapRastr;
using YamlTestCorrFunc;

namespace testYamlTestCorrFunc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Yaml yaml = new Yaml();

        public MainWindow()
        {
            InitializeComponent();

            Random r = new Random();

            var t1 = r.NextDouble() * 900;
            var t2 = r.NextDouble() * 900;
            var t3 = r.NextDouble() * 900;

            var a1 = r.NextDouble();
            var a2 = r.NextDouble();
            var a3 = r.NextDouble();
            var a4 = r.NextDouble();

            int N = 121;

            double[] c1 = new double[N];
            c1 = c1.Select(x => x = r.NextDouble()).ToArray();
            double[] c2 = new double[N];
            c2 = c2.Select(x => x = r.NextDouble()).ToArray();
            double[] c3 = new double[N];
            c3 = c3.Select(x => x = r.NextDouble()).ToArray();

            TestCorrFunc testCorrFunc =
                new TestCorrFunc(
                    t1, t2, t3,
                    a1, a2, a3, a4,
                    c1, c2, c3);

            yaml.YamlSave(testCorrFunc, "CorrfuncSaveTest.yaml");

            ////Класс Расчета дальности
            //fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();
            //var d = classDRM_Dll.f_D_2Points(
            //                        coords[0],
            //                        coords[1],
            //                        nCoords[0],
            //                        nCoords[1],
            //                        1);

            double egorlat = 53.9320254937089d;
            double egorlon = 27.6351292330104d;
        }

        // 13.862 21.235 20.947 13.237

        private fDRM.ClassObjectTmp CalsCoords(List<double> arrtau123, int sign1, int sign2, int sign3)
        {
            //double[] tau = new double[] { 0, arrtau123[0] / 1e9, arrtau123[1] / 1e9, arrtau123[2] / 1e9 };
            double[] tau = new double[] { 0, sign1 * arrtau123[0] * 1e-9, sign2 * arrtau123[1] * 1e-9, sign3 * arrtau123[2] * 1e-9 };
            //Класс Расчета координат
            fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

            //Инициализация листа координат станций и их задержек
            List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

            List<Station> stations = DefaultStations();
            List<Station> DefaultStations()
            {
                //Заполенение листа координат станций и их задержек
                List<Station> listStations = new List<Station>();

                Station station0 = new Station()
                {
                    Position = new MarkCoord(
                        Lat: 53.9312,
                        Long: 27.635555556,
                        Alt: 13.862,
                        LatRLSBase: 53.9312,
                        LongRLSBase: 27.635555556,
                        AltRLSBase: 13.862
                        ),
                    ErrorTime = 0,
                };
                station0.Position.ID = 0;
                listStations.Add(station0);

                Station station1 = new Station()
                {
                    Position = new MarkCoord(
                        Lat: 53.930997222,
                        Long: 27.6349,
                        Alt: 21.235,
                        LatRLSBase: 53.9312,
                        LongRLSBase: 27.635555556,
                        AltRLSBase: 13.862
                        ),
                    ErrorTime = 1E-9,
                };
                station1.Position.ID = 1;
                listStations.Add(station1);

                Station station2 = new Station()
                {
                    Position = new MarkCoord(
                        Lat: 53.931444444,
                        Long: 27.636638889,
                        Alt: 20.947,
                        LatRLSBase: 53.9312,
                        LongRLSBase: 27.635555556,
                        AltRLSBase: 13.862
                        ),
                    ErrorTime = 1E-9,
                };
                station2.Position.ID = 2;
                listStations.Add(station2);

                Station station3 = new Station()
                {
                    Position = new MarkCoord(
                        Lat: 53.932327778,
                        Long: 27.63485,
                        Alt: 13.237,
                        LatRLSBase: 53.9312,
                        LongRLSBase: 27.635555556,
                        AltRLSBase: 13.862
                        ),
                    ErrorTime = 1E-9,
                };
                station3.Position.ID = 3;
                listStations.Add(station3);

                return listStations;
            }

            //Заполенение листа координат станций и их задержек тестовая версия
            for (int k = 0; k < 4; k++)
            {
                fDRM.ClassObject classObject = new fDRM.ClassObject()
                {
                    Latitude = stations[k].Position.Latitude,
                    Longitude = stations[k].Position.Longitude,
                    Altitude = stations[k].Position.Altitude,
                    BaseStation = (k == 0) ? true : false,
                    IndexStation = k + 1,
                    tau = tau[k]
                };

                lstClassObject.Add(classObject);
            }

            //Рассчёт координат источника
            fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject);
            return resultMark;



        }

        public float CorrThreshold { get; set; } = 0.2f;
        public float CorrDivide { get; set; } = 0.5f;

        public float BandWidthMHz { get; set; } = 62.5f;


        private List<double> CalcTauFromCorrFuncs(List<List<double>> CorrDoubleList)
        {
            List<double> tupl = new List<double>() { -1, -1, -1 };
            if (CorrDoubleList.Count() == 3)
            {
                List<double> lMaxes = CorrDoubleList.Select(x => x.Max()).ToList();

                if (lMaxes.All(x => x >= CorrThreshold))
                {
                    for (int w = 0; w < CorrDoubleList.Count(); w++)
                    {
                        double localMax = (lMaxes[w] * CorrDivide);

                        int maxindex = CorrDoubleList[w].IndexOf(lMaxes[w]);
                        int lbindex = -1;
                        int hbindex = -1;

                        List<int> negativeValues = new List<int>();

                        for (int ii = -(CorrDoubleList[w].Count() - 1) / 2; ii <= (CorrDoubleList[w].Count() - 1) / 2; ii++)
                        {
                            negativeValues.Add(ii);
                        }

                        lbindex = getLowerBorder(localMax, maxindex, CorrDoubleList[w]);
                        hbindex = getHigherBorder(localMax, maxindex, CorrDoubleList[w]);

                        if (lbindex != -1 && hbindex != -1)
                        {
                            List<double> subRangeX = new List<double>();
                            for (int j = lbindex; j <= hbindex; j++)
                                //  subRangeX.Add(j - maxindex);
                                subRangeX.Add(negativeValues[j]);

                            List<double> subRangeY = CorrDoubleList[w].GetRange(lbindex, hbindex - lbindex + 1);

                            var ABC = approximationParabellum_returnValue(subRangeX, subRangeY);

                            tupl[w] = ((-1d) * (ABC[1] / (2d * ABC[0]))) * (1d / (BandWidthMHz * 10e6)) * 10e9;

                        }
                    }
                }
            }
            return tupl;
        }

        private List<double> approximationParabellum_returnValue(List<double> x, List<double> y)
        {
            List<double> x2 = new List<double>();
            List<double> x3 = new List<double>();
            List<double> x4 = new List<double>();
            List<double> xy = new List<double>();
            List<double> x2y = new List<double>();

            int n = x.Count();

            for (int i = 0; i < n; i++)
            {
                x2.Add(Math.Pow(x[i], 2));
                x3.Add(Math.Pow(x[i], 3));
                x4.Add(Math.Pow(x[i], 4));
                xy.Add(x[i] * y[i]);
                x2y.Add(Math.Pow(x[i], 2) * y[i]);
            }

            double sumX = x.Sum();
            double sumY = y.Sum();
            double sumX2 = x2.Sum();
            double sumX3 = x3.Sum();
            double sumX4 = x4.Sum();
            double sumXY = xy.Sum();
            double sumX2Y = x2y.Sum();

            double delta = determinant(sumX2, sumX, n, sumX3, sumX2, sumX, sumX4, sumX3, sumX2);

            double delta_a = determinant(sumY, sumX, n, sumXY, sumX2, sumX, sumX2Y, sumX3, sumX2);
            double delta_b = determinant(sumX2, sumY, n, sumX3, sumXY, sumX, sumX4, sumX2Y, sumX2);
            double delta_c = determinant(sumX2, sumX, sumY, sumX3, sumX2, sumXY, sumX4, sumX3, sumX2Y);

            double COEF_A = delta_a / delta;
            double COEF_B = delta_b / delta;
            double COEF_C = delta_c / delta;

            return new List<double>() { COEF_A, COEF_B, COEF_C };
        }

        private double determinant(double a1, double a2, double a3,
                                         double a4, double a5, double a6,
                                         double a7, double a8, double a9)
        {
            return a1 * (a5 * a9 - a8 * a6) - a2 * (a4 * a9 - a7 * a6) + a3 * (a4 * a8 - a7 * a5);
        }


        //получение индекса нижней границы
        private int getLowerBorder(double coef, int index, List<double> arr)
        {
            int border;
            for (int lb = index; lb >= 0; lb--)
            {
                if (arr[lb] < coef)
                {
                    border = lb + 1;
                    return border;
                }
                else if (lb == 0)
                {
                    border = lb;
                    break;
                }
            }
            return index;
        }

        //получение индекса высшей границы
        private int getHigherBorder(double coef, int index, List<double> arr)
        {
            int border;
            for (int hb = index; hb < arr.Count(); hb++)
            {
                if (arr[hb] < coef)
                {
                    border = hb - 1;
                    return border;
                }
                else if (hb == arr.Count() - 1)
                {
                    border = hb;
                    break;
                }
            }
            return index;
        }

        private List<double> SignMirror(List<double> dList, int sign1, int sign2, int sign3)
        {
            return new List<double>() { dList[0] * sign1, dList[1] * sign2, dList[2] * sign3 };
        }

        private string loadName = "CorrfuncSaveTest0.yaml";
        private void Original_Click(object sender, RoutedEventArgs e)
        {
            //Загрузить данные из файла
            TestCorrFunc load = yaml.YamlLoad<TestCorrFunc>(loadName);
            //Ортонормировать корреляционные функции
            List<List<double>> CorrDoubleList = OrthoNormalize(load);
            //Расчет задержек
            var tau123 = CalcTauFromCorrFuncs(CorrDoubleList);
            //Сведение парабол
            var mirrortau123 = SignMirror(tau123, 1, 1, 1);
            //Вычисление координат
            fDRM.ClassObjectTmp resultMark = CalsCoords(mirrortau123, 1, 1, 1);
            //Вывод на консоль
            Console.WriteLine($"Original  Lat: {resultMark.Latitude.ToString("00.000000000")} Lon: {resultMark.Longitude.ToString("00.000000000")} Alt: {resultMark.Altitude.ToString("F1")}");
        }
        private void Pasha_Click(object sender, RoutedEventArgs e)
        {
            //Загрузить данные из файла
            TestCorrFunc load = yaml.YamlLoad<TestCorrFunc>(loadName);
            //Ортонормировать корреляционные функции
            List<List<double>> CorrDoubleList = OrthoNormalize(load);
            //Расчет задержек
            var tau123 = CalcTauFromCorrFuncs(CorrDoubleList);
            //Сведение парабол
            var mirrortau123 = SignMirror(tau123, 1, -1, -1);
            //Вычисление координат
            fDRM.ClassObjectTmp resultMark = CalsCoords(mirrortau123, 1, -1, -1);
            //Вывод на консоль
            Console.WriteLine($"Pasha  Lat: {resultMark.Latitude.ToString("00.000000000")} Lon: {resultMark.Longitude.ToString("00.000000000")} Alt: {resultMark.Altitude.ToString("F1")}");
        }
        private void Egor_Click(object sender, RoutedEventArgs e)
        {
            //Загрузить данные из файла
            TestCorrFunc load = yaml.YamlLoad<TestCorrFunc>(loadName);
            //Ортонормировать корреляционные функции
            List<List<double>> CorrDoubleList = OrthoNormalize(load);
            //Расчет задержек
            var tau123 = CalcTauFromCorrFuncs(CorrDoubleList);
            //Сведение парабол
            var mirrortau123 = SignMirror(tau123, -1, 1, 1);
            //Вычисление координат
            fDRM.ClassObjectTmp resultMark = CalsCoords(mirrortau123, 1, -1, -1);
            //Вывод на консоль
            Console.WriteLine($"Egor  Lat: {resultMark.Latitude.ToString("00.000000000")} Lon: {resultMark.Longitude.ToString("00.000000000")} Alt: {resultMark.Altitude.ToString("F1")}");
        }

        private List<List<double>> OrthoNormalize(TestCorrFunc innerLoad)
        {
            //Ортонормировать корреляционные функции
            List<List<double>> CorrDoubleList = new List<List<double>>();

            double OrthoValue1 = Math.Sqrt(innerLoad.A2 * innerLoad.A1);
            List<double> OrthoList1 = innerLoad.Corr1.Select(x => x / OrthoValue1).ToList<double>();
            CorrDoubleList.Add(OrthoList1);

            double OrthoValue2 = Math.Sqrt(innerLoad.A3 * innerLoad.A1);
            List<double> OrthoList2 = innerLoad.Corr2.Select(x => x / OrthoValue2).ToList<double>();
            CorrDoubleList.Add(OrthoList2);

            double OrthoValue3 = Math.Sqrt(innerLoad.A4 * innerLoad.A1);
            List<double> OrthoList3 = innerLoad.Corr3.Select(x => x / OrthoValue3).ToList<double>();
            CorrDoubleList.Add(OrthoList3);

           return CorrDoubleList;
        }
    }

}
