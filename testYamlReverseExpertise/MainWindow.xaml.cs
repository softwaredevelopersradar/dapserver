﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YamlReverseExpertise;

namespace testYamlReverseExpertise
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<Expertise> expertises = new List<Expertise>();

            Random r = new Random();

            double[] c1 = new double[121];
            c1 = c1.Select(x => x = r.NextDouble()).ToArray();
            double[] c2 = new double[121];
            c2 = c2.Select(x => x = r.NextDouble()).ToArray();
            double[] c3 = new double[121];
            c3 = c3.Select(x => x = r.NextDouble()).ToArray();
            double[] c4 = new double[121];
            c4 = c4.Select(x => x = r.NextDouble()).ToArray();

            expertises.Add(new Expertise());
            expertises.Add(new Expertise(1,2,3,4,5,6,7,8));
            expertises.Add(new Expertise(DateTime.Now, 1, 2, 3, 4, 5, 6, 7, 8, c1, c2, c3, c4));

            ReverseExpertise reverseExpertise = new ReverseExpertise(expertises);

            Yaml yaml = new Yaml();

            yaml.YamlSave<ReverseExpertise>(reverseExpertise, "ReverseExpertise.yaml");

            yaml.YamlSavePro<ReverseExpertise>(reverseExpertise);

            DateTime TimeStart = DateTime.Now;
            string nameDotYaml = $"ReverseExpertise {TimeStart.Hour.ToString("00")}-{TimeStart.Minute.ToString("00")}-{TimeStart.Second.ToString("00")}.yaml";

            yaml.YamlSavePro<ReverseExpertise>(reverseExpertise, nameDotYaml);

            string ReverseExpertise = "ReverseExpertise.yaml";
            //OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.Filter = ".yaml|*.yaml";
            //if (openFileDialog.ShowDialog() == true)
            //{
            //    ReverseExpertise = openFileDialog.FileName;
            //}

            var load = yaml.YamlLoad<ReverseExpertise>(ReverseExpertise);

            yaml.YamlSave<ReverseExpertise>(reverseExpertise, "ReverseExpertise.yaml");
        }
    }
}
