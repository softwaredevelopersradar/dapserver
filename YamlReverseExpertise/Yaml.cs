﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace YamlReverseExpertise
{
    public class Yaml
    {
        public T YamlLoad<T>(string NameDotYaml) where T : new()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(NameDotYaml, System.Text.Encoding.Default))
                {
                    text = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            var deserializer = new DeserializerBuilder().Build();

            var t = new T();
            try
            {
                t = deserializer.Deserialize<T>(text);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (t == null)
            {
                t = new T();
                YamlSave(t, NameDotYaml);
            }
            return t;
        }

        public void YamlSave<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                using (StreamWriter sw = new StreamWriter(NameDotYaml, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSavePro<T>(T t) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                string path = Path.GetFullPath(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

                path = path.Remove(path.LastIndexOf('\\') + 1);

                string folderName = t.GetType().Name;

                string dirPath = path + folderName;
                DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
                if (!dirInfo.Exists)
                {
                    dirInfo.Create();
                }

                string subFolderName = $"{DateTime.Now.Day.ToString("00")}.{DateTime.Now.Month.ToString("00")}.{DateTime.Now.Year.ToString("0000")}";
                string subDirPath = dirPath +"\\" + subFolderName;
                DirectoryInfo subDirinfo = new DirectoryInfo(subDirPath);
                if (!subDirinfo.Exists)
                {
                    subDirinfo.Create();
                }

                string date = $"{DateTime.Now.Hour.ToString("00")}-{DateTime.Now.Minute.ToString("00")}-{DateTime.Now.Second.ToString("00")}";
                string NameDotYaml = folderName +" "+ date + ".yaml";

                string FullPath = subDirPath + "\\"+ NameDotYaml;

                using (StreamWriter sw = new StreamWriter(FullPath, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSavePro<T>(T t, string NameDotYaml) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                string path = Path.GetFullPath(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

                path = path.Remove(path.LastIndexOf('\\') + 1);

                string folderName = t.GetType().Name;

                string dirPath = path + folderName;
                DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
                if (!dirInfo.Exists)
                {
                    dirInfo.Create();
                }

                string subFolderName = $"{DateTime.Now.Day.ToString("00")}.{DateTime.Now.Month.ToString("00")}.{DateTime.Now.Year.ToString("0000")}";
                string subDirPath = dirPath + "\\" + subFolderName;
                DirectoryInfo subDirinfo = new DirectoryInfo(subDirPath);
                if (!subDirinfo.Exists)
                {
                    subDirinfo.Create();
                }

                string FullPath = subDirPath + "\\" + NameDotYaml;

                using (StreamWriter sw = new StreamWriter(FullPath, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void YamlSavePro<T>(T t, DateTime dateTime) where T : new()
        {
            try
            {
                var serializer = new SerializerBuilder().EmitDefaults().Build();
                var yaml = serializer.Serialize(t);

                string path = Path.GetFullPath(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

                path = path.Remove(path.LastIndexOf('\\') + 1);

                string folderName = t.GetType().Name;

                string dirPath = path + folderName;
                DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
                if (!dirInfo.Exists)
                {
                    dirInfo.Create();
                }

                string subFolderName = $"{DateTime.Now.Day.ToString("00")}.{DateTime.Now.Month.ToString("00")}.{DateTime.Now.Year.ToString("0000")}";
                string subDirPath = dirPath + "\\" + subFolderName;
                DirectoryInfo subDirinfo = new DirectoryInfo(subDirPath);
                if (!subDirinfo.Exists)
                {
                    subDirinfo.Create();
                }

                string time = $"{dateTime.Hour.ToString("00")}-{dateTime.Minute.ToString("00")}-{dateTime.Second.ToString("00")}";
                string NameDotYaml = folderName + " " + time + ".yaml";

                string FullPath = subDirPath + "\\" + NameDotYaml;

                using (StreamWriter sw = new StreamWriter(FullPath, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(yaml);
                    sw.Close();
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}


       
