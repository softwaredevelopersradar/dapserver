﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YamlReverseExpertise
{
    public class ReverseExpertise
    {
        public Expertise [] Expertises { get; set; }

        public ReverseExpertise()
        {
            Expertises = new Expertise[0];
        }

        public ReverseExpertise(List<Expertise> expertises)
        {
            Expertises = expertises.ToArray();
        }
    }

    public class Expertise
    {
        public DateTime Time { get; set; }


        public float FreqkHz { get; set; }
        public float BandwidthkHz { get; set; }

        public double Tau1 { get; set; }
        public double Tau2 { get; set; }
        public double Tau3 { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }
        public double Alt { get; set; }

        public double[] Corr1_1 { get; set; }
        public double[] Corr2_1 { get; set; }
        public double[] Corr3_1 { get; set; }
        public double[] Corr4_1 { get; set; }

        public Expertise()
        {
            Time = DateTime.Now;

            BandwidthkHz = 0;

            Tau1 = 0;
            Tau2 = 0;
            Tau3 = 0;

            Lat = 0;
            Lon = 0;
            Alt = 0;
        }

        public Expertise(
         float freqkHz, float bandwidthkHz,
         double tau1, double tau2, double tau3,
         double lat, double lon, double alt)
        {
            Time = DateTime.Now;

            FreqkHz = freqkHz;
            BandwidthkHz = bandwidthkHz;

            Tau1 = tau1;
            Tau2 = tau2;
            Tau3 = tau3;

            Lat = lat;
            Lon = lon;
            Alt = alt;

            Corr1_1 = new double[0];
            Corr2_1 = new double[0];
            Corr3_1 = new double[0];
            Corr4_1 = new double[0];
        }
        public Expertise(
            DateTime time,
            float freqkHz, float bandwidthkHz,
            double tau1, double tau2, double tau3,
            double lat, double lon, double alt)
        {
            Time = time;

            FreqkHz = freqkHz;
            BandwidthkHz = bandwidthkHz;

            Tau1 = tau1;
            Tau2 = tau2;
            Tau3 = tau3;

            Lat = lat;
            Lon = lon;
            Alt = alt;

            Corr1_1 = new double[0];
            Corr2_1 = new double[0];
            Corr3_1 = new double[0];
            Corr4_1 = new double[0];
        }

        public Expertise(
           DateTime time,
           float freqkHz, float bandwidthkHz,
           double tau1, double tau2, double tau3,
           double lat, double lon, double alt,
           double[] corr1_1, double[] corr2_1, double[] corr3_1, double[] corr4_1
            )
        {
            Time = time;

            FreqkHz = freqkHz;
            BandwidthkHz = bandwidthkHz;

            Tau1 = tau1;
            Tau2 = tau2;
            Tau3 = tau3;

            Lat = lat;
            Lon = lon;
            Alt = alt;

            Corr1_1 = corr1_1;
            Corr2_1 = corr2_1;
            Corr3_1 = corr3_1;
            Corr4_1 = corr4_1;
        }
    }
}
