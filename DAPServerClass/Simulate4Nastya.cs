﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WpfMapRastr;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        public class Tau
        {
            public Tau()
            {
            }

            public Tau(int t1, int t2, int t3, double time)
            {
                this.t1 = t1;
                this.t2 = t2;
                this.t3 = t3;
                this.time = time;
            }

            public int t1 { get; set; }
            public int t2 { get; set; }
            public int t3 { get; set; }

            public double time { get; set; }

        }

        private List<Tau> ReadFromFile(string path)
        {
            List<Tau> taus = new List<Tau>();

            using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    char[] separator = new char[] { ' ' };
                    var strings = line.Split(separator, 4);
                    Tau tau = new Tau()
                    {
                        t1 = Convert.ToInt32(strings[0]),
                        t2 = Convert.ToInt32(strings[1]),
                        t3 = Convert.ToInt32(strings[2]),
                        time = Convert.ToDouble(strings[3].Replace(".", ","))
                    };

                    taus.Add(tau);
                }
            }

            return taus;
        }


        private async Task Simulate4NastyaLoop()
        {
            // Get the current directory.
            string path = Directory.GetCurrentDirectory();

            string FolderName = "SimulateFiles";
            string FolderPath = (path + "\\" + FolderName);
            if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);

            string searchPattern = "*.txt";
            string[] stringList = Directory.GetFiles(FolderPath, searchPattern);

            DirectoryInfo di = new DirectoryInfo(FolderPath);
            FileInfo[] fileInfos = di.GetFiles(searchPattern);

            if (stringList.Count() == 0)
            {
                string Str = "Count of Simulate Files == 0\r\nPlease close DAPServer and add some *.txt files to SimulateFilesFolder and run it again.";
                ConsoleLog(false, Str);
                return;
            }

            while (true)
            {
                bool isNice = false;
                int number = 0;

                do
                {
                    Console.Write("Enter number of Simulate File: ");
                    isNice = Int32.TryParse(Console.ReadLine(), out number);
                }
                while (!isNice);

                number--;

                if (number >= 0)
                {
                    if (number < stringList.Count())
                    {
                        string FileName = stringList[number].Substring(stringList[number].LastIndexOf("\\") + 1);
                        Console.WriteLine(FileName);

                        var taus = ReadFromFile(stringList[number]);

                        for (int i = 0; i < taus.Count(); i++)
                        {
                            double[] arrtau123 = new double []  { taus[i].t1, taus[i].t2, taus[i].t3};

                            await NCorrFuncN(arrtau123);

                            var time = (i < taus.Count() - 1) ? taus[i+1].time - taus[i].time : 0;

                            int delay = (int)(time * 1000);
                            await Task.Delay(delay);

                            double p = ((double)i / (double)taus.Count());
                            Console.Write("\r{0}%", (int)(p * 100));
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Simulate File with Number[{number}] not found");
                        continue;
                    }

                    Console.WriteLine($"\rSimulate File with Number[{number}] complete");
                }
                await Task.Delay(1);
            }
        }


        private async Task NCorrFuncN(double[] arrtau123)
        {
            DateTime dateTimeNow = DateTime.Now;

            double innerCurrentFreqkHz =  2_400_000;
            double innerCurrentBandkHz = 20_000;

            double[] tau = new double[] { 0, arrtau123[0] * 1e-9, arrtau123[1] * 1e-9, arrtau123[2] * 1e-9 }; // массив задержек всех станций //Секунды

            //Класс Расчета координат
            fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

            //Инициализация листа координат станций и их задержек
            List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

            //Важная проверочка для рассчета координат
            if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
            {
                //Функция порядочка и определения базовой стации в листе по индексу
                List<int> GeneratesListOfisOwnIndexes()
                {
                    List<int> lintisOwn = new List<int>();
                    for (int k = 0; k < tableLocalPoints.Count; k++)
                    {
                        if (tableLocalPoints[k].IsCetral == true)
                        {
                            lintisOwn.Insert(0, k);
                        }
                        else
                        {
                            lintisOwn.Add(k);
                        }
                    }
                    return lintisOwn;
                }
                List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                //Заполенение листа координат станций и их задержек
                for (int k = 0; k < 4; k++)
                {
                    fDRM.ClassObject classObject = new fDRM.ClassObject()
                    {
                        Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                        Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                        Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                        BaseStation = (k == 0) ? true : false,
                        IndexStation = k + 1,
                        tau = tau[k]
                    };

                    lstClassObject.Add(classObject);
                }

                //Рассчёт координат источника
                fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject, isImageSolution, settings.DesiredHeight);
                //Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                //Анализ FlagRezCalc
                if (resultMark.FlagRezCalc == 0)
                {
                    //Отправка точки в АРМ
                    var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                    double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                    InputPointTrack inputPointTrack = new InputPointTrack()
                    {
                        LatMark = resultMark.Latitude,
                        LongMark = resultMark.Longitude,
                        HMark = resultMark.Altitude,
                        Time = Time,
                        Freq = (float)(innerCurrentFreqkHz),
                        dFreq = (float)(innerCurrentBandkHz),
                        _time = dateTimeNow
                    };
                    _tracksDefinition.f_TracksDefinition(inputPointTrack);

                    //Отработка по Дронам
                    LenaForMain();
                }
            }
        }
    }
}
