﻿using KirasaModelsDBLib;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using WpfMapRastr;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        private void UpdateTrackDefinitionsParameters(TracksDefinition tracksDefinition, List<GlobalProperties> TableGlobalProperties)
        {
            if (TableGlobalProperties.Count > 0)
            {
                _tracksDefinition.twait = TableGlobalProperties[0].KalmanFilterForCoordinate.TimeHoldToDetect;
                // Максимально допустимое время ожидания отметки для сопровождаемой трассы
                _tracksDefinition.twaitAirObject = TableGlobalProperties[0].KalmanFilterForCoordinate.MaxResetTime;
                // Допустимая скорость
                _tracksDefinition.Vdop = TableGlobalProperties[0].KalmanFilterForCoordinate.SpeedUavMax;
                // СКО ускорения на плоскости todo ask later
                _tracksDefinition.CKOA_pl = TableGlobalProperties[0].KalmanFilterForCoordinate.CoordinateDefinitionError;
                // СКО ускорения по высоте todo 
                _tracksDefinition.CKOA_H = TableGlobalProperties[0].KalmanFilterForCoordinate.XyzError;
                // СКО по координатам для формирования матрицы D 
                // ошибок измерения в данный момент времени
                _tracksDefinition.CKO_X = TableGlobalProperties[0].KalmanFilterForCoordinate.RmsX;
                _tracksDefinition.CKO_Y = TableGlobalProperties[0].KalmanFilterForCoordinate.RmsY;
                _tracksDefinition.CKO_Z = TableGlobalProperties[0].KalmanFilterForCoordinate.RmsZ;
                // Количество точек для инициализации фильтра
                _tracksDefinition.numbInit = TableGlobalProperties[0].KalmanFilterForCoordinate.MinAirObjPoints;
                //Статическая матрица ошибок
                _tracksDefinition.flagStatDyn = TableGlobalProperties[0].KalmanFilterForCoordinate.StaticErrorMatrix;
                // Отсеивание по высоте
                _tracksDefinition.Hmax = settings.HMax;

                isImageSolution = !TableGlobalProperties[0].DRMFilters.IsImaginarySolution;
            }
        }

        private void InitTrackDefinitions(ref TracksDefinition tracksDefinition, ref List<TrackAirObject> listTrackAirObject)
        {
            if (tableLocalPoints != null && tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
            {
                List<Station> listStations = new List<Station>();

                //Функция порядочка и определения базовой стации в листе по индексу
                List<int> GeneratesListOfisOwnIndexes()
                {
                    List<int> lintisOwn = new List<int>();
                    for (int k = 0; k < tableLocalPoints.Count; k++)
                    {
                        if (tableLocalPoints[k].IsCetral == true)
                        {
                            lintisOwn.Insert(0, k);
                        }
                        else
                        {
                            lintisOwn.Add(k);
                        }
                    }
                    return lintisOwn;
                }
                List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                //Заполенение листа координат станций и их задержек
                for (int k = 0; k < 4; k++)
                {
                    Station station = new Station()
                    {
                        Position = new MarkCoord(
                            tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                            tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                            tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,

                            tableLocalPoints[lintIsOwn[0]].Coordinates.Latitude,
                            tableLocalPoints[lintIsOwn[0]].Coordinates.Longitude,
                            tableLocalPoints[lintIsOwn[0]].Coordinates.Altitude
                            ),

                        ErrorTime = tableLocalPoints[lintIsOwn[k]].TimeError * 1e-9,
                    };
                    listStations.Add(station);
                }

                tracksDefinition = new TracksDefinition(ref listTrackAirObject)
                {
                    listStations = listStations
                };
            }
        }


        private void LenaForMain(int ID, Track track)
        {
            //new
            track.IDcountTrajNewForMain.Clear();
            for (int w = 0; w < track.listTrackAirObject.Count(); w++)
            {
                track.IDcountTrajNewForMain.Add(track.listTrackAirObject[w].ID, track.listTrackAirObject[w].Marks.Where(mark => mark.Skip == false).Count());
            }

            //Если изменилось кол-во Дронов
            if (track.IDcountTrajOldForMain.Count != track.IDcountTrajNewForMain.Count)
            {
                int dif = track.IDcountTrajNewForMain.Count - track.IDcountTrajOldForMain.Count;

                for (int q = 0; q < dif; q++)
                {
                    //получаем нужный ID для записи
                    int getID = track.IDcountTrajNewForMain.ElementAt(track.IDcountTrajNewForMain.Count - (dif - q)).Key;

                    //получаем по этому ID нужную запись из листа Лены
                    var record = track.listTrackAirObject.Where(el => el.ID == getID).FirstOrDefault();
                    //Получаем по этой записи строб
                    (double RadiusA, double RadiusB) Strob = (record.StrobX, record.StrobZ);
                    int Head = 0;

                    //Пишем в базу источник
                    AddTableUAVs(
                        //ID: record.ID,
                        ID: ID * 10 + record.ID,
                        State: record.State
                        );

                    var skipMarks = record.Marks.Where(mark => mark.Skip == false).ToList();

                    //Пишем все его избранные траектории
                    for (int v = 0; v < skipMarks.Count(); v++)
                    {
                        FRS tempFRS = new FRS();
                        tempFRS.FreqkHz = skipMarks[v].CoordPaint.Freq;
                        tempFRS.BandWidthkHz = skipMarks[v].CoordPaint.dFreq;
                        tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                        tempFRS.Coords.Add
                            (new KirasaModelsDBLib.Coord()
                            {
                                Latitude = skipMarks[v].CoordPaint.Latitude,
                                Longitude = skipMarks[v].CoordPaint.Longitude,
                                Altitude = (float)skipMarks[v].CoordPaint.Altitude
                            }
                            );

                        //Пишем его траекторию и добавляем к ней строб
                        AddTableUAVTrajectoriesWithStrobAndHead
                            (ID: //record.ID,
                            ID * 10 + record.ID,
                            num: (short)v,
                            tempFRS,
                            skipMarks[v].CoordPaint._time,
                            Strob,
                            Head
                            );
                    }
                }
            }
            else
            {
                //получаем нужный ID для записи
                if (track.IDcountTrajOldForMain.Count > 0 || track.IDcountTrajNewForMain.Count > 0)
                {
                    List<int> getIDs = Search4Changes();
                    for (int q = 0; q < getIDs.Count(); q++)
                    {
                        //получаем по этому ID нужную запись из листа Лены
                        var record = track.listTrackAirObject.Where(el => el.ID == getIDs[q]).FirstOrDefault();
                        //Получаем по этой записи строб
                        (double RadiusA, double RadiusB) Strob = (record.StrobX, record.StrobZ);
                        int Head = 0;

                        var skipMarks = record.Marks.Where(mark => mark.Skip == false).ToList();

                        //Дописываем в базу крайнюю избранную точку его траектории
                        if (record != null && skipMarks.Count > 0)
                        {
                            int v = skipMarks.Count - 1;

                            var lastMark = skipMarks[skipMarks.Count - 1];

                            FRS tempFRS = new FRS();
                            tempFRS.FreqkHz = lastMark.CoordPaint.Freq;
                            tempFRS.BandWidthkHz = lastMark.CoordPaint.dFreq;
                            tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                            tempFRS.Coords.Add
                                (new KirasaModelsDBLib.Coord()
                                {
                                    Latitude = lastMark.CoordPaint.Latitude,
                                    Longitude = lastMark.CoordPaint.Longitude,
                                    Altitude = (float)lastMark.CoordPaint.Altitude
                                }
                                );

                            //Дописываем в базу крайнюю избранную точку его траектории и добавляем к ней строб
                            AddTableUAVTrajectoriesWithStrobAndHead
                               (ID: //record.ID,
                               ID * 10 + record.ID,
                               num: (short)v,
                               tempFRS,
                               lastMark.CoordPaint._time,
                               Strob,
                               Head
                               );
                        }
                    }
                }
                List<int> Search4Changes()
                {
                    List<int> temp = new List<int>();
                    for (int l = 0; l < track.IDcountTrajOldForMain.Count(); l++)
                    {
                        if (track.IDcountTrajOldForMain.ElementAt(l).Value != track.IDcountTrajNewForMain.ElementAt(l).Value)
                        {
                            temp.Add(track.IDcountTrajOldForMain.ElementAt(l).Key);
                        }
                    }
                    return temp;
                }
            }

            track.IDcountTrajOldForMain.Clear();
            track.IDcountTrajOldForMain = new Dictionary<int, int>(track.IDcountTrajNewForMain);
            track.IDcountTrajNewForMain.Clear();
        }


        ConcurrentDictionary<int, Track> _concurrentIDTrackDictionary = new ConcurrentDictionary<int, Track>();
    }
}
