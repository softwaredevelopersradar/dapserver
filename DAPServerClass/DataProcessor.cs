﻿using DspDataModel;
using DspDataModel.Correlator;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using SettingsUtilities;
using SharpExtensions;
using Phases;

namespace DataProcessor
{
    public class Signal : ISignal
    {
        public static Config _config;

        //public Signal(Config config)
        //{
        //    _config = config;
        //}

        public float FrequencyKhz { get; }
        public float CentralFrequencyKhz { get; }
        public float Direction { get; }
        public float Reliability { get; }
        public float BandwidthKhz { get; }
        public float Amplitude { get; }
        public float StandardDeviation { get; }
        public float DiscardedDirectionsPart { get; }
        public float PhaseDeviation { get; }
        public SignalModulation Modulation { get; }
        public IReadOnlyList<float> Phases { get; }
        public TimeSpan BroadcastTimeSpan { get; }
        public float RelativeSubScanCount { get; }

        public Signal(
            float frequencyKhz, float centralFrequencyKhz, float direction, float reliability,
            float bandwidthKhz, float amplitude, float standardDeviation, float discardedDirectionsPart, float phaseDeviation, float relativeSubScanCount,
            IReadOnlyList<float> phases, SignalModulation modulation, TimeSpan broadcastTimeSpan)
        {
            FrequencyKhz = frequencyKhz;
            CentralFrequencyKhz = centralFrequencyKhz;
            Direction = direction;
            Reliability = reliability;
            BandwidthKhz = bandwidthKhz;
            Amplitude = amplitude;
            StandardDeviation = standardDeviation;
            DiscardedDirectionsPart = discardedDirectionsPart;
            Phases = phases;
            PhaseDeviation = phaseDeviation;
            Modulation = modulation;
            RelativeSubScanCount = relativeSubScanCount;
            BroadcastTimeSpan = broadcastTimeSpan;
        }

        /// <summary>
        /// Simplified constructor for testing purpose only
        /// </summary>
        public Signal(float frequencyKhz, float amplitude, float direction = 0, float bandwidthKhz = 1)
        {
            FrequencyKhz = frequencyKhz;
            CentralFrequencyKhz = frequencyKhz;
            Direction = direction;
            Reliability = 1;
            BandwidthKhz = bandwidthKhz;
            Amplitude = amplitude;
            StandardDeviation = 0;
            DiscardedDirectionsPart = 0;
            Phases = new float[Constants.PhasesDifferencesCount];
            PhaseDeviation = 0;
            Modulation = SignalModulation.Unknown;
            BroadcastTimeSpan = TimeSpan.Zero;
            RelativeSubScanCount = 1;
        }

        public static List<ISignal> MergeSubScanSignals(IReadOnlyList<ISignal> signals, float threshold, int directionAveragingCount = 1, bool calculateHighQualityPhases = false)
        {
            var resultSignals = new List<ISignal>();

            for (var i = 0; i < signals.Count;)
            {
                var j = i + 1;
                for (; j < signals.Count; ++j)
                {
                    if (!SubScanShouldBeMerged(signals[j - 1], signals[j]))
                    {
                        break;
                    }
                }
                if (j == i + 1)
                {
                    resultSignals.Add(signals[i]);
                }
                else
                {
                    var signalsToMerge = new ListSegment<ISignal>(signals, i, j - i);
                    resultSignals.Add(MergeSignals(signalsToMerge, threshold, directionAveragingCount, calculateHighQualityPhases));
                }
                i = j;
            }

            return resultSignals;
        }

        private static ISignal MergeSignals(IReadOnlyList<ISignal> signalsToMerge, float threshold, int directionAveragingCount = 1, bool calculateHighQualityPhases = false)
        {
            if (signalsToMerge.Count == 0)
            {
                throw new ArgumentException("Can't merge empty list of signals");
            }
            var left = signalsToMerge[0].LeftFrequency();
            var right = signalsToMerge[signalsToMerge.Count - 1].RightFrequency();
            var centralFrequencyKhz = (left + right) / 2;

            var amplitude = signalsToMerge.Max(s => s.Amplitude);
            var reliabilty = signalsToMerge.Max(s => s.Reliability);
            var bestSignal = signalsToMerge.First(s => s.Reliability == reliabilty);

            var frequency = bestSignal.FrequencyKhz;
            var phaseDeviation = bestSignal.PhaseDeviation;
            var subScanCount = signalsToMerge.Sum(s => s.RelativeSubScanCount) / directionAveragingCount;
            var phases = bestSignal.Phases;
            var disardedDirectionsPart = signalsToMerge.Average(s => s.DiscardedDirectionsPart);
            var broadcastTimeSpan = signalsToMerge.Aggregate(TimeSpan.Zero, (time, s) => time + s.BroadcastTimeSpan);

            if (calculateHighQualityPhases && bestSignal.IsDirectionReliable())
            {
                var mergedPhases = new float[Constants.PhasesDifferencesCount];
                for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
                {
                    var phaseFactors = signalsToMerge
                        .Select(s => new PhaseFactor(s.GetMergeFactor(threshold), s.Phases[i])).ToArray();
                    mergedPhases[i] = PhaseMath.CalculatePhase(phaseFactors);
                }
                phases = mergedPhases;
            }

            var direction = 0f;
            var standardDeviation = 0f;
            if (reliabilty > Constants.ReliabilityThreshold)
            {
                var directionFactors = signalsToMerge
                    .Select(s => new PhaseFactor(s.GetMergeFactor(threshold), s.Direction))
                    .ToArray();
                direction = PhaseMath.CalculatePhase(directionFactors);
                standardDeviation = PhaseMath.StandardDeviation(directionFactors);
            }

            return new Signal(
                frequencyKhz: frequency,
                centralFrequencyKhz: centralFrequencyKhz,
                direction: direction,
                reliability: reliabilty,
                bandwidthKhz: right - left,
                amplitude: amplitude,
                discardedDirectionsPart: disardedDirectionsPart,
                standardDeviation: standardDeviation,
                phases: phases,
                relativeSubScanCount: subScanCount,
                modulation: bestSignal.Modulation,
                phaseDeviation: phaseDeviation,
                broadcastTimeSpan: broadcastTimeSpan);
        }

        public static bool ShouldBeMerged(ISignal s1, ISignal s2)
        {
            var config = _config.DirectionFindingSettings;

            if (!s1.IsDirectionReliable() || !s2.IsDirectionReliable())
            {
                return SignalExtensions.Distance(s1, s2) < config.SignalsMergeGapKhz;
            }
            return SignalExtensions.Distance(s1, s2) < config.SignalsMergeGapKhz
                   && PhaseMath.Angle(s1.Direction, s2.Direction) < config.SignalsMergeDirectionDeviation;
        }

        public static bool SubScanShouldBeMerged(ISignal s1, ISignal s2)
        {
            return SignalExtensions.Distance(s1, s2) < _config.DirectionFindingSettings.SignalsMergeGapKhz;
        }
    }
    public class DataProcessor : IDataProcessor
    {
        public static Config _config;

        public DataProcessor(Config config)
        {
            _config = config;
        }
        public IPhaseCorrelator PhaseCorrelator => throw new NotImplementedException();

        public IScanReadTimer ScanReadTimer { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ProcessResult GetSignals(IAmplitudeScan amplitudeScan, ScanProcessConfig config)
        {
            var signalsRanges = GetSignalRanges(amplitudeScan, config.Threshold, config.FrequencyRanges);
            var signals = new List<ISignal>();

            foreach (var range in signalsRanges)
            {
                GetAmplitudeAndFrequency(amplitudeScan, range, out var amplitude, out var frequency);

                var maxIndex = range.From;
                for (int i = range.From; i < range.To; i++)
                {
                    if (amplitudeScan.Amplitudes[i] == amplitude)
                    {
                        maxIndex = i;
                        break;
                    }
                }

                //frequency = Utilities.GetFrequencyKhz(amplitudeScan.BandNumber, maxIndex);
                frequency = Utilities.GetK2FrequencyKhz(amplitudeScan.BandNumber, maxIndex);

                //var centralFrequencyKhz = Utilities.GetFrequencyKhz(amplitudeScan.BandNumber, range.CentralPointNumber);
                var centralFrequencyKhz = Utilities.GetK2FrequencyKhz(amplitudeScan.BandNumber, range.CentralPointNumber);
                var (broadcastTimeSpan, subScanCount) = (new TimeSpan(0, 0, 1), 1);

                var signal = new Signal(
                    frequencyKhz: frequency,
                    centralFrequencyKhz: centralFrequencyKhz,
                    direction: 0,
                    reliability: 0,
                    bandwidthKhz: CalculateBandwidth(amplitudeScan, config.Threshold, range),
                    amplitude: amplitude,
                    discardedDirectionsPart: 0,
                    standardDeviation: 0,
                    phases: null,
                    relativeSubScanCount: subScanCount,
                    modulation: SignalModulation.Unknown,
                    phaseDeviation: 0,
                    broadcastTimeSpan: broadcastTimeSpan);

                signals.Add(signal);
            }

            return new ProcessResult(signals);
        }

        public ISignal GetISignal(float centralFrequencyKhz, float bandwidthKhz)
        {
            var signals = new List<ISignal>();

                var signal = new Signal(
                    frequencyKhz: 0,
                    centralFrequencyKhz: centralFrequencyKhz,
                    direction: 0,
                    reliability: 0,
                    bandwidthKhz: bandwidthKhz,
                    amplitude: 0,
                    discardedDirectionsPart: 0,
                    standardDeviation: 0,
                    phases: null,
                    relativeSubScanCount: 0,
                    modulation: SignalModulation.Unknown,
                    phaseDeviation: 0,
                    broadcastTimeSpan: new TimeSpan());

                signals.Add(signal);

            return signals[0];
        }

        public ISignal CustomSignal(ISignal isignal, float bandwidthKhz)
        {
            var signals = new List<ISignal>();

            var signal = new Signal(
                frequencyKhz: isignal.FrequencyKhz,
                centralFrequencyKhz: isignal.CentralFrequencyKhz,
                direction: isignal.Direction,
                reliability: isignal.Reliability,
                bandwidthKhz: bandwidthKhz,
                amplitude: isignal.Amplitude,
                discardedDirectionsPart: isignal.DiscardedDirectionsPart,
                standardDeviation: isignal.StandardDeviation,
                phases: isignal.Phases,
                relativeSubScanCount: isignal.RelativeSubScanCount,
                modulation: SignalModulation.Unknown,
                phaseDeviation: isignal.PhaseDeviation,
                broadcastTimeSpan: isignal.BroadcastTimeSpan);

            signals.Add(signal);

            return signals[0];
        }

        public Signal CustomSignalWithShift(ISignal isignal, float shiftFrequencyKhz)
        {
            var signal = new Signal(
                frequencyKhz: isignal.FrequencyKhz + shiftFrequencyKhz,
                centralFrequencyKhz: isignal.CentralFrequencyKhz + shiftFrequencyKhz,
                direction: isignal.Direction,
                reliability: isignal.Reliability,
                bandwidthKhz: isignal.BandwidthKhz,
                amplitude: isignal.Amplitude,
                discardedDirectionsPart: isignal.DiscardedDirectionsPart,
                standardDeviation: isignal.StandardDeviation,
                phases: isignal.Phases,
                relativeSubScanCount: isignal.RelativeSubScanCount,
                modulation: isignal.Modulation,
                phaseDeviation: isignal.PhaseDeviation,
                broadcastTimeSpan: isignal.BroadcastTimeSpan);

            return signal;
        }

        internal struct Range
        {
            public readonly int From;
            public readonly int To;

            public int CentralPointNumber => (To + From) / 2;

            public Range(int from, int to) : this()
            {
                From = from;
                To = to;
            }
        }

        private static float CalculateBandwidth(IAmplitudeBand dataBand, float threshold, Range range)
        {
            float leftProportion = 0f, rightProportion = 0f;
            var spectrum = dataBand.Amplitudes;

            if (range.From != 0)
            {
                if (Math.Abs(spectrum[range.From] - spectrum[range.From - 1]) < 1e-3f)
                {
                    leftProportion = 0.5f;
                }
                else
                {
                    leftProportion = 1 - (threshold - spectrum[range.From - 1]) / (spectrum[range.From] - spectrum[range.From - 1]);
                }
            }
            if (range.To != dataBand.Count - 1)
            {
                if (Math.Abs(spectrum[range.To + 1] - spectrum[range.To]) < 1e-3f)
                {
                    rightProportion = 0.5f;
                }
                else
                {
                    rightProportion = (threshold - spectrum[range.To]) / (spectrum[range.To + 1] - spectrum[range.To]);
                }
            }
            //var bandwidth = (range.To - range.From + leftProportion + rightProportion) * Constants.SamplesGapKhz;
            var bandwidth = (range.To - range.From + leftProportion + rightProportion) * ConstantsK.SamplesGapKhz;
            if (bandwidth < 1e-3f)
            {
                bandwidth = 1;
            }
            Contract.Assert(bandwidth > 0 && !float.IsNaN(bandwidth));
            return bandwidth;
        }

        private static IEnumerable<Range> GetSignalRanges(IAmplitudeScan dataScan, float threshold, IReadOnlyList<FrequencyRange> frequencyRanges)
        {
            const float thresholdMultiplier = 0.7f;

            if (frequencyRanges == null)
            {
                foreach (var range in GetSignalsRanges(0, ConstantsK.BandSampleCount - 1))
                {
                    yield return range;
                }
                yield break;
            }

            foreach (var frequencyRange in frequencyRanges)
            {
                var startIndex = Utilities.GetSampleNumber(frequencyRange.StartFrequencyKhz, dataScan.BandNumber);
                var endIndex = Utilities.GetSampleNumber(frequencyRange.EndFrequencyKhz, dataScan.BandNumber);
                startIndex = startIndex.ToBounds(0, Constants.BandSampleCount - 1);
                endIndex = endIndex.ToBounds(0, Constants.BandSampleCount - 1);

                foreach (var range in GetSignalsRanges(startIndex, endIndex))
                {
                    yield return range;
                }
            }

            IEnumerable<Range> GetSignalsRanges(int startIndex, int endIndex)
            {
                for (var i = startIndex; i < endIndex;)
                {
                    if (dataScan.Amplitudes[i] <= threshold)
                    {
                        ++i;
                        continue;
                    }
                    var end = GetSignalEndPointIndex(dataScan, threshold, i, out var maxAmplitude);
                    // filtering straight signals with low amplitude
                    if ((end - i + 1) * Constants.SamplesGapKhz <
                        _config.DirectionFindingSettings.StraightSignalWidthKhz)
                    {
                        if (dataScan.Amplitudes[i] - threshold <
                            _config.DirectionFindingSettings.LowStraightSignalThreshold)
                        {
                            ++i;
                            continue;
                        }
                    }

                    // re-filter range with new threshold based on max amplitude
                    var adaptiveThreshold = maxAmplitude * (1 - thresholdMultiplier) + threshold * thresholdMultiplier;

                    foreach (var range in GetSignalRangesInner(adaptiveThreshold, i, end))
                    {
                        yield return range;
                    }
                    i = end + 1;
                }
            }

            IEnumerable<Range> GetSignalRangesInner(float innerThreshold, int from, int to)
            {
                for (var i = from; i < to;)
                {
                    if (dataScan.Amplitudes[i] <= innerThreshold)
                    {
                        ++i;
                        continue;
                    }
                    var end = GetSignalEndPointIndex(dataScan, innerThreshold, i, out _);

                    yield return new Range(i, end - 1);
                    i = end + 1;
                }
            }
        }
        private static void GetAmplitudeAndFrequency(IAmplitudeScan scan, Range range, out float amplitude, out float frequency)
        {
            var maxIndex = range.From;
            var maxAmplitude = float.MinValue;
            var bestDistanceToSignalCenter = int.MaxValue;
            var centralIndex = range.CentralPointNumber;

            const float maxAmplitudeDeviation = 0.1f;

            for (var i = range.From; i <= range.To; ++i)
            {
                var currentAmplitude = scan.Amplitudes[i];

                if (Math.Abs(currentAmplitude - maxAmplitude) < maxAmplitudeDeviation)
                {
                    var distanceToCenter = Math.Abs(i - centralIndex);
                    if (distanceToCenter < bestDistanceToSignalCenter)
                    {
                        bestDistanceToSignalCenter = distanceToCenter;
                        maxIndex = i;
                        maxAmplitude = currentAmplitude;
                    }
                }
                else if (currentAmplitude > maxAmplitude)
                {
                    bestDistanceToSignalCenter = Math.Abs(i - centralIndex);
                    maxIndex = i;
                    maxAmplitude = currentAmplitude;
                }
            }
            amplitude = maxAmplitude;
            //frequency = ClarifyFrequency(scan, maxIndex);
            frequency = ClarifyKFrequency(scan, maxIndex);
        }

        private static float ClarifyFrequency(IAmplitudeScan scan, int index)
        {
            if (index == 0 || index == scan.Count - 1)
            {
                return Utilities.GetFrequencyKhz(scan.BandNumber, index);
            }
            var leftAmplitude = scan.Amplitudes[index - 1];
            var rightAmplitude = scan.Amplitudes[index + 1];
            var minAmplitude = Math.Min(leftAmplitude, rightAmplitude);
            var maxAmplitude = scan.Amplitudes[index];

            if (Math.Abs(minAmplitude - maxAmplitude) < 1e-3)
            {
                return Utilities.GetFrequencyKhz(scan.BandNumber, index);
            }

            var shift = 0.5f * Constants.SamplesGapKhz * (rightAmplitude - leftAmplitude) / (maxAmplitude - minAmplitude);
            return Utilities.GetFrequencyKhz(scan.BandNumber, index) + shift;
        }
        private static float ClarifyKFrequency(IAmplitudeScan scan, int index)
        {
            if (index == 0 || index == scan.Count - 1)
            {
                return Utilities.GetK2FrequencyKhz(scan.BandNumber, index);
            }
            var leftAmplitude = scan.Amplitudes[index - 1];
            var rightAmplitude = scan.Amplitudes[index + 1];
            var minAmplitude = Math.Min(leftAmplitude, rightAmplitude);
            var maxAmplitude = scan.Amplitudes[index];

            if (Math.Abs(minAmplitude - maxAmplitude) < 1e-3)
            {
                return Utilities.GetK2FrequencyKhz(scan.BandNumber, index);
            }

            var shift = 0.5f * Constants.SamplesGapKhz * (rightAmplitude - leftAmplitude) / (maxAmplitude - minAmplitude);
            return Utilities.GetK2FrequencyKhz(scan.BandNumber, index);
        }
        private static int GetSignalEndPointIndex(IAmplitudeBand dataScan, float threshold, int signalStartPointIndex, out float maxAmplitude)
        {
            var endIndex = signalStartPointIndex + 1;
            maxAmplitude = dataScan.Amplitudes[signalStartPointIndex];
            for (var index = signalStartPointIndex + 1; index < dataScan.Count; ++index)
            {
                var amplitude = dataScan.Amplitudes[index];
                if (amplitude > maxAmplitude)
                {
                    maxAmplitude = amplitude;
                }
                if (amplitude > threshold)
                {
                    endIndex = index;
                    continue;
                }
                var stopSearch = true;
                for (var i = 1; i < _config.DirectionFindingSettings.SignalsGapPointCount; ++i)
                {
                    if (i + index == dataScan.Count)
                    {
                        return dataScan.Amplitudes[dataScan.Count - 1] > threshold ? dataScan.Count - 1 : endIndex;
                    }
                    if (dataScan.Amplitudes[i + endIndex] > threshold)
                    {
                        endIndex += i;
                        index += i;
                        stopSearch = false;
                        break;
                    }
                }
                if (stopSearch)
                    return endIndex;
            }
            return endIndex;
        }

        public ProcessResult GetSignals(IDataScan dataScan, ScanProcessConfig config)
        {
            throw new NotImplementedException();
        }

        public void ReloadRadioPathTable(string radioPathTableFilename)
        { }

        public void SetCorrelationType(int type)
        { }
    }
}
