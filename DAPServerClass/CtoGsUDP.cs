﻿using CtoGsProtocol;
using DAPprotocols;
using KirasaModelsDBLib;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        CtoGsUDPClient ctoGsUDPClient = new CtoGsUDPClient();

        private void InitCtoGsUDP(Settings settings)
        {
            InitCtoGsUDPEvents();

            ctoGsUDPClient.ConnectToUDP(
                localAddress: settings.CtoGsUDPSettings.myIpAdress,
                localPort: settings.CtoGsUDPSettings.myPort,
                remoteAddress: settings.CtoGsUDPSettings.remoteIpAdress,
                remotePort: settings.CtoGsUDPSettings.remotePort
                );

        }

        private void InitCtoGsUDPEvents()
        {
            ctoGsUDPClient.OnConnected += CtoGsUDPClient_OnConnected;
            ctoGsUDPClient.GetCuirasseMessage += CtoGsUDPClient_GetCuirasseMessage;
            ctoGsUDPClient.GetGrozaSMessage += CtoGsUDPClient_GetGrozaSMessage;
            ctoGsUDPClient.GetTypeAndCoordsMessage += CtoGsUDPClient_GetTypeAndCoordsMessage;
        }

        private async void CtoGsUDPClient_GetTypeAndCoordsMessage(TypeAndCoordsMessage answer)
        {
            //Отпправить в БД в Таблицу Другие Средства
            if (answer != null)
            {
                List<TableOtherPoints> tableOtherPoints = new List<TableOtherPoints>();

                for (int i = 0; i < answer.typeAndCoords.Count(); i++)
                {
                    tableOtherPoints.Add(new TableOtherPoints
                    {
                        Type = TypeOtherPoints.GrozaS,
                        Coordinates = new KirasaModelsDBLib.Coord()
                        {
                            Latitude = answer.typeAndCoords[i].coords.Latitude,
                            Longitude = answer.typeAndCoords[i].coords.Longitude,
                            Altitude = answer.typeAndCoords[i].coords.Altitude
                        }
                    });
                }

                await clientDB?.Tables[NameTable.TableOtherPoints].AddRangeAsync(tableOtherPoints);
            }
        }

        private void CtoGsUDPClient_OnConnected(bool onConnected)
        {

        }

        private void CtoGsUDPClient_GetCuirasseMessage(CuirasseMessage answer)
        {
            Console.Beep();
        }

        Dictionary<int, int> SignalIdAndGrozaId = new Dictionary<int, int>();

        private void CtoGsUDPClient_GetGrozaSMessage(GrozaSMessage answer)
        {
            float bandwidthMHz = (answer.gDrone.BandwidthMHz == 0) ? DefaultBandwidthMHzFromGs : answer.gDrone.BandwidthMHz;
            double centralFrequencyMHz = answer.gDrone.FrequencyMHz;

            TableSignalsUAV addSignal = new TableSignalsUAV()
            {
                Id = SignalId++,
                FrequencyKHz = centralFrequencyMHz * 1000d,
                BandKHz = (float)(bandwidthMHz * 1000),
                System = TypeSystem.CP_CuirasseM,
                TimeStart = DateTime.Now,
                TimeStop = DateTime.Now,
                Type = TypeUAVRes.Unknown,
                TypeL = TypeL.Enemy,
                TypeM = TypeM.Unknown,
                Correlation = false
            };

            SignalIdAndGrozaId.Add(addSignal.Id, answer.gDrone.ID);

            AddRecordToTableSignalsUAVs(addSignal);
        }


        private void SendcUAVtoGs(int iD, WpfMapRastr.TimeMarkCoord coordPaint)
        {
            UAV uAV = new UAV()
            {
                ID = iD,
                UAVDrone = new Drone()
                {
                    ID = (SignalIdAndGrozaId.ContainsKey(iD)) ? SignalIdAndGrozaId[iD] : 0,
                    Type = DroneType.Unknown,
                    FrequencyMHz = coordPaint.Freq / 1000f,
                    BandwidthMHz = coordPaint.dFreq / 1000f
                },
                UAVCoords = new Coords
                {
                    Latitude = coordPaint.Latitude,
                    Longitude = coordPaint.Longitude,
                    Altitude = (float)coordPaint.Altitude
                }
            };

            ctoGsUDPClient.SendUAVtoGs(uAV);
        }

        private async void MyDapServer_ExtraMessageResponseIsNeeded(int Response, ExtraMessage extraMessage)
        {
            List<TypeAndCoords> typeAndCoordsMessages = new List<TypeAndCoords>();

            lock (tableLocalPoints)
            {
                for (int i = 0; i < tableLocalPoints.Count; i++)
                {
                    typeAndCoordsMessages.Add
                        (new TypeAndCoords()
                        {
                            objectType = ObjectType.Station,
                            coords = new Coords()
                            {
                                Latitude = tableLocalPoints[i].Coordinates.Latitude,
                                Longitude = tableLocalPoints[i].Coordinates.Longitude,
                                Altitude = tableLocalPoints[i].Coordinates.Altitude,
                            }

                        });
                }
            }

            if (lTableGlobalProperties.Count > 0)
            {
                if (lTableGlobalProperties[0].Common.OperatorLatitude != 0 && lTableGlobalProperties[0].Common.OperatorLongitude != 0)
                {
                    typeAndCoordsMessages.Add
                      (new TypeAndCoords()
                      {
                          objectType = ObjectType.Operator,
                          coords = new Coords()
                          {
                              Latitude = lTableGlobalProperties[0].Common.OperatorLatitude,
                              Longitude = lTableGlobalProperties[0].Common.OperatorLongitude,
                              Altitude = 0,
                          }
                      });
                }
            }

            await ctoGsUDPClient.SendTypeAndCoordsMessage(typeAndCoordsMessages.ToArray());

            await MyDapServer.ServerSendExtraMessage();
        }
    }
}
