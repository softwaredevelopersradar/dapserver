﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System.Collections.ObjectModel;
using Nito.AsyncEx;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;

            (clientDB.Tables[NameTable.TableUAVRes] as ITableUpRecord<TableUAVRes>).OnDeleteRecord += OnDeleteRecord_TableUAVRes;
            (clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord += OnDeleteRecord_TableAeroscope;

            (clientDB.Tables[NameTable.TableSignalsUAV] as ITableUpRecord<TableSignalsUAV>).OnDeleteRecord += OnDeleteRecord_TableSignalsUAV;
        }

      

        async void HandlerConnect(object obj, EventArgs eventArgs)
        {
            //DB connect
            await InitTables2();
        }


        List<TableLocalPoints> tableLocalPoints;
        List<TableRemotePoints> tableRemotePoints;

        List<TableFreqRangesRecon> lTablesFreqRangesRecon = new List<TableFreqRangesRecon>();

        List<TableFreqKnown> lTablesFreqKnowns = new List<TableFreqKnown>();

        List<TableUAVRes> lTablesUAVRes = new List<TableUAVRes>();

        List<TableUAVTrajectory> lTableUAVTrajectory = new List<TableUAVTrajectory>();

        int lastIDTableUAVRes = 0;
        int lastIDTableUAVTrajectory = 0;

        List<TableAeroscope> lTablesAeroscopes = new List<TableAeroscope>();

        List<TableAeroscopeTrajectory> lTableAeroscopeTrajectory = new List<TableAeroscopeTrajectory>();

        int lastIDTablesAeroscopes = 0;
        int lastTableAeroscopeTrajectory = 0;

        List<GlobalProperties> lTableGlobalProperties = new List<GlobalProperties>();

        List<TableSignalsUAV> lTableSignalsUAVs = new List<TableSignalsUAV>();

        private async Task InitTables2()
        {
            tableLocalPoints = await clientDB?.Tables[NameTable.TableLocalPoints].LoadAsync<KirasaModelsDBLib.TableLocalPoints>();
            UpdateMainProcessorBase(tableLocalPoints);

            tableRemotePoints = await clientDB?.Tables[NameTable.TableRemotePoints].LoadAsync<KirasaModelsDBLib.TableRemotePoints>();


            lTablesFreqKnowns = await clientDB?.Tables[NameTable.TableFreqKnown].LoadAsync<KirasaModelsDBLib.TableFreqKnown>();
            UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqKnown);

            lTablesFreqRangesRecon = await clientDB?.Tables[NameTable.TableFreqRangesRecon].LoadAsync<KirasaModelsDBLib.TableFreqRangesRecon>();
            UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqRangesRecon);


            lTablesUAVRes = await clientDB?.Tables[NameTable.TableUAVRes].LoadAsync<KirasaModelsDBLib.TableUAVRes>();
            lastIDTableUAVRes = (lTablesUAVRes.Count > 0) ? lTablesUAVRes.Max(x => x.Id) + 1 : lTablesUAVRes.Count;

            Log($"Load data from Db. {(NameTable.TableUAVRes).ToString()} count records - {lTablesUAVRes.Count}");


            lTableUAVTrajectory = await clientDB?.Tables[NameTable.TableUAVTrajectory].LoadAsync<KirasaModelsDBLib.TableUAVTrajectory>();
            lastIDTableUAVTrajectory = (lTableUAVTrajectory.Count > 0) ? lTableUAVTrajectory.Max(x => x.Id) + 1 : lTableUAVTrajectory.Count;

            Log($"Load data from Db. {(NameTable.TableUAVTrajectory).ToString()} count records - {lTableUAVTrajectory.Count}");


            lTablesAeroscopes = await clientDB?.Tables[NameTable.TableАeroscope].LoadAsync<KirasaModelsDBLib.TableAeroscope>();
            //Console.WriteLine($"Load data from Db. {(NameTable.TableАeroscope).ToString()} count records - {table6.Count} \n");
            lastIDTablesAeroscopes = (lTablesAeroscopes.Count > 0) ? lTablesAeroscopes.Max(x => x.Id) + 1 : lTablesAeroscopes.Count;

            lTableAeroscopeTrajectory = await clientDB?.Tables[NameTable.TableАeroscopeTrajectory].LoadAsync<KirasaModelsDBLib.TableAeroscopeTrajectory>();
            //Console.WriteLine($"Load data from Db. {(NameTable.TableАeroscopeTrajectory).ToString()} count records - {table7.Count} \n");
            lastTableAeroscopeTrajectory = (lTableAeroscopeTrajectory.Count > 0) ? lTableAeroscopeTrajectory.Max(x => x.Id) + 1 : lTableAeroscopeTrajectory.Count;

            lTableGlobalProperties = await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<KirasaModelsDBLib.GlobalProperties>();
            UpdateInnerParameters(lTableGlobalProperties);
            UpdateMainProcessorTTFSettings(lTableGlobalProperties);

            lTableSignalsUAVs = await clientDB.Tables[NameTable.TableSignalsUAV].LoadAsync<TableSignalsUAV>();
            if (lTableSignalsUAVs.Count > 0) SignalId = lTableSignalsUAVs.Select(x => x.Id).Max() + 1;
            else SignalId = 1;

            UpdateTableSignalsUAVs(lTableSignalsUAVs);

            InitTrackDefinitions();
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            //DB disconnect
            if (eventArgs.GetMessage != "")
            {
                Console.WriteLine(eventArgs.GetMessage);
            }
            clientDB = null;
        }

        static AsyncLock asyncLockHandlerUpData = new AsyncLock();

        async void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Console.WriteLine($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            //});

            using (await asyncLockHandlerUpData.LockAsync())
            {
                switch (eventArgs.Name)
                {
                    case NameTable.TableLocalPoints:
                        tableLocalPoints = new List<TableLocalPoints>(eventArgs.AbstractData.ToList<TableLocalPoints>());
                        InitTrackDefinitions();
                        UpdateMainProcessorBase(tableLocalPoints);
                        break;

                    case NameTable.TableRemotePoints:
                        tableRemotePoints = new List<TableRemotePoints>(eventArgs.AbstractData.ToList<TableRemotePoints>());
                        break;

                    case NameTable.TableFreqKnown:
                        lTablesFreqKnowns = new List<TableFreqKnown>(eventArgs.AbstractData.ToList<TableFreqKnown>());
                        UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqKnown);
                        break;

                    case NameTable.TableFreqRangesRecon:
                        lTablesFreqRangesRecon = new List<TableFreqRangesRecon>(eventArgs.AbstractData.ToList<TableFreqRangesRecon>());
                        //ctsMain?.Cancel();
                        //await MainTask;
                        UpdateRROrKnownMinMaxFreqs(NameTable.TableFreqRangesRecon);
                        //MainReLoopIfNeeded();
                        break;

                    case NameTable.TableUAVRes:
                        lTablesUAVRes = new List<TableUAVRes>(eventArgs.AbstractData.ToList<TableUAVRes>());
                        Log($"lTablesUAVRes.Count(): {lTablesUAVRes.Count}");
                        //lastIDTableUAVRes = lTablesUAVRes.Count();

                        if (lTablesUAVRes.Count == 0)
                        {
                            _listTrackAirObject.Clear();
                            _concurrentIDTrackDictionary.Clear();
                            MpuMainProcessor.ClearDictionary();
                            await ctoGsUDPClient.SendDeleteMessage(0);
                        }

                        break;

                    case NameTable.TableUAVTrajectory:
                        lTableUAVTrajectory = new List<TableUAVTrajectory>(eventArgs.AbstractData.ToList<TableUAVTrajectory>());
                        Log($"lTableUAVTrajectory.Count(): {lTableUAVTrajectory.Count}");
                        //lastIDTableUAVTrajectory = lTableUAVTrajectory.Count();
                        break;

                    case NameTable.TableАeroscope:
                        lTablesAeroscopes = new List<TableAeroscope>(eventArgs.AbstractData.ToList<TableAeroscope>());
                        //Log($"lTablesAeroscopes.Count(): {lTablesAeroscopes.Count}");
                        break;

                    case NameTable.TableАeroscopeTrajectory:
                        lTableAeroscopeTrajectory = new List<TableAeroscopeTrajectory>(eventArgs.AbstractData.ToList<TableAeroscopeTrajectory>());
                        //Log($"lTableAeroscopeTrajectory.Count(): {lTableAeroscopeTrajectory.Count}");
                        break;

                    case NameTable.GlobalProperties:
                        lTableGlobalProperties = new List<GlobalProperties>(eventArgs.AbstractData.ToList<GlobalProperties>());
                        Log($"lTableGlobalProperties.Count(): {lTableGlobalProperties.Count}");
                        UpdateTrackDefinitionsParameters(lTableGlobalProperties);
                        UpdateInnerParameters(lTableGlobalProperties);
                        UpdateMainProcessorTTFSettings(lTableGlobalProperties);
                        break;

                    case NameTable.TableSignalsUAV:

                        UpdateTableSignalsUAVs(eventArgs.AbstractData.ToList<TableSignalsUAV>());

                        break;
                }
            }
        }

        static object locker = new object();

        private void UpdateTableSignalsUAVs(IEnumerable<TableSignalsUAV> tableSignalsUAVs)
        {
            lock (locker)
            {
                lTableSignalsUAVs = new List<TableSignalsUAV>(tableSignalsUAVs);

                if (tableSignalsUAVs.Count() == 0)
                {
                    SignalId = 1;
                    RecognitionMapping.Clear();
                }

                List<TableSignalsUAV> lForCorrelationTableSignalsUAVs = lTableSignalsUAVs.Where(x => x.Correlation == true).ToList();
                //SendToCorrelation(lForCorrelationTableSignalsUAVs);
                //if (lForCorrelationTableSignalsUAVs.Count > 0)
                FillDictionaryEPOListTableSignalsUAV(lForCorrelationTableSignalsUAVs);
            }
        }

        private Dictionary<int, List<TableSignalsUAV>> DictionaryEPOListTableSignalsUAV = new Dictionary<int, List<TableSignalsUAV>>();

        private List<int> ListEPOtoCorrelation = new List<int>();

        private void FillDictionaryEPOListTableSignalsUAV(IEnumerable<TableSignalsUAV> tableSignalsUAVs)
        {
            List<int> TempEPOList = new List<int>();

            Dictionary<int, List<TableSignalsUAV>> TempDictionaryEPOListTableSignalsUAV = new Dictionary<int, List<TableSignalsUAV>>();

            for (int i = 0; i < tableSignalsUAVs.Count(); i++)
            {
                int EPO = ConvertValueMHzToEPO(tableSignalsUAVs.ElementAt(i).FrequencyKHz / 1000d);

                if (!TempEPOList.Contains(EPO))
                    TempEPOList.Add(EPO);

                if (TempDictionaryEPOListTableSignalsUAV.ContainsKey(EPO))
                {
                    TempDictionaryEPOListTableSignalsUAV[EPO].Add(tableSignalsUAVs.ElementAt(i));
                }
                else
                {
                    TempDictionaryEPOListTableSignalsUAV.Add(EPO, new List<TableSignalsUAV> { tableSignalsUAVs.ElementAt(i) });
                }
            }

            ListEPOtoCorrelation = new List<int>(TempEPOList);

            DictionaryEPOListTableSignalsUAV = new Dictionary<int, List<TableSignalsUAV>>(TempDictionaryEPOListTableSignalsUAV);
        }
        

        private void SendToCorrelation(List<TableSignalsUAV> ToCorrelation)
        {
            //TO DO:

            //FillDictionaryEPOListTableSignalsUAV(ToCorrelation);

            if (ToCorrelation.Count == 0)
            {
                if (ApplyFilter == true)
                {
                    Task.Run(() => InnerStopLocal());
                }
            }
            if (ToCorrelation.Count == 1)
            {
                FilterMinFrequencyMHz = (float)(ToCorrelation[0].FrequencyKHz - ToCorrelation[0].BandKHz / 2); 
                FilterMaxFrequencyMHz = (float)(ToCorrelation[0].FrequencyKHz + ToCorrelation[0].BandKHz / 2);
                ApplyFilter = true;
            }
        }

        private void OnDeleteRecord_TableUAVRes(object sender, TableUAVRes e)
        {
            lTablesUAVRes.Remove(e);

            var removeIndex = _listTrackAirObject.FindIndex(x => x.ID == e.Id -1);
            if (removeIndex != -1)
                _listTrackAirObject.RemoveAt(removeIndex);

            (int IDtoDeleteDictionary, int IDtoDeleteTraj) = SupportCalc.EjectCiphers(e.Id - 1);

            if (_concurrentIDTrackDictionary.TryGetValue(IDtoDeleteDictionary, out Track track))
            {
                removeIndex = _concurrentIDTrackDictionary[IDtoDeleteDictionary].listTrackAirObject.FindIndex(x => x.ID == IDtoDeleteTraj);

                if (removeIndex != -1)
                    _concurrentIDTrackDictionary[IDtoDeleteDictionary].listTrackAirObject.RemoveAt(removeIndex);

                if (_concurrentIDTrackDictionary[IDtoDeleteDictionary].listTrackAirObject.Count == 0)
                {
                    _concurrentIDTrackDictionary.TryRemove(IDtoDeleteDictionary, out Track value);
                    MpuMainProcessor.RemoveFromDictionaryByID(IDtoDeleteDictionary);
                }
            }

            ctoGsUDPClient.SendDeleteMessage(e.Id);
        }



        private void OnDeleteRecord_TableAeroscope(object sender, TableAeroscope e)
        {
            lTablesAeroscopes.Remove(e);
        }


        void HandlerErrorDataBase(object obj, OperationTableEventArgs eventArgs)
        {
            Console.WriteLine(eventArgs.GetMessage);
        }

        private void OnDeleteRecord_TableSignalsUAV(object sender, TableSignalsUAV e)
        {
            //lTableSignalsUAVs.Remove(e);
            lTableSignalsUAVs.Remove(lTableSignalsUAVs.Where(x => x.Id == e.Id).FirstOrDefault());
            UpdateTableSignalsUAVs(lTableSignalsUAVs);
            RecognitionMappingDelete(e.Id);
        }


        private ObservableCollection<TableUAVTrajectory> GetTrajectory(double Freq, List<Coord> Coords)
        {
            ObservableCollection<TableUAVTrajectory> tableUAVTrajectories = new ObservableCollection<TableUAVTrajectory>();

            for (int i = 0; i < Coords.Count; i++)
            {
                tableUAVTrajectories.Add(new TableUAVTrajectory()
                {
                    Time = DateTime.Now,
                    FrequencyKHz = Freq,
                    Coordinates = new KirasaModelsDBLib.Coord()
                    {
                        Altitude = (float)Coords[i].Alt,
                        Latitude = Coords[i].Lat,
                        Longitude = Coords[i].Lon
                    },

                    Elevation = 0,
                    Num = 0
                }) ;
            }
            return tableUAVTrajectories;
        }

        //private void AddTableUAVs(FRS frsNew)
        //{
        //    var record = new TableUAVRes
        //    {
        //        FrequencyKHz = frsNew.Freq,
        //        Elevation = 0,
        //        Coordinates = new KirasaModelsDBLib.Coord()
        //        {
        //            Altitude = (float)frsNew.Coords[frsNew.Coords.Count - 1].Alt,
        //            Latitude = frsNew.Coords[frsNew.Coords.Count - 1].Lat,
        //            Longitude = frsNew.Coords[frsNew.Coords.Count - 1].Lon,
        //        },
        //        BandKHz = 0,
        //        Points = (short)frsNew.Coords.Count,
        //        Note = DateTime.Now.ToString(),
        //        Trajectory = GetTrajectory(frsNew.Coords)
        //    };

        //    clientDB?.Tables[NameTable.TableUAVRes].Add(record);
        //}

        bool randomUAV = false;
        private void AddTableUAVs(int ID, byte State, TypeUAVRes typeUAVRes = TypeUAVRes.Unknown)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = typeUAVRes;

            if (randomUAV)
            {
                switch (random.Next(0, 5))
                {
                    case 0:
                        tempTypeUAVRes = TypeUAVRes.Unknown;
                        break;
                    case 1:
                        tempTypeUAVRes = TypeUAVRes.Lightbridge;
                        break;
                    case 2:
                        tempTypeUAVRes = TypeUAVRes.Ocusinc;
                        break;
                    case 3:
                        tempTypeUAVRes = TypeUAVRes.WiFi;
                        break;
                    case 4:
                        tempTypeUAVRes = TypeUAVRes.G3;
                        break;
                }
            }

            //tempTypeUAVRes = TypeUAVRes.Ocusinc;

            var record = new TableUAVRes
            {
                Type = tempTypeUAVRes,
                Id = ID,
                State = State,
                IsActive = true,
                //Note = DateTime.Now.Millisecond.ToString()
            };

            try { clientDB?.Tables[NameTable.TableUAVRes].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }
        }

        private void AddTableUAVTrajectories(int ID, short num, FRS frsNew)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = 0,
                //Elevation = frsNew.Coords[0].Altitude,
                Num = num,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++
            };

            //recordtraj.Coordinates.Altitude = 255f + (float)(r.NextDouble() * 5f);

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AddTableUAVTrajectories(int ID, short num, FRS frsNew, DateTime dateTime)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                //Elevation = 0,
                Elevation = frsNew.Coords[0].Altitude,
                Num = num,
                Time = dateTime,
                Id = lastIDTableUAVTrajectory++
            };

            recordtraj.Coordinates.Altitude = 0;

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AddTableUAVTrajectoriesWithStrobAndHead(int ID, short num, FRS frsNew, DateTime dateTime, (double RadiusA, double RadiusB) Strob, int Head = 0)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                //Elevation = 0,
                Elevation = frsNew.Coords[0].Altitude,
                Num = num,
                Time = dateTime,
                Id = lastIDTableUAVTrajectory++,
                RadiusA = (float)Strob.RadiusA,
                RadiusB = (float)Strob.RadiusB,
                Head = (short)Head
            };

            recordtraj.Coordinates.Altitude = 0;

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }


        private void AddRecordToTableSignalsUAVs(TableSignalsUAV record)
        {
            try
            {
                clientDB?.Tables[NameTable.TableSignalsUAV].Add(record);
            }
            catch { }
        }

        private void ChangeRecordInTableSignalsUAVs(TableSignalsUAV record)
        {
            try
            {
                clientDB?.Tables[NameTable.TableSignalsUAV].Change(record);
            }
            catch { }
        }

        private void DeleteRecordInTableSignalsUAVs(TableSignalsUAV record)
        {
            try
            {
                clientDB?.Tables[NameTable.TableSignalsUAV].Delete(record);
            }
            catch { }
        }


        private void NewAddTableUAVs(FRS frsNew)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = TypeUAVRes.Unknown;

            if (randomUAV)
            {
                switch (random.Next(0, 5))
                {
                    case 0:
                        tempTypeUAVRes = TypeUAVRes.Unknown;
                        break;
                    case 1:
                        tempTypeUAVRes = TypeUAVRes.Lightbridge;
                        break;
                    case 2:
                        tempTypeUAVRes = TypeUAVRes.Ocusinc;
                        break;
                    case 3:
                        tempTypeUAVRes = TypeUAVRes.WiFi;
                        break;
                    case 4:
                        tempTypeUAVRes = TypeUAVRes.G3;
                        break;
                }
            }

            var record = new TableUAVRes
            {
                //Type = 0,
                Type = tempTypeUAVRes,
                //Note = DateTime.Now.ToString(),
                Note = DateTime.Now.Millisecond.ToString(),
                Id = lastIDTableUAVRes++,
                //IsActive = Convert.ToBoolean(lastIDTableUAVRes % 2)
                State = 1
            };

            try { clientDB?.Tables[NameTable.TableUAVRes].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }

            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = record.Id,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = frsNew.Coords[0].Altitude,
                Num = 0,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++
            };

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void NewAddTableAeroScopes(FRS frsNew)
        {
            Random random = new Random();

            TypeUAVRes tempTypeUAVRes = TypeUAVRes.Unknown;

            if (randomUAV)
            {
                switch (random.Next(0, 5))
                {
                    case 0:
                        tempTypeUAVRes = TypeUAVRes.Unknown;
                        break;
                    case 1:
                        tempTypeUAVRes = TypeUAVRes.Lightbridge;
                        break;
                    case 2:
                        tempTypeUAVRes = TypeUAVRes.Ocusinc;
                        break;
                    case 3:
                        tempTypeUAVRes = TypeUAVRes.WiFi;
                        break;
                    case 4:
                        tempTypeUAVRes = TypeUAVRes.G3;
                        break;
                }
            }

            var record = new TableAeroscope
            {
                Type = tempTypeUAVRes.ToString(),
                Id = lastIDTablesAeroscopes++,
                SerialNumber = (lastIDTablesAeroscopes - 1).ToString(),
                HomeLatitude = frsNew.Coords[0].Latitude,
                HomeLongitude = frsNew.Coords[0].Longitude,
            };

            try { clientDB?.Tables[NameTable.TableАeroscope].Add(record); }
            catch (Exception e) { Console.WriteLine(e); }

            var recordtraj = new TableAeroscopeTrajectory
            {
                Id = lastTableAeroscopeTrajectory++,
                Num = 0,
                SerialNumber = record.SerialNumber,
                Coordinates = frsNew.Coords[0]
            };
           

            try { clientDB?.Tables[NameTable.TableАeroscopeTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AnotherOneAddTableUAVs(int ID, short num, FRS frsNew)
        {
            var recordtraj = new TableUAVTrajectory
            {
                TableUAVResId = ID,
                FrequencyKHz = frsNew.FreqkHz,
                BandKHz = frsNew.BandWidthkHz,
                Coordinates = frsNew.Coords[0],
                Elevation = 0,
                Num = num,
                Time = DateTime.Now,
                Id = lastIDTableUAVTrajectory++
            };

            try { clientDB?.Tables[NameTable.TableUAVTrajectory].Add(recordtraj); }
            catch { }
        }

        private void AnotherOneAddTableAeroScopes(string ID, short num, FRS frsNew)
        {
            var recordtraj = new TableAeroscopeTrajectory
            {
                Id = lastTableAeroscopeTrajectory++,
                SerialNumber = ID,
                Num = num,
                Coordinates = frsNew.Coords[0]
            };

            try { clientDB?.Tables[NameTable.TableАeroscopeTrajectory].Add(recordtraj); }
            catch { }
        }


        private TableUAVRes GenerateChangeTableUAVs(FRS frsNew, int nufOfRec)
        {
            var record = new TableUAVRes
            {
                Id = nufOfRec,
                //Points = (short)frsNew.Coords.Count,
                Note = DateTime.Now.ToString(),
                //Trajectory = GetTrajectory(frsNew.Freq, frsNew.Coords)
            };
            return record;
        }
        
    }
}
