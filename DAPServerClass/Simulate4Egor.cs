﻿using Filter3DEstimation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WpfMapRastr;
using YamlReverseExpertise;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        private async Task Simulate4EgorLoop()
        {
            // Get the current directory.
            string path = Directory.GetCurrentDirectory();

            string FolderName = "SimulateFiles";
            string FolderPath = (path + "\\" + FolderName);
            if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);

            string searchPattern = "*.yaml";
            string[] stringList = Directory.GetFiles(FolderPath, searchPattern);

            DirectoryInfo di = new DirectoryInfo(FolderPath);
            FileInfo[] fileInfos = di.GetFiles(searchPattern);

            if (stringList.Count() == 0)
            {
                string Str = "Count of Simulate Files == 0\r\nPlease close DAPServer and add some ReverseExpertise.yaml files to SimulateFilesFolder and run it again.";
                ConsoleLog(false, Str);
                return;
            }

            while (true)
            {
                bool isNice = false;
                int number = 0;

                do
                {
                    Console.Write("Enter number of Simulate File: ");
                    isNice = Int32.TryParse(Console.ReadLine(), out number);
                }
                while (!isNice);

                number--;

                if (number >= 0)
                {
                    if (number < stringList.Count())
                    {
                        string FileName = stringList[number].Substring(stringList[number].LastIndexOf("\\") + 1);
                        Console.WriteLine(FileName);

                        Yaml yaml4Simulate = new Yaml();
                        ReverseExpertise reverseExpertise = yaml4Simulate.YamlLoad<ReverseExpertise>(stringList[number]);

                        FreqID.Clear();
                        GenerateFreqIDCountDictionary(FreqID, reverseExpertise);
                        Print(FreqID);

                        for (int i = 0; i < reverseExpertise.Expertises.Count(); i++)
                        {
                            int ID = FreqID[reverseExpertise.Expertises[i].FreqkHz].ID;
                            NCorrFunc2Emini(ID, reverseExpertise.Expertises[i]);
                            var time = (i < reverseExpertise.Expertises.Count() - 1) ? reverseExpertise.Expertises[i + 1].Time - reverseExpertise.Expertises[i].Time : new TimeSpan();

                            int delay = (int)(time.TotalMilliseconds);
                            await Task.Delay(delay);

                            double p = ((double)i / (double)reverseExpertise.Expertises.Count());
                            Console.Write("\r{0}%", (int)(p * 100));

                            //Console.SetCursorPosition(8, 0);
                            //Console.Write("{0}%", k);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Simulate File with Number[{number}] not found");
                        continue;
                    }

                    Console.WriteLine($"\rSimulate File with Number[{number}] complete");
                }

                if (!this.clientDB.IsConnected())
                {
                    this.clientDB.Connect();
                }
                await Task.Delay(1500);
            }
        }

        private async Task Simulate4EgorLoop2()
        {
            // Get the current directory.
            string path = Directory.GetCurrentDirectory();

            string FolderName = "SimulateFiles";
            string FolderPath = (path + "\\" + FolderName);
            if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);

            string searchPattern = "*.yaml";
            string[] stringList = Directory.GetFiles(FolderPath, searchPattern);

            DirectoryInfo di = new DirectoryInfo(FolderPath);
            FileInfo[] fileInfos = di.GetFiles(searchPattern);

            if (stringList.Count() == 0)
            {
                string Str = "Count of Simulate Files == 0\r\nPlease close DAPServer and add some ReverseExpertise.yaml files to SimulateFilesFolder and run it again.";
                ConsoleLog(false, Str);
                return;
            }

            while (true)
            {
                bool isNice = false;
                int number = 0;

                do
                {
                    Console.Write("Enter number of Simulate File: ");
                    isNice = Int32.TryParse(Console.ReadLine(), out number);
                }
                while (!isNice);

                number--;

                if (number >= 0)
                {
                    if (number < stringList.Count())
                    {
                        string FileName = stringList[number].Substring(stringList[number].LastIndexOf("\\") + 1);
                        Console.WriteLine(FileName);

                        Yaml yaml4Simulate = new Yaml();
                        ReverseExpertise reverseExpertise = yaml4Simulate.YamlLoad<ReverseExpertise>(stringList[number]);

                        FreqID.Clear();
                        GenerateFreqIDCountDictionary(FreqID, reverseExpertise);
                        Print(FreqID);

                        for (int i = 0; i < reverseExpertise.Expertises.Count(); i++)
                        {
                            int ID = FreqID[reverseExpertise.Expertises[i].FreqkHz].ID;
                            NCorrFunc2Emini(ID, reverseExpertise.Expertises[i]);

                            var time = (i < reverseExpertise.Expertises.Count() - 1) ? reverseExpertise.Expertises[i + 1].Time - reverseExpertise.Expertises[i].Time : new TimeSpan();

                            int delay = (int)(time.TotalMilliseconds);
                            await Task.Delay(delay);

                            double p = ((double)i / (double)reverseExpertise.Expertises.Count());
                            Console.Write("\r{0}%", (int)(p * 100));
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Simulate File with Number[{number}] not found");
                        continue;
                    }

                    Console.WriteLine($"\rSimulate File with Number[{number}] complete");
                }
                await Task.Delay(1);
            }
        }


        Dictionary<float, (int ID, int Count)> FreqID = new Dictionary<float, (int ID, int Count)>();

        private void GenerateFreqIDCountDictionary(Dictionary<float, (int ID, int Count)> FreqID, ReverseExpertise reverseExpertise)
        {
            int ID = 1;
            foreach (Expertise expertise in reverseExpertise.Expertises)
            {
                if (!FreqID.ContainsKey(expertise.FreqkHz))
                {
                    FreqID.Add(expertise.FreqkHz, (ID++, 0));
                }
                else
                {
                    FreqID[expertise.FreqkHz] = (FreqID[expertise.FreqkHz].ID, FreqID[expertise.FreqkHz].Count + 1);
                }
            }

        }

        private void Print(Dictionary<float, (int ID, int Count)> FreqID)
        {
            Console.WriteLine("FreqIDCountDictionary:");
            foreach (var el in FreqID)
            {
                Console.WriteLine($"FreqkHz: {el.Key} ID: {el.Value.ID} Count: {el.Value.Count}");
            }
        }

        private async Task NCorrFunc2Emini(int ID, Expertise expertise)
        {
            if (expertise != null)
            {
                if (expertise.Corr1_1 != null
                    && expertise.Corr2_1 != null
                    && expertise.Corr3_1 != null
                    && expertise.Corr4_1 != null
                    && expertise.Corr1_1.Count() > 0
                    && expertise.Corr2_1.Count() > 0
                    && expertise.Corr3_1.Count() > 0
                    && expertise.Corr4_1.Count() > 0
                    )
                {

                    DateTime dateTimeNow = expertise.Time;

                    double innerCurrentFreqkHz = expertise.FreqkHz;
                    double innerCurrentBandkHz = expertise.BandwidthkHz;


                    //Сфорировать корреляционные функции
                    List<List<double>> CorrDoubleList = new List<List<double>>();
                    CorrDoubleList.Add(expertise.Corr1_1.ToList());
                    CorrDoubleList.Add(expertise.Corr2_1.ToList());
                    CorrDoubleList.Add(expertise.Corr3_1.ToList());
                    CorrDoubleList.Add(expertise.Corr4_1.ToList());

                    //Сформировать для отправки в АРМ
                    (double[] corrFunc0, double[] corrFunc1, double[] corrFunc2, double[] corrFunc3) corrFuncTuple =
                        (expertise.Corr1_1, expertise.Corr2_1, expertise.Corr3_1, expertise.Corr4_1);

                    //Отправка корреляциооной функции в АРМ Кираса
                    _ = await MyDapServer.ServerSendCorrFunc
                        (0, (int)(innerCurrentFreqkHz), (int)(innerCurrentBandkHz),
                        corrFuncTuple.corrFunc0,
                        corrFuncTuple.corrFunc1,
                        corrFuncTuple.corrFunc2,
                        corrFuncTuple.corrFunc3
                        );

                    //Рассчёт координат по Егору
                    var resultEgorModule = CalcByNewEgorModule(ID, CorrDoubleList, expertise.Time);

                    var resultMark = resultEgorModule.resultMark;
                    var tau = resultEgorModule.originalTau;

                    //Отправка задержек в АРМ
                    _ = await MyDapServer.ServerSendTaus(tau[1], tau[2], tau[3]);

                    //Анализ FlagRezCalc
                    if (resultMark.FlagRezCalc == 0)
                    {
                        //Отправка точки в АРМ
                        var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                        //Запись в файл времён, задержек, координат
                        if (RecordStartStop)
                        {
                            expertises.Add(
                                new Expertise(
                                dateTimeNow,
                                (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                tau[1], tau[2], tau[3],
                                resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                                corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                ));
                        }

                        double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                        InputPointTrack inputPointTrack = new InputPointTrack()
                        {
                            LatMark = resultMark.Latitude,
                            LongMark = resultMark.Longitude,
                            HMark = resultMark.Altitude,
                            Time = Time,
                            Freq = (float)(innerCurrentFreqkHz),
                            dFreq = (float)(innerCurrentBandkHz),
                            _time = dateTimeNow
                        };
                        //_tracksDefinition.f_TracksDefinition(inputPointTrack);
                        EgorForMain(resultEgorModule.ResultByNewEgor, innerCurrentFreqkHz, innerCurrentBandkHz);
                        //Отработка по Дронам
                    }
                    else
                    {
                        //Запись в файл времён, задержек, координат
                        if (RecordStartStop)
                        {
                            expertises.Add(
                                new Expertise(
                                dateTimeNow,
                                (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                tau[1], tau[2], tau[3],
                                -1, -1, -1,
                                corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                ));
                        }
                    }
                }
            }
        }

        private int DiffegorIDCalc = 0;

        private async Task NCorrFunc2Emini2(int ID, Expertise expertise)
        {
            if (expertise != null)
            {
                if (expertise.Corr1_1 != null
                    && expertise.Corr2_1 != null
                    && expertise.Corr3_1 != null
                    && expertise.Corr4_1 != null
                    && expertise.Corr1_1.Count() > 0
                    && expertise.Corr2_1.Count() > 0
                    && expertise.Corr3_1.Count() > 0
                    && expertise.Corr4_1.Count() > 0
                    )
                {

                    DateTime dateTimeNow = expertise.Time;

                    double innerCurrentFreqkHz = expertise.FreqkHz;
                    double innerCurrentBandkHz = expertise.BandwidthkHz;


                    //Сфорировать корреляционные функции
                    List<List<double>> CorrDoubleList = new List<List<double>>();
                    CorrDoubleList.Add(expertise.Corr1_1.ToList());
                    CorrDoubleList.Add(expertise.Corr2_1.ToList());
                    CorrDoubleList.Add(expertise.Corr3_1.ToList());
                    CorrDoubleList.Add(expertise.Corr4_1.ToList());

                    //Сформировать для отправки в АРМ
                    (double[] corrFunc0, double[] corrFunc1, double[] corrFunc2, double[] corrFunc3) corrFuncTuple =
                        (expertise.Corr1_1, expertise.Corr2_1, expertise.Corr3_1, expertise.Corr4_1);

                    //Отправка корреляциооной функции в АРМ Кираса
                    var answer = await MyDapServer.ServerSendCorrFunc
                        (0, (int)(innerCurrentFreqkHz), (int)(innerCurrentBandkHz),
                        corrFuncTuple.corrFunc0,
                        corrFuncTuple.corrFunc1,
                        corrFuncTuple.corrFunc2,
                        corrFuncTuple.corrFunc3
                        );

                    //Рассчёт координат по Егору
                    var ResultByEgor = CalcByNewEgorModule(ID, CorrDoubleList, dateTimeNow);

                    var resultMark = GetResultMarkFromResultBox(ResultByEgor.resultMark);
                    var tau = ResultByEgor.originalTau;

                    //Анализ FlagRezCalc
                    if (resultMark.FlagRezCalc == 0)
                    {
                        //Отправка точки в АРМ
                        var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                        //Запись в файл времён, задержек, координат
                        if (RecordStartStop)
                        {
                            expertises.Add(
                                new Expertise(
                                dateTimeNow,
                                (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                tau[1], tau[2], tau[3],
                                resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                                corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                ));
                        }

                        if (!_concurrentIDTrackDictionary.ContainsKey(ID))
                        {
                            //Add

                            Track trackToAdd = new Track();

                            InitTrackDefinitions(ref trackToAdd.tracksDefinition, ref trackToAdd.listTrackAirObject);
                            UpdateTrackDefinitionsParameters(trackToAdd.tracksDefinition, lTableGlobalProperties);

                            _concurrentIDTrackDictionary.TryAdd(ID, trackToAdd);
                        }

                        if (_concurrentIDTrackDictionary.TryGetValue(ID, out Track trackToUpdate))
                        {
                            //Update
                            double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                            InputPointTrack inputPointTrack = new InputPointTrack()
                            {
                                LatMark = resultMark.Latitude,
                                LongMark = resultMark.Longitude,
                                HMark = resultMark.Altitude,
                                Time = Time,
                                Freq = (float)(innerCurrentFreqkHz),
                                dFreq = (float)(innerCurrentBandkHz),
                                _time = dateTimeNow
                            };
                            trackToUpdate.tracksDefinition.f_TracksDefinition(inputPointTrack);
                            
                            //Отработка по Дронам
                            LenaForMain(ID, trackToUpdate);
                        }
                    }
                    else
                    {
                        //Запись в файл времён, задержек, координат
                        if (RecordStartStop)
                        {
                            expertises.Add(
                                new Expertise(
                                dateTimeNow,
                                (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                tau[1], tau[2], tau[3],
                                -1, -1, -1,
                                corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                ));
                        }
                    }
                }
            }
        }

     
    }
}
