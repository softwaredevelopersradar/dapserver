﻿using Filter3DEstimation;
using System;
using System.Collections.Generic;
using DspDataModel;
using fDRM;


namespace DAPServerClass
{

    public partial class DapServerClass
    {
        private MPU3DEstimation.MainProcessor MpuMainProcessor = new MPU3DEstimation.MainProcessor();

        public void UpdateMainProcessorBase(List<KirasaModelsDBLib.TableLocalPoints> tableLocalPoints)
        {
            if (tableLocalPoints.Count == 4)
            {
                Point base0 = TableLocalPointsToBasePointConfigurationConverter(tableLocalPoints[0]).BasePointConfiguration;

                CommonClassesSOE.Point3D mpuBase0 = TableLocalPointsToMpu3DPointConverter(tableLocalPoints[0]).Mpu3DPoint;


                Point point1 = TableLocalPointsToBasePointConfigurationConverter(tableLocalPoints[1]).BasePointConfiguration;
                double ccv1ns = TableLocalPointsToBasePointConfigurationConverter(tableLocalPoints[1]).tau;

                CommonClassesSOE.Point3D mpuBase1 = TableLocalPointsToMpu3DPointConverter(tableLocalPoints[1]).Mpu3DPoint;


                Point point2 = TableLocalPointsToBasePointConfigurationConverter(tableLocalPoints[2]).BasePointConfiguration;
                double ccv2ns = TableLocalPointsToBasePointConfigurationConverter(tableLocalPoints[2]).tau;

                CommonClassesSOE.Point3D mpuBase2 = TableLocalPointsToMpu3DPointConverter(tableLocalPoints[2]).Mpu3DPoint;


                Point point3 = TableLocalPointsToBasePointConfigurationConverter(tableLocalPoints[3]).BasePointConfiguration;
                double ccv3ns = TableLocalPointsToBasePointConfigurationConverter(tableLocalPoints[3]).tau;

                CommonClassesSOE.Point3D mpuBase3 = TableLocalPointsToMpu3DPointConverter(tableLocalPoints[3]).Mpu3DPoint;

                //Координаты станций
                MpuMainProcessor.SetBaseConfiguration(mpuBase0, mpuBase1, mpuBase2, mpuBase3);

                //значения в наносекундах
                MpuMainProcessor.SetCorrelationCorrectionValues(new CommonClassesSOE.TauContainer() { tau12 = ccv1ns, tau13 = ccv2ns, tau14 = ccv3ns });
            }
        }

        public void UpdateMainProcessorCoefficientThreshold(Settings innerSettings)
        {
            // todo в новом модуле это внутри делается?
            //MainProcessor.SetCoefficientThreshold(innerSettings.CorrThreshold, innerSettings.CorrDivide);
        }

        public void UpdateMainProcessorTTFSettings(List<KirasaModelsDBLib.GlobalProperties> lTableGlobalProperties)
        {
            if (lTableGlobalProperties.Count > 0)
            {
                MpuMainProcessor.Properties = lTableGlobalProperties[0]; //todo check later
            }
        }

        private (Point BasePointConfiguration, double tau) TableLocalPointsToBasePointConfigurationConverter(KirasaModelsDBLib.TableLocalPoints tableLocalPoint)
        {
            return (new Point()
            {
                geodesic = new GeodesicCoordinates()
                {
                    Latitude = tableLocalPoint.Coordinates.Latitude,
                    Longitude = tableLocalPoint.Coordinates.Longitude,
                    Altitude = tableLocalPoint.Coordinates.Altitude
                }
            }, tableLocalPoint.TimeError);

        }

        private (CommonClassesSOE.Point3D Mpu3DPoint, double tau) TableLocalPointsToMpu3DPointConverter(KirasaModelsDBLib.TableLocalPoints tableLocalPoint)
        {
            return (new CommonClassesSOE.Point3D()
            {
                geodesic = new CommonClassesSOE.GeodesicCoordinates()
                {
                    Latitude = tableLocalPoint.Coordinates.Latitude,
                    Longitude = tableLocalPoint.Coordinates.Longitude,
                    Altitude = tableLocalPoint.Coordinates.Altitude
                }
            }, tableLocalPoint.TimeError);

        }

        public (fDRM.ClassObjectTmp resultMark, double[] originalTau, CommonClassesSOE.IDTargetDesignation ResultByNewEgor) CalcByNewEgorModule(int ID, List<List<double>> CorrDoubleList,DateTime time)
        {
            CommonClassesSOE.IDTargetDesignation ResultByNewEgor = MpuMainProcessor.Get3DEstimation(ID, CorrDoubleList, time.TimeOfDay.TotalSeconds);

            fDRM.ClassObjectTmp resultMark = new fDRM.ClassObjectTmp()
            {
                FlagRezCalc = 1,
                Latitude = -1,
                Longitude = -1,
                Altitude = -1
            };

            double[] originalTau = new double[] { 0, 0, 0, 0 };

            if (ResultByNewEgor != null)
            {
                resultMark = new fDRM.ClassObjectTmp()
                {
                    FlagRezCalc = 0,
                    Latitude = ResultByNewEgor.currentPositionPure.geodesic.Latitude,
                    Longitude = ResultByNewEgor.currentPositionPure.geodesic.Longitude,
                    Altitude = ResultByNewEgor.currentPositionPure.geodesic.Altitude
                };

                originalTau = new double[]
                { 0,
                    ResultByNewEgor.tauPure.tau12,
                    ResultByNewEgor.tauPure.tau13,
                    ResultByNewEgor.tauPure.tau14 };
            }

            return (resultMark, originalTau, ResultByNewEgor);
        }

        private fDRM.ClassObjectTmp GetResultMarkFromResultBox(ClassObjectTmp resultBox)
        {
            fDRM.ClassObjectTmp resultMark = new fDRM.ClassObjectTmp()
            {
                FlagRezCalc = 1,
                Latitude = -1,
                Longitude = -1,
                Altitude = -1
            };

            if (resultBox != null)
            {
                resultMark = new fDRM.ClassObjectTmp()
                {
                    FlagRezCalc = resultBox.FlagRezCalc,
                    Latitude = resultBox.Latitude,
                    Longitude = resultBox.Longitude,
                    Altitude = resultBox.Altitude
                };
            }

            return resultMark;
        }


        public double[] GetOriginalTauFromResultBox(ResultBox resultBox)
        {
            double[] originalTau = new double[] { 0, 0, 0, 0 };

            if (resultBox != null)
            {
                originalTau = new double[]
                    { 0,
                    resultBox.PureTau.tau12,
                    resultBox.PureTau.tau13,
                    resultBox.PureTau.tau14 };
            }

            return originalTau;
        }

    }
}
