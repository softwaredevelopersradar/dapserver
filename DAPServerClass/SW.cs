﻿using System;

namespace DAPServerClass
{
    public class SW
    {
        System.Diagnostics.Stopwatch stopWatch;

        private string _Caption;

        public SW(string Caption = "")
        {
            _Caption = (Caption == "") ? Caption : Caption + " ";
            //стопвотч
            stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();
        }

        public void Stop()
        {
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}.{1:000}", ts.Seconds, ts.Milliseconds);
            Console.WriteLine($"{_Caption}RunTime " + elapsedTime);
        }
    }
}
