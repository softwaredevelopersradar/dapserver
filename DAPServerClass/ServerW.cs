﻿using DspDataModel.DataProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using KirasaModelsDBLib;
using UDP_Cuirasse;

using DspDataModel;
using WpfMapRastr;
using YamlReverseExpertise;
using RecognitionSystemProtocol;
using DAPprotocols;
using System.Collections.Concurrent;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        private bool RecognitionStartStop { get; set; } = false;

        public float EpsilonFrequencyKhz { get; set; } = 1000;
        public float EpsilonBandwidthKhz { get; set; } = 5000;

        private int SignalId = 1;

        private int CorrRequestCount = 10;

        public double PrePorog { get; set; } = 110000;

        private void UpdateInnerParameters(List<GlobalProperties> lTableGlobalProperties)
        {
            if (lTableGlobalProperties.Count > 0)
            {
                EpsilonFrequencyKhz = (lTableGlobalProperties[0].RadioIntelegence.FrequencyAccuracy > 0) ? lTableGlobalProperties[0].RadioIntelegence.FrequencyAccuracy * 1000 : EpsilonFrequencyKhz;
                EpsilonBandwidthKhz = (lTableGlobalProperties[0].RadioIntelegence.BandAccuracy > 0) ? lTableGlobalProperties[0].RadioIntelegence.BandAccuracy * 1000 : EpsilonBandwidthKhz;
                CorrRequestCount = (lTableGlobalProperties[0].Correlation.AmountCorrelationQuery > 0) ? lTableGlobalProperties[0].Correlation.AmountCorrelationQuery : CorrRequestCount;
            }
        }

        private DapServerMode SubMode = DapServerMode.Stop;

        private ConcurrentDictionary<int, bool> RecognitionEPO = new ConcurrentDictionary<int, bool>();
        private async Task IntelligenceLoop0(CancellationToken token)
        {
            SubMode = DapServerMode.PureView;
            Console.WriteLine(SubMode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.PureView);

            CurrentFreqkHz = 0;
            _SaveIndex = 0;
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "IntelligenceLoop прерван токеном");
                    return;
                }

                int[] localGlobalRow = new int[0];
                lock (GlobalRow)
                {
                    localGlobalRow = GlobalRow.ToArray();
                }
                if (localGlobalRow.Length == 0)
                {
                    await _AsyncAutoResetEvent4GlobalRow.WaitAsync();
                    continue;
                }

                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "IntelligenceLoop прерван токеном");
                        return;
                    }

                    //token.ThrowIfCancellationRequested();

                    SW sW = new SW("IntelligenceLoop");

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    //Перестройка преселекторов новая последовательная 7
                    await PreselRealignment(centerFreqMHz, (byte)localGlobalRow[i], true);

                    //Настройка РПУ на текущую ЕПО по частоте
                    await RecRealignment(centerFreqMHz, (byte)localGlobalRow[i]);

                    //Чтение данных
                    GetSpectrumEventArgs answerGetSpectrum = await SpectrumWork(localGlobalRow[i]);

                    //Обнаружение ИРИ от Феди
                    ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters(answerGetSpectrum, localGlobalRow[i]);

                    //Обнаружение Сигналов
                    TableSignalsWork(result0);

                    //Работа по распознаванию
                    if (RecognitionStartStop)
                    {
                        await RecognitionL(localGlobalRow, i);
                        //if (!RecognitionEPO.ContainsKey(localGlobalRow[i])) RecognitionEPO.TryAdd(localGlobalRow[i], true);

                        //if (RecognitionEPO[localGlobalRow[i]])
                        //{
                        //    SW sW2 = new SW($"Recognition on EPO #{localGlobalRow[i]}");
                        //    //Запрос на систему распознавания
                        //    SetParamEventArgs answerRecognitionRequest = await AttemptCountCmdUdp<SetParamEventArgs>(7, 0, 0, 1);
                        //    sW2.Stop();
                        //    RecognitionEPO[localGlobalRow[i]] = false;
                        //}
                    }

                    //Работа с очередью запросов на распознование
                    await QueueWork(localGlobalRow[i]);

                    sW.Stop();

                    //await Task.Delay(1000);
                    //Console.WriteLine("PureView For end");

                    //Переключение на другой подрежим работы если нужно
                    SwitchIsNeeded();
                }

                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }

        private async Task RecognitionL(int[] localGlobalRow, int i)
        {
            await RecognitionL(localGlobalRow[i]);
        }

        private async Task RecognitionL(int EPO)
        {
            if (!RecognitionEPO.ContainsKey(EPO)) RecognitionEPO.TryAdd(EPO, true);

            if (RecognitionEPO[EPO])
            {
                SW sW2 = new SW($"Recognition on EPO #{EPO}");
                //Запрос на систему распознавания
                SetParamEventArgs answerRecognitionRequest = await AttemptCountCmdUdp<SetParamEventArgs>(7, 0, 0, 1);
                sW2.Stop();
                RecognitionEPO[EPO] = false;
            }
        }

        private bool IsAdditionalRecognition { get; set; } = false;
        private int NumberLoopRecognition { get; set; } = 4;

        private Dictionary<int, int> EPOinLoop = new Dictionary<int, int>();


        private async Task IntelligenceLoop(CancellationToken token)
        {
            SubMode = DapServerMode.PureView;
            Console.WriteLine(SubMode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.PureView);

            CurrentFreqkHz = 0;
            _SaveIndex = 0;
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "IntelligenceLoop2 прерван токеном");
                    return;
                }

                int[] localGlobalRow = new int[0];
                lock (GlobalRow)
                {
                    localGlobalRow = GlobalRow.ToArray();
                }
                if (localGlobalRow.Length == 0)
                {
                    await _AsyncAutoResetEvent4GlobalRow.WaitAsync();
                    continue;
                }

                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "IntelligenceLoop2 прерван токеном");
                        return;
                    }

                    SW sW = new SW("IntelligenceLoop2");

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    var tasks = new List<Task>();

                    //Перестройка преселекторов новая последовательная 7
                    tasks.Add(Task.Run(() => PreselRealignment(centerFreqMHz, (byte)localGlobalRow[i], true)));
                    //Настройка РПУ на текущую ЕПО по частоте
                    tasks.Add(Task.Run(() => RecRealignment(centerFreqMHz, (byte)localGlobalRow[i])));

                    SW sW1 = new SW("WaitAll Presel & Rec Realignment");
                    Task.WaitAll(tasks.ToArray());
                    sW1.Stop();

                    //Чтение данных
                    GetSpectrumEventArgs answerGetSpectrum = await SpectrumWork(localGlobalRow[i]);

                    //Обнаружение ИРИ от Феди
                    ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters(answerGetSpectrum, localGlobalRow[i]);

                    //Обнаружение Сигналов
                    TableSignalsWork(result0);

                    //Работа по распознаванию
                    if (RecognitionStartStop)
                    {
                        await RecognitionL(localGlobalRow, i);
                    }

                    //Работа с очередью запросов на распознование
                    await QueueWork(localGlobalRow[i]);

                    //Отправка дополнительного запроса на распознование c учётом флага
                    if (IsAdditionalRecognition)
                    {
                        //Если в словаре нет таких ЕПО по добавляем в словарь и делаем запрос на распознавание
                        if (!EPOinLoop.ContainsKey(localGlobalRow[i]))
                        {
                            EPOinLoop.Add((localGlobalRow[i]), 0);

                            SW sW2 = new SW($"Recognition on EPO #{localGlobalRow[i]}");
                            //Запрос на систему распознавания
                            SetParamEventArgs answerRecognitionRequest = await AttemptCountCmdUdp<SetParamEventArgs>(7, 0, 0, 1);
                            sW2.Stop();
                        }
                        //Если в словаре есть, то прибавляем единичку и смотрим не пора ли сделать запрос
                        else
                        {
                            EPOinLoop[localGlobalRow[i]]++;

                            if (EPOinLoop[localGlobalRow[i]] % NumberLoopRecognition == 0)
                            {
                                //обнуляем счётчик и делаем запрос на распознование
                                EPOinLoop[localGlobalRow[i]] = 0;

                                SW sW2 = new SW($"Recognition on EPO #{localGlobalRow[i]}");
                                //Запрос на систему распознавания
                                SetParamEventArgs answerRecognitionRequest = await AttemptCountCmdUdp<SetParamEventArgs>(7, 0, 0, 1);
                                sW2.Stop();
                            }

                        }
                    }

                    sW.Stop();

                    //Переключение на другой подрежим работы если нужно
                    SwitchIsNeeded();
                }

                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }

        private async Task ControlLoop(CancellationToken token)
        {
            SubMode = DapServerMode.Control;
            Console.WriteLine(SubMode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.Control);

            CurrentFreqkHz = 0;
            _SaveIndex = 0;
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "ControlLoop прерван токеном");
                    return;
                }

                int[] localGlobalRow = new int[0];
                lock (ListEPOtoCorrelation)
                {
                    localGlobalRow = ListEPOtoCorrelation.ToArray();
                }

                Dictionary<int, List<TableSignalsUAV>> localDictionary = new Dictionary<int, List<TableSignalsUAV>>();
                lock (DictionaryEPOListTableSignalsUAV)
                {
                    localDictionary = new Dictionary<int, List<TableSignalsUAV>>(DictionaryEPOListTableSignalsUAV);
                }

                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "ControlLoop прерван токеном");
                        return;
                    }

                    //token.ThrowIfCancellationRequested();

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    //Перестройка преселекторов новая последовательная 7
                    await PreselRealignment(centerFreqMHz, (byte)localGlobalRow[i]);

                    //Настройка РПУ на текущую ЕПО по частоте
                    SW sw = new SW("Перестройка РПУ");
                    await RecRealignment(centerFreqMHz, (byte)localGlobalRow[i]);
                    sw.Stop();
                    //Чтение данных
                    GetSpectrumEventArgs answerGetSpectrum = await SpectrumWork(localGlobalRow[i]);

                    //Обнаружение ИРИ от Феди
                    ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters(answerGetSpectrum, localGlobalRow[i]);

                    //Обнаружение Сигналов
                    TableSignalsWork(result0);

                    //Сигналы в конкретной ЕПО
                    var localSignals = localDictionary[localGlobalRow[i]];

                    //Цикл по сигналам в конкретной ЕПО
                    for (int j = 0; j < localSignals.Count; j++)
                    {
                        //Перестройка преселекторов новая последовательная 6
                        await PreselRealignment(localSignals[j].FrequencyKHz / 1000, (byte)localGlobalRow[i]);

                        //Настройка РПУ на текущую ЕПО по частоте
                        await RecRealignment(localSignals[j].FrequencyKHz / 1000, (byte)localGlobalRow[i]);

                        //Запрос корр функции N раз
                        for (int k = 0; k < CorrRequestCount; k++)
                        {
                            await NCorrFunc(localSignals[j].FrequencyKHz, localSignals[j].BandKHz);
                            await Task.Delay(1);
                        }
                    }

                    //Работа с очередью запросов на распознование
                    await QueueWork(localGlobalRow[i]);

                    //await Task.Delay(1000);
                    Console.WriteLine("Control For end");

                    //Переключение на другой подрежим работы если нужно
                    SwitchIsNeeded();
                }

                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }

        private async Task ControlLoop2(CancellationToken token)
        {
            SubMode = DapServerMode.Control;
            Console.WriteLine(SubMode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.Control);

            CurrentFreqkHz = 0;
            _SaveIndex = 0;
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "ControlLoop2 прерван токеном");
                    return;
                }

                int[] localGlobalRow = new int[0];
                lock (ListEPOtoCorrelation)
                {
                    localGlobalRow = ListEPOtoCorrelation.ToArray();
                }

                Dictionary<int, List<TableSignalsUAV>> localDictionary = new Dictionary<int, List<TableSignalsUAV>>();
                lock (DictionaryEPOListTableSignalsUAV)
                {
                    localDictionary = new Dictionary<int, List<TableSignalsUAV>>(DictionaryEPOListTableSignalsUAV);
                }

                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "ControlLoop2 прерван токеном");
                        return;
                    }

                    SW sW = new SW("ControlLoop2");

                    //Сигналы в конкретной ЕПО
                    var localSignals = localDictionary[localGlobalRow[i]];

                    //Цикл по сигналам в конкретной ЕПО
                    for (int j = 0; j < localSignals.Count; j++)
                    {
                        //Перестройка преселекторов новая последовательная 6
                        await PreselRealignment(localSignals[j].FrequencyKHz / 1000, (byte)localGlobalRow[i]);

                        //Настройка РПУ на текущую ЕПО по частоте
                        await RecRealignment(localSignals[j].FrequencyKHz / 1000, (byte)localGlobalRow[i]);

                        //Вычисление центральной частоты сигнала
                        double signalCenterFreqMHz = localSignals[j].FrequencyKHz / 1000;

                        //Чтение данных
                        GetSpectrumEventArgs answerGetSpectrum = await SpectrumWork2(signalCenterFreqMHz, localGlobalRow[i]);

                        //Обнаружение ИРИ от Феди
                        ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters2(answerGetSpectrum, signalCenterFreqMHz, localGlobalRow[i]);

                        //Обнаружение Сигналов
                        TableSignalsWork(result0);

                        //Запрос корр функции N раз
                        for (int k = 0; k < CorrRequestCount; k++)
                        {
                            await NCorrFunc(localSignals[j].FrequencyKHz, localSignals[j].BandKHz);
                            await Task.Delay(1);
                        }
                    }

                    //Работа с очередью запросов на распознование
                    await QueueWork(localGlobalRow[i]);

                    sW.Stop();

                    Console.WriteLine("Control2 For end");

                    //Переключение на другой подрежим работы если нужно
                    SwitchIsNeeded();
                }

                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }

        private async Task ControlLoop3(CancellationToken token)
        {
            SubMode = DapServerMode.Control;
            Console.WriteLine(SubMode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.Control);

            CurrentFreqkHz = 0;
            _SaveIndex = 0;
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "ControlLoop3 прерван токеном");
                    return;
                }

                int[] localGlobalRow = new int[0];
                lock (ListEPOtoCorrelation)
                {
                    localGlobalRow = ListEPOtoCorrelation.ToArray();
                }

                Dictionary<int, List<TableSignalsUAV>> localDictionary = new Dictionary<int, List<TableSignalsUAV>>();
                lock (DictionaryEPOListTableSignalsUAV)
                {
                    localDictionary = new Dictionary<int, List<TableSignalsUAV>>(DictionaryEPOListTableSignalsUAV);
                }

                //Перестройка только по тем ЕПО где есть сигналы
                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "ControlLoop3 прерван токеном");
                        return;
                    }

                    SW sW = new SW("ControlLoop3");

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    //Перестройка преселекторов новая последовательная 7
                    await PreselRealignment(centerFreqMHz, (byte)localGlobalRow[i], true);

                    //Настройка РПУ на текущую ЕПО по частоте
                    SW sw = new SW("Перестройка РПУ");
                    await RecRealignment(centerFreqMHz, (byte)localGlobalRow[i]);
                    sw.Stop();

                    //Чтение данных
                    GetSpectrumEventArgs answerGetSpectrum = await SpectrumWork(localGlobalRow[i]);

                    //Обнаружение ИРИ от Феди
                    ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters(answerGetSpectrum, localGlobalRow[i]);

                    //Обнаружение Сигналов
                    TableSignalsWork(result0);

                    //Сигналы в конкретной ЕПО
                    var localSignals = localDictionary[localGlobalRow[i]];

                    //Цикл по сигналам в конкретной ЕПО
                    for (int j = 0; j < localSignals.Count; j++)
                    {
                        //Настройка параметров корр функции
                        await CorrFuncSetParam(
                            innerFreqOffSetkHz: (int)(localSignals[j].FrequencyKHz - centerFreqMHz * 1000d),
                            innerFreqFilterValueMHz: (byte)(localSignals[j].BandKHz / 1000f)
                            );

                        //Запрос корр функции N раз
                        for (int k = 0; k < CorrRequestCount; k++)
                        {
                            await NCorrFunc(localSignals[j].FrequencyKHz, localSignals[j].BandKHz);
                            await Task.Delay(1);
                        }
                    }

                    //Работа с очередью запросов на распознование
                    await QueueWork(localGlobalRow[i]);

                    sW.Stop();

                    Console.WriteLine("Control3 For end");

                    //Переключение на другой подрежим работы если нужно
                    SwitchIsNeeded();
                }

                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }

        private async Task ControlLoop4(CancellationToken token)
        {
            SubMode = DapServerMode.Control;
            Console.WriteLine(SubMode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.Control);

            CurrentFreqkHz = 0;
            _SaveIndex = 0;
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "ControlLoop4 прерван токеном");
                    return;
                }

                int[] localGlobalRow = new int[0];
                lock (ListEPOtoCorrelation)
                {
                    localGlobalRow = ListEPOtoCorrelation.ToArray();
                }

                Dictionary<int, List<TableSignalsUAV>> localDictionary = new Dictionary<int, List<TableSignalsUAV>>();
                lock (DictionaryEPOListTableSignalsUAV)
                {
                    localDictionary = new Dictionary<int, List<TableSignalsUAV>>(DictionaryEPOListTableSignalsUAV);
                }

                //Перестройка только по тем ЕПО где есть сигналы
                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "ControlLoop4 прерван токеном");
                        return;
                    }

                    SW sW = new SW("ControlLoop4");

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    //Перестройка преселекторов новая последовательная 7
                    await PreselRealignment(centerFreqMHz, (byte)localGlobalRow[i], true);

                    //Настройка РПУ на текущую ЕПО по частоте
                    SW sw = new SW("Перестройка РПУ");
                    await RecRealignment(centerFreqMHz, (byte)localGlobalRow[i]);
                    sw.Stop();

                    //Чтение данных
                    GetSpectrumEventArgs answerGetSpectrum = await SpectrumWork(localGlobalRow[i]);

                    //Обнаружение ИРИ от Феди
                    ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters(answerGetSpectrum, localGlobalRow[i]);

                    //Обнаружение Сигналов
                    TableSignalsWork(result0);

                    //Сигналы в конкретной ЕПО
                    var localSignals = localDictionary[localGlobalRow[i]];

                    //Цикл по сигналам в конкретной ЕПО
                    for (int j = 0; j < localSignals.Count; j++)
                    {
                        //Настройка параметров корр функции
                        await CorrFuncSetParam(
                            innerFreqOffSetkHz: (int)(localSignals[j].FrequencyKHz - centerFreqMHz * 1000d),
                            innerFreqFilterValueMHz: (byte)(localSignals[j].BandKHz / 1000f)
                            );

                        //Запрос корр функции N раз
                        for (int k = 0; k < CorrRequestCount; k++)
                        {
                            await NCorrFunc2E(localSignals[j].Id, localSignals[j].FrequencyKHz, localSignals[j].BandKHz, localSignals[j].Type);
                            await Task.Delay(1);
                        }
                    }

                    //Работа с очередью запросов на распознование
                    await QueueWork(localGlobalRow[i]);

                    sW.Stop();

                    Console.WriteLine("Control4 For end");

                    //Переключение на другой подрежим работы если нужно
                    SwitchIsNeeded();
                }

                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }

        private async Task ControlLoop5(CancellationToken token)
        {
            SubMode = DapServerMode.Control;
            Console.WriteLine(SubMode);
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.Control);

            CurrentFreqkHz = 0;
            _SaveIndex = 0;
            while (_Mode != 0)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "ControlLoop4 прерван токеном");
                    return;
                }

                int[] localGlobalRow = new int[0];
                lock (ListEPOtoCorrelation)
                {
                    localGlobalRow = ListEPOtoCorrelation.ToArray();
                }

                Dictionary<int, List<TableSignalsUAV>> localDictionary = new Dictionary<int, List<TableSignalsUAV>>();
                lock (DictionaryEPOListTableSignalsUAV)
                {
                    localDictionary = new Dictionary<int, List<TableSignalsUAV>>(DictionaryEPOListTableSignalsUAV);
                }

                //Перестройка только по тем ЕПО где есть сигналы
                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "ControlLoop4 прерван токеном");
                        return;
                    }

                    SW sW = new SW("ControlLoop4");

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    //Перестройка преселекторов новая последовательная 7
                    await PreselRealignment(centerFreqMHz, (byte)localGlobalRow[i], true);

                    //Настройка РПУ на текущую ЕПО по частоте
                    SW sw = new SW("Перестройка РПУ");
                    await RecRealignment(centerFreqMHz, (byte)localGlobalRow[i]);
                    sw.Stop();

                    //Чтение данных
                    GetSpectrumEventArgs answerGetSpectrum = await SpectrumWork(localGlobalRow[i]);

                    //Обнаружение ИРИ от Феди
                    ProcessResult result0 = FedyasWorkPlusSignalTimePlusFilters(answerGetSpectrum, localGlobalRow[i]);

                    //Обнаружение Сигналов
                    TableSignalsWork(result0);

                    //Сигналы в конкретной ЕПО
                    var localSignals = localDictionary[localGlobalRow[i]];

                    //Цикл по сигналам в конкретной ЕПО
                    for (int j = 0; j < localSignals.Count; j++)
                    {
                        //Настройка параметров корр функции
                        await CorrFuncSetParam(
                            innerFreqOffSetkHz: (int)(localSignals[j].FrequencyKHz - centerFreqMHz * 1000d),
                            innerFreqFilterValueMHz: (byte)(localSignals[j].BandKHz / 1000f)
                            );

                        //Запрос корр функции N раз
                        for (int k = 0; k < CorrRequestCount; k++)
                        {
                            await NCorrFunc3E(localSignals[j].Id, localSignals[j].FrequencyKHz, localSignals[j].BandKHz, localSignals[j].Type);
                            await Task.Delay(1);
                        }
                    }

                    //Работа с очередью запросов на распознование
                    await QueueWork(localGlobalRow[i]);

                    sW.Stop();

                    Console.WriteLine("Control4 For end");

                    //Переключение на другой подрежим работы если нужно
                    SwitchIsNeeded();
                }

                await Task.Delay(1);
                _SaveIndex = 0;
            }
        }

        private void Inverse(GetCorrelationFuncEventArgs answerCor, bool inv1 = false, bool inv2 = false, bool inv3 = false)
        {
            if (inv1)
            {
                answerCor.Corr_1.Corr.Reverse();
            }
            if (inv2)
            {
               answerCor.Corr_2.Corr.Reverse();
            }
            if (inv3)
            {
                answerCor.Corr_3.Corr.Reverse();
            }
        }

        private double Sdvig { get; set; } = 0;

        private OrthoV OrthoVariant { get; set; } = DapServerClass.OrthoV.Max;

        private async Task NCorrFunc(double innerCurrentFreqkHz, double innerCurrentBandkHz)
        {
            DateTime dateTimeNow = DateTime.Now;

            //Запрос корреляционной функции
            //GetCorrelationFuncEventArgs answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncEventArgs>(4, 0, 0);
            dynamic answerCor0 = null;
            if (UDPVersion == 0)
            {
                answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncEventArgs>(4, 0, 0);
            }
            if (UDPVersion == 1 || UDPVersion == 2)
            {
                SW sw = new SW("Запрос корр функции: ");
                answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncVersionOneEventArgs>(4, 0, 0, 1);
                sw.Stop();
            }

            if (answerCor0 != null)
            {
                //if (OrthoNormalizationPreCheck(answerCor0, PrePorog) == false) return;

                //Inverse(answerCor0);

                //Ортонормировать корреляционные функции
                List<List<double>> CorrDoubleList = OrthoNorm(answerCor0, OrthoVariant);

                //Ортонормирование для отправки в АРМ
                (double[] corrFunc0, double[] corrFunc1, double[] corrFunc2, double[] corrFunc3) corrFuncTuple = OrthoNormToDoubleTuple(answerCor0, OrthoVariant);

                //Отправка корреляциооной функции в АРМ Кираса
                var answer = await MyDapServer.ServerSendCorrFunc
                    (0, (int)(innerCurrentFreqkHz), (int)(innerCurrentBandkHz),
                    corrFuncTuple.corrFunc0,
                    corrFuncTuple.corrFunc1,
                    corrFuncTuple.corrFunc2,
                    corrFuncTuple.corrFunc3
                    );

                //var dListTau123 = CalcTauFromCorrFuncsByEgor4(CorrDoubleList);
                var dListTau123 = CalcTauFromCorrFuncsByEgorTo3(CorrDoubleList);
                dListTau123 = SignMirror(dListTau123, 1, 1, 1); //new
                dListTau123 = ShiftFix(dListTau123, Sdvig);
                dListTau123 = ShiftFromDBTableLocalPoints(dListTau123);

                (double tau1, double tau2, double tau3) tau123 = (dListTau123[0], dListTau123[1], dListTau123[2]);
                double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды
                double[] tau = new double[] { 0, arrtau123[0] * 1e-9, arrtau123[1] * 1e-9, arrtau123[2] * 1e-9 }; // массив задержек всех станций //Секунды
                tau = SignMirror(tau, 1, 1, 1);

                bool isTau = false;

                //Console.WriteLine(tau123);

                if (arrtau123.All(x => x != -1))
                {
                    isTau = true;
                }
                if (tau.Any(x => Double.IsInfinity(x)) || tau.Any(x => Double.IsNaN(x)))
                {
                    isTau = false;
                }

                //Проверочка по задержкам
                if (isTau)
                {
                    //Класс Расчета координат
                    fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                    //Инициализация листа координат станций и их задержек
                    List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                    //Важная проверочка для рассчета координат
                    if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                    {
                        //Функция порядочка и определения базовой стации в листе по индексу
                        List<int> GeneratesListOfisOwnIndexes()
                        {
                            List<int> lintisOwn = new List<int>();
                            for (int k = 0; k < tableLocalPoints.Count; k++)
                            {
                                if (tableLocalPoints[k].IsCetral == true)
                                {
                                    lintisOwn.Insert(0, k);
                                }
                                else
                                {
                                    lintisOwn.Add(k);
                                }
                            }
                            return lintisOwn;
                        }
                        List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                        //Заполенение листа координат станций и их задержек
                        for (int k = 0; k < 4; k++)
                        {
                            fDRM.ClassObject classObject = new fDRM.ClassObject()
                            {
                                Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                BaseStation = (k == 0) ? true : false,
                                IndexStation = k + 1,
                                tau = tau[k]
                            };

                            lstClassObject.Add(classObject);
                        }

                        //Рассчёт координат источника
                        fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject, isImageSolution, settings.DesiredHeight, true, true, true);
                        //Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                        //Анализ FlagRezCalc
                        if (resultMark.FlagRezCalc == 0)
                        {
                            //Отправка точки в АРМ
                            var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                            //Запись в файл времён, задержек, координат
                            if (RecordStartStop)
                            {
                                expertises.Add(
                                    new Expertise(
                                    dateTimeNow,
                                    (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                    tau[1], tau[2], tau[3],
                                    resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                                    corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                    ));
                            }

                            double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                            InputPointTrack inputPointTrack = new InputPointTrack()
                            {
                                LatMark = resultMark.Latitude,
                                LongMark = resultMark.Longitude,
                                HMark = resultMark.Altitude,
                                Time = Time,
                                Freq = (float)(innerCurrentFreqkHz),
                                dFreq = (float)(innerCurrentBandkHz),
                                _time = dateTimeNow
                            };
                            _tracksDefinition.f_TracksDefinition(inputPointTrack);

                            //Отработка по Дронам
                            LenaForMain();
                        }
                        else
                        {
                            //Запись в файл времён, задержек, координат
                            if (RecordStartStop)
                            {
                                expertises.Add(
                                    new Expertise(
                                    dateTimeNow,
                                    (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                    tau[1], tau[2], tau[3],
                                    -1, -1, -1,
                                    corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                    ));
                            }
                        }
                    }
                }
            }
        }

        private async Task NCorrFunc2E(int ID, double innerCurrentFreqkHz, double innerCurrentBandkHz, TypeUAVRes typeUAVRes = TypeUAVRes.Unknown)
        {
            DateTime dateTimeNow = DateTime.Now;

            //Запрос корреляционной функции
            dynamic answerCor0 = null;
            if (UDPVersion == 0)
            {
                answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncEventArgs>(4, 0, 0);
            }
            if (UDPVersion == 1 || UDPVersion == 2)
            {
                SW sw = new SW("Запрос корр функции: ");
                answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncVersionOneEventArgs>(4, 0, 0, 1);
                sw.Stop();
            }

            if (answerCor0 != null)
            {
                //Ортонормировать корреляционные функции
                List<List<double>> CorrDoubleList = OrthoNorm(answerCor0, OrthoVariant);

                //Ортонормирование для отправки в АРМ
                (double[] corrFunc0, double[] corrFunc1, double[] corrFunc2, double[] corrFunc3) corrFuncTuple = OrthoNormToDoubleTuple(answerCor0, OrthoVariant);

                //Отправка корреляциооной функции в АРМ Кираса
                var answer = await MyDapServer.ServerSendCorrFunc
                    (0, (int)(innerCurrentFreqkHz), (int)(innerCurrentBandkHz),
                    corrFuncTuple.corrFunc0,
                    corrFuncTuple.corrFunc1,
                    corrFuncTuple.corrFunc2,
                    corrFuncTuple.corrFunc3
                    );

                //Рассчёт координат по Егору
                var resultEgorModule = CalcByNewEgorModule(ID, CorrDoubleList, DateTime.Now);

                var resultMark = resultEgorModule.resultMark;
                var tau = resultEgorModule.originalTau;

                //Отправка задержек в АРМ
                answer = await MyDapServer.ServerSendTaus(tau[1], tau[2], tau[3]);

                //Анализ FlagRezCalc
                if (resultMark.FlagRezCalc == 0)
                {
                    //Отправка точки в АРМ
                    var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                    //Запись в файл времён, задержек, координат
                    if (RecordStartStop)
                    {
                        expertises.Add(
                            new Expertise(
                            dateTimeNow,
                            (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                            tau[1], tau[2], tau[3],
                            resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                            corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                            ));
                    }

                    double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                    InputPointTrack inputPointTrack = new InputPointTrack()
                    {
                        LatMark = resultMark.Latitude,
                        LongMark = resultMark.Longitude,
                        HMark = resultMark.Altitude,
                        Time = Time,
                        Freq = (float)(innerCurrentFreqkHz),
                        dFreq = (float)(innerCurrentBandkHz),
                        _time = dateTimeNow
                    };
                    //_tracksDefinition.f_TracksDefinition(inputPointTrack);

                    //Отработка по Дронам
                    EgorForMain(resultEgorModule.ResultByNewEgor,innerCurrentFreqkHz, innerCurrentBandkHz);
                }
                else
                {
                    //Запись в файл времён, задержек, координат
                    if (RecordStartStop)
                    {
                        expertises.Add(
                            new Expertise(
                            dateTimeNow,
                            (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                            tau[1], tau[2], tau[3],
                            -1, -1, -1,
                            corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                            ));
                    }
                }
            }
        }
        private async Task NCorrFunc3E(int ID, double innerCurrentFreqkHz, double innerCurrentBandkHz, TypeUAVRes typeUAVRes = TypeUAVRes.Unknown)
        {
            DateTime dateTimeNow = DateTime.Now;

            //Запрос корреляционной функции
            dynamic answerCor0 = null;
            if (UDPVersion == 0)
            {
                answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncEventArgs>(4, 0, 0);
            }
            if (UDPVersion == 1 || UDPVersion == 2)
            {
                SW sw = new SW("Запрос корр функции: ");
                answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncVersionOneEventArgs>(4, 0, 0, 1);
                sw.Stop();
            }

            if (answerCor0 != null)
            {
                //Ортонормировать корреляционные функции
                List<List<double>> CorrDoubleList = OrthoNorm(answerCor0, OrthoVariant);

                //Ортонормирование для отправки в АРМ
                (double[] corrFunc0, double[] corrFunc1, double[] corrFunc2, double[] corrFunc3) corrFuncTuple = OrthoNormToDoubleTuple(answerCor0, OrthoVariant);

                //Отправка корреляциооной функции в АРМ Кираса
                var answer = await MyDapServer.ServerSendCorrFunc
                    (0, (int)(innerCurrentFreqkHz), (int)(innerCurrentBandkHz),
                    corrFuncTuple.corrFunc0,
                    corrFuncTuple.corrFunc1,
                    corrFuncTuple.corrFunc2,
                    corrFuncTuple.corrFunc3
                    );

                //Рассчёт координат по Егору
                var resultEgorModule = CalcByNewEgorModule(ID, CorrDoubleList, DateTime.Now);

                var resultMark = resultEgorModule.resultMark;
                var tau = resultEgorModule.originalTau;

                //Отправка задержек в АРМ
                answer = await MyDapServer.ServerSendTaus(tau[1], tau[2], tau[3]);

                //Анализ FlagRezCalc
                if (resultMark.FlagRezCalc == 0)
                {
                    //Отправка точки в АРМ
                    var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                    //Запись в файл времён, задержек, координат
                    if (RecordStartStop)
                    {
                        expertises.Add(
                            new Expertise(
                            dateTimeNow,
                            (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                            tau[1], tau[2], tau[3],
                            resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                            corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                            ));
                    }

                    //Отработка по Дронам
                    EgorForMain(resultEgorModule.ResultByNewEgor,innerCurrentFreqkHz,innerCurrentBandkHz, typeUAVRes);
                }
                else
                {
                    //Запись в файл времён, задержек, координат
                    if (RecordStartStop)
                    {
                        expertises.Add(
                            new Expertise(
                            dateTimeNow,
                            (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                            tau[1], tau[2], tau[3],
                            -1, -1, -1,
                            corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                            ));
                    }
                }
            }
        }


        private async Task CorrFuncSetParam(int innerFreqOffSetkHz, byte innerFreqFilterValueMHz)
        {
            var answerSetParam = await AttemptCountCmdUdp<SetParamEventArgs>(5, innerFreqOffSetkHz, innerFreqFilterValueMHz, 1);
        }

        private void SwitchIsNeeded()
        {
            if (ListEPOtoCorrelation.Count > 0 && SubMode == DapServerMode.PureView || ListEPOtoCorrelation.Count == 0 && SubMode == DapServerMode.Control)
            {
                Task.Run(() => Switcher3());
            }
        }

        CancellationTokenSource ctsMainGlobal;
        CancellationToken tokenMainGlobal;

        private async Task Switcher3()
        {
            ctsMainGlobal?.Cancel();

            try
            {
                await MainTask;
                Console.WriteLine("Success");
            }
            catch (OperationCanceledException)
            {
                Console.WriteLine("Cancelled");
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
            }

            ctsMainGlobal = new CancellationTokenSource();
            tokenMainGlobal = ctsMainGlobal.Token;

            if (ListEPOtoCorrelation.Count == 0)
            {
                MainTask = Task.Run(() => IntelligenceLoop(tokenMainGlobal));
            }
            else
            {
                if (settings.workMode == WorkMode.Main5)
                    MainTask = Task.Run(() => ControlLoop(tokenMainGlobal));
                if (settings.workMode == WorkMode.Main6)
                    MainTask = Task.Run(() => ControlLoop2(tokenMainGlobal));
                if (settings.workMode == WorkMode.Main7)
                    MainTask = Task.Run(() => ControlLoop3(tokenMainGlobal));
                if (settings.workMode == WorkMode.Main8)
                    MainTask = Task.Run(() => ControlLoop4(tokenMainGlobal));
                if (settings.workMode == WorkMode.Main9)
                    MainTask = Task.Run(() => ControlLoop5(tokenMainGlobal));
            }
        }


        private async Task PreselRealignment(double currentCenterFreqMHz, byte EPO, bool isAlwaysGainChange = false)
        {
            SW sw0 = new SW("Перестройка Преселекторов");
            if (PreselIsOn)
            {
                bool needFreqChange = (CurrentFreqkHz != currentCenterFreqMHz * 1000) ? true : false;

                Gain4Preselectors exitingValue = CurrentDictionary4PreSelGains[EPO];
                Gain4Preselectors needingValue = _GainSettings4.GetGainFromPresel(EPO);

                bool4Flags compareValues = Gain4Preselectors.NotCompareGains(exitingValue, needingValue);

                bool needGainChange = true;
                if (isAlwaysGainChange == false)
                {
                    needGainChange = bool4Flags.TotalOrCompare(compareValues);
                }

                //Перестройка преселекторов новая последовательная 7
                if (needFreqChange || needGainChange)
                {
                    bool[] bb = await SetPreselFreqGainAsyncСonsistently7(
                        Version: settings.PreselectorVersion,
                        Freq: (int)currentCenterFreqMHz,
                        FreqChange: needFreqChange,
                        Gain: needingValue,
                        GainChange: compareValues
                        );

                    CurrentDictionary4PreSelGains[EPO] = new Gain4Preselectors(needingValue);
                    //для отмены если ошибка по преселекторам
                    if (bb.Any(x => x == false))
                    {
                        for (int i4bb = 0; i4bb < bb.Count(); i4bb++)
                        {
                            if (bb[i4bb] == false)
                            {
                                ConsoleLog(false, $"Presel №{i4bb + 1} Error!");
                            }
                        }
                        //Task.Run(() => EndRI());
                        //return;
                    }
                }
            }
            sw0.Stop();
        }

        private async Task RecRealignment(double currentCenterFreqMHz, byte EPO)
        {
            SW sw = new SW("Перестройка РПУ");
            //Настройка РПУ на текущую ЕПО по частоте
            if (CurrentFreqkHz != currentCenterFreqMHz * 1000 ||
                CurrentDictionaryRecGain[EPO] != _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)EPO))
            {
                SetFreqEventArgs answerSetFreq = await AttemptCountCmdUdp<SetFreqEventArgs>(2, (int)(currentCenterFreqMHz * 1000), _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)EPO));

                CurrentDictionaryRecGain[EPO] = _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)EPO);

                CurrentFreqkHz = currentCenterFreqMHz * 1000;
                //для отмены если ошибка по udp приёму или приёмнику
                if (answerSetFreq == null)
                {
                    ConsoleLog(false, "UDP SetFreq Error!");
                    //Task.Run(() => EndRI());
                    //return;
                }
            }
            sw.Stop();
        }

        private async Task PreselAndRecRealignment(double currentCenterFreqMHz, byte EPO)
        {
            await PreselRealignment(currentCenterFreqMHz, EPO);
            await RecRealignment(currentCenterFreqMHz, EPO);
        }

        private async Task RecognitionPause(int EPO, RecognitionSystemProtocol.SetFrequencyRequest SetFrequencyRequest, DapServerMode currentMode)
        {
            var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DapServerMode.Recognition);

            double originalFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * EPO;

            int localEPO = ConvertValueMHzToEPO(SetFrequencyRequest.FrequencyMHz);

            double localFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localEPO;

            if (EPO != localEPO)
            {
                //Перестройка преселекторов и приёмника на нужно место для распознования
                await PreselAndRecRealignment(localFreqMHz, (byte)localEPO);
                SetFrequencyRequest.ReceiverFrequencyMHz = localFreqMHz;
            }

            var answerRecognition = await RecognitionMClient.SetFrequency(SetFrequencyRequest);

            await Task.Delay(2000);

            if (EPO != localEPO)
            {
                //Перестройка преселекторов и приёмника как и было до распознования
                await PreselAndRecRealignment(originalFreqMHz, (byte)EPO);
            }

            answer = await MyDapServer.ServerSendExtraordinaryModeMessage(currentMode);
        }


        private async Task<GetSpectrumEventArgs> SpectrumWork(int EPO)
        {
            //Чтение данных

            //Для большого хранилища
            List<GetSpectrumEventArgs> answersGetSpectrum = new List<GetSpectrumEventArgs>();
            for (int ww = 0; ww < _GlobalN; ww++)
            {
                SW sw = new SW($"Запрос спектра с канала {ww + 1}");
                GetSpectrumEventArgs tempAnswerGetSpectrum = await AttemptCountCmdUdp<GetSpectrumEventArgs>(3, ww, 0, 1);
                sw.Stop();
                if (tempAnswerGetSpectrum == null)
                {
                    ConsoleLog(false, $"UDP GetSpectrum Chanel {ChanelName(ww)} Error!");
                    List<float> innerInitList = new float[_GlobalDotsPerBandCount].ToList<float>();
                    innerInitList = innerInitList.Select(x => x = -130f).ToList<float>();
                    tempAnswerGetSpectrum = new GetSpectrumEventArgs(0, new byte[0], 0, 0, 0, innerInitList, UDPVersion);
                }
                answersGetSpectrum.Add(tempAnswerGetSpectrum);
            }

            GetSpectrumEventArgs answerGetSpectrum = answersGetSpectrum[_ChanelNumber];

            if (answerGetSpectrum.Ampl.Count == 8001) answerGetSpectrum.Ampl.RemoveAt(0);

            if (RecordStartStop)
            {
                DateTime Time = DateTime.Now;
                int ShiftDelay = (int)(Math.Abs(Time.TimeOfDay.TotalMilliseconds - TimeStart.TimeOfDay.TotalMilliseconds));
                SaveRecord(ShiftDelay, EPO, answerGetSpectrum.Ampl.ToArray());
            }

            //Обновляем хранилище спектра
            ListSpectrumStorage[EPO] = new List<float>(answerGetSpectrum.Ampl);

            //Для большого хранилища
            for (int ww = 0; ww < _GlobalN; ww++)
            {
                BigListSpectrumStorage[ww][EPO] = new List<float>(answersGetSpectrum[ww].Ampl);
            }

            return answerGetSpectrum;
        }

        private async Task<GetSpectrumEventArgs> SpectrumWork2(double CenterFreqMHz, int EPO)
        {
            //Чтение данных

            //Для большого хранилища
            List<GetSpectrumEventArgs> answersGetSpectrum = new List<GetSpectrumEventArgs>();
            for (int ww = 0; ww < _GlobalN; ww++)
            {
                SW sw = new SW($"Запрос спектра с канала {ww + 1}");
                GetSpectrumEventArgs tempAnswerGetSpectrum = await AttemptCountCmdUdp<GetSpectrumEventArgs>(3, ww, 0);
                sw.Stop();
                if (tempAnswerGetSpectrum == null)
                {
                    ConsoleLog(false, $"UDP GetSpectrum Chanel {ChanelName(ww)} Error!");
                    List<float> innerInitList = new float[_GlobalDotsPerBandCount].ToList<float>();
                    innerInitList = innerInitList.Select(x => x = -130f).ToList<float>();
                    tempAnswerGetSpectrum = new GetSpectrumEventArgs(0, new byte[0], 0, 0, 0, innerInitList, UDPVersion);
                }
                answersGetSpectrum.Add(tempAnswerGetSpectrum);
            }

            GetSpectrumEventArgs answerGetSpectrum = answersGetSpectrum[_ChanelNumber];
            if (answerGetSpectrum.Ampl.Count == 8001) answerGetSpectrum.Ampl.RemoveAt(0);
            //Обновляем хранилище спектра
            ListSpectrumStorage[EPO] = new List<float>(answerGetSpectrum.Ampl);

            List<ArrAndEPO<float>> ArrAndEPOs = new List<ArrAndEPO<float>>();
            for (int ww = 0; ww < _GlobalN; ww++)
            {
                ArrAndEPO<float> temp = SeparateArrDependCenterFreq2(CenterFreqMHz, answersGetSpectrum[ww].Ampl.ToArray());
                ArrAndEPOs.Add(temp);
            }

            for (int ww = 0; ww < _GlobalN; ww++)
            {
                List<float> LeftList = new List<float>(BigListSpectrumStorage[ww][ArrAndEPOs[ww].LeftEPO]);
                List<float> RightList = new List<float>(BigListSpectrumStorage[ww][ArrAndEPOs[ww].RightEPO]);

                for (int q = 0; q < ArrAndEPOs[ww].ArrToLeftEPO.Count(); q++)
                {
                    LeftList[LeftList.Count() - 1 - q] = ArrAndEPOs[ww].ArrToLeftEPO[ArrAndEPOs[ww].ArrToLeftEPO.Count() - 1 - q];
                }
                for (int q = 0; q < ArrAndEPOs[ww].ArrToRightEPO.Count(); q++)
                {
                    RightList[q] = ArrAndEPOs[ww].ArrToRightEPO[q];
                }

                //Для большого хранилища
                BigListSpectrumStorage[ww][ArrAndEPOs[ww].LeftEPO] = new List<float>(LeftList);
                BigListSpectrumStorage[ww][ArrAndEPOs[ww].RightEPO] = new List<float>(RightList);
            }

            return answerGetSpectrum;
        }

        private ProcessResult FedyasWorkPlusSignalTimePlusFilters(GetSpectrumEventArgs answerGetSpectrum, int EPO)
        {
            //Обнаружение ИРИ от Феди
            DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(answerGetSpectrum.Ampl.ToArray(), EPO, DateTime.Now, 0);

            //Параметр для поиска сигналов по порогу
            ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(_Threshold);

            //Результаты сигналов
            var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

            //Ассоциация сигнал-время
            if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
            {
                //Console.WriteLine(result0.Signals.Count);
                SignalTime.Clear();
                for (int qq = 0; qq < result0.Signals.Count; qq++)
                {
                    SignalTime.Add(result0.Signals[qq], DateTime.Now);
                }
            }

            //Фильтр по диапазонам РР
            {
                var signals = new List<ISignal>();

                for (int v = 0; v < result0.Signals.Count; v++)
                {
                    if (FRSonRangeRI(result0.Signals[v].CentralFrequencyKhz / 1000d))
                    {
                        signals.Add(result0.Signals[v]);
                    }
                }
                result0 = new ProcessResult(signals);
            }

            //Фильтр по ширине
            {
                var signals = new List<ISignal>();

                for (int v = 0; v < result0.Signals.Count; v++)
                {
                    if (result0.Signals[v].BandwidthKhz >= _FilterMinBandWidthkHz && result0.Signals[v].BandwidthKhz <= _FilterMaxBandWidthkHz)
                    {
                        signals.Add(result0.Signals[v]);
                    }
                }
                result0 = new ProcessResult(signals);
            }

            return result0;
        }

        private ProcessResult FedyasWorkPlusSignalTimePlusFilters2(GetSpectrumEventArgs answerGetSpectrum, double signalCenterFreqMHz, int EPO)
        {
            //Вычисление центральной частоты ЕПО
            double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * EPO;

            //Вычисление сдвига в кГц
            double shiftFreqkHz = (signalCenterFreqMHz - centerFreqMHz) * 1000;

            //Обнаружение ИРИ от Феди
            DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(answerGetSpectrum.Ampl.ToArray(), EPO, DateTime.Now, 0);

            //Параметр для поиска сигналов по порогу
            ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(_Threshold);

            //Результаты сигналов
            var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

            //Применение сдвига
            {
                var signals = new List<ISignal>();

                for (int v = 0; v < result0.Signals.Count; v++)
                {
                    var signal = dataProcessor0.CustomSignalWithShift(result0.Signals[v], (float)shiftFreqkHz);
                    signals.Add(signal);
                }
                result0 = new ProcessResult(signals);
            }

            //Ассоциация сигнал-время
            if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
            {
                //Console.WriteLine(result0.Signals.Count);
                SignalTime.Clear();
                for (int qq = 0; qq < result0.Signals.Count; qq++)
                {
                    SignalTime.Add(result0.Signals[qq], DateTime.Now);
                }
            }

            //Фильтр по диапазонам РР
            {
                var signals = new List<ISignal>();

                for (int v = 0; v < result0.Signals.Count; v++)
                {
                    if (FRSonRangeRI(result0.Signals[v].CentralFrequencyKhz / 1000d))
                    {
                        signals.Add(result0.Signals[v]);
                    }
                }
                result0 = new ProcessResult(signals);
            }

            //Фильтр по ширине
            {
                var signals = new List<ISignal>();

                for (int v = 0; v < result0.Signals.Count; v++)
                {
                    if (result0.Signals[v].BandwidthKhz >= _FilterMinBandWidthkHz && result0.Signals[v].BandwidthKhz <= _FilterMaxBandWidthkHz)
                    {
                        signals.Add(result0.Signals[v]);
                    }
                }
                result0 = new ProcessResult(signals);
            }

            return result0;
        }

        private void TableSignalsWork(ProcessResult result0)
        {
            List<double> Freqs = result0.Signals.Select(x => (double)x.FrequencyKhz).ToList<double>();

            TableSignalsUAV[] localTableSignalsUAVs = new TableSignalsUAV[0];
            lock (lTableSignalsUAVs)
            {
                localTableSignalsUAVs = lTableSignalsUAVs.ToArray();
            }

            List<ISignal> lSignals = new List<ISignal>(result0.Signals.ToList());

            //В цикле
            for (int j = 0; j < localTableSignalsUAVs.Count(); j++)
            {
                for (int k = 0; k < lSignals.Count(); k++)
                {
                    //Сравнить сигнал с каждым элементом в листе и обновить в базе
                    (float FrequencyKhz, float BandwidthKhz) signal1 = ((float)localTableSignalsUAVs[j].FrequencyKHz, localTableSignalsUAVs[j].BandKHz);
                    (float FrequencyKhz, float BandwidthKhz) signal2 = (lSignals[k].CentralFrequencyKhz, lSignals[k].BandwidthKhz);
                    if (CompareSignalsKhz(signal1, signal2))
                    {
                        TableSignalsUAV updateSignal = new TableSignalsUAV()
                        {
                            Id = localTableSignalsUAVs[j].Id,
                            FrequencyKHz = lSignals[k].CentralFrequencyKhz,
                            BandKHz = lSignals[k].BandwidthKhz,
                            System = localTableSignalsUAVs[j].System,
                            TimeStart = localTableSignalsUAVs[j].TimeStart,
                            TimeStop = SignalTime[lSignals[k]],
                            Type = localTableSignalsUAVs[j].Type,
                            TypeL = localTableSignalsUAVs[j].TypeL,
                            TypeM = localTableSignalsUAVs[j].TypeM,
                            Correlation = localTableSignalsUAVs[j].Correlation
                        };
                        ChangeRecordInTableSignalsUAVs(updateSignal);
                        lSignals.RemoveAt(k);
                        k--;
                    }
                }
            }

            //Добавить оставшиеся необновленные, значит новые, сигналы в базу
            for (int k = 0; k < lSignals.Count(); k++)
            {
                TableSignalsUAV addSignal = new TableSignalsUAV()
                {
                    Id = SignalId++,
                    FrequencyKHz = lSignals[k].CentralFrequencyKhz,
                    BandKHz = lSignals[k].BandwidthKhz,
                    System = TypeSystem.Spectrum,
                    TimeStart = SignalTime[lSignals[k]],
                    TimeStop = SignalTime[lSignals[k]],
                    Type = TypeUAVRes.Unknown,
                    TypeL = TypeL.Enemy,
                    TypeM = TypeM.Unknown,
                    Correlation = false
                };
                AddRecordToTableSignalsUAVs(addSignal);
            }

        }

        private async Task QueueWork(int EPO)
        {
            if (RecognitionQueue.Count > 0)
            {
                if (RecognitionQueue.TryPeek(out SetFrequencyRequest result))
                {
                    RecognitionQueue.TryDequeue(out SetFrequencyRequest localValue);
                    await RecognitionPause(EPO, localValue, DapServerMode.PureView);
                }
            }

            if (RecognitionLQueue.Count > 0)
            {
                if (RecognitionLQueue.TryPeek(out RangeMessage result))
                {
                    if (ConvertValueMHzToEPO(result.StartRangeMHz + (result.EndRangeMHz - result.StartRangeMHz) / 2d) == EPO)
                    {
                        RecognitionLQueue.TryDequeue(out RangeMessage localValue);
                        await RecognitionL(EPO);
                    }
                }
            }
        }

        private bool CompareSignalsKhz((float FrequencyKhz, float BandwidthKhz) signal1, (float FrequencyKhz, float BandwidthKhz) signal2)
        {
            if ((signal1.FrequencyKhz - EpsilonFrequencyKhz <= signal2.FrequencyKhz && signal2.FrequencyKhz <= signal1.FrequencyKhz + EpsilonFrequencyKhz)
                &&
                (signal1.BandwidthKhz - EpsilonBandwidthKhz <= signal2.BandwidthKhz && signal2.BandwidthKhz <= signal1.BandwidthKhz + EpsilonBandwidthKhz))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
