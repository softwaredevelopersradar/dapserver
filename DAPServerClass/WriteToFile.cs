﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.IO.Compression;
using CompressFile;
using System.Windows.Forms;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        private string FileName = "";

        private string LoadSpectrumFullFilePath = "";

        private async void LoadSpectrumFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = ".gz|*.gz";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                LoadSpectrumFullFilePath = openFileDialog.FileName;
            }

           await LoadSpectrum();
        }

        private void addRecord_Click(object sender, RoutedEventArgs e)
        {
            RecordStartStop = true;
            float[] Amplitudes = new float[8000];
            lRecords.Add(new Record(100, 0, Amplitudes));
            lRecords.Add(new Record(200, 1, Amplitudes));
        }

        private void writeToFile_Click(object sender, RoutedEventArgs e)
        {
            //WriteAllRecords();
            RecordStartStop = false;
        }


        List<Record> lRecords = new List<Record>();

        private void SaveRecord(int ShiftDelay, int EPO, float[] Amplitudes)
        {
            lRecords.Add(new Record(ShiftDelay, EPO, Amplitudes));
        }

        private void WriteAllRecords()
        {
            List<byte> tempByteList = new List<byte>();

            for (int i = 0; i < lRecords.Count(); i++)
            {
                tempByteList.AddRange(lRecords[i].GetBytes());
            }

            if (FileName != "")
            {
                WriteToCompress(GetFullFilePathDotExp(FileName), tempByteList.ToArray());
            }
            else
            {
                WriteToCompress(GetFullFilePathDotExp("Compress"), tempByteList.ToArray());
            }

            lRecords.Clear();
        }

        private async Task LoadSpectrum()
        {
            lRecords.Clear();

            byte[] result2;

            if (LoadSpectrumFullFilePath != "")
            {
                result2 = await DecompressFromFileAsync(LoadSpectrumFullFilePath);
            }
            else
            {
                result2 = await DecompressFromFileAsync(GetFullFilePathDotExp("Compress"));
            }

            int count = result2.Length / Record.BinarySize;

            for (int i = 0; i < count; i++)
            {
                Record temp = Record.Parse(result2, i * Record.BinarySize);
                lRecords.Add(temp);
            }
        }

        private string GetFullFilePathDotExp(string fileName, string folderName = "Record", string dotExp = ".gz" )
        {
            string path = Path.GetFullPath(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

            path = path.Remove(path.LastIndexOf('\\') + 1);

            if (folderName != null && folderName != "")
            {
                path = path.Insert(path.LastIndexOf('\\') + 1, folderName + "\\");
            }
            else
            {
                path = path.Insert(path.LastIndexOf('\\') + 1, "Record\\");
            }

            string dirPath = path.Remove(path.LastIndexOf('\\'));
            DirectoryInfo dirInfo = new DirectoryInfo(dirPath);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }

            string fullFilePathDotTxt = "";

            if (dotExp != null && dotExp != "")
            {
                fullFilePathDotTxt = path + fileName + dotExp;
            }
            else
            {
                fullFilePathDotTxt = path + fileName + ".gz";
            }

            return fullFilePathDotTxt;
        }

        public async Task WriteToCompress(string compressedFile, byte[] data)
        {
            // поток для записи сжатого файла
            using (FileStream targetStream = File.Create(compressedFile))
            {
                // поток архивации
                using (GZipStream compressionStream = new GZipStream(targetStream, CompressionMode.Compress))
                {
                   await compressionStream.WriteAsync(data, 0, data.Length);
                }
            }
        }

        public static async Task<byte[]> DecompressFromFileAsync(string inputFile)
        {
            var outputStream = new MemoryStream();

            using (var inputStream = File.Open(inputFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var gzip = new GZipStream(inputStream, CompressionMode.Decompress))
            {
                await gzip.CopyToAsync(outputStream);

                byte[] deCompressed = outputStream.ToArray();

                return deCompressed;
            }
        }
      
    }

}
