﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WpfMapRastr;
using YamlReverseExpertise;

namespace DAPServerClass
{
    public partial class DapServerClass
    {

        private async Task Simulate4LenaLoop()
        {
            // Get the current directory.
            string path = Directory.GetCurrentDirectory();

            string FolderName = "SimulateFiles";
            string FolderPath = (path + "\\" + FolderName);
            if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);

            string searchPattern = "*.yaml";
            string[] stringList = Directory.GetFiles(FolderPath, searchPattern);

            DirectoryInfo di = new DirectoryInfo(FolderPath);
            FileInfo[] fileInfos = di.GetFiles(searchPattern);

            if (stringList.Count() == 0)
            {
                string Str = "Count of Simulate Files == 0\r\nPlease close DAPServer and add some ReverseExpertise.yaml files to SimulateFilesFolder and run it again.";
                ConsoleLog(false, Str);
                return;
            }

            while (true)
            {
                bool isNice = false;
                int number = 0;

                do
                {
                    Console.Write("Enter number of Simulate File: ");
                    isNice = Int32.TryParse(Console.ReadLine(), out number);
                }
                while (!isNice);

                number--;

                if (number >= 0)
                {
                    if (number < stringList.Count())
                    {
                        string FileName = stringList[number].Substring(stringList[number].LastIndexOf("\\") + 1);
                        Console.WriteLine(FileName);

                        Yaml yaml4Simulate = new Yaml();
                        ReverseExpertise reverseExpertise = yaml4Simulate.YamlLoad<ReverseExpertise>(stringList[number]);

                        for (int i = 0; i < reverseExpertise.Expertises.Count(); i++)
                        {
                            await NCorrFunc2Lmini(reverseExpertise.Expertises[i]);

                            var time = (i < reverseExpertise.Expertises.Count() - 1) ? reverseExpertise.Expertises[i + 1].Time - reverseExpertise.Expertises[i].Time : new TimeSpan();

                            int delay = (int)(time.TotalMilliseconds);
                            await Task.Delay(delay);

                            double p = ((double)i / reverseExpertise.Expertises.Count());
                            Console.Write("\r{0}%", (int)(p * 100));
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Simulate File with Number[{number}] not found");
                        continue;
                    }

                    Console.WriteLine($"\rSimulate File with Number[{number}] complete");
                }
                await Task.Delay(1);
            }
        }

        private async Task NCorrFunc2Lmini(Expertise expertise)
        {
            if (expertise != null)
            {
                if (expertise.Corr1_1 != null
                    && expertise.Corr2_1 != null
                    && expertise.Corr3_1 != null
                    && expertise.Corr4_1 != null
                    && expertise.Corr1_1.Count() > 0
                    && expertise.Corr2_1.Count() > 0
                    && expertise.Corr3_1.Count() > 0
                    && expertise.Corr4_1.Count() > 0
                    )
                {

                    DateTime dateTimeNow = expertise.Time;

                    double innerCurrentFreqkHz = expertise.FreqkHz;
                    double innerCurrentBandkHz = expertise.BandwidthkHz;


                    //Сфорировать корреляционные функции
                    List<List<double>> CorrDoubleList = new List<List<double>>();
                    CorrDoubleList.Add(expertise.Corr1_1.ToList());
                    CorrDoubleList.Add(expertise.Corr2_1.ToList());
                    CorrDoubleList.Add(expertise.Corr3_1.ToList());
                    CorrDoubleList.Add(expertise.Corr4_1.ToList());

                    //Сформировать для отправки в АРМ
                    (double[] corrFunc0, double[] corrFunc1, double[] corrFunc2, double[] corrFunc3) corrFuncTuple =
                        (expertise.Corr1_1, expertise.Corr2_1, expertise.Corr3_1, expertise.Corr4_1);

                    //Отправка корреляциооной функции в АРМ Кираса
                    var answer = await MyDapServer.ServerSendCorrFunc
                        (0, (int)(innerCurrentFreqkHz), (int)(innerCurrentBandkHz),
                        corrFuncTuple.corrFunc0,
                        corrFuncTuple.corrFunc1,
                        corrFuncTuple.corrFunc2,
                        corrFuncTuple.corrFunc3
                        );

                    var dListTau123 = CalcTauFromCorrFuncsByEgorTo3(CorrDoubleList);

                    (double tau1, double tau2, double tau3) tau123 = (dListTau123[0], dListTau123[1], dListTau123[2]);
                    double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды
                    double[] tau = new double[] { 0, arrtau123[0] * 1e-9, arrtau123[1] * 1e-9, arrtau123[2] * 1e-9 }; // массив задержек всех станций //Секунды
                    tau = SignMirror(tau, 1, 1, 1);

                    bool isTau = false;

                    if (arrtau123.All(x => x != -1))
                    {
                        isTau = true;
                    }
                    if (tau.Any(x => Double.IsInfinity(x)) || tau.Any(x => Double.IsNaN(x)))
                    {
                        isTau = false;
                    }

                    //Проверочка по задержкам
                    if (isTau)
                    {
                        //Класс Расчета координат
                        fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                        //Инициализация листа координат станций и их задержек
                        List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                        //Важная проверочка для рассчета координат
                        if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                        {
                            //Функция порядочка и определения базовой стации в листе по индексу
                            List<int> GeneratesListOfisOwnIndexes()
                            {
                                List<int> lintisOwn = new List<int>();
                                for (int k = 0; k < tableLocalPoints.Count; k++)
                                {
                                    if (tableLocalPoints[k].IsCetral == true)
                                    {
                                        lintisOwn.Insert(0, k);
                                    }
                                    else
                                    {
                                        lintisOwn.Add(k);
                                    }
                                }
                                return lintisOwn;
                            }
                            List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                            //Заполенение листа координат станций и их задержек
                            for (int k = 0; k < 4; k++)
                            {
                                fDRM.ClassObject classObject = new fDRM.ClassObject()
                                {
                                    Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                    Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                    Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                    BaseStation = (k == 0) ? true : false,
                                    IndexStation = k + 1,
                                    tau = tau[k]
                                };

                                lstClassObject.Add(classObject);
                            }

                            //Рассчёт координат источника
                            fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject, isImageSolution, settings.DesiredHeight);
                            //Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                            //Анализ FlagRezCalc
                            if (resultMark.FlagRezCalc == 0)
                            {
                                //Отправка точки в АРМ
                                var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                                //Запись в файл времён, задержек, координат
                                if (RecordStartStop)
                                {
                                    expertises.Add(
                                        new Expertise(
                                        dateTimeNow,
                                        (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                        tau[1], tau[2], tau[3],
                                        resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                                        corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                        ));
                                }

                                double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                                InputPointTrack inputPointTrack = new InputPointTrack()
                                {
                                    LatMark = resultMark.Latitude,
                                    LongMark = resultMark.Longitude,
                                    HMark = resultMark.Altitude,
                                    Time = Time,
                                    Freq = (float)(innerCurrentFreqkHz),
                                    dFreq = (float)(innerCurrentBandkHz),
                                    _time = dateTimeNow
                                };
                                _tracksDefinition.f_TracksDefinition(inputPointTrack);

                                //Отработка по Дронам
                                LenaForMain();
                            }
                            else
                            {
                                //Запись в файл времён, задержек, координат
                                if (RecordStartStop)
                                {
                                    expertises.Add(
                                        new Expertise(
                                        dateTimeNow,
                                        (float)(innerCurrentFreqkHz), (float)(innerCurrentBandkHz),
                                        tau[1], tau[2], tau[3],
                                        -1, -1, -1,
                                        corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                        ));
                                }
                            }
                        }
                    }
                }
            }
        }

        private async Task Simulate4Lena2Loop()
        {
            // Get the current directory.
            string path = Directory.GetCurrentDirectory();

            string FolderName = "SimulateFiles";
            string FolderPath = (path + "\\" + FolderName);
            if (!Directory.Exists(FolderPath)) Directory.CreateDirectory(FolderPath);

            string searchPattern = "*.yaml";
            string[] stringList = Directory.GetFiles(FolderPath, searchPattern);

            DirectoryInfo di = new DirectoryInfo(FolderPath);
            FileInfo[] fileInfos = di.GetFiles(searchPattern);

            if (stringList.Count() == 0)
            {
                string Str = "Count of Simulate Files == 0\r\nPlease close DAPServer and add some ReverseExpertise.yaml files to SimulateFilesFolder and run it again.";
                ConsoleLog(false, Str);
                return;
            }

            while (true)
            {
                bool isNice = false;
                int number = 0;

                do
                {
                    Console.Write("Enter number of Simulate File: ");
                    isNice = Int32.TryParse(Console.ReadLine(), out number);
                }
                while (!isNice);

                number--;

                if (number >= 0)
                {
                    if (number < stringList.Count())
                    {
                        string FileName = stringList[number].Substring(stringList[number].LastIndexOf("\\") + 1);
                        Console.WriteLine(FileName);

                        Yaml yaml4Simulate = new Yaml();
                        ReverseExpertise reverseExpertise = yaml4Simulate.YamlLoad<ReverseExpertise>(stringList[number]);

                        for (int i = 0; i < reverseExpertise.Expertises.Count(); i++)
                        {
                            double[] arrtau123 = new double[] 
                            { reverseExpertise.Expertises[i].Tau1 * 1e9, 
                              reverseExpertise.Expertises[i].Tau2 * 1e9, 
                              reverseExpertise.Expertises[i].Tau3 * 1e9 };

                            await NCorrFuncN(arrtau123);

                            var time = (i < reverseExpertise.Expertises.Count() - 1) ? reverseExpertise.Expertises[i + 1].Time - reverseExpertise.Expertises[i].Time : new TimeSpan();

                            int delay = (int)(time.TotalMilliseconds);
                            await Task.Delay(delay);

                            double p = ((double)i / reverseExpertise.Expertises.Count());
                            Console.Write("\r{0}%", (int)(p * 100));
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Simulate File with Number[{number}] not found");
                        continue;
                    }

                    Console.WriteLine($"\rSimulate File with Number[{number}] complete");
                }
                await Task.Delay(1);
            }
        }
    }
}
