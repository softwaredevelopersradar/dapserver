﻿using DAPprotocols;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DAPServerClass
{
    public class GainSettings4 : INotifyPropertyChanged
    {
        private byte[] _PreselGains1;
        public byte[] PreselGains1
        {
            get => _PreselGains1;
            set
            {
                if (_PreselGains1 == value)
                    return;
                _PreselGains1 = value;
                OnPropertyChanged();
            }
        }

        private byte[] _PreselGains2;
        public byte[] PreselGains2
        {
            get => _PreselGains2;
            set
            {
                if (_PreselGains2 == value)
                    return;
                _PreselGains2 = value;
                OnPropertyChanged();
            }
        }

        private byte[] _PreselGains3;
        public byte[] PreselGains3
        {
            get => _PreselGains3;
            set
            {
                if (_PreselGains3 == value)
                    return;
                _PreselGains3 = value;
                OnPropertyChanged();
            }
        }

        private byte[] _PreselGains4;
        public byte[] PreselGains4
        {
            get => _PreselGains4;
            set
            {
                if (_PreselGains4 == value)
                    return;
                _PreselGains4 = value;
                OnPropertyChanged();
            }
        }

        private byte[] _ReceiverGains;
        public byte[] ReceiverGains
        {
            get => _ReceiverGains;
            set
            {
                if (_ReceiverGains == value)
                    return;
                _ReceiverGains = value;
                OnPropertyChanged();
            }
        }

        public GainSettings4()
        {

        }

        public GainSettings4(int NumberOfBands)
        {
            _PreselGains1 = new byte[NumberOfBands];
            _PreselGains1 = _PreselGains1.Select(x => x = 30).ToArray();

            _PreselGains2 = new byte[NumberOfBands];
            _PreselGains2 = _PreselGains2.Select(x => x = 30).ToArray();

            _PreselGains3 = new byte[NumberOfBands];
            _PreselGains3 = _PreselGains3.Select(x => x = 30).ToArray();

            _PreselGains4 = new byte[NumberOfBands];
            _PreselGains4 = _PreselGains4.Select(x => x = 30).ToArray();

            _ReceiverGains = new byte[NumberOfBands];
            _ReceiverGains = _ReceiverGains.Select(x => x = 30).ToArray();
        }

        public static GainSettings4 Load(Yaml innerYaml, int NumberOfBands, string NameDotYaml)
        {
            GainSettings4 innerGainSettings = innerYaml.YamlLoad<GainSettings4>(NameDotYaml);
            if (innerGainSettings == null ||
                innerGainSettings.PreselGains1 == null ||
                 innerGainSettings.PreselGains2 == null ||
                  innerGainSettings.PreselGains3 == null ||
                   innerGainSettings.PreselGains4 == null ||
                innerGainSettings.ReceiverGains == null)
            {
                innerGainSettings = new GainSettings4(NumberOfBands);
                innerYaml.YamlSave<GainSettings4>(innerGainSettings, NameDotYaml);
            }
            return innerGainSettings;
        }

        public void SetDeviceGain(DapDevice Device, byte EPO, byte Gain)
        {
            switch (Device)
            {
                case DapDevice.Preselector1:
                    _PreselGains1[EPO] = Gain;
                    break;
                case DapDevice.Preselector2:
                    _PreselGains2[EPO] = Gain;
                    break;
                case DapDevice.Preselector3:
                    _PreselGains3[EPO] = Gain;
                    break;
                case DapDevice.Preselector4:
                    _PreselGains4[EPO] = Gain;
                    break;
                case DapDevice.Receiver:
                    _ReceiverGains[EPO] = Gain;
                    break;
            }
        }

        public byte GetDeviceGain(DapDevice Device, byte EPO)
        {
            switch (Device)
            {
                case DapDevice.Preselector1:
                    return _PreselGains1[EPO];
                case DapDevice.Preselector2:
                    return _PreselGains2[EPO];
                case DapDevice.Preselector3:
                    return _PreselGains3[EPO];
                case DapDevice.Preselector4:
                    return _PreselGains4[EPO];
                case DapDevice.Receiver:
                    return _ReceiverGains[EPO];
                default: return 30;
            }
        }

        public Gain4Preselectors GetGainFromPresel(byte EPO)
        {
            return new Gain4Preselectors()
            {
                gPresel1 = _PreselGains1[EPO],
                gPresel2 = _PreselGains2[EPO],
                gPresel3 = _PreselGains3[EPO],
                gPresel4 = _PreselGains4[EPO]
            };
        }

        #region INotify 
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }

    public class Gain4Preselectors
    {
        public short gPresel1 { get; set; }
        public short gPresel2 { get; set; }
        public short gPresel3 { get; set; }
        public short gPresel4 { get; set; }

        public Gain4Preselectors() { }

        public Gain4Preselectors(Gain4Preselectors gain4Preselectors)
        {
            this.gPresel1 = gain4Preselectors.gPresel1;
            this.gPresel2 = gain4Preselectors.gPresel2;
            this.gPresel3 = gain4Preselectors.gPresel3;
            this.gPresel4 = gain4Preselectors.gPresel4;
        }

        public static bool4Flags CompareGains(Gain4Preselectors thisValue, Gain4Preselectors newValue)
        {
            return new bool4Flags()
            {
                bFlag1 = (thisValue.gPresel1 == newValue.gPresel1) ? true : false,
                bFlag2 = (thisValue.gPresel2 == newValue.gPresel2) ? true : false,
                bFlag3 = (thisValue.gPresel3 == newValue.gPresel3) ? true : false,
                bFlag4 = (thisValue.gPresel4 == newValue.gPresel4) ? true : false,
            };
        }

        public static bool4Flags NotCompareGains(Gain4Preselectors thisValue, Gain4Preselectors newValue)
        {
            return new bool4Flags()
            {
                bFlag1 = (thisValue.gPresel1 != newValue.gPresel1) ? true : false,
                bFlag2 = (thisValue.gPresel2 != newValue.gPresel2) ? true : false,
                bFlag3 = (thisValue.gPresel3 != newValue.gPresel3) ? true : false,
                bFlag4 = (thisValue.gPresel4 != newValue.gPresel4) ? true : false,
            };
        }

        public override string ToString()
        {
            return ($"Gain1: {this.gPresel1} Gain2: {this.gPresel2} Gain3: {this.gPresel3} Gain4: {this.gPresel4}");
        }
    }

    public class bool4Flags
    {
        public bool bFlag1 { get; set; }
        public bool bFlag2 { get; set; }
        public bool bFlag3 { get; set; }
        public bool bFlag4 { get; set; }

        /// <summary>
        /// почти всегда false
        /// </summary>
        /// <param name="bool4Flags"></param>
        /// <returns></returns>
        public static bool TotalAndCompare(bool4Flags bool4Flags)
        {
            return bool4Flags.bFlag1 & bool4Flags.bFlag2 & bool4Flags.bFlag3 & bool4Flags.bFlag4;
        }

        /// <summary>
        /// почти всегда true
        /// </summary>
        /// <param name="bool4Flags"></param>
        /// <returns></returns>
        public static bool TotalOrCompare(bool4Flags bool4Flags)
        {
            return bool4Flags.bFlag1 | bool4Flags.bFlag2 | bool4Flags.bFlag3 | bool4Flags.bFlag4;
        }
    }
}
