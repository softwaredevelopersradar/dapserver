﻿using System.Collections.Generic;
using System.Linq;
using KirasaModelsDBLib;

namespace DAPServerClass
{
    using System;
    using WpfMapRastr;

    public partial class DapServerClass
    {

        Dictionary<int, int> IDcountTrajOldForMainNewEgor = new Dictionary<int, int>();
        Dictionary<int, int> IDcountTrajNewForMainNewEgor = new Dictionary<int, int>();

        private void EgorForMain(CommonClassesSOE.IDTargetDesignation resultByNewEgor, double frequency, double bandWidth, TypeUAVRes typeUAVRes = TypeUAVRes.Unknown)
        {
            //TO DO:
            var tracksUav = MpuMainProcessor.GetTracksUAV(resultByNewEgor.ID);
            
            //new
            IDcountTrajNewForMainNewEgor.Clear();
            for (int i = 0; i < tracksUav.Count(); i++)
            {
                this.IDcountTrajNewForMainNewEgor.Add(tracksUav[i].ID, tracksUav[i].lstTargetUAV.Count);
            }

            //Если изменилось кол-во Дронов
            if (IDcountTrajOldForMainNewEgor.Count != IDcountTrajNewForMainNewEgor.Count)
            {
                int dif = IDcountTrajNewForMainNewEgor.Count - IDcountTrajOldForMainNewEgor.Count;

                for (int q = 0; q < dif; q++)
                {
                    //получаем нужный ID для записи
                    int getID = IDcountTrajNewForMainNewEgor.ElementAt(IDcountTrajNewForMainNewEgor.Count - (dif - q))
                        .Key;

                    //получаем по этому ID нужную запись из листа Лены
                    var record = tracksUav.Where(el => el.ID == getID).FirstOrDefault();
                    //Получаем по этой записи строб
                    (double RadiusA, double RadiusB) Strob = (0, 0); //todo fix later.
                    int Head = 0;

                    //Пишем в базу источник
                    AddTableUAVs(
                        ID: record.ID + 1,
                        State: 1, //todo 
                        typeUAVRes
                    );

                    //Пишем все его избранные траектории
                    for (int i = 0; i < record.lstTargetUAV.Count; i++)
                    {
                        FRS tempFRS = new FRS();
                        tempFRS.FreqkHz = frequency;
                        tempFRS.BandWidthkHz = (float)bandWidth;
                        tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                        tempFRS.Coords.Add
                        (new KirasaModelsDBLib.Coord()
                            {
                                Latitude = record.lstTargetUAV[i].currentPositionFiltered.geodesic.Latitude,
                                Longitude = record.lstTargetUAV[i].currentPositionFiltered.geodesic.Longitude,
                                Altitude = (float)record.lstTargetUAV[i].currentPositionFiltered.geodesic.Altitude
                            }
                        );

                        //Пишем его траекторию и добавляем к ней строб
                        AddTableUAVTrajectoriesWithStrobAndHead
                        (ID: record.ID + 1,
                            num: (short)i,
                            tempFRS,
                            DateTime.Now, //todo
                            Strob,
                            Head
                        );
                    }



                    //Берём крайнюю точку и отправляем Егору вместе с record.ID
                    if (settings.workMode == WorkMode.Main8 || settings.workMode == WorkMode.Egor2 ||
                        settings.workMode == WorkMode.Egor2)
                    {
                        var lastMark = record.lstTargetUAV.Last();

                        //MpuMainProcessor.SetIDPresenceCC(
                        //             record.ID,
                        //             new CommonClassesSOE.GeodesicCoordinates()
                        //             {
                        //                 Latitude = lastMark.CoordPaint.Latitude,
                        //                 Longitude = lastMark.CoordPaint.Longitude,
                        //                 Altitude = lastMark.CoordPaint.Altitude
                        //             });

                        //А заодно в выкидываем в UDP
                        SendcUAVtoGs(record.ID + 1, new TimeMarkCoord(
                            lastMark.currentPositionFiltered.geodesic.Latitude,
                            lastMark.currentPositionFiltered.geodesic.Latitude,
                            lastMark.currentPositionFiltered.geodesic.Altitude,
                            new List<Station>(),
                            lastMark.inputTime,
                            (float)frequency,
                            (float)bandWidth, DateTime.Now
                        ));
                    }
                }
            }
            else
            {
                //получаем нужный ID для записи
                if (IDcountTrajOldForMainNewEgor.Count > 0 || IDcountTrajNewForMainNewEgor.Count > 0)
                {
                    List<int> getIDs = Search4Changes();
                    for (int q = 0; q < getIDs.Count(); q++)
                    {
                        //получаем по этому ID нужную запись из листа Лены
                        var record = tracksUav.FirstOrDefault(el => el.ID == getIDs[q]);
                        //Получаем по этой записи строб
                        (double RadiusA, double RadiusB) Strob = (0, 0);
                        int Head = 0;

                        //Дописываем в базу крайнюю избранную точку его траектории
                        if (record != null && record.lstTargetUAV.Count > 0)
                        {
                            int v = record.lstTargetUAV.Count - 1;

                            var lastMark = record.lstTargetUAV.Last();

                            FRS tempFRS = new FRS();
                            tempFRS.FreqkHz = frequency;
                            tempFRS.BandWidthkHz = (float)bandWidth;
                            tempFRS.Coords = new List<KirasaModelsDBLib.Coord>();
                            tempFRS.Coords.Add
                                (new KirasaModelsDBLib.Coord()
                                {
                                    Latitude = lastMark.currentPositionFiltered.geodesic.Latitude,
                                    Longitude = lastMark.currentPositionFiltered.geodesic.Longitude,
                                    Altitude = (float)lastMark.currentPositionFiltered.geodesic.Altitude
                                }
                                );

                            //Дописываем в базу крайнюю избранную точку его траектории и добавляем к ней строб
                            AddTableUAVTrajectoriesWithStrobAndHead
                               (ID: record.ID + 1,
                               num: (short)v,
                               tempFRS,
                               DateTime.Now,//todo lastMark.appliedTime,
                               Strob,
                               Head
                               );

                            //Берём крайнюю точку и отправляем Егору вместе с record.ID
                            if (settings.workMode == WorkMode.Main8 || settings.workMode == WorkMode.Egor || settings.workMode == WorkMode.Egor2)
                            {
                                //MpuMainProcessor.SetIDPresenceCC(
                                //    record.ID,
                                //    new CommonClassesSOE.GeodesicCoordinates()
                                //    {
                                //        Latitude = lastMark.CoordPaint.Latitude,
                                //        Longitude = lastMark.CoordPaint.Longitude,
                                //        Altitude = lastMark.CoordPaint.Altitude
                                //    });

                                //А заодно в выкидываем в UDP
                                SendcUAVtoGs(record.ID + 1, new TimeMarkCoord(
                                    lastMark.currentPositionFiltered.geodesic.Latitude,
                                    lastMark.currentPositionFiltered.geodesic.Latitude,
                                    lastMark.currentPositionFiltered.geodesic.Altitude,
                                    new List<Station>(),
                                    lastMark.inputTime,
                                    (float)frequency,
                                    (float)bandWidth, DateTime.Now
                                ));
                            }
                        }
                    }
                }
                List<int> Search4Changes()
                {
                    List<int> temp = new List<int>();
                    for (int l = 0; l < IDcountTrajOldForMainNewEgor.Count(); l++)
                    {
                        if (IDcountTrajOldForMainNewEgor.ElementAt(l).Value != IDcountTrajNewForMainNewEgor.ElementAt(l).Value)
                        {
                            temp.Add(IDcountTrajOldForMainNewEgor.ElementAt(l).Key);
                        }
                    }
                    return temp;
                }
            }
            
            IDcountTrajOldForMainNewEgor.Clear();
            IDcountTrajOldForMainNewEgor = new Dictionary<int, int>(IDcountTrajNewForMainNewEgor);
            IDcountTrajNewForMainNewEgor.Clear();
        }
    }
}
