﻿using DspDataModel.DataProcessor;
using System;
using System.Threading.Tasks;
using UDP_Cuirasse;

namespace DAPServerClass
{
    interface ICuirasse
    {
        //Перестройка преселекторов
        Task PreselRealignment(double currentCenterFreqMHz, byte EPO, bool isAlwaysGainChange = false);

        //Настройка РПУ на текущую ЕПО по частоте
        Task RecRealignment(double currentCenterFreqMHz, byte EPO);

        //Чтение данных
        Task<GetSpectrumEventArgs> SpectrumWork(int EPO);

        //Обнаружение ИРИ от Феди
        ProcessResult FedyasWorkPlusSignalTimePlusFilters(GetSpectrumEventArgs answerGetSpectrum, int EPO);

        //Обнаружение Сигналов
        void TableSignalsWork(ProcessResult result0);

        //Запрос на систему распознавания
        Task<SetParamEventArgs> RecognitionRequest();

        //Работа с очередью запросов на распознование
        Task QueueWork(int EPO);

        Task<T> AttemptCountCmdUdp<T>(int cmdNumber, int value1, int value2, int count = 3) where T : EventArgs;
    }

    public class Simulator : ICuirasse
    {
        public async Task PreselRealignment(double currentCenterFreqMHz, byte EPO, bool isAlwaysGainChange = false)
        {
            await Task.Delay(50);
        }

        public async Task RecRealignment(double currentCenterFreqMHz, byte EPO)
        {
            await Task.Delay(50);
        }

        public async Task<GetSpectrumEventArgs> SpectrumWork(int EPO)
        {
            await Task.Delay(100);
            return null;
        }

        public ProcessResult FedyasWorkPlusSignalTimePlusFilters(GetSpectrumEventArgs answerGetSpectrum, int EPO)
        {
            return new ProcessResult();
        }

        public void TableSignalsWork(ProcessResult result0)
        {
            
        }

        public async Task<SetParamEventArgs> RecognitionRequest()
        {
            await Task.Delay(300);
            return new SetParamEventArgs();
        }

        public async Task QueueWork(int EPO)
        {
            await Task.Delay(100);
        }

        public async Task<T> AttemptCountCmdUdp<T>(int cmdNumber, int value1, int value2, int count = 3) where T : EventArgs
        {
            await Task.Delay(100);
            return null;
        }
    }

}
