﻿using System.Collections.Generic;
using System.Linq;
using WpfMapRastr;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        public List<double> CalcTauFromCorrFuncsByEgor(List<List<double>> CorrDoubleList)
        {
            List<double> tupl = new List<double>() { -1, -1, -1 };
            if (CorrDoubleList.Count() == 3)
            {
                List<double> lMaxes = CorrDoubleList.Select(x => x.Max()).ToList();

                if (lMaxes.All(x => x >= CorrThreshold))
                {
                    for (int w = 0; w < CorrDoubleList.Count(); w++)
                    {
                        double localMax = (lMaxes[w] * CorrDivide);

                        int maxindex = CorrDoubleList[w].IndexOf(lMaxes[w]);
                        int lbindex = -1;
                        int hbindex = -1;

                        List<int> negativeValues = new List<int>();

                        for (int ii = -(CorrDoubleList[w].Count() - 1) / 2; ii <= (CorrDoubleList[w].Count() - 1) / 2; ii++)
                        {
                            negativeValues.Add(ii);
                        }

                        lbindex = getLowerBorder(localMax, maxindex, CorrDoubleList[w]);
                        hbindex = getHigherBorder(localMax, maxindex, CorrDoubleList[w]);

                        if (lbindex != -1 && hbindex != -1)
                        {
                            List<double> subRangeX = new List<double>();
                            for (int j = lbindex; j <= hbindex; j++)
                                subRangeX.Add(negativeValues[j]);

                            List<double> subRangeY = CorrDoubleList[w].GetRange(lbindex, hbindex - lbindex + 1);

                            var ABC = approximationParabellum_returnValue(subRangeX, subRangeY);

                            tupl[w] = ((-1d) * (ABC[1] / (2d * ABC[0]))) * (1d / (BandWidthMHz * 10e6)) * 10e9;
                        }
                    }
                }
            }
            return tupl;
        }

        public List<double> CalcTauFromCorrFuncsByEgorMinusShift(List<List<double>> CorrDoubleList, int shift)
        {
            List<double> tupl = new List<double>() { -1, -1, -1 };
            if (CorrDoubleList.Count() == 3)
            {
                List<double> lMaxes = CorrDoubleList.Select(x => x.Max()).ToList();

                if (lMaxes.All(x => x >= CorrThreshold))
                {
                    for (int w = 0; w < CorrDoubleList.Count(); w++)
                    {
                        double localMax = (lMaxes[w] * CorrDivide);

                        int maxindex = CorrDoubleList[w].IndexOf(lMaxes[w]);
                        int lbindex = -1;
                        int hbindex = -1;

                        List<int> negativeValues = new List<int>();

                        for (int ii = -(CorrDoubleList[w].Count() - 1) / 2; ii <= (CorrDoubleList[w].Count() - 1) / 2; ii++)
                        {
                            negativeValues.Add(ii);
                        }

                        lbindex = getLowerBorder(localMax, maxindex, CorrDoubleList[w]);
                        hbindex = getHigherBorder(localMax, maxindex, CorrDoubleList[w]);

                        if (lbindex != -1 && hbindex != -1)
                        {
                            List<double> subRangeX = new List<double>();
                            for (int j = lbindex; j <= hbindex; j++)
                                subRangeX.Add(negativeValues[j]);

                            List<double> subRangeY = CorrDoubleList[w].GetRange(lbindex, hbindex - lbindex + 1);

                            subRangeX = subRangeX.Select(x => x - shift).ToList();

                            var ABC = approximationParabellum_returnValue(subRangeX, subRangeY);

                            tupl[w] = ((-1d) * (ABC[1] / (2d * ABC[0]))) * (1d / (BandWidthMHz * 10e6)) * 10e9;
                        }
                    }
                }
            }
            return tupl;
        }

        public List<double> CalcTauFromCorrFuncsByEgor4(List<List<double>> CorrDoubleList)
        {
            //work with 1-1
            double maxValue = CorrDoubleList[3].Max();
            int maxindex = CorrDoubleList[3].IndexOf(maxValue);
            int shift = maxindex - 60;

            CorrDoubleList.RemoveAt(3);
            return CalcTauFromCorrFuncsByEgorMinusShift(CorrDoubleList, shift);
        }

        public List<double> CalcTauFromCorrFuncsByEgorTo3(List<List<double>> CorrDoubleList)
        {
            if (CorrDoubleList.Count == 4)
                CorrDoubleList.RemoveAt(3);
            return CalcTauFromCorrFuncsByEgor(CorrDoubleList);
        }

        //получение индекса нижней границы
        private int getLowerBorder(double coef, int index, List<double> arr)
        {
            int border;
            for (int lb = index; lb >= 0; lb--)
            {
                if (arr[lb] < coef)
                {
                    border = lb + 1;
                    return border;
                }
                else if (lb == 0)
                {
                    border = lb;
                    break;
                }
            }
            return index;
        }

        //получение индекса высшей границы
        private int getHigherBorder(double coef, int index, List<double> arr)
        {
            int border;
            for (int hb = index; hb < arr.Count(); hb++)
            {
                if (arr[hb] < coef)
                {
                    border = hb - 1;
                    return border;
                }
                else if (hb == arr.Count() - 1)
                {
                    border = hb;
                    break;
                }
            }
            return index;
        }

        public List<Station> DefaultStationsWithEgorHeights()
        {
            //Заполенение листа координат станций и их задержек
            List<Station> listStations = new List<Station>();

            Station station0 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.9312,
                    Long: 27.635555556,
                    Alt: 13.862,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 13.862
                    ),
                ErrorTime = 0,
            };
            station0.Position.ID = 0;
            listStations.Add(station0);

            Station station1 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.930997222,
                    Long: 27.6349,
                    Alt: 21.235,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 13.862
                    ),
                ErrorTime = 1E-9,
            };
            station1.Position.ID = 1;
            listStations.Add(station1);

            Station station2 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.931444444,
                    Long: 27.636638889,
                    Alt: 20.947,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 13.862
                    ),
                ErrorTime = 1E-9,
            };
            station2.Position.ID = 2;
            listStations.Add(station2);

            Station station3 = new Station()
            {
                Position = new MarkCoord(
                    Lat: 53.932327778,
                    Long: 27.63485,
                    Alt: 13.237,
                    LatRLSBase: 53.9312,
                    LongRLSBase: 27.635555556,
                    AltRLSBase: 13.862
                    ),
                ErrorTime = 1E-9,
            };
            station3.Position.ID = 3;
            listStations.Add(station3);

            return listStations;
        }

        private List<double> SignMirror(List<double> dList, int signNumber)
        {
            (int sign1, int sign2, int sign3) signTuple = (1, 1, 1);
            switch (signNumber)
            {
                case 0:
                    signTuple = (1, 1, 1);
                    break;
                case 1:
                    signTuple = (1, 1, -1);
                    break;
                case 2:
                    signTuple = (1, -1, 1);
                    break;
                case 3:
                    signTuple = (1, -1, -1);
                    break;
                case 4:
                    signTuple = (-1, 1, 1);
                    break;
                case 5:
                    signTuple = (-1, 1, -1);
                    break;
                case 6:
                    signTuple = (-1, -1, 1);
                    break;
                case 7:
                    signTuple = (-1, -1, -1);
                    break;
            }
            return new List<double>() { dList[0] * signTuple.sign1, dList[1] * signTuple.sign2, dList[2] * signTuple.sign3 };
        }

        private List<double> SignMirror(List<double> dList, int sign1, int sign2, int sign3)
        {
            return new List<double>() { dList[0] * sign1, dList[1] * sign2, dList[2] * sign3 };
        }

        private List<double> ShiftFix(List<double> dList, double shift)
        {
            return new List<double>() { dList[0] + shift, dList[1] + shift, dList[2] + shift };
        }

        private List<double> ShiftFromDBTableLocalPoints(List<double> dList)
        {
            return new List<double>() 
            { 
                dList[0] + tableLocalPoints[1].TimeError, 
                dList[1] + tableLocalPoints[2].TimeError, 
                dList[2] + tableLocalPoints[3].TimeError 
            };
        }

        private double[] SignMirror(double[] dArr, int sign1, int sign2, int sign3)
        {
            if (dArr.Count() == 3)
                return new double [] { dArr[0] * sign1, dArr[1] * sign2, dArr[2] * sign3 };
            if (dArr.Count() == 4)
                return new double[] { dArr[0], dArr[1] * sign1, dArr[2] * sign2, dArr[3] * sign3 };
            return dArr;
        }

        public fDRM.ClassObjectTmp CalcCoordsByEgor(List<double> arrtau123, int sign1, int sign2, int sign3, List<Station> innerStations)
        {
            //Вычисление задержек и хи знаков
            double[] tau = new double[] { 0, sign1 * arrtau123[0] * 1e-9, sign2 * arrtau123[1] * 1e-9, sign3 * arrtau123[2] * 1e-9 };
            //Класс Расчета координат
            fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

            //Инициализация листа координат станций и их задержек
            List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

            //Заполенение листа координат станций и их задержек тестовая версия
            for (int k = 0; k < 4; k++)
            {
                fDRM.ClassObject classObject = new fDRM.ClassObject()
                {
                    Latitude = innerStations[k].Position.Latitude,
                    Longitude = innerStations[k].Position.Longitude,
                    Altitude = innerStations[k].Position.Altitude,
                    BaseStation = (k == 0) ? true : false,
                    IndexStation = k + 1,
                    tau = tau[k]
                };

                lstClassObject.Add(classObject);
            }

            //Рассчёт координат источника
            fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject, isImageSolution, settings.DesiredHeight);
            return resultMark;
        }
    }
}
