﻿using DspDataModel.DataProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using KirasaModelsDBLib;
using UDP_Cuirasse;

using DspDataModel;
using WpfMapRastr;
using YamlReverseExpertise;
using Nito.AsyncEx;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        private readonly AsyncAutoResetEvent _taskAddEvent = new AsyncAutoResetEvent();

        CancellationTokenSource ctsLocalSpectrum = new CancellationTokenSource();
        CancellationTokenSource ctsLocalCorrFunc = new CancellationTokenSource();

        Task LocalSpectrumTask;
        Task LocalCorrFuncTask;


        private AsyncAutoResetEvent _AsyncAutoResetEvent4LocalSpectrum = new AsyncAutoResetEvent();
        private AsyncAutoResetEvent _AsyncAutoResetEvent4LocalCorr = new AsyncAutoResetEvent();

        private async Task LocalSpectrum(int bandIndex, CancellationToken token)
        {
            //Запрос спектра
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2,"LocalSpectrum прервана токеном");
                    _AsyncAutoResetEvent4LocalSpectrum.Set();
                    return;
                }

                //Для большого хранилища
                List<GetSpectrumEventArgs> answersGetSpectrum = new List<GetSpectrumEventArgs>();
                for (int ww = 0; ww < _GlobalN; ww++)
                {
                    await _AsyncAutoResetEvent4LocalCorr.WaitAsync();

                    //GetSpectrumEventArgs tempAnswerGetSpectrum = await uDP.GetSpectrum((byte)ww);
                    GetSpectrumEventArgs tempAnswerGetSpectrum = await AttemptCountCmdUdp<GetSpectrumEventArgs>(3, ww, 0);
                    if (tempAnswerGetSpectrum == null)
                    {
                        ConsoleLog(false, $"UDP GetSpectrum Chanel {ww} Error!");
                        List<float> innerInitList = new float[_GlobalDotsPerBandCount].ToList<float>();
                        innerInitList = innerInitList.Select(x => x = -130f).ToList<float>();
                        tempAnswerGetSpectrum = new GetSpectrumEventArgs(0, new byte[0], 0, 0, 0, innerInitList,0);
                    }
                    //Console.WriteLine($"uDP.GetSpectrum Chanel({ww})");
                    answersGetSpectrum.Add(tempAnswerGetSpectrum);

                    _AsyncAutoResetEvent4LocalSpectrum.Set();
                }

                var answerGetSpectrum = answersGetSpectrum[_ChanelNumber];

                //Обновляем хранилище спектра
                ListSpectrumStorage[bandIndex] = new List<float>(answerGetSpectrum.Ampl);

                //Для большого хранилища
                for (int ww = 0; ww < _GlobalN; ww++)
                {
                    BigListSpectrumStorage[ww][bandIndex] = new List<float>(answersGetSpectrum[ww].Ampl);
                }

                await Task.Delay(_ReceiverDelaySolo);
            }
        }

        private async Task LocalCorrFunc(double innerCurrentFreqkHz, double innerFilterMinFrequencyMHz, double innerFilterMaxFrequencyMHz, CancellationToken token)
        {
            bool firsttime = true;
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2,"LocalCorrFunc прервана токеном");
                    _AsyncAutoResetEvent4LocalCorr.Set();
                    return;
                }

                //стопвотч старт
                //SW sW = new SW();

                //Сигнал
                double FilterCenterFrequencyMHz = Math.Min(innerFilterMinFrequencyMHz, innerFilterMaxFrequencyMHz) + Math.Abs((innerFilterMaxFrequencyMHz - innerFilterMinFrequencyMHz) / 2d);
                double FilterBandwidthMHz = Math.Abs(innerFilterMaxFrequencyMHz - innerFilterMinFrequencyMHz);
                DateTime dateTimeNow = DateTime.Now;

                //установка параметров для корреляционной кривой
                if (firsttime)
                {
                    byte filtervalue = BandWidthCodeFromBandWidthkHz(FilterBandwidthMHz * 1000);
                    int offset = (int)(FilterCenterFrequencyMHz * 1000 - innerCurrentFreqkHz);
                    //var answerCorrFuncParamSet = await uDP.SetParam(offset, filtervalue, Alpha1, Alpha2);
                    SetParamEventArgs answerCorrFuncParamSet = await AttemptCountCmdUdp<SetParamEventArgs>(5, offset, filtervalue);
                    if (answerCorrFuncParamSet == null)
                    {
                       ConsoleLog(false, "UDP SetParam Error!");
                    }
                    await Task.Delay(1);
                    firsttime = false;

                    _AsyncAutoResetEvent4LocalSpectrum.Set();
                }

                await _AsyncAutoResetEvent4LocalSpectrum.WaitAsync();
                //Запрос корреляционной функции
                //var answerCor0 = await uDP.GetCorrelationFunc(); // запрос с устройства №0
                GetCorrelationFuncEventArgs answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncEventArgs>(4, 0, 0);
                //Console.WriteLine($"uDP.GetCorrelationFunc()");
                _AsyncAutoResetEvent4LocalCorr.Set();
                if (answerCor0 == null)
                {
                    ConsoleLog(false, "UDP GetCorrelationFunc Error!");
                    continue;
                }
                //Ортонормировать корреляционные функции
                List<List<double>> CorrDoubleList = OrthoNormalization(answerCor0);

                //Ортонормирование для отправки в АРМ
                var corrFuncTuple = OrthoNormalizationToDoubleTuple(answerCor0);
                //Отправка корреляциооной функции в АРМ Кираса
                var answer = await MyDapServer.ServerSendCorrFunc
                    (0, (int)(FilterCenterFrequencyMHz * 1000), (int)(FilterBandwidthMHz * 1000), 
                    corrFuncTuple.corrFunc0, 
                    corrFuncTuple.corrFunc1, 
                    corrFuncTuple.corrFunc2,
                    corrFuncTuple.corrFunc3
                    );

                var dListTau123 = CalcTauFromCorrFuncsByEgor4(CorrDoubleList);
                dListTau123 = SignMirror(dListTau123, 1, 1, 1); //new

                (double tau1, double tau2, double tau3) tau123 = (dListTau123[0], dListTau123[1], dListTau123[2]);
                double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды
                double[] tau = new double[] { 0, arrtau123[0] * 1e-9, arrtau123[1] * 1e-9, arrtau123[2] * 1e-9 }; // массив задержек всех станций //Секунды
                tau = SignMirror(tau, 1, 1, 1);

                bool isTau = false;

                //Console.WriteLine(tau123);

                if (arrtau123.All(x => x != -1))
                {
                    isTau = true;
                }
                if (tau.Any(x => Double.IsInfinity(x)) || tau.Any(x => Double.IsNaN(x)))
                {
                    isTau = false;
                }

                //Проверочка по задержкам
                if (isTau)
                {
                    //Класс Расчета координат
                    fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                    //Инициализация листа координат станций и их задержек
                    List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                    //Важная проверочка для рассчета координат
                    if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                    {
                        //Функция порядочка и определения базовой стации в листе по индексу
                        List<int> GeneratesListOfisOwnIndexes()
                        {
                            List<int> lintisOwn = new List<int>();
                            for (int k = 0; k < tableLocalPoints.Count; k++)
                            {
                                if (tableLocalPoints[k].IsCetral == true)
                                {
                                    lintisOwn.Insert(0, k);
                                }
                                else
                                {
                                    lintisOwn.Add(k);
                                }
                            }
                            return lintisOwn;
                        }
                        List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                        //Заполенение листа координат станций и их задержек
                        for (int k = 0; k < 4; k++)
                        {
                            fDRM.ClassObject classObject = new fDRM.ClassObject()
                            {
                                Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                BaseStation = (k == 0) ? true : false,
                                IndexStation = k + 1,
                                tau = tau[k]
                            };

                            lstClassObject.Add(classObject);
                        }

                        //Рассчёт координат источника
                        fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject, isImageSolution, settings.DesiredHeight);
                        //Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                        //Отправка точки в АРМ
                        var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                        //Запись в файл времён, задержек, координат
                        if (RecordStartStop)
                        {
                            expertises.Add(
                                new Expertise(
                                dateTimeNow,
                                (float)(FilterCenterFrequencyMHz * 1000), (float)(FilterBandwidthMHz * 1000),
                                tau[1], tau[2], tau[3],
                                resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                                corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                ));
                        }

                        double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                        InputPointTrack inputPointTrack = new InputPointTrack()
                        {
                            LatMark = resultMark.Latitude,
                            LongMark = resultMark.Longitude,
                            HMark = resultMark.Altitude,
                            Time = Time,
                            Freq = (float)(FilterCenterFrequencyMHz * 1000),
                            dFreq = (float)(FilterBandwidthMHz * 1000),
                            _time = dateTimeNow
                        };
                        _tracksDefinition.f_TracksDefinition(inputPointTrack);

                        //Отработка по Дронам
                        LenaForMain();
                    }
                }

                //стопвотч стоп
                //sW.Stop();

                await Task.Delay(GetAutoCorrFuncDelay);
            }
        }

        private async Task LocalCorrFunc3(double innerCurrentFreqkHz, double innerFilterMinFrequencyMHz, double innerFilterMaxFrequencyMHz, CancellationToken token)
        {
            //int innerCounter = 0;
            bool firsttime = true;
            while (true)
            {
                if (token.IsCancellationRequested)
                {
                    ConsoleLog(2, "LocalCorrFunc прервана токеном");
                    _AsyncAutoResetEvent4LocalCorr.Set();
                    return;
                }

                //if (innerCounter == CorrRequestCount)
                //{
                //    Task.Run(() => InnerStopLocal());
                //}

                //стопвотч старт
                //SW sW = new SW();

                //Сигнал
                double FilterCenterFrequencyMHz = Math.Min(innerFilterMinFrequencyMHz, innerFilterMaxFrequencyMHz) + Math.Abs((innerFilterMaxFrequencyMHz - innerFilterMinFrequencyMHz) / 2d);
                double FilterBandwidthMHz = Math.Abs(innerFilterMaxFrequencyMHz - innerFilterMinFrequencyMHz);
                DateTime dateTimeNow = DateTime.Now;

                //установка параметров для корреляционной кривой
                if (firsttime)
                {
                    byte filtervalue = BandWidthCodeFromBandWidthkHz(FilterBandwidthMHz * 1000);
                    int offset = (int)(FilterCenterFrequencyMHz * 1000 - innerCurrentFreqkHz);
                    //var answerCorrFuncParamSet = await uDP.SetParam(offset, filtervalue, Alpha1, Alpha2);
                    SetParamEventArgs answerCorrFuncParamSet = await AttemptCountCmdUdp<SetParamEventArgs>(5, offset, filtervalue);
                    if (answerCorrFuncParamSet == null)
                    {
                        ConsoleLog(false, "UDP SetParam Error!");
                    }
                    await Task.Delay(1);
                    firsttime = false;

                    _AsyncAutoResetEvent4LocalSpectrum.Set();
                }

                await _AsyncAutoResetEvent4LocalSpectrum.WaitAsync();
                //Запрос корреляционной функции
                //var answerCor0 = await uDP.GetCorrelationFunc(); // запрос с устройства №0
                GetCorrelationFuncEventArgs answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncEventArgs>(4, 0, 0);
                //Console.WriteLine($"uDP.GetCorrelationFunc()");
                _AsyncAutoResetEvent4LocalCorr.Set();
                if (answerCor0 == null)
                {
                    ConsoleLog(false, "UDP GetCorrelationFunc Error!");
                    continue;
                }
                //Ортонормировать корреляционные функции
                List<List<double>> CorrDoubleList = OrthoNormalizationMax(answerCor0);

                //Ортонормирование для отправки в АРМ
                var corrFuncTuple = OrthoNormalizationToDoubleTupleMax(answerCor0);
                //Отправка корреляциооной функции в АРМ Кираса
                var answer = await MyDapServer.ServerSendCorrFunc
                    (0, (int)(FilterCenterFrequencyMHz * 1000), (int)(FilterBandwidthMHz * 1000),
                    corrFuncTuple.corrFunc0,
                    corrFuncTuple.corrFunc1,
                    corrFuncTuple.corrFunc2,
                    corrFuncTuple.corrFunc3
                    );

                var dListTau123 = CalcTauFromCorrFuncsByEgor4(CorrDoubleList);
                dListTau123 = SignMirror(dListTau123, 1, 1, 1); //new

                (double tau1, double tau2, double tau3) tau123 = (dListTau123[0], dListTau123[1], dListTau123[2]);
                double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды
                double[] tau = new double[] { 0, arrtau123[0] * 1e-9, arrtau123[1] * 1e-9, arrtau123[2] * 1e-9 }; // массив задержек всех станций //Секунды
                tau = SignMirror(tau, 1, 1, 1);

                bool isTau = false;

                //Console.WriteLine(tau123);

                if (arrtau123.All(x => x != -1))
                {
                    isTau = true;
                }
                if (tau.Any(x => Double.IsInfinity(x)) || tau.Any(x => Double.IsNaN(x)))
                {
                    isTau = false;
                }

                //Проверочка по задержкам
                if (isTau)
                {
                    //Класс Расчета координат
                    fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                    //Инициализация листа координат станций и их задержек
                    List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                    //Важная проверочка для рассчета координат
                    if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                    {
                        //Функция порядочка и определения базовой стации в листе по индексу
                        List<int> GeneratesListOfisOwnIndexes()
                        {
                            List<int> lintisOwn = new List<int>();
                            for (int k = 0; k < tableLocalPoints.Count; k++)
                            {
                                if (tableLocalPoints[k].IsCetral == true)
                                {
                                    lintisOwn.Insert(0, k);
                                }
                                else
                                {
                                    lintisOwn.Add(k);
                                }
                            }
                            return lintisOwn;
                        }
                        List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                        //Заполенение листа координат станций и их задержек
                        for (int k = 0; k < 4; k++)
                        {
                            fDRM.ClassObject classObject = new fDRM.ClassObject()
                            {
                                Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                BaseStation = (k == 0) ? true : false,
                                IndexStation = k + 1,
                                tau = tau[k]
                            };

                            lstClassObject.Add(classObject);
                        }

                        //Рассчёт координат источника
                        fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject, isImageSolution, settings.DesiredHeight);
                        //Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                        //Отправка точки в АРМ
                        var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                        //Запись в файл времён, задержек, координат
                        if (RecordStartStop)
                        {
                            expertises.Add(
                                new Expertise(
                                dateTimeNow,
                                (float)(FilterCenterFrequencyMHz * 1000), (float)(FilterBandwidthMHz * 1000),
                                tau[1], tau[2], tau[3],
                                resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                                corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                ));
                        }

                        double Time = dateTimeNow.TimeOfDay.TotalSeconds;
                        InputPointTrack inputPointTrack = new InputPointTrack()
                        {
                            LatMark = resultMark.Latitude,
                            LongMark = resultMark.Longitude,
                            HMark = resultMark.Altitude,
                            Time = Time,
                            Freq = (float)(FilterCenterFrequencyMHz * 1000),
                            dFreq = (float)(FilterBandwidthMHz * 1000),
                            _time = dateTimeNow
                        };
                        _tracksDefinition.f_TracksDefinition(inputPointTrack);

                        //Отработка по Дронам
                        LenaForMain();
                    }
                }

                //стопвотч стоп
                //sW.Stop();

                await Task.Delay(GetAutoCorrFuncDelay);

                //innerCounter++;
            }
        }


        private readonly AsyncAutoResetEvent _AsyncAutoResetEvent4GlobalRow = new AsyncAutoResetEvent();

        private async Task MainLoop(CancellationToken token)
        {
            CurrentFreqkHz = 0;
            //Получаем список ЕПО где ведём Радиоразведку
            //var Row = DoIt(MinFreqs, MaxFreqs);
            //Row.Sort();
            //GlobalRow = new List<int>(Row.Distinct().ToList());
            while (_Mode != 0)
            {
                //SW sW = new SW();

                int[] localGlobalRow = new int[0];
                lock (GlobalRow)
                {
                    localGlobalRow = GlobalRow.ToArray();
                }
                if (localGlobalRow.Length == 0)
                {
                    await _AsyncAutoResetEvent4GlobalRow.WaitAsync();
                    continue;
                    //не ждать чуда, да дипазонов РР, а вернуться в подготовку и всем сообщить об этом
                    //Пока отмена, чтобы не погибнуть
                    //Console.WriteLine("Диапазонов нет, не ало");
                    //Mode = 0;
                    //var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DAPprotocols.DapServerMode.Stop);
                    //return;
                }

                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2,"MainLoop прерван токеном");
                        return;
                    }

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    //Перестройка преселекторов старая
                    {
                        //if (CurrentFreqkHz != centerFreqMHz * 1000)
                        //{
                        //    SetPreselFreqGain(settings.PreselectorVersion, (int)centerFreqMHz, (byte)_PreSelValue);
                        //    await Task.Delay(1);
                        //}
                    }
                    //Перестройка преселекторов новая последовательная
                    //if (CurrentFreqkHz != centerFreqMHz * 1000)
                    //    await SetPreselFreqGainAsyncСonsistently(settings.PreselectorVersion, (int)centerFreqMHz, (byte)_PreSelValue);

                    bool needFreqChange = (CurrentFreqkHz != centerFreqMHz * 1000) ? true : false;

                    Gain4Preselectors exitingValue = CurrentDictionary4PreSelGains[localGlobalRow[i]];
                    Gain4Preselectors needingValue = _GainSettings4.GetGainFromPresel((byte)localGlobalRow[i]);

                    bool4Flags compareValues = Gain4Preselectors.NotCompareGains(exitingValue, needingValue);

                    bool needGainChange = bool4Flags.TotalOrCompare(compareValues);

                    //Перестройка преселекторов новая последовательная 7
                    if (needFreqChange || needGainChange)
                    {
                        bool[] bb = await SetPreselFreqGainAsyncСonsistently7(
                            Version : settings.PreselectorVersion, 
                            Freq: (int)centerFreqMHz,
                            FreqChange: needFreqChange,
                            Gain : needingValue,
                            GainChange: compareValues
                            );

                        CurrentDictionary4PreSelGains[localGlobalRow[i]] = new Gain4Preselectors(needingValue);
                        //для отмены если ошибка по преселекторам
                        if (bb.Any(x => x == false))
                        {
                            for (int i4bb = 0; i4bb < bb.Count(); i4bb++)
                            {
                                if (bb[i4bb] == false)
                                {
                                    ConsoleLog(false, $"Presel №{i4bb + 1} Error!");
                                }
                            }
                            //Task.Run(() => EndRI());
                            //return;
                        }
                    }

                    //SetFreqEventArgs answerSetFreq;
                    ////Настройка РПУ на текущую ЕПО по частоте
                    //if (CurrentFreqkHz != centerFreqMHz * 1000)
                    //    answerSetFreq = await uDP.SetFreq((int)(centerFreqMHz * 1000), 30);

                    //Настройка РПУ на текущую ЕПО по частоте
                    if (CurrentFreqkHz != centerFreqMHz * 1000 || 
                        CurrentDictionaryRecGain[localGlobalRow[i]] != _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)localGlobalRow[i]))
                    {
                        //SetFreqEventArgs answerSetFreq = await uDP.SetFreq((int)(centerFreqMHz * 1000), 30);
                        //SetFreqEventArgs answerSetFreq = await AttemptCountCmdUdp<SetFreqEventArgs>(2, (int)(centerFreqMHz * 1000), 30);
                        SetFreqEventArgs answerSetFreq = await AttemptCountCmdUdp<SetFreqEventArgs>(2, (int)(centerFreqMHz * 1000), _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)localGlobalRow[i]));
                        
                        CurrentDictionaryRecGain[localGlobalRow[i]] = _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)localGlobalRow[i]);
                       
                        await Task.Delay(_ReceiverDelay);
                        CurrentFreqkHz = centerFreqMHz * 1000;
                        //для отмены если ошибка по udp приёму или приёмнику
                        if (answerSetFreq == null)
                        {
                            ConsoleLog(false, "UDP SetFreq Error!");
                            //Task.Run(() => EndRI());
                            //return;
                        }
                    }

                    {
                        //if (CurrentFreqkHz == centerFreqMHz * 1000)
                        //{
                        //    //await Task.Delay(_ReceiverDelaySolo);
                        //}
                        //else
                        //{
                        //    await Task.Delay(_ReceiverDelay);
                        //    CurrentFreqkHz = centerFreqMHz * 1000;
                        //}
                    }

                    //Чтение данных

                    //Для большого хранилища
                    List<GetSpectrumEventArgs> answersGetSpectrum = new List<GetSpectrumEventArgs>();
                    for (int ww = 0; ww < _GlobalN; ww++)
                    {
                        //GetSpectrumEventArgs tempAnswerGetSpectrum = await uDP.GetSpectrum((byte)ww);
                        GetSpectrumEventArgs tempAnswerGetSpectrum = await AttemptCountCmdUdp<GetSpectrumEventArgs>(3, ww, 0);
                        if (tempAnswerGetSpectrum == null)
                        {
                            ConsoleLog(false, $"UDP GetSpectrum Chanel {ChanelName(ww)} Error!");
                            List<float> innerInitList = new float[_GlobalDotsPerBandCount].ToList<float>();
                            innerInitList = innerInitList.Select(x => x = -130f).ToList<float>();
                            tempAnswerGetSpectrum = new GetSpectrumEventArgs(0, new byte[0], 0, 0, 0, innerInitList,0);
                        }
                        answersGetSpectrum.Add(tempAnswerGetSpectrum);
                    }

                    //var answerGetSpectrum = await uDP.GetSpectrum((byte)_ChanelNumber);
                    var answerGetSpectrum = answersGetSpectrum[_ChanelNumber];

                    if (answerGetSpectrum.Ampl.Count == 8001) answerGetSpectrum.Ampl.RemoveAt(0);

                    if (RecordStartStop)
                    {
                        DateTime Time = DateTime.Now;
                        int ShiftDelay = (int)(Math.Abs(Time.TimeOfDay.TotalMilliseconds - TimeStart.TimeOfDay.TotalMilliseconds));
                        SaveRecord(ShiftDelay, localGlobalRow[i], answerGetSpectrum.Ampl.ToArray());
                    }

                    //Массив для ужатия спектра
                    //float[] vs1 = answerGetSpectrum.Ampl.ToArray();
                    //Массив для поиска частот
                    lastSavedSpectrum0 = answerGetSpectrum.Ampl.ToArray();

                    ////Ужатие Спектра от Феди
                    //var segment = new ArraySegment<float>(vs1, 0, vs1.Count());
                    //var spectrum = segment.StrechSpectrum(_GlobalDotsPerBandCount);
                    //vs1 = spectrum.Amplitudes;

                    //Обновляем хранилище спектра
                    //ListSpectrumStorage[Row[i]] = vs1.ToList();
                    ListSpectrumStorage[localGlobalRow[i]] = new List<float>(answerGetSpectrum.Ampl);

                    //Для большого хранилища
                    for (int ww = 0; ww < _GlobalN; ww++)
                    {
                        BigListSpectrumStorage[ww][localGlobalRow[i]] = new List<float>(answersGetSpectrum[ww].Ampl);
                    }

                    //Обнаружение ИРИ от Феди
                    DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(lastSavedSpectrum0, localGlobalRow[i], DateTime.Now, 0);

                    //Параметр для поиска сигналов по порогу
                    ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(_Threshold);

                    //Результаты сигналов
                    var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

                    //Ассоциация сигнал-время
                    if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
                    {
                        //Console.WriteLine(result0.Signals.Count);
                        SignalTime.Clear();
                        for (int qq = 0; qq < result0.Signals.Count; qq++)
                        {
                            SignalTime.Add(result0.Signals[qq], DateTime.Now);
                        }
                    }

                    //Исполнительный фильтр
                    if (ApplyFilter && FilterMinFrequencyMHz != -1 && FilterMaxFrequencyMHz != -1)
                    {
                        var indexFilter = ConvertValueToIndexes(new double[] { FilterMinFrequencyMHz, FilterMaxFrequencyMHz });
                        if (indexFilter[0] == localGlobalRow[i] && indexFilter[0] == indexFilter[1])
                        {
                            int bandIndex = localGlobalRow[i];

                            ctsLocalSpectrum = new CancellationTokenSource();
                            CancellationToken tokenLocalSpectrum = ctsLocalSpectrum.Token;

                            ctsLocalCorrFunc = new CancellationTokenSource();
                            CancellationToken tokenLocalCorrFunc = ctsLocalCorrFunc.Token;

                            LocalSpectrumTask = LocalSpectrum(bandIndex, tokenLocalSpectrum);

                            LocalCorrFuncTask = LocalCorrFunc(CurrentFreqkHz, FilterMinFrequencyMHz, FilterMaxFrequencyMHz, tokenLocalCorrFunc);

                            await _taskAddEvent.WaitAsync();

                            continue;
                        }

                        //Фильтр по выделенной области
                        bool filterSelectedArea = false;
                        if (filterSelectedArea && FilterMinFrequencyMHz != -1 && FilterMaxFrequencyMHz !=-1)
                        {
                            var signals = new List<ISignal>();

                            for (int v = 0; v < result0.Signals.Count; v++)
                            {
                                if (result0.Signals[v].FrequencyKhz >= FilterMinFrequencyMHz && result0.Signals[v].FrequencyKhz <= FilterMaxFrequencyMHz)
                                {
                                    signals.Add(result0.Signals[v]);
                                }
                            }
                            result0 = new ProcessResult(signals);
                        }
                    }

                    //Фильтр по диапазонам РР
                    {
                        var signals = new List<ISignal>();

                        for (int v = 0; v < result0.Signals.Count; v++)
                        {
                            if (FRSonRangeRI(result0.Signals[v].CentralFrequencyKhz / 1000d))
                            {
                                signals.Add(result0.Signals[v]);
                            }
                        }
                        result0 = new ProcessResult(signals);
                    }

                    //Фильтр по ширине
                    {
                        var signals = new List<ISignal>();

                        for (int v = 0; v < result0.Signals.Count; v++)
                        {
                            if (result0.Signals[v].BandwidthKhz >= _FilterMinBandWidthkHz && result0.Signals[v].BandwidthKhz <= _FilterMaxBandWidthkHz)
                            {
                                signals.Add(result0.Signals[v]);
                            }
                        }
                        result0 = new ProcessResult(signals);
                    }

                    //Обнаружение ИРИ
                    List<double> Freqs = result0.Signals.Select(x => (double)x.FrequencyKhz).ToList<double>();

                    //В цикле по ИРИ
                    //for (int j = 0; j < Freqs.Count(); j++)
                    for (int j = 0; j < result0.Signals.Count(); j++)
                    {
                        if (token.IsCancellationRequested)
                        {
                            ConsoleLog(2, "Операция прервана токеном");
                            return;
                        }

                        if (isFRSonListKnown(Freqs[j]))
                        {
                            //установка параметров для корреляционной кривой
                            byte filtervalue = BandWidthCodeFromBandWidthkHz(result0.Signals[j].BandwidthKhz);
                            int offset = (int)(result0.Signals[j].CentralFrequencyKhz - CurrentFreqkHz);
                            //var answerCorrFuncParamSet = await uDP.SetParam(offset, filtervalue, Alpha1, Alpha2);
                            SetParamEventArgs answerCorrFuncParamSet = await AttemptCountCmdUdp<SetParamEventArgs>(5, offset, filtervalue);
                            if (answerCorrFuncParamSet == null)
                            {
                                ConsoleLog(false, "UDP SetParam Error!");
                                continue;
                            }
                            //var answerCorrFuncParamSet = await uDP.SetParam(filtervalue, offset, Alpha1, Alpha2);
                            await Task.Delay(1);

                            //Запрос корреляционной функции
                            //var answerCor0 = await uDP.GetCorrelationFunc(); // запрос с устройства №0
                            GetCorrelationFuncEventArgs answerCor0 = await AttemptCountCmdUdp<GetCorrelationFuncEventArgs>(4, 0, 0);
                            if (answerCor0 == null)
                            {
                                ConsoleLog(false, "UDP GetCorrelationFunc Error!");
                                continue;
                            }

                            //Ортонормировать корреляционные функции
                            List<List<double>> CorrDoubleList = OrthoNormalization(answerCor0);

                            //Ортонормирование для отправки в АРМ
                            var corrFuncTuple = OrthoNormalizationToDoubleTuple(answerCor0);
                            //Отправка корреляциооной функции в АРМ Кираса
                            var answer = await MyDapServer.ServerSendCorrFunc
                                (1, (int)(result0.Signals[j].CentralFrequencyKhz), (int)result0.Signals[j].BandwidthKhz, 
                                corrFuncTuple.corrFunc0, 
                                corrFuncTuple.corrFunc1, 
                                corrFuncTuple.corrFunc2,
                                corrFuncTuple.corrFunc3
                                );

                            var dListTau123 = CalcTauFromCorrFuncsByEgor4(CorrDoubleList);
                            dListTau123 = SignMirror(dListTau123, 1, 1, 1); //new

                            (double tau1, double tau2, double tau3) tau123 = (dListTau123[0], dListTau123[1], dListTau123[2]);

                            double[] arrtau123 = new double[] { tau123.tau1, tau123.tau2, tau123.tau3 }; // массив задержек всех станций // наноСекунды

                            double[] tau = new double[] { 0, arrtau123[0] * 1e-9, arrtau123[1] * 1e-9, arrtau123[2] * 1e-9 }; // массив задержек всех станций //Секунды

                            tau = SignMirror(tau, 1, 1, 1);

                            bool isTau = false;

                            //Console.WriteLine(tau123);

                            if (arrtau123.All(x => x != -1))
                            {
                                //Console.WriteLine(tau123); // в наносекундах
                                isTau = true;
                            }
                            if (tau.Any(x => Double.IsInfinity(x)) || tau.Any(x => Double.IsNaN(x)))
                            {
                                isTau = false;
                            }

                            //Проверочка по задержкам
                            if (isTau)
                            {
                                //Класс Расчета координат
                                fDRM.ClassDRM_dll classDRM_Dll = new fDRM.ClassDRM_dll();

                                //Инициализация листа координат станций и их задержек
                                List<fDRM.ClassObject> lstClassObject = new List<fDRM.ClassObject>();

                                //Важная проверочка для рассчета координат
                                if (tableLocalPoints.Count == 4 && tableLocalPoints.Any(x => x.IsCetral == true))
                                {
                                    //Функция порядочка и определения базовой стации в листе по индексу
                                    List<int> GeneratesListOfisOwnIndexes()
                                    {
                                        List<int> lintisOwn = new List<int>();
                                        for (int k = 0; k < tableLocalPoints.Count; k++)
                                        {
                                            if (tableLocalPoints[k].IsCetral == true)
                                            {
                                                lintisOwn.Insert(0, k);
                                            }
                                            else
                                            {
                                                lintisOwn.Add(k);
                                            }
                                        }
                                        return lintisOwn;
                                    }
                                    List<int> lintIsOwn = GeneratesListOfisOwnIndexes();

                                    //Заполенение листа координат станций и их задержек
                                    for (int k = 0; k < 4; k++)
                                    {
                                        fDRM.ClassObject classObject = new fDRM.ClassObject()
                                        {
                                            Latitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Latitude,
                                            Longitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Longitude,
                                            Altitude = tableLocalPoints[lintIsOwn[k]].Coordinates.Altitude,
                                            BaseStation = (k == 0) ? true : false,
                                            IndexStation = k + 1,
                                            tau = tau[k]
                                        };

                                        lstClassObject.Add(classObject);
                                    }

                                    //Рассчёт координат источника
                                    fDRM.ClassObjectTmp resultMark = classDRM_Dll.f_DRM(lstClassObject, isImageSolution, settings.DesiredHeight);
                                    //Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Longitude.ToString("F1")}");

                                    //Отправка точки в АРМ
                                    var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                                    //Запись в файл времён, задержек, координат
                                    if (RecordStartStop)
                                    {
                                        expertises.Add(
                                            new Expertise(
                                            SignalTime[result0.Signals[j]],
                                            result0.Signals[j].CentralFrequencyKhz, result0.Signals[j].BandwidthKhz,
                                            tau[1], tau[2], tau[3],
                                            resultMark.Latitude, resultMark.Longitude, resultMark.Altitude,
                                            corrFuncTuple.corrFunc0, corrFuncTuple.corrFunc1, corrFuncTuple.corrFunc2, corrFuncTuple.corrFunc3
                                            ));
                                    }

                                    //Проверка принадлежности ИРИ (по дальности)
                                    double D = 40000; // 40 km

                                    //Проверка по одной станции (опорная/центральная)
                                    //var d = Bearing.ClassBearing.f_D_2Points(lt[0], ln[0], lat, lon, 1);

                                    //Рассчет расстояния между двумя точками
                                    var d = classDRM_Dll.f_D_2Points(
                                        tableLocalPoints[lintIsOwn[0]].Coordinates.Latitude,
                                        tableLocalPoints[lintIsOwn[0]].Coordinates.Longitude,
                                        resultMark.Latitude,
                                        resultMark.Longitude,
                                        1);


                                    //Проверка на аномалию
                                    bool isCheck = (d < D) ? true : false;

                                    isCheck = true; //для теста чтоль

                                    //double Time = 0;
                                    double Time = SignalTime[result0.Signals[j]].TimeOfDay.TotalSeconds;
                                    InputPointTrack inputPointTrack = new InputPointTrack()
                                    {
                                        LatMark = resultMark.Latitude,
                                        LongMark = resultMark.Longitude,
                                        HMark = resultMark.Altitude,
                                        Time = Time,
                                        Freq = result0.Signals[j].CentralFrequencyKhz,
                                        dFreq = result0.Signals[j].BandwidthKhz,
                                        _time = SignalTime[result0.Signals[j]]
                                    };
                                    _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                    //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                    //Отработка по Дронам
                                    LenaForMain();

                                }
                                else
                                {
                                    //Заполнения листа месторасположения станций
                                    //List<Station> stations = DefaultStations();
                                    List<Station> stations = DefaultStationsWithEgorHeights();

                                    //Рассчёт координат источника
                                    fDRM.ClassObjectTmp resultMark = CalcCoordsByEgor(dListTau123, 1, 1, 1, stations); //new
                                    //Console.WriteLine($"Lat: {resultMark.Latitude.ToString("00.000000")} Lon: {resultMark.Longitude.ToString("00.000000")} Alt: {resultMark.Altitude.ToString("F1")}");

                                    //Отправка точки в АРМ
                                    var answerSendCoords = await MyDapServer.ServerSendCoords(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude);

                                    //Запись в файл времён, задержек, координат
                                    if (RecordStartStop)
                                    {
                                        expertises.Add(
                                            new Expertise(
                                            SignalTime[result0.Signals[j]],
                                            result0.Signals[j].CentralFrequencyKhz, result0.Signals[j].BandwidthKhz,
                                            tau[1], tau[2], tau[3],
                                            resultMark.Latitude, resultMark.Longitude, resultMark.Altitude
                                            ));
                                    }

                                    //Проверка принадлежности ИРИ (по дальности)
                                    double D = 40000; // 40 km

                                    var d = classDRM_Dll.f_D_2Points(
                                    stations[0].Position.Latitude,
                                    stations[0].Position.Longitude,
                                    resultMark.Latitude,
                                    resultMark.Longitude,
                                    1);

                                    //Проверка на аномалию
                                    bool isCheck = (d < D) ? true : false;

                                    isCheck = true; //для теста чтоль

                                    //double Time = 0;
                                    double Time = SignalTime[result0.Signals[j]].TimeOfDay.TotalSeconds;
                                    InputPointTrack inputPointTrack = new InputPointTrack()
                                    {
                                        LatMark = resultMark.Latitude,
                                        LongMark = resultMark.Longitude,
                                        HMark = resultMark.Altitude,
                                        Time = Time,
                                        Freq = result0.Signals[j].CentralFrequencyKhz,
                                        dFreq = result0.Signals[j].BandwidthKhz,
                                        _time = SignalTime[result0.Signals[j]]
                                    };
                                    _tracksDefinition.f_TracksDefinition(inputPointTrack);
                                    //_tracksDefinition.f_TracksDefinition(resultMark.Latitude, resultMark.Longitude, resultMark.Altitude, Time);

                                    //Отработка по Дронам
                                    LenaForMain();
                                }
                            }
                        }
                    }

                    //await Task.Delay(1);
                }
                await Task.Delay(1);
                _SaveIndex = 0;

                //sW.Stop();
            }
        }

        private async Task MainLoop3(CancellationToken token)
        {
            CurrentFreqkHz = 0;
            while (_Mode != 0)
            {
                SW sW = new SW();

                int[] localGlobalRow = new int[0];
                lock (GlobalRow)
                {
                    localGlobalRow = GlobalRow.ToArray();
                }
                if (localGlobalRow.Length == 0)
                {
                    await _AsyncAutoResetEvent4GlobalRow.WaitAsync();
                    continue;
                    //не ждать чуда, да дипазонов РР, а вернуться в подготовку и всем сообщить об этом
                    //Пока отмена, чтобы не погибнуть
                    //Console.WriteLine("Диапазонов нет, не ало");
                    //Mode = 0;
                    //var answer = await MyDapServer.ServerSendExtraordinaryModeMessage(DAPprotocols.DapServerMode.Stop);
                    //return;
                }

                for (int i = _SaveIndex; i < localGlobalRow.Count(); i++, _SaveIndex = i)
                {
                    if (token.IsCancellationRequested)
                    {
                        ConsoleLog(2, "MainLoop прерван токеном");
                        return;
                    }

                    //Вычисление центральной частоты ЕПО
                    double centerFreqMHz = _GlobalRangeXmin + _GlobalBandWidthMHz / 2d + _GlobalBandWidthMHz * localGlobalRow[i];

                    //Перестройка преселекторов новая последовательная 7
                    await PreselRealignment(centerFreqMHz, (byte)localGlobalRow[i]);

                    //Настройка РПУ на текущую ЕПО по частоте
                    if (CurrentFreqkHz != centerFreqMHz * 1000 && CurrentDictionaryRecGain[i] != _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)i))
                    {
                        //SetFreqEventArgs answerSetFreq = await uDP.SetFreq((int)(centerFreqMHz * 1000), 30);
                        //SetFreqEventArgs answerSetFreq = await AttemptCountCmdUdp<SetFreqEventArgs>(2, (int)(centerFreqMHz * 1000), 30);
                        SetFreqEventArgs answerSetFreq = await AttemptCountCmdUdp<SetFreqEventArgs>(2, (int)(centerFreqMHz * 1000), _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)i));
                        CurrentDictionaryRecGain[i] = _GainSettings4.GetDeviceGain(DAPprotocols.DapDevice.Receiver, (byte)i);
                        await Task.Delay(_ReceiverDelay);
                        CurrentFreqkHz = centerFreqMHz * 1000;
                        //для отмены если ошибка по udp приёму или приёмнику
                        if (answerSetFreq == null)
                        {
                            ConsoleLog(false, "UDP SetFreq Error!");
                            //Task.Run(() => EndRI());
                            //return;
                        }
                    }

                    //Чтение данных

                    //Для большого хранилища
                    List<GetSpectrumEventArgs> answersGetSpectrum = new List<GetSpectrumEventArgs>();
                    for (int ww = 0; ww < _GlobalN; ww++)
                    {
                        //GetSpectrumEventArgs tempAnswerGetSpectrum = await uDP.GetSpectrum((byte)ww);
                        GetSpectrumEventArgs tempAnswerGetSpectrum = await AttemptCountCmdUdp<GetSpectrumEventArgs>(3, ww, 0);
                        if (tempAnswerGetSpectrum == null)
                        {
                            ConsoleLog(false, $"UDP GetSpectrum Chanel {ChanelName(ww)} Error!");
                            List<float> innerInitList = new float[_GlobalDotsPerBandCount].ToList<float>();
                            innerInitList = innerInitList.Select(x => x = -130f).ToList<float>();
                            tempAnswerGetSpectrum = new GetSpectrumEventArgs(0, new byte[0], 0, 0, 0, innerInitList,0);
                        }
                        answersGetSpectrum.Add(tempAnswerGetSpectrum);
                    }

                    //var answerGetSpectrum = await uDP.GetSpectrum((byte)_ChanelNumber);
                    var answerGetSpectrum = answersGetSpectrum[_ChanelNumber];

                    if (answerGetSpectrum.Ampl.Count == 8001) answerGetSpectrum.Ampl.RemoveAt(0);

                    if (RecordStartStop)
                    {
                        DateTime Time = DateTime.Now;
                        int ShiftDelay = (int)(Math.Abs(Time.TimeOfDay.TotalMilliseconds - TimeStart.TimeOfDay.TotalMilliseconds));
                        SaveRecord(ShiftDelay, localGlobalRow[i], answerGetSpectrum.Ampl.ToArray());
                    }

                    //Массив для ужатия спектра
                    //float[] vs1 = answerGetSpectrum.Ampl.ToArray();
                    //Массив для поиска частот
                    lastSavedSpectrum0 = answerGetSpectrum.Ampl.ToArray();

                    ////Ужатие Спектра от Феди
                    //var segment = new ArraySegment<float>(vs1, 0, vs1.Count());
                    //var spectrum = segment.StrechSpectrum(_GlobalDotsPerBandCount);
                    //vs1 = spectrum.Amplitudes;

                    //Обновляем хранилище спектра
                    //ListSpectrumStorage[Row[i]] = vs1.ToList();
                    ListSpectrumStorage[localGlobalRow[i]] = new List<float>(answerGetSpectrum.Ampl);

                    //Для большого хранилища
                    for (int ww = 0; ww < _GlobalN; ww++)
                    {
                        BigListSpectrumStorage[ww][localGlobalRow[i]] = new List<float>(answersGetSpectrum[ww].Ampl);
                    }

                    //Обнаружение ИРИ от Феди
                    DspDataModel.Data.IAmplitudeScan amplitudeScan0 = new DspDataModel.Data.AmplitudeScan(lastSavedSpectrum0, localGlobalRow[i], DateTime.Now, 0);

                    //Параметр для поиска сигналов по порогу
                    ScanProcessConfig scanProcessConfig = ScanProcessConfig.CreateConfigWithoutDf(_Threshold);

                    //Результаты сигналов
                    var result0 = dataProcessor0.GetSignals(amplitudeScan0, scanProcessConfig);

                    //Ассоциация сигнал-время
                    if (result0.RawSignals.Count != 0 || result0.Signals.Count != 0)
                    {
                        //Console.WriteLine(result0.Signals.Count);
                        SignalTime.Clear();
                        for (int qq = 0; qq < result0.Signals.Count; qq++)
                        {
                            SignalTime.Add(result0.Signals[qq], DateTime.Now);
                        }
                    }

                    //Исполнительный фильтр
                    if (ApplyFilter && FilterMinFrequencyMHz != -1 && FilterMaxFrequencyMHz != -1)
                    {
                        var indexFilter = ConvertValueToIndexes(new double[] { FilterMinFrequencyMHz, FilterMaxFrequencyMHz });
                        if (indexFilter[0] == localGlobalRow[i] && indexFilter[0] == indexFilter[1])
                        {
                            int bandIndex = localGlobalRow[i];

                            ctsLocalSpectrum = new CancellationTokenSource();
                            CancellationToken tokenLocalSpectrum = ctsLocalSpectrum.Token;

                            ctsLocalCorrFunc = new CancellationTokenSource();
                            CancellationToken tokenLocalCorrFunc = ctsLocalCorrFunc.Token;

                            LocalSpectrumTask = LocalSpectrum(bandIndex, tokenLocalSpectrum);

                            //LocalCorrFuncTask = LocalCorrFunc(CurrentFreqkHz, FilterMinFrequencyMHz, FilterMaxFrequencyMHz, tokenLocalCorrFunc);
                            LocalCorrFuncTask = LocalCorrFunc3(CurrentFreqkHz, FilterMinFrequencyMHz, FilterMaxFrequencyMHz, tokenLocalCorrFunc);

                            await _taskAddEvent.WaitAsync();

                            continue;
                        }

                        //Фильтр по выделенной области
                        bool filterSelectedArea = false;
                        if (filterSelectedArea && FilterMinFrequencyMHz != -1 && FilterMaxFrequencyMHz != -1)
                        {
                            var signals = new List<ISignal>();

                            for (int v = 0; v < result0.Signals.Count; v++)
                            {
                                if (result0.Signals[v].FrequencyKhz >= FilterMinFrequencyMHz && result0.Signals[v].FrequencyKhz <= FilterMaxFrequencyMHz)
                                {
                                    signals.Add(result0.Signals[v]);
                                }
                            }
                            result0 = new ProcessResult(signals);
                        }
                    }

                    //Фильтр по диапазонам РР
                    {
                        var signals = new List<ISignal>();

                        for (int v = 0; v < result0.Signals.Count; v++)
                        {
                            if (FRSonRangeRI(result0.Signals[v].CentralFrequencyKhz / 1000d))
                            {
                                signals.Add(result0.Signals[v]);
                            }
                        }
                        result0 = new ProcessResult(signals);
                    }

                    //Фильтр по ширине
                    {
                        var signals = new List<ISignal>();

                        for (int v = 0; v < result0.Signals.Count; v++)
                        {
                            if (result0.Signals[v].BandwidthKhz >= _FilterMinBandWidthkHz && result0.Signals[v].BandwidthKhz <= _FilterMaxBandWidthkHz)
                            {
                                signals.Add(result0.Signals[v]);
                            }
                        }
                        result0 = new ProcessResult(signals);
                    }

                    //Обнаружение ИРИ
                    List<double> Freqs = result0.Signals.Select(x => (double)x.FrequencyKhz).ToList<double>();

                    TableSignalsUAV[] localTableSignalsUAVs = new TableSignalsUAV[0];
                    lock (lTableSignalsUAVs)
                    {
                        localTableSignalsUAVs = lTableSignalsUAVs.ToArray();
                    }

                    List<ISignal> lSignals = new List<ISignal>(result0.Signals.ToList());

                    //В цикле
                    for (int j = 0; j < localTableSignalsUAVs.Count(); j++)
                    {
                        for (int k = 0; k < lSignals.Count(); k++)
                        {
                            //Сравнить сигнал с каждым элементом в листе и обновить в базе
                            (float FrequencyKhz, float BandwidthKhz) signal1 = ((float)localTableSignalsUAVs[j].FrequencyKHz, localTableSignalsUAVs[j].BandKHz);
                            (float FrequencyKhz, float BandwidthKhz) signal2 = (lSignals[k].FrequencyKhz, lSignals[k].BandwidthKhz);
                            if (CompareSignalsKhz(signal1, signal2))
                            {
                                TableSignalsUAV updateSignal = new TableSignalsUAV()
                                {
                                    Id = localTableSignalsUAVs[j].Id,
                                    FrequencyKHz = lSignals[k].FrequencyKhz,
                                    BandKHz = lSignals[k].BandwidthKhz,
                                    System = localTableSignalsUAVs[j].System,
                                    TimeStart = localTableSignalsUAVs[j].TimeStart,
                                    TimeStop = SignalTime[lSignals[k]],
                                    Type = localTableSignalsUAVs[j].Type,
                                    TypeL = localTableSignalsUAVs[j].TypeL,
                                    TypeM = localTableSignalsUAVs[j].TypeM,
                                    Correlation = localTableSignalsUAVs[j].Correlation
                                };
                                ChangeRecordInTableSignalsUAVs(updateSignal);
                                lSignals.RemoveAt(k);
                                k--;
                            }
                        }
                    }

                    //Добавить оставшиеся необновленные, значит новые, сигналы в базу
                    for (int k = 0; k < lSignals.Count(); k++)
                    {
                        TableSignalsUAV addSignal = new TableSignalsUAV()
                        {
                            Id = SignalId++,
                            FrequencyKHz = lSignals[k].FrequencyKhz,
                            BandKHz = lSignals[k].BandwidthKhz,
                            System = TypeSystem.CP_CuirasseM,
                            TimeStart = SignalTime[lSignals[k]],
                            TimeStop = SignalTime[lSignals[k]],
                            Type = TypeUAVRes.Unknown,
                            TypeL = TypeL.Enemy,
                            TypeM = TypeM.Unknown,
                            Correlation = false
                        };
                        AddRecordToTableSignalsUAVs(addSignal);
                    }

                }
                await Task.Delay(1);
                _SaveIndex = 0;

                sW.Stop();
            }
        }

    }
}
