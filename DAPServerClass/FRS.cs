﻿using System.Collections.Generic;

namespace DAPServerClass
{
    public class FRS
    {
        public double FreqkHz;
        public float BandWidthkHz;
        public List<KirasaModelsDBLib.Coord> Coords;

        public FRS()
        {
            FreqkHz = 0;
            BandWidthkHz = 0;
            Coords = new List<KirasaModelsDBLib.Coord>();
        }
    }

    public struct Coord
    {
        public double Lat;
        public double Lon;
        public double Alt;
    }
}
