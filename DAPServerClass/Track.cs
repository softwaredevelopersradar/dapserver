﻿using System.Collections.Generic;
using WpfMapRastr;

namespace DAPServerClass
{
    public class Track
    {
        public TracksDefinition tracksDefinition;

        public List<TrackAirObject> listTrackAirObject = new List<TrackAirObject>();

        public Dictionary<int, int> IDcountTrajOldForMain { get; set; } = new Dictionary<int, int>();
        public Dictionary<int, int> IDcountTrajNewForMain { get; set; } = new Dictionary<int, int>();
    }
}

