﻿using System;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        private string GenerateDBendPoint(EndPointConnection EndPointConnection)
        {
            return ($"{EndPointConnection.IpAddress}:{EndPointConnection.Port}");
        }

        private (double[] lt, double[] ln, double[] H) ConvertStationFromPolinaToLena()
        {
            double[] lt = new[] {
                tableLocalPoints[0].Coordinates.Latitude,
                tableRemotePoints[0].Coordinates.Latitude,
                 tableRemotePoints[1].Coordinates.Latitude,
                  tableRemotePoints[2].Coordinates.Latitude
            };

            double[] ln = new[] {
                tableLocalPoints[0].Coordinates.Longitude,
                tableRemotePoints[0].Coordinates.Longitude,
                 tableRemotePoints[1].Coordinates.Longitude,
                  tableRemotePoints[2].Coordinates.Longitude
            };

            double[] H = new[] {
                (double)(tableLocalPoints[0].Coordinates.Altitude),
                (double)(tableRemotePoints[1].Coordinates.Altitude),
                (double)(tableRemotePoints[2].Coordinates.Altitude),
                   (double)(tableRemotePoints[3].Coordinates.Altitude)
            };

            return (lt, ln, H);
        }

        private void Log(string logString, bool consoleFlag = true, bool textBoxFlag = true)
        {
            if (consoleFlag)
            {
                Console.WriteLine(logString);
            }
            if (textBoxFlag)
            {
                //DispatchIfNecessary(() =>
                //{
                //    //tbhelp.Text += logString + "\n";
                //    //sLog.AddTextToLog(logString);
                //});
            }
        }
        private void slog(string logString)
        {
            //DispatchIfNecessary(() =>
            //{
            //    sLog.AddTextToLog(logString);
            //});
        }

    }
}
