﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using WpfMapRastr;
using YamlReverseExpertise;

namespace DAPServerClass
{
    public partial class DapServerClass
    {
        UDPReceiver.UDPReceiver uDPReceiver;

        private void InitUDPReceiver(string UDPmyIP, int UDPmyPort, string UDPremoteIP, int UDPremotePort)
        {
            //TryParse
            var myIp = IPAddress.Parse(UDPmyIP);
            var myport = UDPmyPort;
            var remoteIp = IPAddress.Parse(UDPremoteIP);
            var remoteport = UDPremotePort;
            byte addrSender = 0;
            byte addrRecipient = 0;

            uDPReceiver = new UDPReceiver.UDPReceiver(myIp, myport, remoteIp, remoteport, addrSender, addrRecipient);

            if (true)
            {
                uDPReceiver.OnGetCoordRDM += UDPReceiver_OnGetCoordRDM;
            }
            if (true)
            {
                uDPReceiver.OnGetCoordAero += UDPReceiver_OnGetCoordAero;
            }

            uDPReceiver.OnConnect += UDPReceiver_OnConnect;
            uDPReceiver.OnDisconnect += UDPReceiver_OnDisconnect;

            uDPReceiver.Connect();

        }

        private void UDPReceiverStop()
        {
            uDPReceiver.OnGetCoordRDM -= UDPReceiver_OnGetCoordRDM;
            uDPReceiver.OnGetCoordAero -= UDPReceiver_OnGetCoordAero;

            uDPReceiver.OnConnect -= UDPReceiver_OnConnect;
            uDPReceiver.OnDisconnect -= UDPReceiver_OnDisconnect;

            uDPReceiver.Disconnect();
        }

        private void UDPReceiver_OnConnect(object sender, bool e)
        {
            //UPD.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Green);
        }
        private void UDPReceiver_OnDisconnect(object sender, bool e)
        {
            //UPD.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red);
        }

        private void UDPReceiver_OnGetCoordRDM(object sender, UDPReceiver.Coord e)
        {
            double Time = DateTime.Now.TimeOfDay.TotalSeconds;

            InputPointTrack inputPointTrack = new InputPointTrack()
            {
                LatMark = e.latitude,
                LongMark = e.longitude,
                HMark = e.altitude,
                Time = Time,
                Freq = (float)(2412000d + r.NextDouble() * 1000d),
                dFreq = 10000f + (float)(r.NextDouble() * 12f),
                _time = DateTime.Now
            };
            _tracksDefinition.f_TracksDefinition(inputPointTrack);
            //_tracksDefinition.f_TracksDefinition(e.latitude, e.longitude, e.altitude, Time);

            if (RecordStartStop)
            {
                expertises.Add(
                    new Expertise(
                    DateTime.Now,
                    0, 0,
                    e.tau1, e.tau2, e.tau3,
                    e.latitude, e.longitude, e.altitude
                    ));
            }

            string s = $"RDM: {e.tau1} {e.tau2} {e.tau3} {e.latitude} {e.longitude} {e.altitude} {(int)Time}";
            slog(s);

            //Отработка по Дронам
            LenaForMain();
        }

        bool firstEgorAero = true;

        private void UDPReceiver_OnGetCoordAero(object sender, UDPReceiver.Coord e)
        {
            string s = $"Aero: {e.latitude} {e.longitude} {e.altitude}";
            slog(s);

            if (firstEgorAero)
            {
                KirasaModelsDBLib.Coord coord = new KirasaModelsDBLib.Coord()
                {
                    Latitude = e.latitude,
                    Longitude = e.longitude,
                    Altitude = e.altitude
                };

                List<KirasaModelsDBLib.Coord> coords = new List<KirasaModelsDBLib.Coord>();
                coords.Add(coord);

                FRS frsNew = new FRS()
                {
                    BandWidthkHz = 0,
                    FreqkHz = 0,
                    Coords = coords
                };
                NewAddTableAeroScopes(frsNew);
                firstEgorAero = false;
            }
            else
            {
                KirasaModelsDBLib.Coord coord = new KirasaModelsDBLib.Coord()
                {
                    Latitude = e.latitude,
                    Longitude = e.longitude,
                    Altitude = e.altitude
                };

                List<KirasaModelsDBLib.Coord> coords = new List<KirasaModelsDBLib.Coord>();
                coords.Add(coord);

                FRS frsNew = new FRS()
                {
                    BandWidthkHz = 0,
                    FreqkHz = 0,
                    Coords = coords
                };

                short num = (short)lastTableAeroscopeTrajectory;

                AnotherOneAddTableAeroScopes("0", num, frsNew);
            }
        }

        private void eupdteststart_Click(object sender, RoutedEventArgs e)
        {
            InitUDPReceiver(
               settings.EgorTestUDPSettings.myIpAdress,
               settings.EgorTestUDPSettings.myPort,
               settings.EgorTestUDPSettings.remoteIpAdress,
               settings.EgorTestUDPSettings.remotePort
               );
        }

        private void eupdtestend_Click(object sender, RoutedEventArgs e)
        {
            UDPReceiverStop();

            _listTrackAirObject.Clear();
            IDcountTrajOldForMain.Clear();
            IDcountTrajNewForMain.Clear();

            firstEgorAero = true;

            lastIDTablesAeroscopes = 0;
            lastTableAeroscopeTrajectory = 0;
        }
    }
}
