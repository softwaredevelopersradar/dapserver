﻿namespace DAPServerClass
{
    public static class SupportCalc
    {
        public static (int IDtoDeleteDictionary, int IDtoDeleteTraj) EjectCiphers(int ID, int pow = 10)
        {
            int IDtoDeleteDictionary = ID / pow;

            int IDtoDeleteTraj = ((IDtoDeleteDictionary != 0)) ? ID % (IDtoDeleteDictionary * pow) : ID;

            return (IDtoDeleteDictionary, IDtoDeleteTraj);
        }

    }
}
