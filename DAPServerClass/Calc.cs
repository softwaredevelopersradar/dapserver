﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DAPServerClass
{
    public partial class DapServerClass
    {

        private List<double> Divide = new List<double>();

        private void InitDivide(Settings innerSettings)
        {
            _GlobalNumberOfBands = innerSettings.NumberOfBands;
            _GlobalBandWidthMHz = innerSettings.BandwidthMHz;
            _GlobalRangeXmin = innerSettings.RangeXMin;
            _GlobalDotsPerBandCount = innerSettings.DotsPerBand;
        }

        private void RecalcDivide()
        {
            Divide.Clear();
            for (int i = 0; i < _GlobalNumberOfBands + 1; i++)
            {
                Divide.Add(_GlobalRangeXmin + _GlobalBandWidthMHz * i);
            }
        }

        private int _GlobalDotsPerBandCount = 8000;

        private int _GlobalNumberOfBands = 96;
        public int GlobalNumberOfBands
        {
            get { return _GlobalNumberOfBands; }
            set
            {
                if (_GlobalNumberOfBands != value)
                {
                    _GlobalNumberOfBands = value;
                    _RecalcGlobalRangeMax();
                    RecalcDivide();
                }
            }
        }


        private double _GlobalRangeXmin = 10.0;
        public double GlobalRangeXmin
        {
            get { return _GlobalRangeXmin; }
            set
            {
                if (_GlobalRangeXmin != value)
                {
                    _GlobalRangeXmin = value;
                    _RecalcGlobalRangeMax();
                    RecalcDivide();
                }
            }
        }

        private void _RecalcGlobalRangeMax()
        {
            _GlobalRangeXmax = _GlobalRangeXmin + _GlobalNumberOfBands * _GlobalBandWidthMHz;
        }

        private double _GlobalRangeXmax = 6010;
        private double GlobalRangeXmax
        {
            get { return _GlobalRangeXmax; }
            set
            {
                if (_GlobalRangeXmax != value)
                {
                    _GlobalRangeXmax = value;
                    //!отправить на все контролы
                }
            }
        }

        private double _GlobalBandWidthMHz = 62.5;
        public double GlobalBandWidthMHz
        {
            get { return _GlobalBandWidthMHz; }
            set
            {
                if (_GlobalBandWidthMHz != value)
                {
                    _GlobalBandWidthMHz = value;
                    _RecalcGlobalRangeMax();
                    RecalcDivide();
                }
            }
        }



        public List<int> DoIt(double[] TargetMinFreqs, double[] TargetMaxFreqs)
        {
            List<(int indexStart, int indexEnd)> listTyples = new List<(int indexStart, int indexEnd)>();

            if (TargetMinFreqs != null && TargetMaxFreqs != null)
            {
                int Count = Math.Min(TargetMinFreqs.Count(), TargetMaxFreqs.Count());
                for (int i = 0; i < Count; i++)
                {
                    var temp = CalcLikePro(TargetMinFreqs[i], TargetMaxFreqs[i]);
                    if (temp.indexStart != -1 && temp.indexEnd != -1)
                    {
                        listTyples.Add(temp);
                    }
                }

                List<int> Row = new List<int>();

                for (int i = 0; i < listTyples.Count(); i++)
                {
                    var row = GenerateFromStartToEndRowList(listTyples[i].indexStart, listTyples[i].indexEnd);
                    Row = Row.Concat(row).ToList<int>();
                }

                listTyples.Clear();

                return Row;
            }
            return new List<int>();
        }

        public (int indexStart, int indexEnd) CalcLikePro(double StartFrequency, double EndFrequency)
        {
            double [] checkArr = new double[_GlobalNumberOfBands + 1];

            for (int i = 0; i < _GlobalNumberOfBands + 1; i++)
            {
                checkArr[i] = (double)(_GlobalRangeXmin + i * GlobalBandWidthMHz);
            }

            int startIndex = -1;
            int endIndex = -1;

            for (int w = 0; w < _GlobalNumberOfBands; w++)
            {
                if (StartFrequency >= checkArr[w] && StartFrequency < checkArr[w + 1])
                {
                    startIndex = w;
                    break;
                }
            }
            for (int w = (startIndex == 0) ? 0 : startIndex - 1; w < _GlobalNumberOfBands; w++)
            {
                if (EndFrequency > checkArr[w] && EndFrequency <= checkArr[w + 1])
                {
                    endIndex = w;
                    break;
                }
            }
            return (startIndex, endIndex);
        }

        private List<int> GenerateFromStartToEndRowList(int start, int end)
        {
            List<int> lint = new List<int>();
            for (int i = start; i <= end; i++)
            {
                lint.Add(i);
            }
            return lint;
        }

        public int ConvertValueMHzToEPO(double ValueMHz)
        {
            return ConvertValueToIndexes(new double[] { ValueMHz })[0];
        }

        public int[] ConvertValueToIndexes(double[] Values)
        {
            List<int> lint = new List<int>();

            for (int i = 0; i < Values.Count(); i++)
            {
                if (Values[i] >= _GlobalRangeXmin && Values[i] <= _GlobalRangeXmax)
                {
                    int index = (int)((Values[i] - _GlobalRangeXmin) / _GlobalBandWidthMHz); ;
                    if (Divide.Contains(Values[i])) index--;
                    lint.Add(index);
                }
            }
            return lint.ToArray();
        }

        public ArrAndEPO<T> SeparateArrDependCenterFreq<T>(double CenterFreqMHz, T [] ArrToSeparate)
        {
            double LeftEdgeFreqMHz = CenterFreqMHz - GlobalBandWidthMHz / 2;
            double RightEdgeFreqMHz = CenterFreqMHz + GlobalBandWidthMHz / 2;

            int LeftEPO = ConvertValueMHzToEPO(LeftEdgeFreqMHz);
            int CurrentEPO = ConvertValueMHzToEPO(CenterFreqMHz);
            int RightEPO = ConvertValueMHzToEPO(RightEdgeFreqMHz);

            ArrAndEPO<T> arrAndEPO = new ArrAndEPO<T>
            {
                LeftEPO = LeftEPO,
                RightEPO = RightEPO
            };

            if (CurrentEPO == RightEPO)
            {
                double SpreadMHz = Divide[CurrentEPO] - LeftEdgeFreqMHz;
                int LeftDotsCount = (int)(SpreadMHz * _GlobalDotsPerBandCount / GlobalBandWidthMHz);
                int RightDotsCount = _GlobalDotsPerBandCount - LeftDotsCount;

                arrAndEPO.ArrToLeftEPO = new T[LeftDotsCount];
                arrAndEPO.ArrToRightEPO = new T[RightDotsCount];

                Array.Copy(ArrToSeparate, 0, arrAndEPO.ArrToLeftEPO, 0, LeftDotsCount);
                Array.Copy(ArrToSeparate, LeftDotsCount, arrAndEPO.ArrToRightEPO, 0, RightDotsCount);
            }

            if (LeftEPO == CurrentEPO)
            {
                double SpreadMHz = RightEdgeFreqMHz - Divide[CurrentEPO + 1];

                //TO DO:

                int RightDotsCount = (int)(SpreadMHz * _GlobalDotsPerBandCount / GlobalBandWidthMHz);
                int LeftDotsCount = _GlobalDotsPerBandCount - RightDotsCount;

                arrAndEPO.ArrToLeftEPO = new T[LeftDotsCount];
                arrAndEPO.ArrToRightEPO = new T[RightDotsCount];

                Array.Copy(ArrToSeparate, 0, arrAndEPO.ArrToLeftEPO, 0, LeftDotsCount);
                Array.Copy(ArrToSeparate, LeftDotsCount, arrAndEPO.ArrToRightEPO, 0, RightDotsCount);
            }


            return arrAndEPO;
        }
        public ArrAndEPO<T> SeparateArrDependCenterFreq2<T>(double CenterFreqMHz, T[] ArrToSeparate)
        {
            double LeftEdgeFreqMHz = CenterFreqMHz - GlobalBandWidthMHz / 2;
            double RightEdgeFreqMHz = CenterFreqMHz + GlobalBandWidthMHz / 2;

            int LeftEPO = ConvertValueMHzToEPO(LeftEdgeFreqMHz);
            int CurrentEPO = ConvertValueMHzToEPO(CenterFreqMHz);
            int RightEPO = ConvertValueMHzToEPO(RightEdgeFreqMHz);

            int LeftDotsCount = 0;
            int RightDotsCount = 0;

            ArrAndEPO <T> arrAndEPO = new ArrAndEPO <T>()
            {
                LeftEPO = LeftEPO,
                RightEPO = RightEPO
            };

            if (CurrentEPO == RightEPO)
            {
                double SpreadMHz = Divide[CurrentEPO] - LeftEdgeFreqMHz;
                LeftDotsCount = (int)(SpreadMHz * _GlobalDotsPerBandCount / GlobalBandWidthMHz);
                RightDotsCount = _GlobalDotsPerBandCount - LeftDotsCount;
            }
            if (LeftEPO == CurrentEPO)
            {
                double SpreadMHz = RightEdgeFreqMHz - Divide[CurrentEPO + 1];

                RightDotsCount = (int)(SpreadMHz * _GlobalDotsPerBandCount / GlobalBandWidthMHz);
                LeftDotsCount = _GlobalDotsPerBandCount - RightDotsCount;
            }

            arrAndEPO.ArrToLeftEPO = new T[LeftDotsCount];
            arrAndEPO.ArrToRightEPO = new T[RightDotsCount];

            Array.Copy(ArrToSeparate, 0, arrAndEPO.ArrToLeftEPO, 0, LeftDotsCount);
            Array.Copy(ArrToSeparate, LeftDotsCount, arrAndEPO.ArrToRightEPO, 0, RightDotsCount);

            return arrAndEPO;
        }

        public class ArrAndEPO <T>
        {
            public int LeftEPO { get; set; }
            public T[] ArrToLeftEPO { get; set; }
            public int RightEPO { get; set; }
            public T[] ArrToRightEPO { get; set; }
        }
    }
}
